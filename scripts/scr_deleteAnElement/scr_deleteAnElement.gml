
///@arg itemIndex
///@arg gridToDelete
gridToDelete = argument1;
var size = ds_grid_height(gridToDelete);
for (var i = argument0; i < size-1; i++){
	ds_grid_set(gridToDelete, 0, i, gridToDelete[# 0,i+1]);	
	ds_grid_set(gridToDelete, 1, i, gridToDelete[# 1,i+1]);	
	ds_grid_set(gridToDelete, 2, i, gridToDelete[# 2,i+1]);	
	ds_grid_set(gridToDelete, 3, i, gridToDelete[# 3,i+1]);	
	ds_grid_set(gridToDelete, 4, i, gridToDelete[# 4,i+1]);	
}

ds_grid_resize(gridToDelete, 5, size-1);