{
    "id": "62f63973-64f2-418c-9001-3b080ceb13a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_canteen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "843a9d61-7278-4a03-9e6d-35321c435cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62f63973-64f2-418c-9001-3b080ceb13a1",
            "compositeImage": {
                "id": "b6b7b4d6-d059-4e17-b285-0e08a8e04bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843a9d61-7278-4a03-9e6d-35321c435cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81c07b6-003c-4059-b50e-f47c95a65d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843a9d61-7278-4a03-9e6d-35321c435cea",
                    "LayerId": "9cbc76a2-766a-47e1-ab2e-64c87c3994ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "9cbc76a2-766a-47e1-ab2e-64c87c3994ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62f63973-64f2-418c-9001-3b080ceb13a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}