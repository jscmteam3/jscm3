{
    "id": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenEatDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c873a4e-4234-4929-9b58-778854f064ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
            "compositeImage": {
                "id": "b3a85ff9-7fae-4b4c-8873-6ce794a29c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c873a4e-4234-4929-9b58-778854f064ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd84adc-1b03-4da4-8f81-19190e4e8cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c873a4e-4234-4929-9b58-778854f064ea",
                    "LayerId": "93a1dfe6-b378-4030-9e87-e88cf44dc1af"
                }
            ]
        },
        {
            "id": "58c95b24-56f0-47e1-b060-9067166ff1f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
            "compositeImage": {
                "id": "03599cea-cf98-49f2-a215-507ecd0870ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c95b24-56f0-47e1-b060-9067166ff1f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6a8485-7610-461a-90d1-7fd3766c4391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c95b24-56f0-47e1-b060-9067166ff1f1",
                    "LayerId": "93a1dfe6-b378-4030-9e87-e88cf44dc1af"
                }
            ]
        },
        {
            "id": "7cc99c01-f9fb-4b6a-ae5b-426c91a1c6d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
            "compositeImage": {
                "id": "8b14b11b-3e8c-48ca-b312-b260f0db89d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc99c01-f9fb-4b6a-ae5b-426c91a1c6d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1910ad9f-dc56-496e-9978-a122b5fecd6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc99c01-f9fb-4b6a-ae5b-426c91a1c6d5",
                    "LayerId": "93a1dfe6-b378-4030-9e87-e88cf44dc1af"
                }
            ]
        },
        {
            "id": "893f069a-2ee1-4e76-aecb-b8f856ed4d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
            "compositeImage": {
                "id": "0e2494d7-5846-4d1b-87bc-bbc9ddf25509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "893f069a-2ee1-4e76-aecb-b8f856ed4d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58487597-84c1-433f-b50d-89510c2cc83e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "893f069a-2ee1-4e76-aecb-b8f856ed4d3a",
                    "LayerId": "93a1dfe6-b378-4030-9e87-e88cf44dc1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "93a1dfe6-b378-4030-9e87-e88cf44dc1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23609d1d-a16e-4497-80ff-9fc6d2c9db70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}