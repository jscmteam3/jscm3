///@argument0 DSGrid Grid_to_add
///@argument1 String ItemName
///@argument2 Int ItemAmount
///@argument3 String ItemType
///@argument4 Sprite Item_Sprite
///@argument5 Script Item_Script

gridAddTo = argument0;
newItemName = argument1;
newItemAmount = argument2;
newItemType = argument3;
newItemSprite = argument4;
newItemScript = argument5;
var i;
//Case1 - Item is already in the inventory
for (i = 0; i < ds_grid_height(gridAddTo); i++){
	if (ds_grid_get(gridAddTo, 0,i) == newItemName){
		ds_grid_set(gridAddTo, 1, i, ds_grid_get(gridAddTo, 1, i) + newItemAmount)
		return true;
	}
}
//Case2 - Item is not in the inventory
if (ds_grid_get(gridAddTo, 0, 0) != 0){
	ds_grid_resize(gridAddTo, 5, ds_grid_height(gridAddTo)+1);
}
	newItemSpot = ds_grid_height(gridAddTo) - 1;
	ds_grid_set(gridAddTo, 0, newItemSpot, newItemName);
	ds_grid_set(gridAddTo, 1, newItemSpot, newItemAmount);
	ds_grid_set(gridAddTo, 2, newItemSpot, newItemType);
	ds_grid_set(gridAddTo, 3, newItemSpot, newItemSprite);
	ds_grid_set(gridAddTo, 4, newItemSpot, newItemScript);
	return true;
