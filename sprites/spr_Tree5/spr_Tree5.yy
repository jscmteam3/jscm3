{
    "id": "d86131d5-d361-45b2-b8c3-895941d3d2a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 43,
    "bbox_right": 83,
    "bbox_top": 126,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf4233f3-e44e-4f3c-b378-1bad1bb6daad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d86131d5-d361-45b2-b8c3-895941d3d2a0",
            "compositeImage": {
                "id": "406e7100-ebac-416f-b8cf-937b705c5f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4233f3-e44e-4f3c-b378-1bad1bb6daad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3b0386-3583-462c-94b7-ed9307982f02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4233f3-e44e-4f3c-b378-1bad1bb6daad",
                    "LayerId": "da0cabb2-57b7-4be6-9d0b-b83195cf1948"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 162,
    "layers": [
        {
            "id": "da0cabb2-57b7-4be6-9d0b-b83195cf1948",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d86131d5-d361-45b2-b8c3-895941d3d2a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 137
}