{
    "id": "f5dfee53-6399-4a18-8ef1-01012162ac44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemFishDish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "710eec9c-1a2e-4f4a-9d0b-2bda0ca3a9b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5dfee53-6399-4a18-8ef1-01012162ac44",
            "compositeImage": {
                "id": "42bf1e59-9a71-4d0c-a104-ab1420c33345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710eec9c-1a2e-4f4a-9d0b-2bda0ca3a9b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c023ead0-036e-4380-a242-6e179cec0158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710eec9c-1a2e-4f4a-9d0b-2bda0ca3a9b9",
                    "LayerId": "0c56069f-3098-45f4-a170-8afabb0efff1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c56069f-3098-45f4-a170-8afabb0efff1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5dfee53-6399-4a18-8ef1-01012162ac44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}