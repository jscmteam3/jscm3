{
    "id": "ff90ff18-551e-428f-875f-a8600ed5081f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemRawBeef",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d51596d6-7de0-4afc-a57d-418c824181eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff90ff18-551e-428f-875f-a8600ed5081f",
            "compositeImage": {
                "id": "954fee4f-bae3-47ef-bcb6-aa7833cbbc87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51596d6-7de0-4afc-a57d-418c824181eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f181ac-6cce-4a0f-8747-fdd7e22cf8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51596d6-7de0-4afc-a57d-418c824181eb",
                    "LayerId": "17fe8793-3180-4c6b-bfee-0b73cef4120f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "17fe8793-3180-4c6b-bfee-0b73cef4120f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff90ff18-551e-428f-875f-a8600ed5081f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}