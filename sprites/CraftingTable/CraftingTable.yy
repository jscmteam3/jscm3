{
    "id": "90efae07-36a9-44df-8a23-b35711810843",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CraftingTable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f31d1e5a-86b8-4676-897a-254d2966f8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90efae07-36a9-44df-8a23-b35711810843",
            "compositeImage": {
                "id": "1a35b03a-3d49-4b1b-89a0-0527ce27bb33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f31d1e5a-86b8-4676-897a-254d2966f8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511ccc30-03a0-4104-9d53-e075884d24a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f31d1e5a-86b8-4676-897a-254d2966f8bd",
                    "LayerId": "c18a4384-b75b-4642-9449-8567380950c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c18a4384-b75b-4642-9449-8567380950c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90efae07-36a9-44df-8a23-b35711810843",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}