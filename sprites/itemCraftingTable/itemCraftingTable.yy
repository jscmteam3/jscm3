{
    "id": "dc93be6b-4a23-4338-925e-68a4b4a65eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemCraftingTable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ebc0334-bb40-46b3-989b-b668164bf992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc93be6b-4a23-4338-925e-68a4b4a65eca",
            "compositeImage": {
                "id": "289f67b3-1062-4b1e-92bb-099925fcba52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebc0334-bb40-46b3-989b-b668164bf992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e647f690-94f9-4df1-95d1-72779236e8a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebc0334-bb40-46b3-989b-b668164bf992",
                    "LayerId": "4aebb925-bd1d-4f01-81d0-813e768bcd24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4aebb925-bd1d-4f01-81d0-813e768bcd24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc93be6b-4a23-4338-925e-68a4b4a65eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}