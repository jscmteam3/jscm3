{
    "id": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pSlashRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 31,
    "bbox_right": 91,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1daba6b2-3c8b-4769-8718-911e1961a314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "compositeImage": {
                "id": "80e96285-f59f-4895-b82c-51cfcdfd984b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daba6b2-3c8b-4769-8718-911e1961a314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4499ccd0-578b-41f9-b0f3-052cd2996b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daba6b2-3c8b-4769-8718-911e1961a314",
                    "LayerId": "f5580f56-7256-4d8c-8986-b1466e65ebe4"
                },
                {
                    "id": "22de18ad-da30-4d7b-8d42-aed649dec548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daba6b2-3c8b-4769-8718-911e1961a314",
                    "LayerId": "d0820c4f-5def-430c-aa0e-8d8c02b58591"
                }
            ]
        },
        {
            "id": "3773a999-7c32-43b4-aa76-ce07e9bb2a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "compositeImage": {
                "id": "1c5bf030-49ce-4069-81cc-09ae31f70813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3773a999-7c32-43b4-aa76-ce07e9bb2a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4812046f-b9c6-42bb-b56b-5ce51c58d00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3773a999-7c32-43b4-aa76-ce07e9bb2a7c",
                    "LayerId": "f5580f56-7256-4d8c-8986-b1466e65ebe4"
                },
                {
                    "id": "0f8e70f9-f545-4824-a492-9d55c329269e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3773a999-7c32-43b4-aa76-ce07e9bb2a7c",
                    "LayerId": "d0820c4f-5def-430c-aa0e-8d8c02b58591"
                }
            ]
        },
        {
            "id": "68cceada-7de3-4f4b-ad83-c2f26b18d8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "compositeImage": {
                "id": "7d2c485b-3ff3-487a-ae8d-4c412eb6cc03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68cceada-7de3-4f4b-ad83-c2f26b18d8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9bc86da-f82c-41ad-bd38-5cd9f69dc8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cceada-7de3-4f4b-ad83-c2f26b18d8f4",
                    "LayerId": "f5580f56-7256-4d8c-8986-b1466e65ebe4"
                },
                {
                    "id": "acd5cf79-f552-495a-9091-b72fe195ad32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cceada-7de3-4f4b-ad83-c2f26b18d8f4",
                    "LayerId": "d0820c4f-5def-430c-aa0e-8d8c02b58591"
                }
            ]
        },
        {
            "id": "51d2d7ec-bd1d-474b-91bf-fa6cd3599903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "compositeImage": {
                "id": "71eefa51-133c-4b79-9a37-0d5b8ef27f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d2d7ec-bd1d-474b-91bf-fa6cd3599903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69bb64de-b419-46c7-b399-a15f52f196f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d2d7ec-bd1d-474b-91bf-fa6cd3599903",
                    "LayerId": "f5580f56-7256-4d8c-8986-b1466e65ebe4"
                },
                {
                    "id": "6b98e550-d241-4d33-9f3d-8222016aab1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d2d7ec-bd1d-474b-91bf-fa6cd3599903",
                    "LayerId": "d0820c4f-5def-430c-aa0e-8d8c02b58591"
                }
            ]
        }
    ],
    "gridX": 48,
    "gridY": 48,
    "height": 92,
    "layers": [
        {
            "id": "f5580f56-7256-4d8c-8986-b1466e65ebe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d0820c4f-5def-430c-aa0e-8d8c02b58591",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e41595b-3bf3-4d95-94c0-fa6dd8be3535",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578432,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}