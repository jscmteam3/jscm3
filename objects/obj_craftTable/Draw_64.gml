/// @description Insert description here

var c = c_white;
var boxGap = 75;
var sprReturn;
var xmouse = device_mouse_x_to_gui(0); 
var ymouse = device_mouse_y_to_gui(0);


if (show_crafting){
	with(inventoryGUI){
		other.x_top_craftGUI = x + x_offset*2;
		other.y_top_craftGUI = y - y_offset*2;
	}
	draw_sprite_ext(spr_CraftGUI, 0, x_top_craftGUI, y_top_craftGUI, scaleSprite, scaleSprite, 0, c_white, 1);	
	for (var i = 0; i < ds_list_size(list_ingre); i++){
		draw_sprite_ext(playerInventory[# 3, list_ingre[| i]], 0, x_top_craftGUI + 10*scaleSprite + boxGap*i*scaleSprite,y_top_craftGUI + 47*scaleSprite,
			scale, scale, 0, c, 1);				
}
if (ds_list_size(list_ingre)>0){
	sprReturn	= scr_ItemCrafting(x_top_craftGUI, y_top_craftGUI);
	var x1 = x_top_craftGUI + 41*scaleSprite;
	var y1 = y_top_craftGUI + 193*scaleSprite
	if (xmouse >= x1 && xmouse<= x1 + 32*scaleSprite && ymouse>= y1 && ymouse <= y1 + 32*scaleSprite){		
			
				draw_sprite_ext(spr_slotBG, 0, x1, y1, scale + 0.5, scale + 0.5, 0, c_white, 1);
				if (mouse_check_button_pressed(mb_left)){
					audio_play_sound(chose, 10,false);
					if (sprReturn == itemStone)	{								
						scr_addItems(playerInventory, "Khoi Da", 1, "ingredient", itemStone, "For crafting Stove");
						playerInventory[# 1, list_ingre[| 0] ] -= 8;	
					}
					if (sprReturn == itemCoalStove){					
							scr_addItems(playerInventory,
								"Lo Nuong", 1, "tool", itemCoalStove, "Nau Nuong");
							playerInventory[# 1, list_ingre[| 0] ] -= 2;															
					}
					if (sprReturn == itemWoodPlat){
						scr_addItems(playerInventory,
							"Thanh go", 4, "ingredient", itemWoodPlat, "Dung de xay nha");
						var k = ds_grid_get(playerInventory, 3, list_ingre[| 0]) == itemAxe ? 1:0; // if ingre 1 la axe ->giam amount ingre2
						playerInventory[# 1, list_ingre[| k] ] -= 1;
					}
					if (sprReturn == itemAxe){
						scr_addItems(playerInventory,
							"Riu da", 1, "weapon", itemAxe, "Don cui");
						playerInventory[# 1, list_ingre[| 0] ] -= 5;
						playerInventory[# 1, list_ingre[| 1] ] -= 5;
					}
					ds_list_clear(list_ingre);
				}
			}	
		}
}
else{
	ds_list_clear(list_ingre);	
}