{
    "id": "5bc46257-63f1-494f-ba98-3b731ba7bba3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite83",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e17768-5cbd-4282-8e0e-62c654d641cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc46257-63f1-494f-ba98-3b731ba7bba3",
            "compositeImage": {
                "id": "4dfd0c81-cb82-43d3-9263-cad894b78262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e17768-5cbd-4282-8e0e-62c654d641cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ee0a8b-f5e5-48f2-9dd8-3c97951f34e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e17768-5cbd-4282-8e0e-62c654d641cb",
                    "LayerId": "9c0d717a-3554-4e0b-b7a2-1e7af7ae9bb4"
                }
            ]
        },
        {
            "id": "ef416046-39b5-4546-a428-ec8cdd78c506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc46257-63f1-494f-ba98-3b731ba7bba3",
            "compositeImage": {
                "id": "b2cbd716-ef4a-4190-9c39-650f630f7d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef416046-39b5-4546-a428-ec8cdd78c506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acb91aa-2003-471a-aac8-8ba788101e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef416046-39b5-4546-a428-ec8cdd78c506",
                    "LayerId": "9c0d717a-3554-4e0b-b7a2-1e7af7ae9bb4"
                }
            ]
        },
        {
            "id": "85e67d9c-d94d-48a3-b6ce-f41e70a40233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bc46257-63f1-494f-ba98-3b731ba7bba3",
            "compositeImage": {
                "id": "83950802-bcdd-4c8a-875b-c3d31b69cdde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e67d9c-d94d-48a3-b6ce-f41e70a40233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17b3f6b-afe7-4708-9eec-74897d66f48e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e67d9c-d94d-48a3-b6ce-f41e70a40233",
                    "LayerId": "9c0d717a-3554-4e0b-b7a2-1e7af7ae9bb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c0d717a-3554-4e0b-b7a2-1e7af7ae9bb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bc46257-63f1-494f-ba98-3b731ba7bba3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}