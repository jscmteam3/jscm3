{
    "id": "ac087163-e810-4bcd-8280-4517fb8c33f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 43,
    "bbox_right": 86,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa03c4c-6e0e-4191-bbe3-0975a08d2dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac087163-e810-4bcd-8280-4517fb8c33f3",
            "compositeImage": {
                "id": "e790b362-3168-498c-8831-497a87839343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa03c4c-6e0e-4191-bbe3-0975a08d2dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2eed54-d995-433f-bd2e-b21b5147a670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa03c4c-6e0e-4191-bbe3-0975a08d2dc8",
                    "LayerId": "dfa71b44-e43c-4b1a-9fd3-6d3f4ee2613d"
                },
                {
                    "id": "4d85f361-e2ea-4a06-85c4-f71a93de72ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa03c4c-6e0e-4191-bbe3-0975a08d2dc8",
                    "LayerId": "95f1c01e-f5ac-4d6a-aaff-1e0b10d2f360"
                }
            ]
        },
        {
            "id": "deccda23-183f-421f-96b6-0b06b33e8d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac087163-e810-4bcd-8280-4517fb8c33f3",
            "compositeImage": {
                "id": "f9314f6b-9ea4-42dd-ad31-468445fb269f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deccda23-183f-421f-96b6-0b06b33e8d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2375aca3-f7f1-4bed-bf49-4a1d07368254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deccda23-183f-421f-96b6-0b06b33e8d8b",
                    "LayerId": "dfa71b44-e43c-4b1a-9fd3-6d3f4ee2613d"
                },
                {
                    "id": "2f54cbd4-5eac-4b0f-9c58-c666ccb2d650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deccda23-183f-421f-96b6-0b06b33e8d8b",
                    "LayerId": "95f1c01e-f5ac-4d6a-aaff-1e0b10d2f360"
                }
            ]
        },
        {
            "id": "f0d86129-837e-4b15-8081-7ca2c84e3c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac087163-e810-4bcd-8280-4517fb8c33f3",
            "compositeImage": {
                "id": "c889cc7e-3f60-4eb2-95fd-b976bb21b40c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d86129-837e-4b15-8081-7ca2c84e3c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1a019c-4f0a-4b35-9672-f40d054e2100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d86129-837e-4b15-8081-7ca2c84e3c8b",
                    "LayerId": "dfa71b44-e43c-4b1a-9fd3-6d3f4ee2613d"
                },
                {
                    "id": "e287bd0d-8c02-416e-8661-71983342ffac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d86129-837e-4b15-8081-7ca2c84e3c8b",
                    "LayerId": "95f1c01e-f5ac-4d6a-aaff-1e0b10d2f360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dfa71b44-e43c-4b1a-9fd3-6d3f4ee2613d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac087163-e810-4bcd-8280-4517fb8c33f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "95f1c01e-f5ac-4d6a-aaff-1e0b10d2f360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac087163-e810-4bcd-8280-4517fb8c33f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}