{
    "id": "cb6c9c86-1402-4532-917c-6254dd510b3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FrameSword1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 10,
    "bbox_right": 52,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8145bada-3122-454b-9e07-bb1b23786a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "a7402cfe-4aca-40c8-893f-dd6565280142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8145bada-3122-454b-9e07-bb1b23786a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0e9228-338e-4908-8c3d-631924d4b03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8145bada-3122-454b-9e07-bb1b23786a6a",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "a13cbd9d-fd37-44fe-97c9-9984f624222c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "4c6915ea-a56d-45ef-993b-4e65a1e6e013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13cbd9d-fd37-44fe-97c9-9984f624222c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6f83e1-6a97-451f-aef5-090c2eb87564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13cbd9d-fd37-44fe-97c9-9984f624222c",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "7d050d82-3697-4253-abd9-6a3813cb7f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "432376af-7d23-4aa7-9af5-5782bf1399a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d050d82-3697-4253-abd9-6a3813cb7f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb4c6e60-06ac-44b6-9d19-fc33d7766a6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d050d82-3697-4253-abd9-6a3813cb7f35",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "c7e21d7d-6270-4f18-84cb-20c50d7d75ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "778130ce-a75c-4bf8-baa6-0066a13eee30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e21d7d-6270-4f18-84cb-20c50d7d75ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f5409a-36e0-4127-964a-6c6ca8f7fddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e21d7d-6270-4f18-84cb-20c50d7d75ad",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "84a62407-2431-4bb0-ad52-84eaa3364b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "1869fb23-e8f3-4aa6-863a-751c52c822c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a62407-2431-4bb0-ad52-84eaa3364b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eda93ea-cbee-435b-a427-5678f39eb567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a62407-2431-4bb0-ad52-84eaa3364b4d",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "cecf5441-bff7-4e00-bc9b-74ed49d34f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "ed4c0b4a-f3f5-4b95-949c-5f24785c0e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecf5441-bff7-4e00-bc9b-74ed49d34f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a4b658-daf6-4f79-9a7f-c0b58f1b87f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecf5441-bff7-4e00-bc9b-74ed49d34f6f",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        },
        {
            "id": "097d772e-33b6-4b67-abae-866f7c394511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "compositeImage": {
                "id": "0be48df0-3bc2-4399-8ab2-65263f17d2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097d772e-33b6-4b67-abae-866f7c394511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da91e367-3fcb-400b-873c-f0a4c3c022d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097d772e-33b6-4b67-abae-866f7c394511",
                    "LayerId": "a8536758-01d4-4ea3-8723-3dbf5a723843"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8536758-01d4-4ea3-8723-3dbf5a723843",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb6c9c86-1402-4532-917c-6254dd510b3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        2583691263,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}