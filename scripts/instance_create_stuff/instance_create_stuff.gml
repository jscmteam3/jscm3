///@description instance_create_stuff
///@arg x
///@arg y
///@arg stuffName

var cs  = crops.cellSize;
var xx = argument0 div cs;
var yy = argument1 div cs;
var instID = argument2;

xx *= cs;
yy *= cs;

///create instances
var inst = instance_create_layer(xx, yy, "Instances", instID);

return inst;