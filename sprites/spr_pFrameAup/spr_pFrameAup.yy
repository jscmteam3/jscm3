{
    "id": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameAup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 21,
    "bbox_right": 101,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ded45b3a-0959-4282-8eb1-033c22604733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "09ed9fda-7784-4279-82a5-57d89ff42c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded45b3a-0959-4282-8eb1-033c22604733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94494d5e-22cc-42a3-a609-2af55213cb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded45b3a-0959-4282-8eb1-033c22604733",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                },
                {
                    "id": "1cb8b626-3463-4352-b142-8a9730a2b818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded45b3a-0959-4282-8eb1-033c22604733",
                    "LayerId": "4cf0afaa-1223-4b76-86ca-9a7375a573d4"
                }
            ]
        },
        {
            "id": "f10e3f16-ce69-4009-9911-4e00416abbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "d3c07212-df8f-4f23-a210-e52d9f3a9a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10e3f16-ce69-4009-9911-4e00416abbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3a5a37-63b1-4a67-9cc9-3df6f2074fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10e3f16-ce69-4009-9911-4e00416abbf5",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                },
                {
                    "id": "0737fba3-bf51-48fe-af2d-1573cadd6a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10e3f16-ce69-4009-9911-4e00416abbf5",
                    "LayerId": "4cf0afaa-1223-4b76-86ca-9a7375a573d4"
                }
            ]
        },
        {
            "id": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "2c4c3a4d-e5e4-48cd-bc72-d5ce5104c341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc586b69-a69e-4316-91f0-8cd3cf3352c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                },
                {
                    "id": "2ff61be3-cd93-40de-a48f-a074bcb69210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
                    "LayerId": "4cf0afaa-1223-4b76-86ca-9a7375a573d4"
                }
            ]
        },
        {
            "id": "66c1395e-93b5-4f3c-921c-7370935208ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "babc4007-85c6-4133-be51-c2aafc34df4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c1395e-93b5-4f3c-921c-7370935208ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2301aa-c57f-4451-87fc-6b95e54eb423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c1395e-93b5-4f3c-921c-7370935208ca",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                },
                {
                    "id": "296d00de-8640-4f80-b02d-ce417cac9efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c1395e-93b5-4f3c-921c-7370935208ca",
                    "LayerId": "4cf0afaa-1223-4b76-86ca-9a7375a573d4"
                }
            ]
        },
        {
            "id": "befff69e-a358-4e1e-aba4-aaa958a3d9ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "8de654af-2883-4816-b033-b7add3eb1bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "befff69e-a358-4e1e-aba4-aaa958a3d9ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208a4f61-5577-48cf-a752-5dabdba71814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "befff69e-a358-4e1e-aba4-aaa958a3d9ef",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                },
                {
                    "id": "ab66ec43-bdee-4fdb-b162-b91301171b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "befff69e-a358-4e1e-aba4-aaa958a3d9ef",
                    "LayerId": "4cf0afaa-1223-4b76-86ca-9a7375a573d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "664f265c-e451-455b-ba15-65fff20df142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4cf0afaa-1223-4b76-86ca-9a7375a573d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}