{
    "id": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashFrameLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92317ac1-7073-4c13-b829-bf79c74cf9e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
            "compositeImage": {
                "id": "f859469d-f1b6-410b-a497-ce31be9eda0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92317ac1-7073-4c13-b829-bf79c74cf9e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eba9376-2646-436e-a35f-1c2215d1dd15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92317ac1-7073-4c13-b829-bf79c74cf9e5",
                    "LayerId": "c9ae1824-537d-4d31-8075-ff2070ec33df"
                }
            ]
        },
        {
            "id": "c17a052b-1131-4d93-99d2-e41a68568b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
            "compositeImage": {
                "id": "6a77699d-17c7-492e-b308-f63f08779fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17a052b-1131-4d93-99d2-e41a68568b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aae14a3-c4b0-43ec-a1c6-2abf951fea26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17a052b-1131-4d93-99d2-e41a68568b45",
                    "LayerId": "c9ae1824-537d-4d31-8075-ff2070ec33df"
                }
            ]
        },
        {
            "id": "1c4c6c9b-e9be-4ac1-a0f1-046f070bc7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
            "compositeImage": {
                "id": "a484b675-0f8a-41cb-a14d-4eb3c2a9383e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4c6c9b-e9be-4ac1-a0f1-046f070bc7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35918758-fe1a-486a-b4d7-ff11049fe397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4c6c9b-e9be-4ac1-a0f1-046f070bc7d6",
                    "LayerId": "c9ae1824-537d-4d31-8075-ff2070ec33df"
                }
            ]
        },
        {
            "id": "7ae4bec7-a876-41e1-a1fe-81b9cc1f4ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
            "compositeImage": {
                "id": "43e4242a-6c58-4e93-b23c-cb5f0b89028d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae4bec7-a876-41e1-a1fe-81b9cc1f4ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e89b3e-fa1d-4123-82ee-29c35a2aa447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae4bec7-a876-41e1-a1fe-81b9cc1f4ee1",
                    "LayerId": "c9ae1824-537d-4d31-8075-ff2070ec33df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "c9ae1824-537d-4d31-8075-ff2070ec33df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34fc561d-0d0b-4fe0-a343-c09f510c7836",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}