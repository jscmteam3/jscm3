{
    "id": "d8098c82-870e-4fae-86dd-6f51040a6f65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 48,
    "bbox_right": 76,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bfcc43f-6e90-4522-b192-53a5eb52b38a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8098c82-870e-4fae-86dd-6f51040a6f65",
            "compositeImage": {
                "id": "e9eca55f-0299-451e-b73c-9fdde0a3dd62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bfcc43f-6e90-4522-b192-53a5eb52b38a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be906535-5627-4c65-9523-f8daff2258d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bfcc43f-6e90-4522-b192-53a5eb52b38a",
                    "LayerId": "d0f35301-e188-44a8-a0b0-8b8e44b6d51c"
                }
            ]
        },
        {
            "id": "0f774f09-b1ec-410e-83eb-5c884c617eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8098c82-870e-4fae-86dd-6f51040a6f65",
            "compositeImage": {
                "id": "03ccbf6a-8553-4236-b7a8-ace0fb8840d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f774f09-b1ec-410e-83eb-5c884c617eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063e7230-4b32-4264-8ecf-d7b69ecd87ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f774f09-b1ec-410e-83eb-5c884c617eb3",
                    "LayerId": "d0f35301-e188-44a8-a0b0-8b8e44b6d51c"
                }
            ]
        },
        {
            "id": "37de3fd9-ce3c-41e5-a898-c6828c59aa19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8098c82-870e-4fae-86dd-6f51040a6f65",
            "compositeImage": {
                "id": "6cad4edb-2c36-40c4-975a-414c862e9aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37de3fd9-ce3c-41e5-a898-c6828c59aa19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84bfc56-a072-435b-8503-864315ff54db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37de3fd9-ce3c-41e5-a898-c6828c59aa19",
                    "LayerId": "d0f35301-e188-44a8-a0b0-8b8e44b6d51c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d0f35301-e188-44a8-a0b0-8b8e44b6d51c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8098c82-870e-4fae-86dd-6f51040a6f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}