{
    "id": "489e6321-590e-4479-a42b-ce698929b77b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMonster",
    "eventList": [
        {
            "id": "afb06585-cfc1-4b20-ba93-250f4512691f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        },
        {
            "id": "5b13409c-a166-4719-930c-d80735c51536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        },
        {
            "id": "0f661bfc-fe2e-472f-aa7d-eb7cdcd951ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        },
        {
            "id": "2cbcde53-cf2d-43cd-b14f-86613c3dbb2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        },
        {
            "id": "247c3265-11ed-4ee3-86a4-5a25a8ec9fc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        },
        {
            "id": "db4f5f0c-3003-42e8-af84-515f79ee5b37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "489e6321-590e-4479-a42b-ce698929b77b"
        }
    ],
    "maskSpriteId": "cf695127-dbba-4e8d-8602-a8f37362dff3",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf695127-dbba-4e8d-8602-a8f37362dff3",
    "visible": false
}