{
    "id": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenEatUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14a5ad46-122c-4806-9804-0aa4724abaa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
            "compositeImage": {
                "id": "42bb91c0-2940-4410-851c-4f924451ba8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a5ad46-122c-4806-9804-0aa4724abaa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebac748f-62ef-405d-b47b-897a748c69b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a5ad46-122c-4806-9804-0aa4724abaa4",
                    "LayerId": "d50b2ca6-a013-4d7c-96d1-3b69b2267d51"
                }
            ]
        },
        {
            "id": "6adf2015-90dd-4ad6-b1a0-2723e254e796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
            "compositeImage": {
                "id": "5476977a-24ce-4583-93ef-e3bc7fe24964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6adf2015-90dd-4ad6-b1a0-2723e254e796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49204c1-38ac-4949-89b4-7b47b3bd2fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6adf2015-90dd-4ad6-b1a0-2723e254e796",
                    "LayerId": "d50b2ca6-a013-4d7c-96d1-3b69b2267d51"
                }
            ]
        },
        {
            "id": "cc275508-0579-4059-a557-b83a54ca06e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
            "compositeImage": {
                "id": "8b8cad24-325d-4782-9ea8-1d0add1a6b47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc275508-0579-4059-a557-b83a54ca06e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162cb486-f9dc-47eb-831a-37095c1d5b79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc275508-0579-4059-a557-b83a54ca06e8",
                    "LayerId": "d50b2ca6-a013-4d7c-96d1-3b69b2267d51"
                }
            ]
        },
        {
            "id": "a3714c01-3598-4d98-86bc-ff610b984f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
            "compositeImage": {
                "id": "29f36702-6da2-4a07-ba0b-f01a5ac5ca39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3714c01-3598-4d98-86bc-ff610b984f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af312ac9-b13b-4b53-8ff8-a467c87118b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3714c01-3598-4d98-86bc-ff610b984f35",
                    "LayerId": "d50b2ca6-a013-4d7c-96d1-3b69b2267d51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d50b2ca6-a013-4d7c-96d1-3b69b2267d51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1852267f-681c-47d3-a9d0-1ba568d2c3f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}