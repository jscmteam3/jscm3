{
    "id": "f898c094-2bc2-4257-ba08-83f3008507ea",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font3",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "NanumGothic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6b04cdea-126f-47a3-8242-727f92aa552a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 158,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "85e84986-6a5d-454a-81c7-a47b977f302c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e01024e1-c963-4288-bd25-fe19cfbe0f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "02f5893c-a0c2-444b-b20a-fa16d21fd821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "db97d1ee-f889-48a8-a6eb-c2f2a5aa62d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "dcd9b87e-bd16-4c30-b13a-07ce60988517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b69a4b2f-2021-47af-bbbc-cafadb7679d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "65ddcaeb-9782-488a-84eb-da9a2319141b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 196,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "573a633a-94ee-473c-ab01-f97619780830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e88bb255-3e17-47f6-a008-feb5fb0cc160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ae0e57bb-6254-4c5d-bb52-6484cc0f78a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 55,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5bd6d48d-6f2e-4af5-862f-3307ef02c430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ec900f87-c4c9-4efd-926c-7fdbd1f4a370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 140,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d4582df0-d942-4e08-8b8a-067b43b0e2f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 63,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "81fcad54-40d4-4b0b-bad4-ceff941a99ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f9025bbf-129f-4a3d-9eb0-4a6f2271f553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8ea34d8a-d63a-4149-aa1d-2ddee127ae28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "651f33e0-ed08-4cb1-bf47-d205d1f1b99a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ff874fa4-51ef-435d-bcd6-2cf602dfbf3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e0982f90-ea13-4457-b632-bab8facc995a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 219,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9c662b55-32cb-40d1-a1c2-70cf7147446f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7e7a68ac-82b1-470f-a681-dd61d596fa29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9a79a64a-b665-45db-8130-216e412bf4a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 175,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8dda6289-ed6b-4a5c-b562-84ceaad0539a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 195,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d9f5e0df-c2f6-4c1e-82a7-63c585fb8a14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fc46d8e8-2783-4cdb-934d-5c04cc589274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "813c2f55-968a-4cd7-b177-1562ec8c9b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 191,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "957fb05e-28de-4eea-bc9f-8babf5f151d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fbb7968f-03b3-4523-9ced-60080e4786f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 215,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "22eebd8f-a5fc-40af-adfc-9bf23c643124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0f70038a-8d67-41e6-b206-cf9b121e54b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 205,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "676729f6-8b02-4cf5-8c82-140ef4be1388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4f135db9-1c68-4bd3-a865-b5d51de3045e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0ef3605e-21f6-4c4a-91f8-507527654323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "56af1d67-d5db-4301-a822-52738b30c220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 155,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f2b4ece2-8379-4d33-bbbb-16fc5f9083f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 241,
                "y": 22
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "31e55dd3-22e3-4b1a-bb5d-2571b65754a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "550bd8c5-e2d2-437a-ac82-494d66b955f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 185,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e418ba37-d53a-4d7f-98a5-692c10151be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 245,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ea11ed33-de3a-4737-9c8a-aecfb9499193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "19eee9f1-eb24-4a90-a73c-98a72353d2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4c4b2319-71a9-454d-a616-eb9cb1e1b46c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c480a445-fb11-4bc0-8206-9c4329216f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "76e6fcbd-c4fa-428f-a7e2-3473a85b2007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c8474a24-824d-4138-bf98-34e6412cefd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2a427266-210f-4537-9ce8-50ea1fe4315f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "80f1225b-6747-4543-b97a-7eec12f3e304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0d6c7a9c-057f-4847-9808-481e8f599448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1bf17a67-ff95-404d-be21-216be71c6402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4ac83e08-ae67-450b-8b2e-e174d4bddbee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c037861a-8f0f-44d6-a22a-c4cee578bd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "faf80ad6-4542-4ae4-8a06-7e36aabfd109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cb052337-e6a6-4269-992a-f768057b146b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "519936d9-dac6-4dff-a1fb-8f94c25b1679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ec5086d5-94d1-4925-9dbb-1228f2114f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f0fa7e2c-6aac-46ca-804a-d3602de8644c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6bd41d52-6925-4299-9070-50b7211cb828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c0663a17-315c-49b6-9331-46cc8f016ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9294a50e-b624-4d52-a607-2369fe147432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 175,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ccd27af5-d128-4aa5-a81d-343a6c93f182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f68e0c3f-0498-49b9-9530-2b78f6101906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a5cbf862-fd8c-4be5-b48f-a480abc22cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 152,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "377e4af2-8642-4c93-88ff-032c38516505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b11f1de6-66dd-4034-b0cc-ff95aedf881c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c74a184b-4868-4345-beb3-fefd73b5f6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 113,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "864ba499-2eb7-4abb-b986-41c28b2ecb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "44275336-33d4-48af-80cd-9eb5f983c918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9a1a6070-5a46-41e2-9326-8f40bd0ac1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 45,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0246284f-b0a3-4a68-89a6-8c228bd8f107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 164,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "240332b6-4dfb-480f-b5e8-4aa5520ca66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 153,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dffa7c71-bc55-4cb4-9dce-b01904f69baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 47,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c40df9ab-daf5-4565-a9ce-69af87953c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d249f875-96dd-401e-b4d9-087b51df993c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "94117b76-1e8d-4f67-946c-c87663454c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 170,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3a7497fd-91ad-4553-a48a-370d30a21234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "eb9e1806-63de-42a8-9139-30c53dc6a1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d0e129b0-6bbc-4b99-bb13-a29956c91590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 205,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "be5ca0e8-dfe6-4de7-a527-30bdeec34e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7f92766a-59a7-4eaa-bb2e-54395422c535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 75,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "78f202f8-a1b9-49d0-9f91-9a8847d8711c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ffd16461-e38d-4839-979a-1a28763fe590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 85,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8351e032-7a3a-4a55-8793-72af56af8b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "96b6b9fb-b1e1-46d3-8b8e-7c4d916e5359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 99,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d9e8bee4-cd71-4e95-9754-3bd007c9259e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b2c0f507-4137-44b7-8dc9-e989d2ef6584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a882afd4-deeb-4415-ae83-1167f899946d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 105,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "06331a71-58ea-4780-a1e3-d54c656b638f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 115,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b3a9cebe-c74a-4b9f-b519-cbebb658b074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "07f17c43-3150-4575-88d9-566de3c71867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 98,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "651b285b-e458-420a-9576-2a9c9817c55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 125,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b641d6e9-3e93-4d57-9a5f-d30d60f9f1cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 135,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "20b270c8-42a0-488e-9dda-90d27203b974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3bc2d96a-448c-45e5-bccf-bbfd793d2971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 201,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1569ff99-c300-4275-9407-466e1a98d03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 71,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1124fd05-215c-4280-937f-bd367a0882b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "43bfadcc-7953-459c-8186-287e27824918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "d398e10f-336b-4bd0-a87b-93da894cfbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "cdff38f5-6ca5-4e68-9dd4-dcd829b79fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "04b81cdf-b4c4-41b2-ac31-6a535f94275e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "4a60e1d4-2dff-438d-a1dd-183353b07157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "e96ceccc-9e62-4025-ba6d-d5d10efb9178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "12dba7fc-e9f9-4a25-b095-84a692130613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "f4cab84e-9000-441f-bfef-02bbc622c039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "2b228757-f0c6-4d61-a29e-65bcc5ca57f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "3b91a50c-a529-4f69-93af-b6581412339c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "4ec9f0fe-2c8a-42c5-8d22-3eddd7752247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "5f23c49c-587d-44f8-bd89-8f5d22165952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "590ecb91-e943-448d-94ce-800ea3769191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "d7125674-d6f4-4414-bbc1-5ca734472e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "6721d0f1-9b7f-471b-95f0-7c9c1dd9afbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "e24ac517-9047-443b-a10c-4528164bc4a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "7e2ec607-8059-4c1b-9e15-6a4166d949e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "2404cc2a-15b2-48e6-8823-39d65a318b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "0eb93072-4c91-4fe2-b12a-22794ffbd73f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "e79cef09-ea44-4655-acc0-39bd1e6a0a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "b832c86d-6e64-4115-bdfc-c98bc9b8ac95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "8f13b26e-e729-4750-bc9c-0084c0990ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "206dd97e-3e6f-4ce1-acc0-b6ed5482727e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "db14077b-7916-461f-979f-08b8a01b9024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "b1b2792e-f1c2-4421-a215-6ffe6a47723f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "dff07060-38bf-4678-9ace-b2e4ce43984a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5bd1987c-7c74-471a-9cf3-208e1a1adb5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "dd40cc61-8b73-4803-9de0-b47f726d0d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "dd5bdba9-bd94-42f2-a550-d58119fb3cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "6dae166c-1871-4c6d-ad43-c025da3ff406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "c70deb9b-3649-4823-83c5-11f8568a27e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "1532b013-2672-48b4-a101-b31b7ae31d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "0800ae02-70d0-4d4b-92e4-a5ffb8b13d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "16cda9df-04c0-4d1a-b5c4-59f1bcaa06a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "dc03451f-bb6d-4175-bb18-59af63e6bf2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "523f8264-f610-4b6b-8a97-a8946f2fbef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "03352785-de1d-422e-be06-45a03fcaf608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "4af0d72a-7c4a-4f80-9035-673e0fc88332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "a9984d68-f1da-412a-9ea5-1b0e4e93e3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "7be2d568-5d3f-4c72-a818-59971a7de19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "868510d8-e579-432f-96ef-8a8ec3f03f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "1ebb8f26-a41b-4ff6-b026-6880c0067f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "acbf16cf-27df-49ec-b8ae-d708048dd8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "64bf9d71-b77b-44dd-9e36-03544f960a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "f8254f52-2cf8-4c1a-93d9-3bc9579ff226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "53b3b053-99d0-4761-afdf-2d2c5275ad82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "f50d84d2-7b3c-410f-bb73-bd6577d0b526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "f23eebf8-a647-4862-bbf2-27004a0c4729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "453f127c-b811-4beb-b3bc-f65a76e7e9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "ae5d8700-6cf4-45bd-a976-84e3d3a53527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "0f6adedd-24f7-46c6-8cb1-6d001f4c7385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "26c20a5a-6c26-4acf-8abf-45dbc435c9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "8eab9632-99c8-4a48-b966-d0450c61a57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "f1422bf5-19f9-4025-85ac-367eaeb670cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "9390f2f7-c4f3-43c1-bb65-8d7df9818786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "b184b5b2-19e0-4cdd-a9b1-2373af3e2ac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "b764f24a-244d-47e6-a402-d85df10e4156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "89db88d7-88a5-4e86-9e56-7c6150667936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "7accda4c-3493-44d8-8261-74ca86e33e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}