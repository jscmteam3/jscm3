{
    "id": "4df2c81c-3e9a-41fe-a7eb-444ba332b51c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CraftGUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 131,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae6aed26-79ea-4b55-b7e7-247344243125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4df2c81c-3e9a-41fe-a7eb-444ba332b51c",
            "compositeImage": {
                "id": "a9f98d83-a094-4394-b868-968a1ac9917d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae6aed26-79ea-4b55-b7e7-247344243125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4605549d-28aa-42e1-8518-9812b9d69751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae6aed26-79ea-4b55-b7e7-247344243125",
                    "LayerId": "ebe8a9a1-11b6-45b0-bdb7-5f00bcdd0f6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ebe8a9a1-11b6-45b0-bdb7-5f00bcdd0f6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4df2c81c-3e9a-41fe-a7eb-444ba332b51c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 132,
    "xorig": 0,
    "yorig": 0
}