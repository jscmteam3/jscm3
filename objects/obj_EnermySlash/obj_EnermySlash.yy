{
    "id": "d6cfc11c-9c48-4ae2-b2ed-718be282af4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EnermySlash",
    "eventList": [
        {
            "id": "850f1c9c-fc48-4fd7-acae-3278937012ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6cfc11c-9c48-4ae2-b2ed-718be282af4d"
        },
        {
            "id": "5850375c-3d68-4260-9b96-b408c768edc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6cfc11c-9c48-4ae2-b2ed-718be282af4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "381f921b-173c-4685-982a-10124d1bb0da",
    "visible": true
}