{
    "id": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DAYONE",
    "eventList": [
        {
            "id": "44500529-597e-4375-841b-2e321d96b10f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63"
        },
        {
            "id": "f64a0647-8c2f-4c9b-a307-8171035947d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63"
        },
        {
            "id": "6b055967-d34b-4c39-9e66-d0b679ede642",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63"
        },
        {
            "id": "5fe623f1-497c-4064-8b4b-a181a92367e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}