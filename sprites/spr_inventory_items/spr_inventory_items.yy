{
    "id": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 208,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e69eb8da-7f27-4bd5-85c9-3afbfb308e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
            "compositeImage": {
                "id": "49f6fcf7-674c-4725-bec0-ac5e0fc7af9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69eb8da-7f27-4bd5-85c9-3afbfb308e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e78516f9-17a8-4815-b515-1dd5aa889239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69eb8da-7f27-4bd5-85c9-3afbfb308e82",
                    "LayerId": "6cbd326b-5c98-4b76-97b2-10f98d8b4569"
                },
                {
                    "id": "5546b4a1-d517-4265-b75f-5cea5ee0973e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69eb8da-7f27-4bd5-85c9-3afbfb308e82",
                    "LayerId": "b4ae1f9e-3340-4b34-9a4d-b8b34c18ce1e"
                }
            ]
        },
        {
            "id": "fb2e84ff-2723-4c29-8452-1d1d21ae4023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
            "compositeImage": {
                "id": "fb54c08d-c258-4f50-b907-401e1bcfeb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2e84ff-2723-4c29-8452-1d1d21ae4023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425c9759-5881-463b-8574-2541ef268f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2e84ff-2723-4c29-8452-1d1d21ae4023",
                    "LayerId": "6cbd326b-5c98-4b76-97b2-10f98d8b4569"
                },
                {
                    "id": "b73dbc24-7943-4e6b-9809-64746c9a4748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2e84ff-2723-4c29-8452-1d1d21ae4023",
                    "LayerId": "b4ae1f9e-3340-4b34-9a4d-b8b34c18ce1e"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 209,
    "layers": [
        {
            "id": "6cbd326b-5c98-4b76-97b2-10f98d8b4569",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b4ae1f9e-3340-4b34-9a4d-b8b34c18ce1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 104
}