{
    "id": "346e3252-f15a-4c85-9f54-af325142ac94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SnakeLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6b8ea36-0d05-4413-bf3e-4d8dd04a7453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346e3252-f15a-4c85-9f54-af325142ac94",
            "compositeImage": {
                "id": "c646ecc2-0dd2-4095-af3d-03e12db34435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b8ea36-0d05-4413-bf3e-4d8dd04a7453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b96ca4-88cf-43e2-af73-c25a722edcd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b8ea36-0d05-4413-bf3e-4d8dd04a7453",
                    "LayerId": "adb5a502-45b9-44ee-a782-24654368afca"
                }
            ]
        },
        {
            "id": "3628e34e-1576-4f99-9192-674c9a1d1593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346e3252-f15a-4c85-9f54-af325142ac94",
            "compositeImage": {
                "id": "532e9de0-2c4e-404b-8b9b-c1c8fcc857f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3628e34e-1576-4f99-9192-674c9a1d1593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08da351e-594d-4654-99bf-e33a70399e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3628e34e-1576-4f99-9192-674c9a1d1593",
                    "LayerId": "adb5a502-45b9-44ee-a782-24654368afca"
                }
            ]
        },
        {
            "id": "ff7982cc-c421-4c04-bebb-e6e85eef50e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346e3252-f15a-4c85-9f54-af325142ac94",
            "compositeImage": {
                "id": "b0590cd9-ecba-4ecc-b69a-fc4584abd9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff7982cc-c421-4c04-bebb-e6e85eef50e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb42ad8e-8b2f-4f24-a106-de641c9c910e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff7982cc-c421-4c04-bebb-e6e85eef50e8",
                    "LayerId": "adb5a502-45b9-44ee-a782-24654368afca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "adb5a502-45b9-44ee-a782-24654368afca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "346e3252-f15a-4c85-9f54-af325142ac94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}