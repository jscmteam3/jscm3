XAxis = _lengthX;
YAxis = _lengthY;
getDirection();
var checkArea = 144;
if (monster_hp < max_monster_hp) checkArea = 248;
if(!collision_circle(x, y, 248, objPlayer, false, false)) checkArea = 144;
image_xscale = XAxis > 0 ? 1:-1;
if (state == states.idle)
{
	counter +=1;
	//transition triggers
	if (counter >= room_speed * 3){ // make sure condition is true every 3 seconds
		var change = choose(0,1);	// choosing argument randomly
		switch(change){
			case 0: state = states.wander;
			case 1: counter = 0;
					break;// reset 
		}
	}
	if(collision_circle(x, y, checkArea, objPlayer, false, false))
			state = states.alert;
		sprite_index = spr_giantIdle;
}
else if (state == states.wander)
{
	counter +=1;
	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	if (counter >= room_speed * 3){
		var change = choose(0,1);
		switch(change){
			case 0: state = states.idle;
			case 1:
				snake_dir = irandom_range(0,359);
				_lengthX = lengthdir_x(Speed,snake_dir);
				_lengthY = lengthdir_y(Speed,snake_dir);
				counter = 0;
				break;	
		}	
	}
	if (collision_circle(x, y, checkArea, objPlayer, false, false))
		state = states.alert;	
	sprite_index = spr_giantIdle;
}
else if (state == states.alert)
{
	counter += 1;
	snake_dir = point_direction(x,y, objPlayer.x, objPlayer.y);
	
	_lengthX = lengthdir_x(Speed,snake_dir);	
	_lengthY = lengthdir_y(Speed,snake_dir);	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	//out of chasing
	
	if (!collision_circle(x, y, checkArea, objPlayer, false, false))
		state = states.idle;
	if (collision_circle(x, y, 64, objPlayer, false, false))
		state = states.attack;
	
	sprite_index = spr_giantIdle;
}
else if (state == states.attack){	


		sprite_index = spr_giantAttack;
		
	if (!instance_exists(obj_EnermySlash) && image_index > image_number-1 ){
	instance_create_layer(x + _lengthX, y + _lengthY, "Instances", obj_EnermySlash);	
	audio_play_sound(snd_lightning, 10, false);
	}

}
if (monster_hp <= 0){
	var instID = instance_create_layer(x, y, "Inventory",objPickup);
	with(instID){
		instID.myItemName = "Nam cham";
		instID.myItemAmount = 1;
		instID.myItemType = "mission";
		instID.myItemScript = "Vat pham nhiem vu";
		instID.myItemSprite = itemMagnet;
	}
	ds_list_add(persistentData.ds_disapearID, id);
	instance_destroy();
}
