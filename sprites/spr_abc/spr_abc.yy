{
    "id": "974a381a-4ae0-4a87-b390-e0c4ef5db699",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 243,
    "bbox_left": 7,
    "bbox_right": 209,
    "bbox_top": 93,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d51c2abc-94c4-47c5-9ccf-7e66bc7f69dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "974a381a-4ae0-4a87-b390-e0c4ef5db699",
            "compositeImage": {
                "id": "5c9663ed-a481-40e5-b571-a689fd5ac407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51c2abc-94c4-47c5-9ccf-7e66bc7f69dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac223ecf-df18-433f-97c4-8915b33330bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51c2abc-94c4-47c5-9ccf-7e66bc7f69dc",
                    "LayerId": "c03c9b4e-a581-4346-a0a0-9d5a83a58c35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c03c9b4e-a581-4346-a0a0-9d5a83a58c35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "974a381a-4ae0-4a87-b390-e0c4ef5db699",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 110,
    "yorig": 145
}