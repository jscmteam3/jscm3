{
    "id": "2374a3ef-311b-4906-9e64-d5d5ec7653b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemStone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fbf444f-708b-40c5-b174-91d4c1e7bc79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2374a3ef-311b-4906-9e64-d5d5ec7653b2",
            "compositeImage": {
                "id": "949cb9c5-c409-4407-bdbc-3373b3c2b8f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fbf444f-708b-40c5-b174-91d4c1e7bc79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11763c65-a6e8-40e8-b4e4-b453a0d6c31f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fbf444f-708b-40c5-b174-91d4c1e7bc79",
                    "LayerId": "ab166562-60f7-49d2-971f-1f52060c96fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ab166562-60f7-49d2-971f-1f52060c96fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2374a3ef-311b-4906-9e64-d5d5ec7653b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}