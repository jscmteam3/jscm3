{
    "id": "5a78e610-938a-4be5-b2c9-3e5a481a3c0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reptileCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 4,
    "bbox_right": 65,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1b725d4-800f-40fb-91e7-95b52a8ee742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a78e610-938a-4be5-b2c9-3e5a481a3c0e",
            "compositeImage": {
                "id": "01dc3587-4df0-44f1-a495-2b2320c1fa7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b725d4-800f-40fb-91e7-95b52a8ee742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "109a1ce0-9c88-41eb-8dbf-c1539eaa9f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b725d4-800f-40fb-91e7-95b52a8ee742",
                    "LayerId": "1380e4f6-5c5c-4ab8-9da1-0b90885e5937"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "1380e4f6-5c5c-4ab8-9da1-0b90885e5937",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a78e610-938a-4be5-b2c9-3e5a481a3c0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 76,
    "xorig": 38,
    "yorig": 34
}