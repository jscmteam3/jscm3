{
    "id": "74b8c360-3cc7-4564-b67d-c286f9dbbca2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11e930a1-9b15-41ce-891f-7a71f67c35c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b8c360-3cc7-4564-b67d-c286f9dbbca2",
            "compositeImage": {
                "id": "9b74a9af-b42d-4851-84df-fb4ae103bc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e930a1-9b15-41ce-891f-7a71f67c35c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524d2b7b-bd02-4c1d-bd54-4e7f35a4af94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e930a1-9b15-41ce-891f-7a71f67c35c5",
                    "LayerId": "b833e6e0-fb3e-4c68-aa66-6e8706c93e42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "b833e6e0-fb3e-4c68-aa66-6e8706c93e42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74b8c360-3cc7-4564-b67d-c286f9dbbca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 0
}