{
    "id": "e6500498-a209-42ec-bb99-d0a6b4baa74d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 17,
    "bbox_right": 48,
    "bbox_top": 120,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3afc4d10-4dcf-46ac-9015-3d0d4b9f2e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6500498-a209-42ec-bb99-d0a6b4baa74d",
            "compositeImage": {
                "id": "c508c7e1-f8b3-4983-87a8-7209039e42cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3afc4d10-4dcf-46ac-9015-3d0d4b9f2e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263a46a7-d377-46ba-9c22-26b83398426c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3afc4d10-4dcf-46ac-9015-3d0d4b9f2e89",
                    "LayerId": "e17562a8-ebd9-49c2-810e-d585ac270334"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "e17562a8-ebd9-49c2-810e-d585ac270334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6500498-a209-42ec-bb99-d0a6b4baa74d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 139
}