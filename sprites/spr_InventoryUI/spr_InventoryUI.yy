{
    "id": "9979904e-a350-4def-80b6-355102a78a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_InventoryUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a469566-d2b2-46a0-8ecd-ec9b6d161933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9979904e-a350-4def-80b6-355102a78a55",
            "compositeImage": {
                "id": "4d632dbd-8b8e-4bcf-918c-ad3a67eb495f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a469566-d2b2-46a0-8ecd-ec9b6d161933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85dca044-16cd-4997-a33b-85fa0a9026cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a469566-d2b2-46a0-8ecd-ec9b6d161933",
                    "LayerId": "c90b2307-d522-41ed-8cf1-488653b9a5f8"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 192,
    "layers": [
        {
            "id": "c90b2307-d522-41ed-8cf1-488653b9a5f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9979904e-a350-4def-80b6-355102a78a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}