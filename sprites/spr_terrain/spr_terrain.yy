{
    "id": "5ce368ef-cab3-4f4a-8eaa-d3bcfb856c7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_terrain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85b89966-b1da-4329-ae4e-b1603ca79085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ce368ef-cab3-4f4a-8eaa-d3bcfb856c7a",
            "compositeImage": {
                "id": "151f2235-fab1-4d1d-b6e5-3347056fb31e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b89966-b1da-4329-ae4e-b1603ca79085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8732d287-ad86-4994-a2f6-66f64256980c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b89966-b1da-4329-ae4e-b1603ca79085",
                    "LayerId": "952331da-a87c-499c-b032-e4f754e2afb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "952331da-a87c-499c-b032-e4f754e2afb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ce368ef-cab3-4f4a-8eaa-d3bcfb856c7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}