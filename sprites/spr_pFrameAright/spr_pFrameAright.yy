{
    "id": "63118e42-3d0e-440f-b26b-447e909758f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameAright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 32,
    "bbox_right": 117,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f24f578-039c-4125-8f94-d0169e8f3e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "ec51c2ea-f846-434f-825e-708d4e8d051f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f24f578-039c-4125-8f94-d0169e8f3e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d09630f-656d-4b14-b660-790d7b95b8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f24f578-039c-4125-8f94-d0169e8f3e43",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                },
                {
                    "id": "44f71e88-4b8f-4b76-ad2b-b9cc00f900a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f24f578-039c-4125-8f94-d0169e8f3e43",
                    "LayerId": "136165da-3451-4aad-b33a-59f071811015"
                }
            ]
        },
        {
            "id": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "841a3c93-ee83-443f-b7ff-6123b2562046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f167e9-4d75-4c74-af26-d2edc8891837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                },
                {
                    "id": "42543c94-e78e-448a-aa98-7a680e9f18dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
                    "LayerId": "136165da-3451-4aad-b33a-59f071811015"
                }
            ]
        },
        {
            "id": "d3a97e76-7a21-4771-b9d1-b32045a03726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "0af09777-c576-4d31-a298-1f68d8c9ea50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a97e76-7a21-4771-b9d1-b32045a03726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d8ee1d-09a0-4f89-b119-75af8799f0f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a97e76-7a21-4771-b9d1-b32045a03726",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                },
                {
                    "id": "363a6837-0fbd-4c19-a242-81e5a23e8f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a97e76-7a21-4771-b9d1-b32045a03726",
                    "LayerId": "136165da-3451-4aad-b33a-59f071811015"
                }
            ]
        },
        {
            "id": "0686fae2-bcab-44fd-8920-adb9575882d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "85774f13-e187-476f-9df6-dd1b02062574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0686fae2-bcab-44fd-8920-adb9575882d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75152167-6cc2-45dd-a5b8-1c7c02fe893d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0686fae2-bcab-44fd-8920-adb9575882d8",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                },
                {
                    "id": "79aaa725-b992-4983-8d7f-2f90b90fcb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0686fae2-bcab-44fd-8920-adb9575882d8",
                    "LayerId": "136165da-3451-4aad-b33a-59f071811015"
                }
            ]
        },
        {
            "id": "93a64210-95c8-4f04-9b80-9605c5008a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "583d9b0d-5c2b-469f-b412-0206860e366a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a64210-95c8-4f04-9b80-9605c5008a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccaa8d23-f484-49f1-b06a-256d4b1c9fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a64210-95c8-4f04-9b80-9605c5008a40",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                },
                {
                    "id": "377666d6-71c5-4f30-add7-cd4c0a670450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a64210-95c8-4f04-9b80-9605c5008a40",
                    "LayerId": "136165da-3451-4aad-b33a-59f071811015"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "136165da-3451-4aad-b33a-59f071811015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}