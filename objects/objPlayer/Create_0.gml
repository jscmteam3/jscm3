/// @description Insert description here
// You can write your code in this editor
XAxis = 0;
YAxis = 0;
Speed = 2;

isAttack = false;

isUnarm = true;

delayAttack = 60;

//for collision
_lengthX = 0;
_lengthY = 0;

x_sword = sprite_get_xoffset(sprite_index);
y_sword = sprite_get_yoffset(sprite_index);

Direction = LEFT;
//Condition = IDLE;

//sprite 48x48
itemUsing = 0;
View2[RIGHT,IDLE] = spr_pMoveRight;
View2[LEFT,IDLE] = spr_pMoveLeft;
View2[UP,IDLE] = spr_pMoveUp;
View2[DOWN,IDLE] = spr_pMoveDown;

View2[RIGHT,ATTACK] = spr_pSlashRight;
View2[LEFT,ATTACK] = spr_pSlashLeft;
View2[UP,ATTACK] = spr_pSlashUp;
View2[DOWN,ATTACK] = spr_pSlashDown;

View2[RIGHT,AMODE] = spr_swordModeRight;
View2[LEFT,AMODE] = spr_swordModeLeft;
View2[UP,AMODE] = spr_swordModeUp;
View2[DOWN,AMODE] = spr_swordModeDown;

View2[RIGHT,AXEMODE] = spr_axeMoveRight;
View2[LEFT,AXEMODE] = spr_axeMoveLeft;
View2[UP,AXEMODE] = spr_axeMoveUp;
View2[DOWN,AXEMODE] = spr_axeMoveDown;

View2[RIGHT,AXEATTACK] = spr_axeATRight;
View2[LEFT,AXEATTACK] = spr_axeATLeft;
View2[UP,AXEATTACK] = spr_axeATUP;
View2[DOWN,AXEATTACK] = spr_axeATDown;

View2[RIGHT,FRAMEMODE] = spr_pFrameRight;
View2[LEFT,FRAMEMODE] = spr_pFrameLeft;
View2[UP,FRAMEMODE] = spr_pFrameUp;
View2[DOWN,FRAMEMODE] = spr_pFrameDown;

View2[RIGHT,FRAMEATTACK] = spr_pFrameAright;
View2[LEFT,FRAMEATTACK] = spr_pFrameAleft;
View2[UP,FRAMEATTACK] = spr_pFrameAup;
View2[DOWN,FRAMEATTACK] = spr_pFrameAdown;

currentCondition = -1;
