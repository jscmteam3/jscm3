{
    "id": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite148",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1759a95-26b6-462c-a02b-f4fa8872c971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "557efb8a-9c45-435b-b585-a124256cbed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1759a95-26b6-462c-a02b-f4fa8872c971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db09da2d-8bc2-473a-af1c-a698d8af9ed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1759a95-26b6-462c-a02b-f4fa8872c971",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "7c37dd73-0b9a-4773-9177-6d30e0a41a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "0b539738-5ea6-417d-970f-2f3d6f75a5bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c37dd73-0b9a-4773-9177-6d30e0a41a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5300a775-d8c3-4e70-a411-5cc5edb65a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c37dd73-0b9a-4773-9177-6d30e0a41a5c",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "a66350ce-7fd8-4d9f-81ac-42f6e2130983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "fc5a2577-5fb5-4609-98fb-5f137f18a827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a66350ce-7fd8-4d9f-81ac-42f6e2130983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41e1ce3-778e-4611-a649-38514be8166d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a66350ce-7fd8-4d9f-81ac-42f6e2130983",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "44197ff1-83ac-427e-8b74-8c0a95ebc210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "6f3beec3-0331-497e-b902-c25f93383bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44197ff1-83ac-427e-8b74-8c0a95ebc210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c8eb8b1-7341-439b-8bca-a32edb6fcec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44197ff1-83ac-427e-8b74-8c0a95ebc210",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "f67453b2-acb9-429a-983e-689cfa4f6a2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "1d44b1a2-08f7-40c2-a2fa-e23568090cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67453b2-acb9-429a-983e-689cfa4f6a2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1fc111-e212-488e-afc5-ce2667c4e6f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67453b2-acb9-429a-983e-689cfa4f6a2b",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "f509969e-820e-4c9c-b12b-0576e6678bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "05ae57df-ee3d-4003-ba55-43ee3ccc9677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f509969e-820e-4c9c-b12b-0576e6678bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb87806-a382-4d09-8e4d-fe56b44a508d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f509969e-820e-4c9c-b12b-0576e6678bed",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "a044cfc6-f4e1-49fa-8bf3-0e99eaca078e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "63d59198-42b9-426c-8002-a466a28d5cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a044cfc6-f4e1-49fa-8bf3-0e99eaca078e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2819c8be-b678-46b8-bef8-5cd828c8e077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a044cfc6-f4e1-49fa-8bf3-0e99eaca078e",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "0313288e-a5dc-44af-bfe5-6869da4bfc94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "099f9a30-1591-435f-b639-552032a2a559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0313288e-a5dc-44af-bfe5-6869da4bfc94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be10aa63-337d-41fa-add8-a6b60ab501e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0313288e-a5dc-44af-bfe5-6869da4bfc94",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "7cd5524d-32ff-4031-9f99-e58e529a59ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "21338e7c-2b24-4cbc-b5c8-ffb61905a99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd5524d-32ff-4031-9f99-e58e529a59ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72a41f8f-11f2-4126-9614-c191bf944b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd5524d-32ff-4031-9f99-e58e529a59ec",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "1baf4338-f88d-44bc-8be0-9c264fde96cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "6211cfeb-f296-484a-98e0-cab8d1ddfca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1baf4338-f88d-44bc-8be0-9c264fde96cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b21647-a961-4c5d-bb5f-e39e7808cdda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1baf4338-f88d-44bc-8be0-9c264fde96cb",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "d58b5719-50ef-4738-a0ce-e197fd87fb7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "53baa936-fbe5-4e23-b902-511154b94389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58b5719-50ef-4738-a0ce-e197fd87fb7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34204aa5-a65c-46ed-93e5-6399f49c0a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58b5719-50ef-4738-a0ce-e197fd87fb7d",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "fd634dc2-3786-47c5-ba3a-d9b199bb4b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "be8f4039-c9cf-4250-aa01-d07b86d15827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd634dc2-3786-47c5-ba3a-d9b199bb4b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efd03b4-dd6c-4f68-bcb9-e69158126a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd634dc2-3786-47c5-ba3a-d9b199bb4b68",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "d4da70ed-3e23-41ba-847a-78b8da781eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76af811e-0981-4b7b-a546-8b29ec4c5f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "76ea3f49-4606-4705-b070-73a09d58febe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "f5385910-08a0-48c0-b49e-3650b2f24b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ea3f49-4606-4705-b070-73a09d58febe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01365c4-c781-47f9-b75e-06edb097d57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ea3f49-4606-4705-b070-73a09d58febe",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "eca633f2-2802-426c-b442-73703df3ffa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b17c2edb-020c-41eb-bb20-8a976d00f2f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "c0c989f6-7166-484f-9515-f3853ec7c003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "2132977a-5cc1-4303-adaf-ac52581f6826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c989f6-7166-484f-9515-f3853ec7c003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a20ddfe-2f7d-4b2d-b412-16e738003e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c989f6-7166-484f-9515-f3853ec7c003",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7853d77-1050-4c6e-86fd-9b71ee8b3652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}