{
    "id": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashAXEright1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 55,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9166ff1-f333-456e-b4c1-c90cf3ae190f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
            "compositeImage": {
                "id": "a96789e9-068b-4bc4-9bc7-d539004477c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9166ff1-f333-456e-b4c1-c90cf3ae190f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2524755b-cf1b-4cf2-9cb6-4cf5a4c0a74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9166ff1-f333-456e-b4c1-c90cf3ae190f",
                    "LayerId": "833a17c3-f3ba-4de8-9a18-59f59ce29f14"
                }
            ]
        },
        {
            "id": "1dcd93e6-4bc9-46b5-a024-3aa17ae3f2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
            "compositeImage": {
                "id": "11a7f208-587a-475b-8ef3-dd16bcdf771f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcd93e6-4bc9-46b5-a024-3aa17ae3f2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb842ad-71d1-4569-8ba7-263244b73980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcd93e6-4bc9-46b5-a024-3aa17ae3f2e5",
                    "LayerId": "833a17c3-f3ba-4de8-9a18-59f59ce29f14"
                }
            ]
        },
        {
            "id": "d63ca6cf-e093-4ff3-ade3-11d295707fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
            "compositeImage": {
                "id": "518a57b3-8cc3-49cb-a595-f4ff76fb1371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63ca6cf-e093-4ff3-ade3-11d295707fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f393603-b8a1-4a6f-802c-32c206deb08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63ca6cf-e093-4ff3-ade3-11d295707fe5",
                    "LayerId": "833a17c3-f3ba-4de8-9a18-59f59ce29f14"
                }
            ]
        },
        {
            "id": "6b59317f-d599-4f74-92bf-0f4a22bafb6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
            "compositeImage": {
                "id": "81a0b018-5022-4dfc-9596-7f979de79984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b59317f-d599-4f74-92bf-0f4a22bafb6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd58ada-c0ea-4fb9-bf2f-29c6538887cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b59317f-d599-4f74-92bf-0f4a22bafb6f",
                    "LayerId": "833a17c3-f3ba-4de8-9a18-59f59ce29f14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "833a17c3-f3ba-4de8-9a18-59f59ce29f14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91972ba4-d37e-4632-920c-8964bb4e0fc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": -3,
    "yorig": -96
}