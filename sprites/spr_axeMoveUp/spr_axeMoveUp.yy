{
    "id": "b595a036-da63-4020-99d3-d23ba933ee85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_axeMoveUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 15,
    "bbox_right": 54,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d4f94e9-749e-4b80-9b95-80fe0f0b3ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b595a036-da63-4020-99d3-d23ba933ee85",
            "compositeImage": {
                "id": "5181eb8e-3369-4da0-8ba2-7d4bdf25c9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4f94e9-749e-4b80-9b95-80fe0f0b3ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "233260de-e1af-4161-b368-4bc8fdc4638b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4f94e9-749e-4b80-9b95-80fe0f0b3ace",
                    "LayerId": "f43210fc-38fe-475e-8259-7b8f623ba93a"
                },
                {
                    "id": "0ab3fd32-8221-4b07-82f4-f5772639ba8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4f94e9-749e-4b80-9b95-80fe0f0b3ace",
                    "LayerId": "d16c21a8-40b9-46f1-9bec-68294f13d134"
                }
            ]
        },
        {
            "id": "316b5529-e8c7-4220-82f1-6598be767745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b595a036-da63-4020-99d3-d23ba933ee85",
            "compositeImage": {
                "id": "419dcca2-dc4f-46b8-b04e-c219820a410c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "316b5529-e8c7-4220-82f1-6598be767745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad80c11b-00e6-488b-8c9c-907a3d1a464c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "316b5529-e8c7-4220-82f1-6598be767745",
                    "LayerId": "f43210fc-38fe-475e-8259-7b8f623ba93a"
                },
                {
                    "id": "7dd15e94-f379-40eb-ab5a-e346ca0590f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "316b5529-e8c7-4220-82f1-6598be767745",
                    "LayerId": "d16c21a8-40b9-46f1-9bec-68294f13d134"
                }
            ]
        },
        {
            "id": "2ce65ca3-7ec4-4896-b032-ac80a899f34d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b595a036-da63-4020-99d3-d23ba933ee85",
            "compositeImage": {
                "id": "662217df-9394-4e32-859c-225b12696ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce65ca3-7ec4-4896-b032-ac80a899f34d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c675ff-7100-409e-bb2e-33c13183526f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce65ca3-7ec4-4896-b032-ac80a899f34d",
                    "LayerId": "f43210fc-38fe-475e-8259-7b8f623ba93a"
                },
                {
                    "id": "57b48486-6b6c-4872-9b9d-7e45c98fdbe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce65ca3-7ec4-4896-b032-ac80a899f34d",
                    "LayerId": "d16c21a8-40b9-46f1-9bec-68294f13d134"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d16c21a8-40b9-46f1-9bec-68294f13d134",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b595a036-da63-4020-99d3-d23ba933ee85",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f43210fc-38fe-475e-8259-7b8f623ba93a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b595a036-da63-4020-99d3-d23ba933ee85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}