{
    "id": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite139",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ac72f2e-c9c6-4a5a-950a-d348ba6039a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "ef19a7a8-5fda-44ff-b544-a1ef97593a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac72f2e-c9c6-4a5a-950a-d348ba6039a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6642f2-3028-4b8d-9d9e-140863d768e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac72f2e-c9c6-4a5a-950a-d348ba6039a8",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "29de0e1b-9c25-4f01-9508-0ad994c75524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "d3ca0e5f-0fd3-46b2-bc4c-c9156f3bdd0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29de0e1b-9c25-4f01-9508-0ad994c75524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c3f941-a9e3-43cc-a33a-4aacc5fba300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29de0e1b-9c25-4f01-9508-0ad994c75524",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "be1a3806-ad8b-4bce-ae1c-550db4e30d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "39911c45-087c-4107-bd12-20fef2d75325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1a3806-ad8b-4bce-ae1c-550db4e30d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832e0bb0-86e4-48d1-9e88-0532fffc5e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1a3806-ad8b-4bce-ae1c-550db4e30d24",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "e9fa1fba-a26e-4a74-88e9-b373acf3cb99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "2c3f723e-300f-4671-955a-badfd0dc4bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fa1fba-a26e-4a74-88e9-b373acf3cb99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb713e73-69cc-4bcf-9e79-95e25803640c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fa1fba-a26e-4a74-88e9-b373acf3cb99",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "22cf6c49-44e2-4941-814b-4e095e111ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "8a720bf4-1faa-4baf-bbbf-98b783c2aa56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22cf6c49-44e2-4941-814b-4e095e111ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "346b49f0-3be4-424b-bf9a-0bc80be1f8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22cf6c49-44e2-4941-814b-4e095e111ca6",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "07d27492-2888-4a39-85db-08ca53542d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "8bb33959-80a2-44b2-a42c-2798be6934fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d27492-2888-4a39-85db-08ca53542d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d60ddbcc-a12b-472d-b074-2fc3866af0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d27492-2888-4a39-85db-08ca53542d86",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "25d492fb-27e2-400e-9c23-3c27bf8f9ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "6cc73194-fbd8-4745-a653-a703772ae0b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d492fb-27e2-400e-9c23-3c27bf8f9ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ebd6b2c-5c0d-4da8-8df7-66296fd2e8ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d492fb-27e2-400e-9c23-3c27bf8f9ef0",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        },
        {
            "id": "0f5f3955-2fd5-4ef7-9daa-ffb499dd009c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "compositeImage": {
                "id": "0907daee-0576-4b25-b272-839868b963f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5f3955-2fd5-4ef7-9daa-ffb499dd009c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3baebc-c1a5-41bc-91d4-0f6811b4c4ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5f3955-2fd5-4ef7-9daa-ffb499dd009c",
                    "LayerId": "6f8a8046-fbb0-4afb-8290-c8b525f5789c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f8a8046-fbb0-4afb-8290-c8b525f5789c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "768f1cf8-850e-4e95-8225-6bf9d0b515ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}