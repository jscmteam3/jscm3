{
    "id": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "073f5d39-638f-4ef3-8b9c-f8b5ae7577f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
            "compositeImage": {
                "id": "ed8bae2e-861a-4ddf-a351-6f44059198b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "073f5d39-638f-4ef3-8b9c-f8b5ae7577f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc29966-f09e-4b69-b1cc-2e566cacfc18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "073f5d39-638f-4ef3-8b9c-f8b5ae7577f4",
                    "LayerId": "9226a307-8a92-4e42-8265-b456eb9db0fe"
                }
            ]
        },
        {
            "id": "bb6a006d-d27e-4e1b-a024-09d4fb094fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
            "compositeImage": {
                "id": "84632a35-036b-4322-92ac-671bd87aa2b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb6a006d-d27e-4e1b-a024-09d4fb094fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3572c7fc-7ab1-404d-83f7-69c11e356120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb6a006d-d27e-4e1b-a024-09d4fb094fbd",
                    "LayerId": "9226a307-8a92-4e42-8265-b456eb9db0fe"
                }
            ]
        },
        {
            "id": "52a09f02-414d-4206-8d0c-fe8a541c3835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
            "compositeImage": {
                "id": "86381d11-61a0-4272-bf61-5a023f0d4ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a09f02-414d-4206-8d0c-fe8a541c3835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78dcfbed-8730-41c7-a694-4ed62217c583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a09f02-414d-4206-8d0c-fe8a541c3835",
                    "LayerId": "9226a307-8a92-4e42-8265-b456eb9db0fe"
                }
            ]
        },
        {
            "id": "6d5d9213-ca58-488e-9a83-d96a2295ae27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
            "compositeImage": {
                "id": "8f2cf641-41fb-4c25-8caf-425fe05e99cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5d9213-ca58-488e-9a83-d96a2295ae27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a1d876-8a4a-4ef7-ad51-6cb14cb9339d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5d9213-ca58-488e-9a83-d96a2295ae27",
                    "LayerId": "9226a307-8a92-4e42-8265-b456eb9db0fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "9226a307-8a92-4e42-8265-b456eb9db0fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "397573a4-733d-48b5-8baf-55cf41ccd6c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}