{
    "id": "71ef442c-fa25-4eef-878c-621af3045fd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemRawFish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a873855-add3-42e3-b522-4983a03ef52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71ef442c-fa25-4eef-878c-621af3045fd5",
            "compositeImage": {
                "id": "5a92962a-8e06-4019-aa39-725cf2d8d35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a873855-add3-42e3-b522-4983a03ef52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe0fc53-517b-455f-a28d-06db1f338b11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a873855-add3-42e3-b522-4983a03ef52e",
                    "LayerId": "2f0ee6e6-b59b-4b4b-a2dc-65f8d4fbe077"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f0ee6e6-b59b-4b4b-a2dc-65f8d4fbe077",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71ef442c-fa25-4eef-878c-621af3045fd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}