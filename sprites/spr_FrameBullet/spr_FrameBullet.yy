{
    "id": "707e2bf9-8bac-47c0-8f2f-ecce21c483af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FrameBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1325f60c-710e-40d4-89b0-c1170cd6aea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "707e2bf9-8bac-47c0-8f2f-ecce21c483af",
            "compositeImage": {
                "id": "a610c27c-db73-4a30-801e-106598ac5164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1325f60c-710e-40d4-89b0-c1170cd6aea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b34087-ab82-4ed1-883c-42cae7367f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1325f60c-710e-40d4-89b0-c1170cd6aea9",
                    "LayerId": "72444b5d-14a9-48e4-b7eb-a9057a73ed5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "72444b5d-14a9-48e4-b7eb-a9057a73ed5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "707e2bf9-8bac-47c0-8f2f-ecce21c483af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}