{
    "id": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveUp1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f10e3f16-ce69-4009-9911-4e00416abbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "d3c07212-df8f-4f23-a210-e52d9f3a9a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10e3f16-ce69-4009-9911-4e00416abbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3a5a37-63b1-4a67-9cc9-3df6f2074fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10e3f16-ce69-4009-9911-4e00416abbf5",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                }
            ]
        },
        {
            "id": "1c64fc4e-6cb0-414b-938c-5ad064553f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "5978e0c1-969b-4497-8f28-ae27e97299ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c64fc4e-6cb0-414b-938c-5ad064553f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ab2b1c-b342-46d4-9c00-ae7f6a55ebf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c64fc4e-6cb0-414b-938c-5ad064553f3b",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                }
            ]
        },
        {
            "id": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "compositeImage": {
                "id": "2c4c3a4d-e5e4-48cd-bc72-d5ce5104c341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc586b69-a69e-4316-91f0-8cd3cf3352c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f780598-b341-4cb9-9119-3b3d1862ed2b",
                    "LayerId": "664f265c-e451-455b-ba15-65fff20df142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "664f265c-e451-455b-ba15-65fff20df142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e0e5ff9-7f5d-44dc-81d6-7d26cf0068bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}