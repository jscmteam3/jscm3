{
    "id": "30fdbb45-f4eb-45dd-b681-5995342cef34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4af9c8cd-0324-4fd9-abd0-9e8542c0785d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30fdbb45-f4eb-45dd-b681-5995342cef34",
            "compositeImage": {
                "id": "a39fb94d-d615-4070-a701-28caab1e8d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af9c8cd-0324-4fd9-abd0-9e8542c0785d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057220c8-59aa-4492-8769-367bcddcb982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af9c8cd-0324-4fd9-abd0-9e8542c0785d",
                    "LayerId": "6874ee5f-c8b3-446e-b841-fce439a5f131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6874ee5f-c8b3-446e-b841-fce439a5f131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30fdbb45-f4eb-45dd-b681-5995342cef34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}