{
    "id": "df91c5f3-5b92-40cb-8bb7-7276e26d9db0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemBlueSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50f4e02d-8ce7-4753-a953-cee90391f082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df91c5f3-5b92-40cb-8bb7-7276e26d9db0",
            "compositeImage": {
                "id": "97b90c7a-2800-4221-bbf9-cb58facf1a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f4e02d-8ce7-4753-a953-cee90391f082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66cdc65-d42a-472c-82e6-4db2cc36bca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f4e02d-8ce7-4753-a953-cee90391f082",
                    "LayerId": "b77a9753-6965-4309-91e5-ff354bcbf00b"
                },
                {
                    "id": "9aecc585-dcca-4978-809f-c38a4f4757d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f4e02d-8ce7-4753-a953-cee90391f082",
                    "LayerId": "fccd3724-0e20-4873-8ac9-5bb307fe53dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fccd3724-0e20-4873-8ac9-5bb307fe53dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df91c5f3-5b92-40cb-8bb7-7276e26d9db0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b77a9753-6965-4309-91e5-ff354bcbf00b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df91c5f3-5b92-40cb-8bb7-7276e26d9db0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}