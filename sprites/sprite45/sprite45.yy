{
    "id": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite45",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 40,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d243a62-42b2-49f3-ac23-1b71736f01d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "46fd3baf-5988-416a-8b2b-146c1ed524fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d243a62-42b2-49f3-ac23-1b71736f01d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "756d710c-f1a7-4e9b-871d-6ef8cd570841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d243a62-42b2-49f3-ac23-1b71736f01d2",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "79331a49-6c6c-4c43-9c08-4e278fb09b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "da9eb638-4011-40e1-8106-65f12d1ed1e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79331a49-6c6c-4c43-9c08-4e278fb09b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cea3c5d-74d4-48f2-a301-20f171393ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79331a49-6c6c-4c43-9c08-4e278fb09b75",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "ea849242-e1a9-4b74-be70-cbe8ce0bb68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "0066622a-610a-4d6a-9077-2dab9975d223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea849242-e1a9-4b74-be70-cbe8ce0bb68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "711dfd64-a1e5-4e47-8e87-5ce33aebdffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea849242-e1a9-4b74-be70-cbe8ce0bb68b",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "2817fcce-605d-4f67-b232-fce39c11bb5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "71b89235-ff27-4beb-8aa9-be492e464e9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2817fcce-605d-4f67-b232-fce39c11bb5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a8e7e5-3451-4ce7-b34a-e73d679d8f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2817fcce-605d-4f67-b232-fce39c11bb5d",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "0c0dce02-0fbc-4c8d-9c9b-8ace3f391d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "68ab3faa-848a-4ee9-852a-faa36b7d422f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0dce02-0fbc-4c8d-9c9b-8ace3f391d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13800b5-936c-4bde-8ff6-1e5792df37a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0dce02-0fbc-4c8d-9c9b-8ace3f391d86",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "c3243427-868a-45be-b417-7da39090d153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "49d02992-125e-4d31-902b-ac74d2bbde4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3243427-868a-45be-b417-7da39090d153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f55b07-1b31-4b36-8837-f8c4eb87bafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3243427-868a-45be-b417-7da39090d153",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "e97bfd75-c6c2-4c08-91b1-b18d630591c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "60a638ed-10ae-4675-94c7-0a59e9805f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97bfd75-c6c2-4c08-91b1-b18d630591c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dbb3c47-e37f-40c9-9fb4-432ab42e6078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97bfd75-c6c2-4c08-91b1-b18d630591c7",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "f0be7515-db63-47c7-8024-4978937e5426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "8750d1a1-bdec-48a3-9187-54c8d59a9ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0be7515-db63-47c7-8024-4978937e5426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803dc644-1954-48e7-8ce8-e2fefa9c91f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0be7515-db63-47c7-8024-4978937e5426",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "be59e998-3cc9-4804-9812-7c9c660f9458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "69e4db52-48bc-414c-9a50-6c850341d8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be59e998-3cc9-4804-9812-7c9c660f9458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6447e6f-22af-4257-a743-61596132e629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be59e998-3cc9-4804-9812-7c9c660f9458",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "9ddc7a41-fe41-4a08-9c6b-9c4ab2e009c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "fe7419ff-c8b3-4105-9cb5-c8ded7457675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddc7a41-fe41-4a08-9c6b-9c4ab2e009c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2e7fd8-0b6e-4368-a9c2-4cfa46f29d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddc7a41-fe41-4a08-9c6b-9c4ab2e009c2",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "57089955-775c-479b-8ded-1fe84d8270eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "1c5be771-40ec-4576-a02c-aac2cd99e9e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57089955-775c-479b-8ded-1fe84d8270eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3abe5d19-7847-4b8b-8e01-873f3177652f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57089955-775c-479b-8ded-1fe84d8270eb",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        },
        {
            "id": "9c868d82-12d5-4f2d-b541-9252d5e3e124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "compositeImage": {
                "id": "11890ee8-e47f-48fe-abec-3e1802339c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c868d82-12d5-4f2d-b541-9252d5e3e124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c5cd78-c5da-42e4-9acb-5a458ceea987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c868d82-12d5-4f2d-b541-9252d5e3e124",
                    "LayerId": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4e7fd5a1-4a42-4a1b-bc22-0a6ca3a8c806",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa6378ff-8c65-4270-b1a0-b245304d8caf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}