{
    "id": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swordModeLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 10,
    "bbox_right": 47,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2ab6cfc-987b-4998-8fda-eb1876f6a251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
            "compositeImage": {
                "id": "25bd93b0-830c-48fb-8bf1-2e8bfb5f1677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ab6cfc-987b-4998-8fda-eb1876f6a251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f29db792-edf8-4d20-9046-fe665dcfe4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ab6cfc-987b-4998-8fda-eb1876f6a251",
                    "LayerId": "ea9b66ed-595d-4e48-b233-140a1cc133be"
                },
                {
                    "id": "d460c9cf-3895-4202-bf11-195632884edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ab6cfc-987b-4998-8fda-eb1876f6a251",
                    "LayerId": "4b6bda9c-c7df-40a8-9eb3-a761aaeaba7c"
                }
            ]
        },
        {
            "id": "54464de9-9fd5-4c0a-877b-d5d6ed6383d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
            "compositeImage": {
                "id": "f7afa498-48bf-4482-965a-8d7e0c2b3421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54464de9-9fd5-4c0a-877b-d5d6ed6383d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09cdcffd-73fa-4fa9-9bab-722e4e329bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54464de9-9fd5-4c0a-877b-d5d6ed6383d2",
                    "LayerId": "ea9b66ed-595d-4e48-b233-140a1cc133be"
                },
                {
                    "id": "045fff7e-b032-4931-9446-04ac35744f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54464de9-9fd5-4c0a-877b-d5d6ed6383d2",
                    "LayerId": "4b6bda9c-c7df-40a8-9eb3-a761aaeaba7c"
                }
            ]
        },
        {
            "id": "81d768ab-8e58-4a49-8c32-efb728c9eed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
            "compositeImage": {
                "id": "d12a116d-9626-410f-b4d4-7d2e4cefb023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d768ab-8e58-4a49-8c32-efb728c9eed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7722ac9c-fcb4-4251-b164-7c04a017c9d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d768ab-8e58-4a49-8c32-efb728c9eed8",
                    "LayerId": "ea9b66ed-595d-4e48-b233-140a1cc133be"
                },
                {
                    "id": "25b42c9b-4742-4ff6-bfe6-697e72e630db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d768ab-8e58-4a49-8c32-efb728c9eed8",
                    "LayerId": "4b6bda9c-c7df-40a8-9eb3-a761aaeaba7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ea9b66ed-595d-4e48-b233-140a1cc133be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4b6bda9c-c7df-40a8-9eb3-a761aaeaba7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "379564ed-98bc-4085-adb2-afbd7e1a39b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}