{
    "id": "15889ecc-4b65-4107-a1a7-c53be453c3d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_craftTable",
    "eventList": [
        {
            "id": "2a877611-f387-4cde-97c0-3001a736aed1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15889ecc-4b65-4107-a1a7-c53be453c3d7"
        },
        {
            "id": "e960b7d0-0063-4973-97cb-9d2b98615574",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "15889ecc-4b65-4107-a1a7-c53be453c3d7"
        },
        {
            "id": "d5918ce5-ddd7-4169-a0f2-dba8239e45c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "15889ecc-4b65-4107-a1a7-c53be453c3d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90efae07-36a9-44df-8a23-b35711810843",
    "visible": true
}