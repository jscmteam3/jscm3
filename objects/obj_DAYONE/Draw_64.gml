/// @description Insert description here
// You can write your code in this editor
var gui_width = display_get_gui_width()
var gui_height = display_get_gui_height()
var c = c_black

if (mission == step.WAKEUP){

	draw_sprite_ext(spr_textBox, 0, gui_width/2, gui_height - box_height - 100, 1, 1, 0, c_white, 1);
	var xx = gui_width/2;
	var yy = gui_height - box_height - 100;
	//draw text
	draw_set_font(fnt_text_24);
	
	if(counter < string_length(text[page_text])){
		counter++;
		audio_play_sound(snd_typingtext, 10, false);
	}
	var substr= string_copy(text[page_text], 1, counter);
	draw_text_ext_color(xx, yy + 15, substr,text_height, 450 , c,c,c,c,1); 
 
}
if (mission == step.PICKUP){
	draw_set_font(fnt_text_12)
	draw_text_color(gui_width/2, 30, notice[0],c_red,c_red,c_red,c_red,1);
	
	if (room != rm_D107)
		mission = step.GETOUT;
}
if (mission == step.GETOUT){
	draw_text_color(gui_width/2, 30, notice[1],c_red,c_red,c_red,c_red,1);
	
}