{
    "id": "4136ad13-610d-4564-b9b5-8253bd292fee",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_smalldigits",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7b16ea4e-636a-46a6-b76e-7e68cb72d8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 68,
                "y": 114
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d1f29df2-9f2f-406d-8cc9-6eb9ebea8eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 111,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b297992c-783f-49c4-84e5-ddfa89f318a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 44,
                "y": 114
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1549e061-5789-4eea-aab6-057b2be86b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3a8c5870-3820-4641-b0ea-7c6e013d95a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 60,
                "y": 30
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9a5832a1-87f9-4bcf-b9de-17b090dca320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5cf3d3ea-1170-4317-985e-f1d0f10fa254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 74,
                "y": 30
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0f0aa47a-9dd7-43a2-a052-f6ba4050b647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 106,
                "y": 114
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5f2ad4ab-b7f9-462e-b973-ca3a15e3aa80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 52,
                "y": 114
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ec1aff62-4257-4716-a0b4-45ef08cf622d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 28,
                "y": 114
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8caf9e3d-d721-49fe-a60f-3a1fcd93d3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "eee416b5-5322-4f40-af2d-b2653d77dfdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "45b03ff2-d662-483e-8f78-539809fa42d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 89,
                "y": 114
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a95bfd4d-0a8b-45b5-9098-1b29ca88d659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "57ef7bf0-1ce4-4e78-b73c-68ef80846997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 116,
                "y": 114
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e616ef68-7c2d-4dd3-9ad6-527c589a1182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1d94d382-bc50-40b2-85b7-522dea3b9121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6c197405-f693-4d15-b09f-dbc3cfa9fb63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 114
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3904739b-b57d-40d0-acc5-fcb7be8cbe5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 141,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "edc34dda-2655-486c-8e7f-db5b67fc4b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 189,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a8d2d9ef-032c-4e4d-b2e6-875ef5b077c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 130,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "87fb40bd-2219-459f-b9d7-94e07b7605a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 153,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1276fa37-eaa0-46a6-9c3b-d81967702657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 165,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fb3b3187-6bdf-4929-affc-6f43fccc1edf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 172,
                "y": 30
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d70bd33d-ce11-464d-96fa-a1df75a21092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 201,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9567168a-3af3-4223-b398-88a2f258970e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 237,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5723fe75-1f9e-490b-b523-7557df676100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 101,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c77bbeab-81ab-4028-9a96-5531d67db0f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 95,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e8cb7f0f-fc3d-4c59-ad28-3d2654148e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 60,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "862e8efa-1caf-4861-a6a4-86af8188a2c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b8192515-c533-40b1-a244-d05650c5deaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 229,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "657b3cd3-9f2c-4eef-9180-59925e91ef7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 145,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0d9f2780-5470-429d-964e-3b9b505f4f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6656f9f0-c309-4899-a5fc-e9485bcc0a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 46,
                "y": 30
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9c921764-9f6c-4415-acee-e4e72a430891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 58
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f9177955-9ae8-48f0-8043-d598b8b9ab07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 158,
                "y": 30
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4d0a55fa-efda-4367-af54-8a2dff8dae25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 116,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "15f0f317-3885-4e75-b232-027cac559969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d4c17818-94df-4b56-870d-fd4ab3cca0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1d30bf97-53ed-4bd5-84e5-4430a89102e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "84d6459d-90c2-4310-9f5d-20262c15dfa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f822a1ea-9fcd-4a27-a914-2a2d005807e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 213,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "21b57920-757e-4301-b559-1a5003c3d122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "683aecbe-d946-4d93-9c4a-6b31042941cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 225,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7484502e-e507-4f51-91a6-678d0db49da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 41,
                "y": 58
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b5e4bf48-58cf-4dcd-a298-3be6f68d10f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f6c0dbee-ce3a-4d9d-be7c-dd32e314ff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bbc1fe48-6569-43f5-8425-18bbb8b9c309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5194529a-5d7b-4219-9588-ab123466cf00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 177,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6c1c88ff-804a-4d3c-b9dc-22a72e1854f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ee599e2d-fe9e-48e3-91fd-d6d48b5c3d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 212,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "863499e8-b9ff-4680-925b-35e10a2d8c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 144,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "552307e6-58bb-4bef-90c9-43953d14af9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "072fd161-6c70-400e-ab1f-cf74e33b78db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 32,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "16efa52b-1729-4d83-a487-6778874a710f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ba14c223-2d7a-4381-afcb-801ea7ce2e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "95cfdc31-fec3-469b-b1b2-fb9478620703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f6546a4e-a0be-429a-a145-f30a5cc49cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 102,
                "y": 30
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3ea80a26-9885-407f-80f0-1ba788fcce58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4e9eda07-3005-4e4f-bbc8-380945060403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 20,
                "y": 114
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4d6d3365-a9a4-4b38-bf55-4c1013f9cad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 86
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "54eff146-bea6-4339-bbf2-fed9f9131c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 36,
                "y": 114
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "80909176-0fb3-4b37-9521-48f26453a92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 86
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4f486c14-c464-49d8-ae76-aff22f24c575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "41790ad7-f1f9-401a-a3be-81aabf3743dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 5,
                "x": 76,
                "y": 114
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "025466c6-4de5-464e-944b-04376a95740b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 225,
                "y": 58
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3e1f0e27-dec3-4003-9d67-a0d19c266a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 93,
                "y": 58
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7486fc4d-a5ed-4112-8d50-08ea617faf34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 86
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e04648b7-de1d-4fe1-b6a9-16cdac9876ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 199,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8bfbade3-1fc1-4d8e-8221-3cde69a04fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 186,
                "y": 30
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fd10b396-d12a-4f52-8b46-b6811a7bfed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 86
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8fc99788-ec49-422f-b208-4851b57558ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 105,
                "y": 58
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f57090b5-1897-40dc-824b-992a689692ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "256627b8-4bb8-47fd-84be-62ee159f56a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 114
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ec428ea3-35c5-40ba-922e-9e63fd056295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 210,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8a2f389c-d3d1-43f6-be03-d24b3658156a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a57dccd0-f61b-47a9-9b47-3015664a20db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 121,
                "y": 114
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "603f99b5-994e-4a96-b8a1-090518d8c3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dfa11433-2383-44ca-b069-92915dc7daf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 86
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "276ab8ab-7c09-462f-a1e2-89ffdf48743f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cd0d0264-a67a-4a41-9355-459e83a70f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 86
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "43a3111b-313d-4384-a619-61f61924215c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 86
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a498d1aa-3bae-4700-ac94-9b815ae8e8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "38979d46-db65-43c4-abfe-4a9fd1c2adb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "12a15cf1-118e-494e-a48b-adc6b281059f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e468058b-bf33-4bba-83ea-9487e9292850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 86
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "14d0b18d-6765-4900-942b-fbc120047d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 86
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "62bb3ac8-c741-4a8e-8a0a-00928e49d12e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6b45839a-9c58-4c62-a2cd-f5c4bb1af6db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 238,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4dc588ef-d83f-4d16-acba-b4b0dcb10c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 15,
                "y": 58
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4d3dbcb5-6bc0-4b5c-a305-f126dca1bbf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 86
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4798d20b-e88d-47f3-9042-f38e04b4452a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 220,
                "y": 86
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3a907fd2-c946-4c1e-9721-92c9e8f7ce8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 126,
                "y": 114
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1530d5d2-62c2-42b6-b8d9-084a9e4d818c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 238,
                "y": 86
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7317ad62-67d5-4557-9d93-68519bf0f864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 58
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}