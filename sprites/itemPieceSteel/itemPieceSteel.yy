{
    "id": "fb12ca6c-4529-4f34-8d5e-49bd43805188",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemPieceSteel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 12,
    "bbox_right": 54,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0855f558-0c79-4d2a-91f7-7beb36a3b858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb12ca6c-4529-4f34-8d5e-49bd43805188",
            "compositeImage": {
                "id": "47edfce7-110f-4511-98b1-ab1bed657861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0855f558-0c79-4d2a-91f7-7beb36a3b858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58780671-8a6a-454e-974e-07dffc43565e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0855f558-0c79-4d2a-91f7-7beb36a3b858",
                    "LayerId": "12144404-e395-4c37-8cc0-6e98a515c00c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "12144404-e395-4c37-8cc0-6e98a515c00c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb12ca6c-4529-4f34-8d5e-49bd43805188",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}