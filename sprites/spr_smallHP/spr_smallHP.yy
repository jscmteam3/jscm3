{
    "id": "6eec7c11-3d56-4c86-a8a8-eeeb465eff81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smallHP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 32,
    "bbox_right": 59,
    "bbox_top": 53,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a48afbc9-971b-407a-8abf-a6957700f9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6eec7c11-3d56-4c86-a8a8-eeeb465eff81",
            "compositeImage": {
                "id": "dd000851-10cf-4992-ac32-256ca551d7e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48afbc9-971b-407a-8abf-a6957700f9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07444134-652f-4285-868c-9d02767e1ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48afbc9-971b-407a-8abf-a6957700f9c8",
                    "LayerId": "a965350e-bd16-4c28-beac-ef31ff4dc574"
                },
                {
                    "id": "e4fe9f65-fa9c-4f27-87f2-5a7e99eb181c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48afbc9-971b-407a-8abf-a6957700f9c8",
                    "LayerId": "8c4dd8ed-a8a5-42cb-94d5-f1dcfdabc0b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a965350e-bd16-4c28-beac-ef31ff4dc574",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6eec7c11-3d56-4c86-a8a8-eeeb465eff81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8c4dd8ed-a8a5-42cb-94d5-f1dcfdabc0b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6eec7c11-3d56-4c86-a8a8-eeeb465eff81",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}