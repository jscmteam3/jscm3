{
    "id": "b16759c5-d1e9-4ec0-a4e0-08c03f2914f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_banner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dd376e5-746e-459b-ad42-e7fb77cabd4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b16759c5-d1e9-4ec0-a4e0-08c03f2914f7",
            "compositeImage": {
                "id": "373a7667-533b-4b0b-a27a-d7bf770da5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd376e5-746e-459b-ad42-e7fb77cabd4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c071f1-0433-4396-8546-78ef4b4cc312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd376e5-746e-459b-ad42-e7fb77cabd4b",
                    "LayerId": "20a7ec75-081d-4e15-9dff-9ff65e8c4566"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "20a7ec75-081d-4e15-9dff-9ff65e8c4566",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b16759c5-d1e9-4ec0-a4e0-08c03f2914f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}