{
    "id": "63118e42-3d0e-440f-b26b-447e909758f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveRight1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 16,
    "bbox_right": 44,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "841a3c93-ee83-443f-b7ff-6123b2562046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f167e9-4d75-4c74-af26-d2edc8891837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7ecb4c-3be6-4a10-8a2c-d02920436774",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                }
            ]
        },
        {
            "id": "d3a97e76-7a21-4771-b9d1-b32045a03726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "0af09777-c576-4d31-a298-1f68d8c9ea50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a97e76-7a21-4771-b9d1-b32045a03726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d8ee1d-09a0-4f89-b119-75af8799f0f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a97e76-7a21-4771-b9d1-b32045a03726",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                }
            ]
        },
        {
            "id": "47612bc1-8c2d-4073-acff-fcc9b20bb011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "compositeImage": {
                "id": "a7fc91c0-8315-4561-8a05-b2907fe833c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47612bc1-8c2d-4073-acff-fcc9b20bb011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60464863-d7c9-488f-85f2-b945bac5a079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47612bc1-8c2d-4073-acff-fcc9b20bb011",
                    "LayerId": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e411e274-1a9f-44a3-b0ad-df7f639dc0e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63118e42-3d0e-440f-b26b-447e909758f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}