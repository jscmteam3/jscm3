{
    "id": "8d88583b-69e7-47f9-aaeb-e7f6510291cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemWood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d97bcb9-4b4e-4e27-a353-b7074da7e3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d88583b-69e7-47f9-aaeb-e7f6510291cf",
            "compositeImage": {
                "id": "69224b20-e00a-4609-b47f-8e896989f4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d97bcb9-4b4e-4e27-a353-b7074da7e3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806db4b6-35e8-4dcd-9114-2ae80ef49548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d97bcb9-4b4e-4e27-a353-b7074da7e3d5",
                    "LayerId": "51e4274e-5c7a-458d-bce5-1e10b75be447"
                },
                {
                    "id": "3c7bb631-fa61-4c6c-bf02-603868d9046a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d97bcb9-4b4e-4e27-a353-b7074da7e3d5",
                    "LayerId": "9dfac719-d4d4-4299-b906-48be24e017b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "51e4274e-5c7a-458d-bce5-1e10b75be447",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d88583b-69e7-47f9-aaeb-e7f6510291cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9dfac719-d4d4-4299-b906-48be24e017b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d88583b-69e7-47f9-aaeb-e7f6510291cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 16
}