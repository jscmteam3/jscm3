{
    "id": "77c0f0f7-87af-420d-bcff-ea4c2417e447",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBrokenT",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "752409de-0d59-4711-97ba-11b777800763",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "50c672e8-b7c2-4daf-81c7-ff52d916e59c",
    "visible": true
}