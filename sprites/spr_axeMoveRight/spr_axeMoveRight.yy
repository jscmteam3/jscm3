{
    "id": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_axeMoveRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 16,
    "bbox_right": 57,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a2a4884-4c70-4858-a133-28affb81388f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
            "compositeImage": {
                "id": "e11d66b1-23ee-49b5-ba25-1992d578d863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2a4884-4c70-4858-a133-28affb81388f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae761206-951d-434d-9cd3-a14a22ec397a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2a4884-4c70-4858-a133-28affb81388f",
                    "LayerId": "97f66ade-4f70-4487-87fe-8c42b2a17f37"
                },
                {
                    "id": "1366f922-a07a-48ea-86e3-65c96b5e7a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2a4884-4c70-4858-a133-28affb81388f",
                    "LayerId": "5d0381cc-ffee-4c74-9f89-639c8255ac56"
                }
            ]
        },
        {
            "id": "cf3c3e22-4ec6-43c0-87cf-46517f4d2eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
            "compositeImage": {
                "id": "83227525-9844-419f-a9de-34ba162b8bb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3c3e22-4ec6-43c0-87cf-46517f4d2eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a053c38-5d91-4b82-b6b2-c91790bef8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3c3e22-4ec6-43c0-87cf-46517f4d2eae",
                    "LayerId": "97f66ade-4f70-4487-87fe-8c42b2a17f37"
                },
                {
                    "id": "e02e0f4a-d883-443e-aac9-e9974511cf48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3c3e22-4ec6-43c0-87cf-46517f4d2eae",
                    "LayerId": "5d0381cc-ffee-4c74-9f89-639c8255ac56"
                }
            ]
        },
        {
            "id": "488f99b7-e919-45dd-95d5-ffb1e9aa7e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
            "compositeImage": {
                "id": "fe00c2b6-4f20-44aa-b2d4-71f7f4e004f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488f99b7-e919-45dd-95d5-ffb1e9aa7e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56754e02-5b08-4956-bf3b-d25800f0cd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488f99b7-e919-45dd-95d5-ffb1e9aa7e86",
                    "LayerId": "97f66ade-4f70-4487-87fe-8c42b2a17f37"
                },
                {
                    "id": "be425a4e-f162-4401-803f-583b052b319a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488f99b7-e919-45dd-95d5-ffb1e9aa7e86",
                    "LayerId": "5d0381cc-ffee-4c74-9f89-639c8255ac56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "97f66ade-4f70-4487-87fe-8c42b2a17f37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5d0381cc-ffee-4c74-9f89-639c8255ac56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13a9d2c6-67ae-4ec9-b3f9-24a483dd84df",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}