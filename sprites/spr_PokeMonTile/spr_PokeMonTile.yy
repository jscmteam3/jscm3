{
    "id": "57314bb8-f506-4606-acab-f41aaef56939",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PokeMonTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2271,
    "bbox_left": 0,
    "bbox_right": 1502,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0865a69f-0941-4bc9-9500-0c25440aee74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57314bb8-f506-4606-acab-f41aaef56939",
            "compositeImage": {
                "id": "b29d0f3b-5a0d-4183-9cec-fc5f35033588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0865a69f-0941-4bc9-9500-0c25440aee74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a40d6d-d87c-48a9-b1ea-258c85fd5cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0865a69f-0941-4bc9-9500-0c25440aee74",
                    "LayerId": "14f53d61-2bd0-41a3-99ef-17583849abb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2519,
    "layers": [
        {
            "id": "14f53d61-2bd0-41a3-99ef-17583849abb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57314bb8-f506-4606-acab-f41aaef56939",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1504,
    "xorig": 0,
    "yorig": 0
}