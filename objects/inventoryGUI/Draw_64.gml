/// @description Insert description here
// You can write your code in this editor
var scale = 2;
draw_sprite_ext(spr_inventory_items, 0, x, y, scale, scale, 0, c_white, 1);
var c = c_black;
draw_text_color(
	x, 
	y - (y_offset-7)*scale,"Name: Player",c,c,c,c,1
	);
//------------------------------- Inventory
#region	//inventory and mouse position
var hh = ds_grid_height(playerInventory);
var inv_slot = 0;
if (ds_grid_get(playerInventory, 0, 0)!= 0)	{
		for (var i = 0; i < ds_grid_height(playerInventory)/11; i++){
			for (var j = 0; j < 11; j++){
				ds_list_add(list_mousex, (x - x_offset*scale + 4*scale) + (CELLSIZE*scale + 4*scale)*j);	
				if (inv_slot != spTemp){	
				
					draw_sprite_ext(spr_backInventory, 0, (x - x_offset*scale + 4*scale) + (CELLSIZE*scale + 4*scale)*j,
						(y - y_offset*scale + 30*scale) + (CELLSIZE*scale + 4*scale)*i, scale, scale, 0, c_white, 1
					);				
					draw_sprite_ext(playerInventory[# 3,inv_slot], 0, (x - x_offset*scale +4*scale) + (CELLSIZE*scale + 4*scale)*j,
						(y - y_offset*scale + 30*scale) + (CELLSIZE*scale + 4*scale)*i, scale, scale, 0, c_white, 1
					);
					draw_set_font(fnt_smalldigits);
					
					draw_text_color((x - x_offset*scale + 4*scale) + (CELLSIZE*scale + 4*scale)*j + 48,
						(y - y_offset*scale + 30*scale) + (CELLSIZE*scale + 4*scale)*i + 44, playerInventory[# 1,inv_slot], c,c,c,c,1
					);	
			
				}inv_slot ++;
				if(inv_slot == hh) break;
			}
			ds_list_add(list_mousey, (y - y_offset*scale + 30*scale) + (CELLSIZE*scale + 4*scale)*i);
		}
}

xmouse = device_mouse_x_to_gui(0);
ymouse = device_mouse_y_to_gui(0);

var dx,dy;
var flag = 0;
var i = 0, j= 0;
var test = ds_list_size(list_mousey);
var limit_x = ds_list_size(list_mousex);
if (!checkBusy){
	
		for (j = 0; j < ds_list_size(list_mousey); j++){
			if (ymouse > list_mousey[| j] && ymouse < list_mousey[| j] + 64){
				if(j >= test-1) limit_x = hh - 11*(test-1); 
				for (i = 0; i < limit_x; i++){
						if (xmouse > list_mousex[| i] && xmouse < list_mousex[| i] + 64)
						{		
								draw_sprite_ext(spr_slotBG, 0, list_mousex[| i], list_mousey[| j], scale, scale, 0, c_white, 1);
									dx = list_mousex[| i] + 16;
									dy = list_mousey[| j] + 16;	
								
								if (!instance_exists(itemGUI) && !mouse_check_button_pressed(mb_right)){
									instance_create_depth(dx, dy, -2, itemGUI);		
										itemGUI.inv_index = j >= (test-1) ? j*11+i : i;
								}
								if (mouse_check_button_pressed(mb_right)) spRightClick = 1;					
									flag = 1;							
									break;
					} 
				} if (flag == 1) break; 
			}
		}
}

if (!flag) instance_destroy(itemGUI)
//-------------DRAG ITEM
if (mouse_check_button_pressed(mb_left) && flag == 1){
	audio_play_sound(chose, 10, false);
	spTemp = j >= (test-1) ? 11*j+i : i;
	checkBusy = true;
}
//---------DRAG ITEM
if (spTemp != -1)
	draw_sprite_ext(playerInventory[# 3,spTemp], 0, xmouse, ymouse, scale, scale, 0, c_white, 1);

//----------------DROP ITEM
if (spTemp != -1 && mouse_check_button_pressed(mb_right)){
	
	if (xmouse < x - scale*x_offset || xmouse > x + scale*x_offset || ymouse > y + scale*y_offset || ymouse< y - scale*y_offset){
			checkBusy = false;	
			audio_play_sound(chose, 10, false);
			var instID = instance_create_layer(objPlayer.x + 10, objPlayer.y, "Inventory", objPickup);
			with (instID){
				instID.myItemName =  ds_grid_get(playerInventory, 0, other.spTemp);
				instID.myItemAmount = ds_grid_get(playerInventory, 1, other.spTemp);
				instID.myItemSprite =  ds_grid_get(playerInventory, 3, other.spTemp);
				instID.myItemType =  ds_grid_get(playerInventory, 2, other.spTemp);
				instID.myItemScript =  ds_grid_get(playerInventory, 4, other.spTemp);
			}
			scr_deleteAnElement(spTemp, playerInventory);
		}
	checkBusy = false;
	spTemp = -1;
}


if (spRightClick && !instance_exists(objRightClick)){
		instance_destroy(itemGUI);
		checkBusy = true;
		itemIndex = j >= (test-1) ? 11*j+i : i;;
		instance_create_depth(dx, dy, -4, objRightClick);
		audio_play_sound(chose, 10, false);
		spRightClick = 0;
}
if (ymouse >= obj_itembar.y + 4.5 && ymouse <= obj_itembar.y + 54 && spTemp!=-1){
	var slot = 0; repeat(5){
		with (obj_itembar){
			if (other.xmouse >= x + 26 + 66*slot && other.xmouse <= x + 26 + 66*slot + 31){
				if (mouse_check_button_pressed(mb_left)){
					audio_play_sound(chose, 10, false);
					gridItemBar[# 3, slot] = 1;
					slotIndex = slot;
					sprIndex = other.spTemp;	
					inventoryGUI.spTemp = -1;
					checkBusy = false;
				}
			}	
		}
			slot ++;		
	}
}


ds_list_clear(list_mousex);
ds_list_clear(list_mousey);
#endregion




