{
    "id": "5ca0e197-7f3c-4110-9d94-5f57a3901b55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backInventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a27163d2-cfa9-4141-b40e-a096b2b4883a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ca0e197-7f3c-4110-9d94-5f57a3901b55",
            "compositeImage": {
                "id": "1d3dc131-8113-472e-a0cc-88d245086a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a27163d2-cfa9-4141-b40e-a096b2b4883a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4850be7-e1b7-4260-821b-0b83e96332ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a27163d2-cfa9-4141-b40e-a096b2b4883a",
                    "LayerId": "ddb399e6-db40-4d39-a038-8951652d8501"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ddb399e6-db40-4d39-a038-8951652d8501",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ca0e197-7f3c-4110-9d94-5f57a3901b55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}