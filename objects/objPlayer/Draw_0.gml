/// @description
draw_self();
if (game.Condition == ATTACK || game.Condition == AXEATTACK || game.Condition == FRAMEATTACK){
		var i;
		checkCollision();
		image_speed = 1;
		sprite_index = View2[Direction,game.Condition];
		draw_set_alpha(1);
		//draw_sprite(spr_SlashLeft,-1,x - 15, y);
		if(!instance_exists(Slash)){
	
		if(game.Condition == ATTACK){
			audio_play_sound(snd_slash, 10, false);
			var slashID = instance_create_layer(x , y,"Instances", Slash);
		}
		else if(game.Condition == AXEATTACK){
			audio_play_sound(sound10, 10, false);
			var slashID = instance_create_layer(x , y,"Instances", SlashAxe);
		}
		else if(game.Condition == FRAMEATTACK){
			audio_play_sound(sound10, 10, false);
			var slashID = instance_create_layer(x , y,"Instances", SlashFrame);
			if (image_index > (image_number-2) && persistentData.playerShield>0){
				persistentData.playerShield -= 0.6;
				instance_create_layer(x , y,"Instances", FrameBullets);
				audio_play_sound(attac_player, 10, false);
			}
		}
	}
}
