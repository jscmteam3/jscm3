{
    "id": "ae3b371a-df18-42d2-bea3-1d53cdb776a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 5,
    "bbox_right": 28,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbccef68-f251-45f8-8b34-450a04441d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3b371a-df18-42d2-bea3-1d53cdb776a2",
            "compositeImage": {
                "id": "30e0a6f2-f3ac-4518-856a-d1ff74e862e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbccef68-f251-45f8-8b34-450a04441d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e30e95b-cfee-4b14-bae8-d7565356e9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbccef68-f251-45f8-8b34-450a04441d7d",
                    "LayerId": "ac5aa916-c82e-4697-9c24-b9bf426f3830"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac5aa916-c82e-4697-9c24-b9bf426f3830",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae3b371a-df18-42d2-bea3-1d53cdb776a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}