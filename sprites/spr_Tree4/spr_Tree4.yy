{
    "id": "8f069636-494a-4e23-937a-e54978eca9ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 43,
    "bbox_right": 84,
    "bbox_top": 125,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd447ee0-654f-4e7d-94cc-04bbd14e156a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f069636-494a-4e23-937a-e54978eca9ef",
            "compositeImage": {
                "id": "c9f44c4b-c8ee-4b5a-bd48-1efefda945c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd447ee0-654f-4e7d-94cc-04bbd14e156a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a1890a-f108-4c5b-9c3e-bda270ac2f3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd447ee0-654f-4e7d-94cc-04bbd14e156a",
                    "LayerId": "f1e27a3b-8d06-4a89-90f6-3768c650787f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 162,
    "layers": [
        {
            "id": "f1e27a3b-8d06-4a89-90f6-3768c650787f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f069636-494a-4e23-937a-e54978eca9ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 136
}