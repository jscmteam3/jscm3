/// @description 
/*
if (!planting) exit;

var cs = cellSize;
var xx = (mx div cs);
var yy = (my div cs);
xx *= cs;
yy *= cs;

var c = c_red;

//Draw a rectangle red or green
draw_rectangle_color(xx ,yy , xx+cs, yy+cs,c, c, c, c,false);

//Draw crops to be planted
draw_set_alpha(1);
draw_sprite(spr_crops_picked, selectCrop, xx + cs/2, yy +cs/2);	
*/

if (!isUsingCrafting) exit;

var cs = cellSize;
var xx = (mx div cs);
var yy = (my div cs);
xx *= cs;
yy *= cs;
var c = c_green;

draw_rectangle_color(xx ,yy , xx+cs*2, yy+cs*2,c, c, c, c,false);

draw_set_alpha(0.5);
if (stuffID == obj_craftTable)
	draw_sprite(CraftingTable, 0, xx, yy);	
else if (stuffID == obj_kilnGUI)
	draw_sprite(CraftingStove, 0, xx, yy);	
	
