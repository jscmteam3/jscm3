{
    "id": "cd08c150-9ef8-4c70-8262-73c80376ce3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_redButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 3,
    "bbox_right": 36,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "035740e7-db3c-4411-84d2-549125f0d715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd08c150-9ef8-4c70-8262-73c80376ce3b",
            "compositeImage": {
                "id": "cc81a669-d1c2-44fd-b8ca-5e35d7e4f788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035740e7-db3c-4411-84d2-549125f0d715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40fc1c83-1388-4bb0-9812-f8ebe09fe855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035740e7-db3c-4411-84d2-549125f0d715",
                    "LayerId": "335ba13a-a60b-476b-b471-dcf67e878fc5"
                }
            ]
        },
        {
            "id": "7b5b1340-4a2e-4238-9eb6-b176107ae81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd08c150-9ef8-4c70-8262-73c80376ce3b",
            "compositeImage": {
                "id": "20ceba52-48be-41ad-84fc-41981d9a9a23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5b1340-4a2e-4238-9eb6-b176107ae81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f25e39fe-52dd-46f4-b285-d1c7cbbeaa02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5b1340-4a2e-4238-9eb6-b176107ae81a",
                    "LayerId": "335ba13a-a60b-476b-b471-dcf67e878fc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "335ba13a-a60b-476b-b471-dcf67e878fc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd08c150-9ef8-4c70-8262-73c80376ce3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}