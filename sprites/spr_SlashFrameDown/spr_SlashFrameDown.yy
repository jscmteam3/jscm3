{
    "id": "615624a6-f16a-4adf-82bf-e28fcbe98541",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashFrameDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "602e3946-262f-4718-b28b-681f32bc1f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615624a6-f16a-4adf-82bf-e28fcbe98541",
            "compositeImage": {
                "id": "8dbacb85-e097-4505-9e01-f94f8d4d7b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602e3946-262f-4718-b28b-681f32bc1f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9b5d39-1243-459d-ba7f-2aec92c1deb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602e3946-262f-4718-b28b-681f32bc1f6d",
                    "LayerId": "e0978d6a-e689-4109-b515-834ac764bc34"
                }
            ]
        },
        {
            "id": "84c9c741-668c-4b9c-a410-38d81448f44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615624a6-f16a-4adf-82bf-e28fcbe98541",
            "compositeImage": {
                "id": "a722f42a-a4a0-4d8f-91cb-30d29d856614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c9c741-668c-4b9c-a410-38d81448f44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e03aa5-2cee-41d6-a64e-65c70726d37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c9c741-668c-4b9c-a410-38d81448f44a",
                    "LayerId": "e0978d6a-e689-4109-b515-834ac764bc34"
                }
            ]
        },
        {
            "id": "1fbf3325-0163-485f-b4da-fa227af39648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615624a6-f16a-4adf-82bf-e28fcbe98541",
            "compositeImage": {
                "id": "d7515b31-43fc-4b3c-9823-6bf080512d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbf3325-0163-485f-b4da-fa227af39648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd70c73-4958-4dff-8c9b-c422332792ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbf3325-0163-485f-b4da-fa227af39648",
                    "LayerId": "e0978d6a-e689-4109-b515-834ac764bc34"
                }
            ]
        },
        {
            "id": "16f00af7-ff71-4807-ac96-e0bc9055bd23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615624a6-f16a-4adf-82bf-e28fcbe98541",
            "compositeImage": {
                "id": "28088023-8172-4f2e-8906-0c80ee85b354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f00af7-ff71-4807-ac96-e0bc9055bd23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1af3bad-b5bd-47b7-a9b6-f6897efb8515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f00af7-ff71-4807-ac96-e0bc9055bd23",
                    "LayerId": "e0978d6a-e689-4109-b515-834ac764bc34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "e0978d6a-e689-4109-b515-834ac764bc34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615624a6-f16a-4adf-82bf-e28fcbe98541",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}