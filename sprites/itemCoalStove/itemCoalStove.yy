{
    "id": "e937e311-3591-4ea2-96f6-97bc19f1a4f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemCoalStove",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28e01174-8f53-47b5-8321-980ba5559b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e937e311-3591-4ea2-96f6-97bc19f1a4f2",
            "compositeImage": {
                "id": "04f2e8b0-3503-4edc-abe3-a0d16bc1c7fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28e01174-8f53-47b5-8321-980ba5559b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8127cc-7158-4b6f-8f46-4c77072ad6f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28e01174-8f53-47b5-8321-980ba5559b3c",
                    "LayerId": "fc007377-9ae9-450c-8810-15991f4c2621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc007377-9ae9-450c-8810-15991f4c2621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e937e311-3591-4ea2-96f6-97bc19f1a4f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}