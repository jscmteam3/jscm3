{
    "id": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62186470-e4d7-4b29-a1b7-a95061d9f024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
            "compositeImage": {
                "id": "eadf2b51-3475-4a54-9bdf-a894ac120a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62186470-e4d7-4b29-a1b7-a95061d9f024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5329b343-2ada-4c5c-886c-49978579c256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62186470-e4d7-4b29-a1b7-a95061d9f024",
                    "LayerId": "2fb23d5d-5e3b-490d-ad20-4a667f904c55"
                }
            ]
        },
        {
            "id": "8cb64908-1d9f-4fcc-9499-c4c2b36e64ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
            "compositeImage": {
                "id": "b81a1d12-8253-4895-a475-06bf34b5734a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb64908-1d9f-4fcc-9499-c4c2b36e64ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6c0c54-3c2e-40f0-b19a-fa64fe0f4e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb64908-1d9f-4fcc-9499-c4c2b36e64ae",
                    "LayerId": "2fb23d5d-5e3b-490d-ad20-4a667f904c55"
                }
            ]
        },
        {
            "id": "cd0d1698-c9c7-4235-a20f-c276dfd98aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
            "compositeImage": {
                "id": "28860b5f-95da-4a1e-b0a0-ff1071af92f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0d1698-c9c7-4235-a20f-c276dfd98aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabc3d6b-c3b8-4acb-8c2c-9dd518941d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0d1698-c9c7-4235-a20f-c276dfd98aea",
                    "LayerId": "2fb23d5d-5e3b-490d-ad20-4a667f904c55"
                }
            ]
        },
        {
            "id": "3fb03f61-6d10-458c-9899-3ade63c3bb16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
            "compositeImage": {
                "id": "b1db6150-fdb8-4e49-a526-d976c7165253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb03f61-6d10-458c-9899-3ade63c3bb16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066e9eab-f7bc-439c-b34e-1bea210d0b54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb03f61-6d10-458c-9899-3ade63c3bb16",
                    "LayerId": "2fb23d5d-5e3b-490d-ad20-4a667f904c55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "2fb23d5d-5e3b-490d-ad20-4a667f904c55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3f46736-b9ca-4b45-9b06-8e76ad5cff8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}