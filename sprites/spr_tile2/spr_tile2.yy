{
    "id": "e16a8dad-5013-4797-ac03-7dbde827a167",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a94d19e1-dcf1-46b6-9a79-d9fdd2a5f646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e16a8dad-5013-4797-ac03-7dbde827a167",
            "compositeImage": {
                "id": "45454d31-6521-485a-95a6-9877ba53fe25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94d19e1-dcf1-46b6-9a79-d9fdd2a5f646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12a8c70-8410-428d-a6d3-6b76914f8d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94d19e1-dcf1-46b6-9a79-d9fdd2a5f646",
                    "LayerId": "30f8e88c-7637-47cd-8beb-e13c5cd4e6f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "30f8e88c-7637-47cd-8beb-e13c5cd4e6f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e16a8dad-5013-4797-ac03-7dbde827a167",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 0
}