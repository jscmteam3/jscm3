{
    "id": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenEatRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4a3d859-be86-4b07-94d5-30dd723d8c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
            "compositeImage": {
                "id": "34f8644e-69d2-4abd-a09e-ef0460b67260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a3d859-be86-4b07-94d5-30dd723d8c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c780b9-1433-4953-ae58-062b7c9d9191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a3d859-be86-4b07-94d5-30dd723d8c83",
                    "LayerId": "1ce38d14-9737-4b48-af18-4e3ddff55984"
                }
            ]
        },
        {
            "id": "f8a894f0-24db-40b0-a1e7-5a2173f14634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
            "compositeImage": {
                "id": "4a248e20-e90c-4b21-9547-ae3c10c7f7ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a894f0-24db-40b0-a1e7-5a2173f14634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6cd24e-fb8a-4aa8-a0b5-586af28767bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a894f0-24db-40b0-a1e7-5a2173f14634",
                    "LayerId": "1ce38d14-9737-4b48-af18-4e3ddff55984"
                }
            ]
        },
        {
            "id": "bd91836b-694a-47e4-a226-14ee3059f912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
            "compositeImage": {
                "id": "738a8554-977a-4e68-a9a6-c7a2b2d5740a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd91836b-694a-47e4-a226-14ee3059f912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951440a4-4d29-4643-aa17-bbe0de83f715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd91836b-694a-47e4-a226-14ee3059f912",
                    "LayerId": "1ce38d14-9737-4b48-af18-4e3ddff55984"
                }
            ]
        },
        {
            "id": "42a817c9-f480-49be-a46b-fec74d6722da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
            "compositeImage": {
                "id": "216c6f86-745d-4696-9eb7-a49a0af426b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a817c9-f480-49be-a46b-fec74d6722da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a288ac2-2e3a-40de-ba46-6f3af6b43eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a817c9-f480-49be-a46b-fec74d6722da",
                    "LayerId": "1ce38d14-9737-4b48-af18-4e3ddff55984"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ce38d14-9737-4b48-af18-4e3ddff55984",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73813fcd-f70d-4dce-b041-e7cf1d98c9f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}