{
    "id": "b9ba2edd-0c9d-4d77-97e8-cef6d0d73c48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31fdbe9b-1c81-4d13-acaf-449b1257f6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ba2edd-0c9d-4d77-97e8-cef6d0d73c48",
            "compositeImage": {
                "id": "17b673c4-dc05-4073-946e-8dd1312afa63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31fdbe9b-1c81-4d13-acaf-449b1257f6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "308edd70-6f84-48a7-92f8-4034bbb84a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31fdbe9b-1c81-4d13-acaf-449b1257f6f7",
                    "LayerId": "87e98de4-133b-46da-b9ee-73c8bb89a292"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "87e98de4-133b-46da-b9ee-73c8bb89a292",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9ba2edd-0c9d-4d77-97e8-cef6d0d73c48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}