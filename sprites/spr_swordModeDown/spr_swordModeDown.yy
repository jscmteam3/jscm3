{
    "id": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swordModeDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1886b905-fee2-45b9-8516-7a3a25a4abc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
            "compositeImage": {
                "id": "dd36bd9b-507e-41e9-a486-f6d7117ef553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1886b905-fee2-45b9-8516-7a3a25a4abc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a8f08a-ea2e-485b-9d50-a9d4d2bb6667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1886b905-fee2-45b9-8516-7a3a25a4abc8",
                    "LayerId": "53659f3c-f7f2-4c9b-a10b-c70451cd5c8e"
                },
                {
                    "id": "fa50de58-41ee-49df-a889-54ba157a746e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1886b905-fee2-45b9-8516-7a3a25a4abc8",
                    "LayerId": "9af8ea5f-603f-4d6c-b059-1e76be022faf"
                }
            ]
        },
        {
            "id": "2d0c5e3a-62e7-485e-8afd-96dc3ac187f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
            "compositeImage": {
                "id": "d04a97fd-38e5-4296-8eb2-7407af208781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d0c5e3a-62e7-485e-8afd-96dc3ac187f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98348dbe-74f3-44ed-90ea-c142c068fb94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0c5e3a-62e7-485e-8afd-96dc3ac187f5",
                    "LayerId": "53659f3c-f7f2-4c9b-a10b-c70451cd5c8e"
                },
                {
                    "id": "35bcdbc1-fb47-47f0-9e5e-d27a007a6f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d0c5e3a-62e7-485e-8afd-96dc3ac187f5",
                    "LayerId": "9af8ea5f-603f-4d6c-b059-1e76be022faf"
                }
            ]
        },
        {
            "id": "d2aa5bd0-0d8f-4efa-af53-1135b7b8333e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
            "compositeImage": {
                "id": "59301950-2429-48f6-bdf1-ca0d08a72d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2aa5bd0-0d8f-4efa-af53-1135b7b8333e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc5f37c-24be-4070-b8a8-08d60a2bc0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2aa5bd0-0d8f-4efa-af53-1135b7b8333e",
                    "LayerId": "53659f3c-f7f2-4c9b-a10b-c70451cd5c8e"
                },
                {
                    "id": "8bcb3287-b1fa-4019-9f88-3f4cf218e84f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2aa5bd0-0d8f-4efa-af53-1135b7b8333e",
                    "LayerId": "9af8ea5f-603f-4d6c-b059-1e76be022faf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "53659f3c-f7f2-4c9b-a10b-c70451cd5c8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9af8ea5f-603f-4d6c-b059-1e76be022faf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07ff4f0d-26bd-41b7-ad54-6cabc739c4a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}