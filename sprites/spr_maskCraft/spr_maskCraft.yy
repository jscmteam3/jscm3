{
    "id": "0dad99b4-f77d-4756-8e7f-e5260d913c5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_maskCraft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "102e6e46-f097-4de3-8425-65fa80810a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad99b4-f77d-4756-8e7f-e5260d913c5f",
            "compositeImage": {
                "id": "6d3aa48b-fef8-4a62-9e7a-0b20832539f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102e6e46-f097-4de3-8425-65fa80810a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c21ddc22-c5aa-4a6d-bd54-6cc506d90ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102e6e46-f097-4de3-8425-65fa80810a92",
                    "LayerId": "28f025a8-e9a8-4e9d-ad39-0ae4a45d44fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "28f025a8-e9a8-4e9d-ad39-0ae4a45d44fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dad99b4-f77d-4756-8e7f-e5260d913c5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}