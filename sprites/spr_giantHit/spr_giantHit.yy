{
    "id": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_giantHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 8,
    "bbox_right": 95,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea2aaa4d-fec6-4265-b6b6-eba2eeb38ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "compositeImage": {
                "id": "161dd5d9-f597-43b0-be9a-2d08e58bc932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea2aaa4d-fec6-4265-b6b6-eba2eeb38ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ad73dd-3e2a-4137-9d44-c056273b5885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea2aaa4d-fec6-4265-b6b6-eba2eeb38ce1",
                    "LayerId": "0c4ce416-4967-4ad7-b3cc-276fea69f568"
                }
            ]
        },
        {
            "id": "bf18f1b9-39d8-4da8-bdbb-1c73e00e3ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "compositeImage": {
                "id": "1ac66674-9b03-4477-af25-ebb989b51201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf18f1b9-39d8-4da8-bdbb-1c73e00e3ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef935339-25db-4483-90c8-dd55bedd3c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf18f1b9-39d8-4da8-bdbb-1c73e00e3ba1",
                    "LayerId": "0c4ce416-4967-4ad7-b3cc-276fea69f568"
                }
            ]
        },
        {
            "id": "a36cd250-152d-44e7-8cca-03fd6c6eadd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "compositeImage": {
                "id": "75d5b807-e059-412f-9c4b-82b2c3f7ce4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36cd250-152d-44e7-8cca-03fd6c6eadd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6fd686d-128e-45d6-8fbd-63f6f3120bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36cd250-152d-44e7-8cca-03fd6c6eadd9",
                    "LayerId": "0c4ce416-4967-4ad7-b3cc-276fea69f568"
                }
            ]
        },
        {
            "id": "30a3da18-c66b-4774-b503-8c8fd54a6932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "compositeImage": {
                "id": "dadde229-7874-4f95-9b3a-8d74d8761b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a3da18-c66b-4774-b503-8c8fd54a6932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169fa010-2654-4f8d-b6f5-beedab61a0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a3da18-c66b-4774-b503-8c8fd54a6932",
                    "LayerId": "0c4ce416-4967-4ad7-b3cc-276fea69f568"
                }
            ]
        },
        {
            "id": "15a2eb28-2b70-4b5a-a6b3-613fa0ae051a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "compositeImage": {
                "id": "0724d274-f6d9-42a8-ab66-325224661952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a2eb28-2b70-4b5a-a6b3-613fa0ae051a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c575d6f4-c208-48d5-a149-418a10c84230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a2eb28-2b70-4b5a-a6b3-613fa0ae051a",
                    "LayerId": "0c4ce416-4967-4ad7-b3cc-276fea69f568"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "0c4ce416-4967-4ad7-b3cc-276fea69f568",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc4601bf-5807-411f-9d70-feb0b3fc303f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 53
}