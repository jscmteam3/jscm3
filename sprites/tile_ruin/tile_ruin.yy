{
    "id": "2a7151d0-4ed4-4a9a-83f7-36db464ea84b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile_ruin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5311,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25f53812-a651-417d-b394-7872de77eec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7151d0-4ed4-4a9a-83f7-36db464ea84b",
            "compositeImage": {
                "id": "e26c823b-4796-458b-82a3-2d6084dbc189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f53812-a651-417d-b394-7872de77eec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7c1e493-11a2-48d0-a418-199b0d2c0265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f53812-a651-417d-b394-7872de77eec1",
                    "LayerId": "cf8126e3-1974-4668-ac9e-4525d7976cf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6000,
    "layers": [
        {
            "id": "cf8126e3-1974-4668-ac9e-4525d7976cf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a7151d0-4ed4-4a9a-83f7-36db464ea84b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}