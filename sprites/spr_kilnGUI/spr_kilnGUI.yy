{
    "id": "b9ea1337-3623-4e9b-9b57-537b3b7f1454",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_kilnGUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 187,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7c8b201-dde8-4bd7-aa2f-2011bb06df9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ea1337-3623-4e9b-9b57-537b3b7f1454",
            "compositeImage": {
                "id": "918584d0-e4e1-4f7b-8aa4-415650485626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c8b201-dde8-4bd7-aa2f-2011bb06df9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1d08553-9d31-4190-bac2-04e3e8686c01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c8b201-dde8-4bd7-aa2f-2011bb06df9f",
                    "LayerId": "d7723910-028a-44e5-8842-8b7d1d76378e"
                }
            ]
        },
        {
            "id": "070b0898-1fa8-43b5-870e-45db3732da80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ea1337-3623-4e9b-9b57-537b3b7f1454",
            "compositeImage": {
                "id": "fcdc204c-5830-4a07-817f-f4cf16be17d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070b0898-1fa8-43b5-870e-45db3732da80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def3f159-6b8d-4eee-8b50-a22aa65c21aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070b0898-1fa8-43b5-870e-45db3732da80",
                    "LayerId": "d7723910-028a-44e5-8842-8b7d1d76378e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "d7723910-028a-44e5-8842-8b7d1d76378e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9ea1337-3623-4e9b-9b57-537b3b7f1454",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 188,
    "xorig": 0,
    "yorig": 0
}