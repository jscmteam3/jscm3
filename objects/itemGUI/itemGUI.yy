{
    "id": "c558ddd0-2234-4986-b088-d46f22ddf5f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "itemGUI",
    "eventList": [
        {
            "id": "55d009c9-b359-4a36-807b-348468ebc996",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c558ddd0-2234-4986-b088-d46f22ddf5f2"
        },
        {
            "id": "de34dffd-18b6-4a5d-8d97-fce1824e06f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c558ddd0-2234-4986-b088-d46f22ddf5f2"
        },
        {
            "id": "96f71fdf-1d4a-45c2-b8d8-1ff1a68f7581",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c558ddd0-2234-4986-b088-d46f22ddf5f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a97e1d38-e4dd-40f0-8a50-9d66b83738cc",
    "visible": true
}