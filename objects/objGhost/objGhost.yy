{
    "id": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGhost",
    "eventList": [
        {
            "id": "4f92ce70-cb89-455a-ac73-771ee05716fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        },
        {
            "id": "c17822ee-459a-48c9-8230-11b43d123b53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        },
        {
            "id": "2f0cc3be-bc06-4ec6-a9e0-90b26c26ef84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        },
        {
            "id": "1d6d0809-6307-4cff-a357-974397e383d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        },
        {
            "id": "0a7b38bc-c29f-44df-a92b-0b315d459322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        },
        {
            "id": "071b57e7-b6cf-48a7-a80c-d633171285b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "90a9b3f7-f676-4dcd-a6dc-16c9465f3807"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
    "visible": true
}