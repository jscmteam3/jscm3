{
    "id": "4423e897-291f-4cde-9daa-d4ca23e781f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_HealthBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f30878-0dc6-4973-8336-f12e6ba2ac54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4423e897-291f-4cde-9daa-d4ca23e781f3",
            "compositeImage": {
                "id": "2b35f3cf-77c0-41b7-8f0e-c23926f2226c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f30878-0dc6-4973-8336-f12e6ba2ac54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dceecaa-74fc-43c1-b2c7-25c02e28111f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f30878-0dc6-4973-8336-f12e6ba2ac54",
                    "LayerId": "edd55eab-856d-40f3-a6b5-9824c7e27834"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "edd55eab-856d-40f3-a6b5-9824c7e27834",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4423e897-291f-4cde-9daa-d4ca23e781f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}