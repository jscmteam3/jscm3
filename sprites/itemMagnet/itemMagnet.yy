{
    "id": "9e0a28f5-eadb-4439-a348-f1215312e92a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemMagnet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 4,
    "bbox_right": 24,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "949f55f8-bbf3-4cbf-b9f0-c4034a47cb57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e0a28f5-eadb-4439-a348-f1215312e92a",
            "compositeImage": {
                "id": "7bd5e617-9614-4213-91c1-773d5aa103a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949f55f8-bbf3-4cbf-b9f0-c4034a47cb57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "079e1f49-4181-48b2-9e69-ff552c5c22c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949f55f8-bbf3-4cbf-b9f0-c4034a47cb57",
                    "LayerId": "7dca266d-7135-49fe-9074-83e2cb9ddfb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7dca266d-7135-49fe-9074-83e2cb9ddfb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e0a28f5-eadb-4439-a348-f1215312e92a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}