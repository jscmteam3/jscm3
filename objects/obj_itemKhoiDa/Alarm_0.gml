/// @description Insert description here
// You can write your code in this editor
tree_hp -= 1;
if (tree_hp<=0){
	var instID = instance_create_layer(x, y, "Inventory",objPickup);
	with(instID){
		instID.myItemName = "Vien da";
		instID.myItemAmount = 4;
		instID.myItemType = "ingredient";
		instID.myItemScript = "Che tao dung cu";
		instID.myItemSprite = itemVienDa;
	}
	ds_list_add(persistentData.ds_disapearID, id);
	instance_destroy();
}