{
    "id": "dfd6741f-34bc-4646-af44-d6533828a036",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text_12",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fd7c9ed4-88fe-488e-b9b5-b59d7285b2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 192,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "294d5627-89f5-44b8-9e53-96a64ff6bcf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 216,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6bfa3c4f-6fc0-4b60-969f-01dcf74cd40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 131,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "75cf2855-1393-40f9-8129-48eb92dc95e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "daa74b22-e980-419b-a151-62006b7cb648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5d080062-f764-4fcf-b719-91a7f8dcc362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "aa20d616-2476-47dd-839e-ea045e04f469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a2d759ee-34ac-4089-8e3f-c583489a1148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 246,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4d5d91a8-d413-4886-aaf5-f609422abcb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "55b488b7-4ab0-443b-bfe2-995b08870502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 123,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "87144f34-a097-4ec0-b1a4-802b637fd098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ca74e5b7-60c0-4956-bf3b-5d2774dc756a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "148c7f49-3afe-43ac-957d-ad21e6000b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 241,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "856866dc-6b00-4372-925c-f5fb6fd32961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 163,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ff0bb851-4d31-4dac-9fb5-7c33c17db3ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 236,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "117fc07d-2227-408c-a91d-e4d758b387d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "58803175-06cc-4097-a070-a0053f006c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 158,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cdf904c7-fbf1-4350-84d5-112a55ee1e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 155,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8d7a3706-b455-498b-a38d-290229cfd34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "673f7ce4-0374-4035-afe9-cf809e76eca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "10ce7d22-a12c-43cd-bb1a-f9925a274229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4a9b21b3-9530-4e24-bb23-61152b1851bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 122,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ce49f2a6-b708-4493-ac36-358724fc0374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "785f4c87-1376-4407-836f-94fe2549d81b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fbf561cc-d45a-43a0-845b-40d8e6a2910f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "182517aa-5d71-4ed3-ba77-bd191f308360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "79b76c28-90a9-49e0-9738-f8a4797e5205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 221,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "887307c5-4d1a-4cb8-89a9-d4b8605ec0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 199,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ad83796e-0285-48a1-bde2-ef229048b6d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6ccbc1fc-d382-4c3c-9f0a-3379108423a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 71,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "36282ec1-742a-4152-bf1f-4d12678239a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "608dabb8-f018-4e80-9427-b2215a9cc4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4d740f6a-6c31-4e9d-84dd-943a3c5c98bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "806d1053-c394-47e1-9bfe-de3664139e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2bc6be03-f685-440c-a8f2-c4563c993ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c2118bc4-f2d4-4ccb-80d8-0ddcbddcd487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 26
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fe027317-1fd0-4ba3-8291-9d0f3ddc108b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e2a52b23-cc03-4c11-9ac0-c43a4d953818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ae904e46-dcee-4d91-98e4-083e3ae3e8a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "da9bbccc-72c5-46ac-9023-cf91f8cf130c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "59aec9f0-5cd1-4340-a803-197207403f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "366bdc53-c551-4db3-98c9-9a37237999b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f4670ce6-788f-473d-bccf-c43196d0eb6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "455e98cf-0d33-40f0-a330-1a3797b4de47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "67480e2a-28d5-4a0c-a281-b61d7fb1bee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "61a334e5-028d-4749-bb29-4d3dd21cea63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8fc2f6fe-727d-41bd-8c61-82e48f31fecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6dc7d1ad-563a-41e7-82bb-4e221083bcaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7614326b-4cf1-4bb6-8469-2bac71d94f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dd08d5dc-8040-4af2-b7d2-f9cf393d2809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a61f1552-e856-4726-8647-527753f6649f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "928747e2-afa2-4e15-b4bb-b56ee069c027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3dae3e43-b0e8-449a-a2c4-c219f41de8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "291ad76e-f112-4570-854c-11a34614f274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5e50c878-07f1-4a8b-a3ce-07e050e7ea18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 26
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8abd469e-ffaf-422c-acbc-b2480b36cdbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1999a167-6f79-41ae-bceb-6fd50b0c5e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "592b5eb5-d410-4a3a-9cb8-17c4ab31fe83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b5141be0-93cf-4679-b295-be81a977bcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5267b0b7-0afc-4be8-af97-c9c26eb028cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5ba81789-fe26-4dbe-9d30-90bed24bff11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "086e5d20-ca13-451f-ad71-fdbdd96ba174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 185,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0bce9d47-ce3c-45e4-b3f0-2e04367c8b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b7cb7121-f170-4348-bb00-6a2916504123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "81b32741-1a52-49e1-879e-42d87b8166dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 205,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1e56b9aa-189f-45a1-bef3-7e5cc052266e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 50
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0cea47eb-b317-43e1-a1ed-5d13bf41e59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 170,
                "y": 50
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "08c0b6df-ccc6-41fe-8acd-614165cd6064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "01fd10ce-bd69-46db-83d4-bf6f2c533955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0bda3dcd-2a4a-4065-98ea-3599cb0157c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 26
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d3b82806-0509-4ece-9c53-29e6fd7a9144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 120,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3df7f27d-dcf2-4b32-965e-21487997b534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4778c9cf-2949-4b85-af1b-92268b082dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 140,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3458ec5c-4804-4bbb-98e1-197b17204a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 231,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d76659ad-4aaf-46b1-99ab-29669cd96e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 98,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3adc00e4-08cb-4082-882c-4ba1a27ce8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 160,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fdd66c61-0102-476b-9d86-d7c9821e871c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9d802086-b4b2-45fe-b343-b461929d23ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "36367cc5-a0fb-4db5-b222-6789dab75568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "be31fc9a-fbf2-4bea-ad22-de4f9645c445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 190,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c54fc7d0-54b7-4357-8b3e-4b06b6dd12f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "77da699a-8cd3-4fca-88b7-2d7cc03e022d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 210,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "609af571-e403-4e79-aed1-a1b80ff9bef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b56d1e65-e7ca-4c13-81a5-ba01dc225940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5ca6501a-a69c-415d-97de-de460e652203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eb8a3bac-3dd6-4143-8a3b-8e53c34375c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3b6edfa5-8459-49d8-8ec5-037b4851ef41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1076b861-6c17-4c62-bf09-d3f2f7766ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "10847f62-6b71-4358-9fed-0d7a4ca07d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "461e5d14-7935-4c84-b5b1-36749051e5b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 215,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "03935586-e5e4-4e4d-9a58-a427adbd6c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "06421e65-6437-4416-b32e-4e64950b5883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 147,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2c305bd1-4480-4b06-b4ae-7f7f1e822782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 211,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6df6a44f-36f4-4919-a411-c2fb734dd812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ea246c4a-bbfd-439b-a26c-ee4d7fcf5d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 50
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}