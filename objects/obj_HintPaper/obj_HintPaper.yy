{
    "id": "372388d2-256f-4605-bcf7-08e19f1e2f32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HintPaper",
    "eventList": [
        {
            "id": "920aedff-189c-455f-8f07-7b9d4d60ce19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "372388d2-256f-4605-bcf7-08e19f1e2f32"
        },
        {
            "id": "c0090f47-c62d-4970-a636-615077a90cdf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "372388d2-256f-4605-bcf7-08e19f1e2f32"
        },
        {
            "id": "6d4ee70e-4bf2-4212-a723-eb81aa5e48c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "372388d2-256f-4605-bcf7-08e19f1e2f32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fa8ebcdb-3a57-4ff8-9f00-c576057a6a63",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}