{
    "id": "d2b112a5-49bc-4e59-871d-e7e659253e33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDraw",
    "eventList": [
        {
            "id": "7e28e52c-d4e3-49f9-9747-8fab196defee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2b112a5-49bc-4e59-871d-e7e659253e33"
        },
        {
            "id": "3954225e-5d01-4c24-9c87-83f6fe966d4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "d2b112a5-49bc-4e59-871d-e7e659253e33"
        },
        {
            "id": "52d9acd9-2acb-4e03-ad8c-dde803b1e067",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d2b112a5-49bc-4e59-871d-e7e659253e33"
        },
        {
            "id": "0ce69502-af3e-44ed-9920-e6481ac33f10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d2b112a5-49bc-4e59-871d-e7e659253e33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}