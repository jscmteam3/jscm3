{
    "id": "55211fd9-855b-4ea7-a1a2-bf3170498a07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 96,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c18067-c9a3-4a34-94b9-968fcad4dee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55211fd9-855b-4ea7-a1a2-bf3170498a07",
            "compositeImage": {
                "id": "15ea7221-6665-47f8-aefa-77d7a92d016f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c18067-c9a3-4a34-94b9-968fcad4dee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce138ed6-da77-47e3-a89e-294b56e08df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c18067-c9a3-4a34-94b9-968fcad4dee3",
                    "LayerId": "c606b6b8-ffaf-4ea5-880f-3a3659adb0c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "c606b6b8-ffaf-4ea5-880f-3a3659adb0c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55211fd9-855b-4ea7-a1a2-bf3170498a07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}