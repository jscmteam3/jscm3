{
    "id": "1e6d6a44-5763-417e-856c-2c797bacdcf9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "crops",
    "eventList": [
        {
            "id": "5d5894c6-51a0-4446-b21d-c7663d76a299",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e6d6a44-5763-417e-856c-2c797bacdcf9"
        },
        {
            "id": "02044c6a-9e8c-4d9b-adbc-a7ee41699dfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "1e6d6a44-5763-417e-856c-2c797bacdcf9"
        },
        {
            "id": "32491e0d-c7db-4abc-ad1b-879685e91597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e6d6a44-5763-417e-856c-2c797bacdcf9"
        },
        {
            "id": "0d1afd69-c49e-47ac-a16f-091103da1526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1e6d6a44-5763-417e-856c-2c797bacdcf9"
        },
        {
            "id": "302ea3a1-9493-4b74-8f21-f8f70c40e324",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1e6d6a44-5763-417e-856c-2c797bacdcf9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}