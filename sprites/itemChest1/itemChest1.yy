{
    "id": "672b3b6f-ea77-4c97-bda7-855ff08239f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemChest1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03587a68-96d8-4ec6-a486-1d51d8241b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "672b3b6f-ea77-4c97-bda7-855ff08239f9",
            "compositeImage": {
                "id": "bcbf9f6a-68e0-499f-8645-d8a031117fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03587a68-96d8-4ec6-a486-1d51d8241b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68b95d46-f41a-4fb0-ae10-458975b62311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03587a68-96d8-4ec6-a486-1d51d8241b6d",
                    "LayerId": "1b0f5c0f-f586-4575-8bbb-5b40d76b4a95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b0f5c0f-f586-4575-8bbb-5b40d76b4a95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "672b3b6f-ea77-4c97-bda7-855ff08239f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}