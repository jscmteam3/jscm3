{
    "id": "9a7d1090-4ed7-4679-8a30-2d1b2ff7bf1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemGloves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 26,
    "bbox_right": 46,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80b67b0f-0e89-4178-9d77-1465af19149b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a7d1090-4ed7-4679-8a30-2d1b2ff7bf1e",
            "compositeImage": {
                "id": "4ae7b32c-b8e2-49c7-9f6d-9a29b9972d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b67b0f-0e89-4178-9d77-1465af19149b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85fbd35b-49a4-4b66-b1f2-a139edcb1954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b67b0f-0e89-4178-9d77-1465af19149b",
                    "LayerId": "17f33879-f91c-4723-9fad-364f36bb110c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "17f33879-f91c-4723-9fad-364f36bb110c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a7d1090-4ed7-4679-8a30-2d1b2ff7bf1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}