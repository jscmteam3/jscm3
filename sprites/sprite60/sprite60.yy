{
    "id": "9edbc989-028a-41d1-a827-fc5d434fddc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite60",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "748bece5-7beb-40f9-a890-7156010af41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edbc989-028a-41d1-a827-fc5d434fddc2",
            "compositeImage": {
                "id": "002ec98e-959e-4775-ab1f-c4360419d261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748bece5-7beb-40f9-a890-7156010af41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9da8e7e-049a-4a39-a179-77c47f58538a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748bece5-7beb-40f9-a890-7156010af41c",
                    "LayerId": "37e1a1cc-0e3c-4653-9bfd-cbd83b6b8b1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "37e1a1cc-0e3c-4653-9bfd-cbd83b6b8b1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9edbc989-028a-41d1-a827-fc5d434fddc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}