{
    "id": "f6b85e7a-6cbd-471a-ab3a-53974f37824b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile123",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "992e9539-ad0a-4bde-ae0b-bf89fc4a4b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6b85e7a-6cbd-471a-ab3a-53974f37824b",
            "compositeImage": {
                "id": "4fb3ec78-786d-4ecd-a18c-4cf777b49fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992e9539-ad0a-4bde-ae0b-bf89fc4a4b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2bc0eaa-4636-4580-8570-addab3afff62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992e9539-ad0a-4bde-ae0b-bf89fc4a4b3e",
                    "LayerId": "a3df6ef3-fb7d-43af-a6b0-99e4d570dccc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "a3df6ef3-fb7d-43af-a6b0-99e4d570dccc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6b85e7a-6cbd-471a-ab3a-53974f37824b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}