{
    "id": "de664d52-0ecb-4a3d-9745-48ed25c61973",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 55,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bf16488-a401-4420-9d09-a18b0e2b0ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
            "compositeImage": {
                "id": "0531358f-20a9-4499-8054-e21049cf5298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf16488-a401-4420-9d09-a18b0e2b0ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84037fc-4353-46b2-86ce-ffd14866597c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf16488-a401-4420-9d09-a18b0e2b0ee3",
                    "LayerId": "8ed4956d-d12a-4546-9463-0af49d02ff93"
                }
            ]
        },
        {
            "id": "9ddf10fc-2b92-41ed-80a9-edcc607b6336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
            "compositeImage": {
                "id": "d6d3cb47-2107-4f63-8e4a-3447d68a371b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddf10fc-2b92-41ed-80a9-edcc607b6336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e69486-1096-4ccf-b36a-94c54a105804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddf10fc-2b92-41ed-80a9-edcc607b6336",
                    "LayerId": "8ed4956d-d12a-4546-9463-0af49d02ff93"
                }
            ]
        },
        {
            "id": "3ee6749d-457a-4076-97c9-8af22c5e42f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
            "compositeImage": {
                "id": "faedcb7e-999a-4033-8c39-9d3b23b270d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee6749d-457a-4076-97c9-8af22c5e42f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee0303c-bfef-4f96-8899-c1b679763c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee6749d-457a-4076-97c9-8af22c5e42f3",
                    "LayerId": "8ed4956d-d12a-4546-9463-0af49d02ff93"
                }
            ]
        },
        {
            "id": "5084903d-3403-4da9-aad3-4aa523bdd098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
            "compositeImage": {
                "id": "32c92d63-5519-4c81-a3d5-d5f0530bbd61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5084903d-3403-4da9-aad3-4aa523bdd098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726d0b6b-c502-42a4-b003-b12ea923f651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5084903d-3403-4da9-aad3-4aa523bdd098",
                    "LayerId": "8ed4956d-d12a-4546-9463-0af49d02ff93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "8ed4956d-d12a-4546-9463-0af49d02ff93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}