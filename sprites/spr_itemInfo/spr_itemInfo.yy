{
    "id": "a97e1d38-e4dd-40f0-8a50-9d66b83738cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_itemInfo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 251,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82c5f031-354b-45b7-adc7-db015d1e7b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a97e1d38-e4dd-40f0-8a50-9d66b83738cc",
            "compositeImage": {
                "id": "bd76adfa-40b2-43ee-ba25-7f019a5997ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c5f031-354b-45b7-adc7-db015d1e7b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f954b4-3cb8-4d5a-afef-5584fe6268db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c5f031-354b-45b7-adc7-db015d1e7b13",
                    "LayerId": "c910a4b3-5b0c-4bfe-97c5-17aae405324b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c910a4b3-5b0c-4bfe-97c5-17aae405324b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a97e1d38-e4dd-40f0-8a50-9d66b83738cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 252,
    "xorig": 0,
    "yorig": 0
}