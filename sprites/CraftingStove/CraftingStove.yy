{
    "id": "fc821566-55ee-47f3-8132-ffab96adc558",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CraftingStove",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6884eaa5-3f59-45d7-8e17-b3361e97c1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc821566-55ee-47f3-8132-ffab96adc558",
            "compositeImage": {
                "id": "eb74a587-20b7-4fd0-a437-d5ae5dae4acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6884eaa5-3f59-45d7-8e17-b3361e97c1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d82dcb-6568-4f7a-bca0-1b05947397bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6884eaa5-3f59-45d7-8e17-b3361e97c1eb",
                    "LayerId": "5d5083bd-a4a0-404f-a5a6-6de590bbb0dc"
                }
            ]
        },
        {
            "id": "68b8476b-78a7-4097-be55-6b2960eed3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc821566-55ee-47f3-8132-ffab96adc558",
            "compositeImage": {
                "id": "0fda9ed1-bd48-4c86-883b-0abac7eeba9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b8476b-78a7-4097-be55-6b2960eed3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eeb439d-cd3c-412e-abf1-e1baf3420d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b8476b-78a7-4097-be55-6b2960eed3d0",
                    "LayerId": "5d5083bd-a4a0-404f-a5a6-6de590bbb0dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "5d5083bd-a4a0-404f-a5a6-6de590bbb0dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc821566-55ee-47f3-8132-ffab96adc558",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}