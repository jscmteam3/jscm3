{
    "id": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigWalkRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 7,
    "bbox_right": 61,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "d4da70ed-3e23-41ba-847a-78b8da781eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76af811e-0981-4b7b-a546-8b29ec4c5f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2a81a3-d1fc-4821-81b6-f07917fede7e",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "76ea3f49-4606-4705-b070-73a09d58febe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "f5385910-08a0-48c0-b49e-3650b2f24b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ea3f49-4606-4705-b070-73a09d58febe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01365c4-c781-47f9-b75e-06edb097d57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ea3f49-4606-4705-b070-73a09d58febe",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "eca633f2-2802-426c-b442-73703df3ffa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b17c2edb-020c-41eb-bb20-8a976d00f2f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233fd3e6-0d4d-4586-af90-b20a842c6d62",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        },
        {
            "id": "c0c989f6-7166-484f-9515-f3853ec7c003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "compositeImage": {
                "id": "2132977a-5cc1-4303-adaf-ac52581f6826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c989f6-7166-484f-9515-f3853ec7c003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a20ddfe-2f7d-4b2d-b412-16e738003e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c989f6-7166-484f-9515-f3853ec7c003",
                    "LayerId": "e7853d77-1050-4c6e-86fd-9b71ee8b3652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7853d77-1050-4c6e-86fd-9b71ee8b3652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c5a7cea-63ad-4c20-9163-1767b404bd67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}