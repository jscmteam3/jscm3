{
    "id": "ac786b7e-2568-4ab8-b4d8-458c3c9bd024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SnakeDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "543e47b8-2ed3-40d5-a052-6be6e9bd214c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac786b7e-2568-4ab8-b4d8-458c3c9bd024",
            "compositeImage": {
                "id": "0050bbde-c642-45c6-a361-eea37e2c2837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "543e47b8-2ed3-40d5-a052-6be6e9bd214c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aabb2d6d-d1bd-4582-921c-4f8f6ce24c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "543e47b8-2ed3-40d5-a052-6be6e9bd214c",
                    "LayerId": "a3baba2b-9d3e-4206-880a-6081567d27ee"
                }
            ]
        },
        {
            "id": "fd3d919c-7423-4778-a4a3-f25d289ab4a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac786b7e-2568-4ab8-b4d8-458c3c9bd024",
            "compositeImage": {
                "id": "5a092d9c-d67f-4756-8385-83fa01b6cde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3d919c-7423-4778-a4a3-f25d289ab4a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf7c074d-27a0-4463-b39e-1c14751b4103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3d919c-7423-4778-a4a3-f25d289ab4a0",
                    "LayerId": "a3baba2b-9d3e-4206-880a-6081567d27ee"
                }
            ]
        },
        {
            "id": "6b6d1d07-828b-41a2-b89c-262d7b523905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac786b7e-2568-4ab8-b4d8-458c3c9bd024",
            "compositeImage": {
                "id": "69af4d98-3024-430a-bba7-3997a6bf719b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6d1d07-828b-41a2-b89c-262d7b523905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f0defa-d8ee-4750-ae70-a2adb34de924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6d1d07-828b-41a2-b89c-262d7b523905",
                    "LayerId": "a3baba2b-9d3e-4206-880a-6081567d27ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a3baba2b-9d3e-4206-880a-6081567d27ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac786b7e-2568-4ab8-b4d8-458c3c9bd024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}