{
    "id": "b0ee0d69-4b48-4d88-bda0-83458234945c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTree2",
    "eventList": [
        {
            "id": "b601bee0-7030-4eba-9c77-84a745e21e5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        },
        {
            "id": "f3e049b0-4a5e-475f-9c70-e52c2d54be86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        },
        {
            "id": "8a42e4f0-a9b1-454e-8230-4799c4812325",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        },
        {
            "id": "4e522da6-33ac-4ec8-af6c-fadbeeab60a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        },
        {
            "id": "f4ae8eaa-eafc-4e15-9797-5c94dea991bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        },
        {
            "id": "3604f932-202c-4a24-9f0b-5ae41fb7989c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ff7bc3ed-a408-4954-89ff-2bc516efdedb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0ee0d69-4b48-4d88-bda0-83458234945c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba0f2c25-69c4-424a-9ff8-93a16d639369",
    "visible": false
}