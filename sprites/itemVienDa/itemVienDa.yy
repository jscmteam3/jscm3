{
    "id": "aa29ffe8-75c1-40b1-b5b9-342f31f0bc80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemVienDa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fced7cb-12f1-4d95-b2dc-2d6e009557d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa29ffe8-75c1-40b1-b5b9-342f31f0bc80",
            "compositeImage": {
                "id": "4365e1a7-cc8f-42d0-ab7e-e817b1e278cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fced7cb-12f1-4d95-b2dc-2d6e009557d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e878e2f-b326-4bc1-b43b-8715ec11395f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fced7cb-12f1-4d95-b2dc-2d6e009557d6",
                    "LayerId": "f2192a6f-2750-4356-b91f-b40e9e3d3bf4"
                },
                {
                    "id": "0741e4af-f924-4011-8842-08d648b62d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fced7cb-12f1-4d95-b2dc-2d6e009557d6",
                    "LayerId": "c9c66162-b978-4ef0-be68-91ebbe21d639"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2192a6f-2750-4356-b91f-b40e9e3d3bf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa29ffe8-75c1-40b1-b5b9-342f31f0bc80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c9c66162-b978-4ef0-be68-91ebbe21d639",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa29ffe8-75c1-40b1-b5b9-342f31f0bc80",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}