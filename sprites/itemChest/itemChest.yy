{
    "id": "27054339-af1a-4f5d-ae64-c51a5699ef6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemChest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "642344b2-3ea1-45cf-a5dd-bbb5dd9b163e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27054339-af1a-4f5d-ae64-c51a5699ef6d",
            "compositeImage": {
                "id": "5041114f-5dc6-42e7-8d22-44624afa9bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "642344b2-3ea1-45cf-a5dd-bbb5dd9b163e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9451b10a-addd-4169-b05a-80d89109bd22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "642344b2-3ea1-45cf-a5dd-bbb5dd9b163e",
                    "LayerId": "f3dfb862-73d7-49cd-b672-709439e76a20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3dfb862-73d7-49cd-b672-709439e76a20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27054339-af1a-4f5d-ae64-c51a5699ef6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}