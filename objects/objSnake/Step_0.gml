XAxis = _lengthX;
YAxis = _lengthY;
getDirection()
var checkArea = 96;
if (monster_hp < max_monster_hp) checkArea = 192;
if(!collision_circle(x, y, 248, objPlayer, false, false)) var checkArea = 96;

if (state == states.idle)
{
	counter +=1;
	//transition triggers
	if (counter >= room_speed * 3){ // make sure condition is true every 3 seconds
		var change = choose(0,1);	// choosing argument randomly
		switch(change){
			case 0: state = states.wander;
			case 1: counter = 0;
					break;// reset 
		}
	}
	if(collision_circle(x, y, checkArea, objPlayer, false, false))
			state = states.alert;
		sprite_index = ViewSnake[Direction,IDLE];
}
else if (state == states.wander)
{
	counter +=1;
	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	if (counter >= room_speed * 3){
		var change = choose(0,1);
		switch(change){
			case 0: state = states.idle;
			case 1:
				snake_dir = irandom_range(0,359);
				_lengthX = lengthdir_x(Speed,snake_dir);
				_lengthY = lengthdir_y(Speed,snake_dir);
				counter = 0;
				break;	
		}	
	}
	if (collision_circle(x, y, checkArea, objPlayer, false, false))
		state = states.alert;	
	sprite_index = ViewSnake[Direction,IDLE];
	
}
else if (state == states.alert)
{
	counter += 1;
	snake_dir = point_direction(x,y, objPlayer.x, objPlayer.y);
	
	_lengthX = lengthdir_x(Speed,snake_dir);	
	_lengthY = lengthdir_y(Speed,snake_dir);	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	//out of chasing
	
	if (!collision_circle(x, y, checkArea, objPlayer, false, false))
		state = states.idle;
	if (collision_circle(x, y, 48, objPlayer, false, false))
		state = states.attack;
	
	sprite_index = ViewSnake[Direction,IDLE];
}
else if (state == states.attack){
	if(delay >= 90){
		my_slash = instance_create_layer(x + 35*sign(_lengthX), y, "Instances", skill1);	
		my_slash.image_xscale = XAxis > 0 ? 1:-1;
		delay = 0;
	} else delay +=1;
	

}
if (monster_hp <= 0){		
	ds_list_add(persistentData.ds_disapearID, id);
	instance_destroy();
}
