{
    "id": "decf3b98-96c1-4956-9dad-51755d42ddc3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OBJ_GAMESYSTEM",
    "eventList": [
        {
            "id": "c94ded55-f12b-4661-b555-d0991619289a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "decf3b98-96c1-4956-9dad-51755d42ddc3"
        },
        {
            "id": "1c228823-12a2-4532-9eea-ed3d99f178d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "decf3b98-96c1-4956-9dad-51755d42ddc3"
        },
        {
            "id": "9f92061e-e4b1-4c15-afda-53061fc772ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "decf3b98-96c1-4956-9dad-51755d42ddc3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}