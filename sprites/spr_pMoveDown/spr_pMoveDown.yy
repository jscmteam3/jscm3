{
    "id": "6994db48-0dba-443b-b4b3-22d6e03817f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bd57cc9-b1d8-4272-9092-58cced89f80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6994db48-0dba-443b-b4b3-22d6e03817f7",
            "compositeImage": {
                "id": "36288f08-4608-4a7a-9628-982316d906f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd57cc9-b1d8-4272-9092-58cced89f80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb6b5c4-d0a6-494e-b1c5-f26d63da9922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd57cc9-b1d8-4272-9092-58cced89f80d",
                    "LayerId": "d92c93cc-afd5-40fd-a0b2-f11f25e0c7ec"
                }
            ]
        },
        {
            "id": "d3a3a6ac-decf-43f9-8299-674018689dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6994db48-0dba-443b-b4b3-22d6e03817f7",
            "compositeImage": {
                "id": "bd02724c-ff97-45b1-8d4c-22f16e321f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a3a6ac-decf-43f9-8299-674018689dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cabe582f-b16b-4a6c-9862-f6ce16dec54f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a3a6ac-decf-43f9-8299-674018689dc5",
                    "LayerId": "d92c93cc-afd5-40fd-a0b2-f11f25e0c7ec"
                }
            ]
        },
        {
            "id": "2d5030e8-aa94-4bff-ae19-18ea9b880f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6994db48-0dba-443b-b4b3-22d6e03817f7",
            "compositeImage": {
                "id": "803491e0-3d9f-48e2-8887-22f8e2bc22c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5030e8-aa94-4bff-ae19-18ea9b880f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea62fcee-5f0c-40dd-8a6b-2c75ad3954ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5030e8-aa94-4bff-ae19-18ea9b880f54",
                    "LayerId": "d92c93cc-afd5-40fd-a0b2-f11f25e0c7ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d92c93cc-afd5-40fd-a0b2-f11f25e0c7ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6994db48-0dba-443b-b4b3-22d6e03817f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}