{
    "id": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CheckenWalkDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2634712f-0865-4844-b6b2-7b5806555e37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
            "compositeImage": {
                "id": "241821c6-db80-4c6d-a7dd-80cd557a2c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2634712f-0865-4844-b6b2-7b5806555e37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f917290-fc50-4c00-9512-33280ce4c9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2634712f-0865-4844-b6b2-7b5806555e37",
                    "LayerId": "ef98992a-7d79-4837-9399-2742b64aba59"
                }
            ]
        },
        {
            "id": "51cb87ff-c10e-4fe5-8fd5-fdefb8f19d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
            "compositeImage": {
                "id": "826dc765-3346-47cb-969e-4d7bc5658bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51cb87ff-c10e-4fe5-8fd5-fdefb8f19d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "374d54c9-f265-4dcc-b534-fec5d82c86ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51cb87ff-c10e-4fe5-8fd5-fdefb8f19d64",
                    "LayerId": "ef98992a-7d79-4837-9399-2742b64aba59"
                }
            ]
        },
        {
            "id": "c10aab79-1a34-4b22-a193-4b870aab7688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
            "compositeImage": {
                "id": "cf847ae4-ccd5-4f2f-91e5-988d718ea859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10aab79-1a34-4b22-a193-4b870aab7688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825b4ea4-8dcc-4545-8182-01f210715061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10aab79-1a34-4b22-a193-4b870aab7688",
                    "LayerId": "ef98992a-7d79-4837-9399-2742b64aba59"
                }
            ]
        },
        {
            "id": "3ff4aa9f-dbfc-44d9-b5bd-2dfdb75d750b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
            "compositeImage": {
                "id": "df743271-bb5d-488d-91d2-3154452658f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff4aa9f-dbfc-44d9-b5bd-2dfdb75d750b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297251c0-8b31-4285-a7a4-1bf0ba10cf45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff4aa9f-dbfc-44d9-b5bd-2dfdb75d750b",
                    "LayerId": "ef98992a-7d79-4837-9399-2742b64aba59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef98992a-7d79-4837-9399-2742b64aba59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e56eec7d-d0b6-4482-b178-70af9afb3b1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}