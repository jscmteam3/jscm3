{
    "id": "d97d4845-008e-4404-8e76-d71de75b3e02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPigs",
    "eventList": [
        {
            "id": "0ceb8ba4-bb4b-44d6-be64-8d20b7f1f94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d97d4845-008e-4404-8e76-d71de75b3e02"
        },
        {
            "id": "c8e4599d-d382-474b-ac61-e6cb9067f192",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d97d4845-008e-4404-8e76-d71de75b3e02"
        },
        {
            "id": "55964f22-6952-4aa3-bc9a-3304169c9ac1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d97d4845-008e-4404-8e76-d71de75b3e02"
        },
        {
            "id": "03c51aca-15de-4385-a246-43cd22009a53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d97d4845-008e-4404-8e76-d71de75b3e02"
        }
    ],
    "maskSpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
    "visible": false
}