{
    "id": "f1127180-0495-44d9-b9de-d6e67b927ab3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 24,
    "bbox_right": 42,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e0040b3-d1c5-4e28-9766-638659e067a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1127180-0495-44d9-b9de-d6e67b927ab3",
            "compositeImage": {
                "id": "8cf399c5-3408-4a86-aa28-211625bbf64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e0040b3-d1c5-4e28-9766-638659e067a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ccaa38-c60b-4e93-8fe4-0a1ea0a228d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e0040b3-d1c5-4e28-9766-638659e067a0",
                    "LayerId": "68341ddb-6221-48ca-91d1-a349a2b3c59b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68341ddb-6221-48ca-91d1-a349a2b3c59b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1127180-0495-44d9-b9de-d6e67b927ab3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}