{
    "id": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Slash",
    "eventList": [
        {
            "id": "8ea983c1-9098-4e03-b5f2-0b9900a1b44f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "df8ef4cf-18e6-4b37-bd07-b117003369c3"
        },
        {
            "id": "1b7689db-ba24-435f-8606-8c5b21fd0fce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df8ef4cf-18e6-4b37-bd07-b117003369c3"
        },
        {
            "id": "4f7220fc-fd01-4e14-917a-d78a97f254f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "eda6048b-f7eb-4163-9e06-e86c94204160",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df8ef4cf-18e6-4b37-bd07-b117003369c3"
        },
        {
            "id": "ac1494ab-0fc6-4710-a4ee-a5e85f3c3bd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df8ef4cf-18e6-4b37-bd07-b117003369c3"
        }
    ],
    "maskSpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
    "visible": false
}