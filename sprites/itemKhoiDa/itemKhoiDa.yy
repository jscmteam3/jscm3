{
    "id": "4d034316-4453-461c-92ea-acdf1fd99c31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemKhoiDa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cd1c71c-026a-47b3-bfc5-836a92ecd3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d034316-4453-461c-92ea-acdf1fd99c31",
            "compositeImage": {
                "id": "ca11edaa-e1c1-4613-82a4-f7cf21ef2cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd1c71c-026a-47b3-bfc5-836a92ecd3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd97012e-04c1-4295-8579-7c3e7b258611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd1c71c-026a-47b3-bfc5-836a92ecd3e1",
                    "LayerId": "02c7c034-b06e-4da5-a38e-fea16e6e3fb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "02c7c034-b06e-4da5-a38e-fea16e6e3fb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d034316-4453-461c-92ea-acdf1fd99c31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 9
}