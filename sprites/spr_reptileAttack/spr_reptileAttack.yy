{
    "id": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reptileAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 26,
    "bbox_right": 123,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad8901a5-cd89-47f0-b490-89c34020c3d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "4ef1fc53-24f7-43ca-a212-e1e1bbb59f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8901a5-cd89-47f0-b490-89c34020c3d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f860d294-3b8e-4191-9d9a-e380d54d3f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8901a5-cd89-47f0-b490-89c34020c3d7",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        },
        {
            "id": "24a8c367-b185-44dc-bcfe-57f80dc99dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "dc6cb133-02c9-4cba-9cb9-8e311c672d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a8c367-b185-44dc-bcfe-57f80dc99dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b489622e-3fb6-4ab5-8418-37509813660a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a8c367-b185-44dc-bcfe-57f80dc99dd8",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        },
        {
            "id": "3741432f-af8d-40ec-8e1b-4c76495f80ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "45e53fbe-f80b-4b43-a1a5-ad360e31b660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3741432f-af8d-40ec-8e1b-4c76495f80ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdd1cac6-e548-4b89-b1e3-c4b68ff3446f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3741432f-af8d-40ec-8e1b-4c76495f80ff",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        },
        {
            "id": "2e9717b2-c606-4dc0-99e7-56889d7f5e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "542f3e1e-15fe-42b4-9725-b2ad00a80ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9717b2-c606-4dc0-99e7-56889d7f5e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3820a5a-1867-468a-b4f8-6f76684a1c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9717b2-c606-4dc0-99e7-56889d7f5e51",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        },
        {
            "id": "9aee0570-7fd4-49b2-9fd9-43753b3517cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "8a98974c-a2e1-431c-a7b6-4261e4cba705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aee0570-7fd4-49b2-9fd9-43753b3517cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d453a4-8f58-427b-9f33-fd1acfd39f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aee0570-7fd4-49b2-9fd9-43753b3517cb",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        },
        {
            "id": "5995a830-bb12-4c0e-8218-cb96983e1de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "compositeImage": {
                "id": "799ad366-f2a6-49cb-ac0c-c9a57df6086f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5995a830-bb12-4c0e-8218-cb96983e1de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae25006-6ec1-4201-a75d-806b273af3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5995a830-bb12-4c0e-8218-cb96983e1de4",
                    "LayerId": "b37e8f41-5c40-4496-a420-288edf487579"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "b37e8f41-5c40-4496-a420-288edf487579",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a7ad0ee-fe0e-4e9d-a79d-68e11f21e033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 57
}