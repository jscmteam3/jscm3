{
    "id": "a34c1eef-1aa4-4b39-bcc5-187d2f7e9b17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Slash1",
    "eventList": [
        {
            "id": "7e06d97d-6bc1-4ced-8a08-72c06be06093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "a34c1eef-1aa4-4b39-bcc5-187d2f7e9b17"
        },
        {
            "id": "e0dee980-3d41-419d-89b9-996add3282e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a34c1eef-1aa4-4b39-bcc5-187d2f7e9b17"
        },
        {
            "id": "7eb12ba4-88c2-4270-972a-5bfc481d3555",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "eda6048b-f7eb-4163-9e06-e86c94204160",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a34c1eef-1aa4-4b39-bcc5-187d2f7e9b17"
        },
        {
            "id": "bea640b8-6ca3-449e-ae82-10888b8866c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a34c1eef-1aa4-4b39-bcc5-187d2f7e9b17"
        }
    ],
    "maskSpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
    "visible": false
}