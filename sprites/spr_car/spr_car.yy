{
    "id": "1e38b9e1-bbaf-4ee6-ab01-0f37d9581c74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_car",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0da53b7-31fb-4467-8af3-eee3a3cd601b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e38b9e1-bbaf-4ee6-ab01-0f37d9581c74",
            "compositeImage": {
                "id": "93ebbf21-fced-4e29-991c-9c1e3d0ab313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0da53b7-31fb-4467-8af3-eee3a3cd601b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065392a9-63c2-455a-856f-f1eaa61fe29e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0da53b7-31fb-4467-8af3-eee3a3cd601b",
                    "LayerId": "1b25f7bf-e6ef-41be-b3ad-de0c705a908a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1b25f7bf-e6ef-41be-b3ad-de0c705a908a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e38b9e1-bbaf-4ee6-ab01-0f37d9581c74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 43,
    "yorig": 29
}