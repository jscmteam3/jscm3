/// @description Insert description here
// You can write your code in this editor
draw_self();
var x_off = sprite_get_xoffset(sprite_index);
var y_off = sprite_get_yoffset(sprite_index);
var i;
var c= c_white
	draw_text_color(x + x_off + 50, y + 4, "Use", c,c,c,c,1);	
	draw_text_color(x + x_off+ 50, y + 8*4, "Crafting",c,c,c,c,1);
	draw_text_color(x + x_off+ 50, y + 8*8, "Cook",c,c,c,c,1);	

var xmouse = device_mouse_x_to_gui(0);
var ymouse = device_mouse_y_to_gui(0);
var c = c_red;
if (xmouse >=x && xmouse <= x + 192 ){
	for (i = 0; i < 4; i++)
		if (ymouse >= y + 32*i && ymouse <= y + 32*(i+1)){
				draw_rectangle_color(x, y + 32*i,x + 192, y + 32*(i+1),c,c,c,c, true);
				if (mouse_check_button_pressed(mb_left)){
					audio_play_sound(chose, 10,  false);
					switch(i){
						case 0:
								scr_UseItem(inventoryGUI.itemIndex);
								checkBusy = false;
								instance_destroy();
								break;
						case 1:
								if(instance_exists(obj_craftTable) && obj_craftTable.show_crafting){
									var s = inventoryGUI.itemIndex;							
									if (ds_list_size(obj_craftTable.list_ingre)==2)
									{		ds_list_clear(obj_craftTable.list_ingre);
											checkBusy = false;
											instance_destroy();
									}							
									if (ds_list_size(obj_craftTable.list_ingre)<2)
										ds_list_add(obj_craftTable.list_ingre,s);
								}
									checkBusy = false;
									instance_destroy();
							
									break;
						case 2: 
								if (instance_exists(obj_kilnGUI) && obj_kilnGUI.show_stove && obj_kilnGUI.indexOfStoveItem < 2){							
									scr_switch_ingredient(
										obj_kilnGUI.indexOfStoveItem, obj_kilnGUI.gridCooking, 
										inventoryGUI.itemIndex, playerInventory
										);		
									obj_kilnGUI.indexOfStoveItem ++;
															
								}
								checkBusy = false;
								instance_destroy();		
								break;
					}					
				}
				
		}
}
if (xmouse < x || xmouse > x + 192 || ymouse < y || ymouse > y +128){
	if (mouse_check_button_pressed(mb_left))
	{	
		checkBusy = false
		instance_destroy();
	}
}