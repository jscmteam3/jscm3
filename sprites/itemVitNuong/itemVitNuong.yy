{
    "id": "a4bdb6ca-3b08-4c30-be4e-d20e79432a21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemVitNuong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c7b2105-a757-408b-97ec-9816a29bc217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4bdb6ca-3b08-4c30-be4e-d20e79432a21",
            "compositeImage": {
                "id": "a35bcdb3-2ada-4849-9d21-d60156a7bf39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7b2105-a757-408b-97ec-9816a29bc217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa56f020-6a34-4b11-9276-a136343d155c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7b2105-a757-408b-97ec-9816a29bc217",
                    "LayerId": "911a9e4d-b673-4a76-bfa2-8f0a5a254b0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "911a9e4d-b673-4a76-bfa2-8f0a5a254b0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4bdb6ca-3b08-4c30-be4e-d20e79432a21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}