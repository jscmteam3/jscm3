if (gameST == gameStage.DAYONE && !instance_exists(obj_DAYONE)){
	instance_create_layer(0, 0, "Instances", obj_DAYONE);	
}

if (gameST == gameStage.DAYTWO && !instance_exists(obj_DAYTWO)){
	instance_create_layer(0, 0, "Instances", obj_DAYTWO);		
}

if (gameST == gameStage.DAYTHREE && !instance_exists(obj_DAYTHREE)){
	instance_create_layer(0, 0, "Instances", obj_DAYTHREE);		
}
if (keyboard_check(vk_shift)) objPlayer.Speed = 4;
else objPlayer.Speed = 2;
if (keyboard_check(vk_control) && keyboard_check(vk_shift) && keyboard_check_pressed(ord("H")))
	scr_cheatcode();
if (persistentData.playerhp <= 0)
	game_restart();