var random_x = random(room_width);
var random_y = random(room_height);
var number_instance = instance_number(obj_itemVienDa);
if (number_instance < 10){
	var instID = instance_create_layer(random_x, random_y, "Inventory", item_list[0]);
	with(instID){
		myItemName = "Vien Da";
		myItemScript = "Che tao";
		myItemAmount = 1;
		myItemType = "ingredient";
		myItemSprite = itemVienDa;		
	}
	random_x = random(room_width);
	random_y = random(room_height);
	instID = instance_create_layer(random_x, random_y, "Inventory", item_list[1]);
	with(instID){
		myItemName = "Than ";
		myItemScript = "Dung trong lo nuong";
		myItemAmount = 1;
		myItemType = "ingredient";
		myItemSprite = itemCoal;		
	}
	random_x = random(room_width);
	random_y = random(room_height);
	instance_create_layer(random_x, random_y, "Inventory", item_list[2]);	
}
else
	instance_destroy();
	random_x = random(room_width);
	random_y = random(room_height);
	number_instance = instance_number(obj_itemKhoiGo);
	if (number_instance < 5){
	instID = instance_create_layer(random_x, random_y, "Inventory", item_list[3]);
		with(instID){
			myItemName = "Wood";
			myItemAmount = 1;
			myItemType = "ingredient";
			myItemScript = "Khoi go";
			myItemSprite = itemWood;		
		}
}