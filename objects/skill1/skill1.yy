{
    "id": "ebe08970-4313-4604-bba0-ef95ea1e2f0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "skill1",
    "eventList": [
        {
            "id": "a312dde1-f8c1-44fa-8ef4-fde91bf4d0bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ebe08970-4313-4604-bba0-ef95ea1e2f0f"
        },
        {
            "id": "0a0c8b51-0099-4dc4-8758-a3f9df0a1e64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ebe08970-4313-4604-bba0-ef95ea1e2f0f"
        },
        {
            "id": "9185b7e1-0ff9-4e28-8af7-0a1d1411b3cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ebe08970-4313-4604-bba0-ef95ea1e2f0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
    "visible": false
}