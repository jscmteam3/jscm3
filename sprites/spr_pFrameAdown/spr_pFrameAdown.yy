{
    "id": "a960b9ce-ee52-45fc-94c6-072421cf2024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameAdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 20,
    "bbox_right": 111,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7b3d839-2a03-4281-9259-d65675021dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "34a7e7a2-0d6c-48f7-ab18-b7188f1d11de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7b3d839-2a03-4281-9259-d65675021dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450b5441-504f-4e1c-8e54-fd3434cc1602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b3d839-2a03-4281-9259-d65675021dd4",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "2e5260a8-6c7d-492d-8127-b802ea327ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b3d839-2a03-4281-9259-d65675021dd4",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "0385b73a-f3c1-409f-8fae-5ae29fbbbfcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b3d839-2a03-4281-9259-d65675021dd4",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "4d65b50a-9664-431a-a8b2-588f0ff0ea30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7b3d839-2a03-4281-9259-d65675021dd4",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        },
        {
            "id": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "94df149d-2ca9-4983-9d43-007f2b6936db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9642177d-0552-43be-9855-3ba7e2444a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "286dc0b3-f613-4f1e-973d-e30f735cd627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "302e8a35-5a55-4967-8312-c66366c57b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "75c65a70-fff4-4735-a74f-bd80684d3b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        },
        {
            "id": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "c4d47631-7be6-4283-818e-77962447daa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e212fa59-78b8-4be9-a451-3a6b2fad697d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "c8eb74a6-b715-493c-aff1-acf88035d4d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "768578db-8ed6-433c-aaa5-c3e0dd8df9cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "dac993fb-6111-45c8-a393-9cba4adc377a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        },
        {
            "id": "1102eaef-53eb-4862-94a9-105c3ad768d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "9f0f4f80-a4c9-4a01-be58-cd76c969f1b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9c112f-1275-4416-9c12-6f85dc75d52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "23c1fd5c-e6fe-4898-aebf-4479f6fefeb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "ee659535-f2d0-48b3-abb7-2fc77cbf4859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "6daf3dc6-9340-4dae-8d30-3db61a63f896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        },
        {
            "id": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "13262e5f-076c-428c-9a2c-f1d78e3c41a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5351883d-5392-440f-9df4-4f3eb990b0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "081b4489-151b-48fc-a18e-94f28868d229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "1860690b-5efe-4479-97c3-56fbe24ee25c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "ca5bfa98-32c3-4110-9ba8-416856de8957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5ba4b5-f852-495a-a7a5-2d31692c5594",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        },
        {
            "id": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "82ddc18c-0c78-46b4-ad84-decded56968b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38204c2-d570-4e8e-ab97-4f5ea2774c90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
                    "LayerId": "04573588-8312-46f1-bdbe-435050b13fb9"
                },
                {
                    "id": "9b9668e7-d9df-48f5-a7d8-8ead7ba53baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                },
                {
                    "id": "6f6a87ac-de01-440d-90b8-93117bf38ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
                    "LayerId": "ea12bdea-f894-45a1-b3e9-29be3b46aa55"
                },
                {
                    "id": "2c66d746-a0e0-417a-bc6c-b22bb41ba77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b11551a-f2bd-419f-b335-1dccfe133b2d",
                    "LayerId": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "04573588-8312-46f1-bdbe-435050b13fb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "512a0610-8a68-4da7-afb8-7e6654a384e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ea12bdea-f894-45a1-b3e9-29be3b46aa55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bfcfdd4f-082b-48a0-aea1-16e57a4b9692",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}