///@description switch item to stove and vice versa
///@arg indexDestination
///@arg gridDestination
///@arg indexSource
///@arg gridSource
var index_des = argument0
gridDestination = argument1
var index_source = argument2
gridSource = argument3

ds_grid_set(gridDestination, 0, index_des, ds_grid_get(gridSource, 0, index_source));
ds_grid_set(gridDestination, 1, index_des, ds_grid_get(gridSource, 1, index_source));
ds_grid_set(gridDestination, 2, index_des, ds_grid_get(gridSource, 2, index_source));
ds_grid_set(gridDestination, 3, index_des, ds_grid_get(gridSource, 3, index_source));
ds_grid_set(gridDestination, 4, index_des, ds_grid_get(gridSource, 4, index_source));


gridSource[# 1, index_source] --;