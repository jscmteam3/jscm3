{
    "id": "ba0f2c25-69c4-424a-9ff8-93a16d639369",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 29,
    "bbox_right": 67,
    "bbox_top": 81,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4f31652-587d-4d9b-a0fe-f888a9cdbce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba0f2c25-69c4-424a-9ff8-93a16d639369",
            "compositeImage": {
                "id": "09b5fa78-3691-4ff2-ac58-81d21c79020b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f31652-587d-4d9b-a0fe-f888a9cdbce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a46ca4-5d12-4007-ab7b-80c89fb8f439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f31652-587d-4d9b-a0fe-f888a9cdbce0",
                    "LayerId": "7cf5e5dd-6aac-456b-8408-e5e9a3110fe1"
                },
                {
                    "id": "22479702-4939-4021-a476-9afbba2bf4ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f31652-587d-4d9b-a0fe-f888a9cdbce0",
                    "LayerId": "e8eb0ca1-aff4-4931-8454-a9a5a0f6167a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7cf5e5dd-6aac-456b-8408-e5e9a3110fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0f2c25-69c4-424a-9ff8-93a16d639369",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e8eb0ca1-aff4-4931-8454-a9a5a0f6167a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0f2c25-69c4-424a-9ff8-93a16d639369",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 105
}