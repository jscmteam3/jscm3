{
    "id": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "attack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 23,
    "bbox_right": 48,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d4e5823-edc1-49f0-9b45-63a47a85bc63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "27e51a12-0410-4f80-bf6b-397f809417c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4e5823-edc1-49f0-9b45-63a47a85bc63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63aea527-71bd-4a48-b660-d5c45bdbae24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4e5823-edc1-49f0-9b45-63a47a85bc63",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "e9f3ffeb-5332-4b7a-bf8e-580e277f9530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "bfc49a01-9e65-4a51-ab65-d9f6f41522fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f3ffeb-5332-4b7a-bf8e-580e277f9530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103cfbeb-6058-4523-bf0b-8462ec64ded6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f3ffeb-5332-4b7a-bf8e-580e277f9530",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "db564c8d-97b3-482a-b899-f0b797bb7aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "226ccbb3-ac08-49ad-927d-2ec264afefb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db564c8d-97b3-482a-b899-f0b797bb7aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "679c8571-606f-4689-b89b-69218875c2ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db564c8d-97b3-482a-b899-f0b797bb7aac",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "c5f9619e-c0bd-4c84-afac-6e5b23bef336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "fedbecfb-a004-44a5-973c-4d4570b9e7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f9619e-c0bd-4c84-afac-6e5b23bef336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f032fcb8-5608-4dde-b047-29f48c028060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f9619e-c0bd-4c84-afac-6e5b23bef336",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "30feb4c1-d724-4cb6-985e-5352cb024611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "498b368d-65a3-4cb4-9df9-dd63dd52a7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30feb4c1-d724-4cb6-985e-5352cb024611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b25c5e-a599-4772-980e-0f9355fbb297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30feb4c1-d724-4cb6-985e-5352cb024611",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "0978d3e3-c6ed-4c4a-a616-a4fa6aca8fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "da70df15-0c49-434d-a269-c650d8e8accb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0978d3e3-c6ed-4c4a-a616-a4fa6aca8fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8489346-b330-476d-bff5-f9da016ae42e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0978d3e3-c6ed-4c4a-a616-a4fa6aca8fe1",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "0be2ca48-1eb2-49ee-8f4b-30f575c7cb82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "a72c64ce-db6b-4b04-bac8-baed64dfcf5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0be2ca48-1eb2-49ee-8f4b-30f575c7cb82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc523f41-816d-4243-b660-5762c342e162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0be2ca48-1eb2-49ee-8f4b-30f575c7cb82",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        },
        {
            "id": "ea67b392-6b68-4d7c-aa11-b7752a93741e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "compositeImage": {
                "id": "db8730af-6e0b-4580-a34d-c08d3d6dd0d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea67b392-6b68-4d7c-aa11-b7752a93741e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96721fb3-e84d-44a9-ae71-dce9d3a0c94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea67b392-6b68-4d7c-aa11-b7752a93741e",
                    "LayerId": "961168c0-980b-43a6-806e-d2af2b010b15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "961168c0-980b-43a6-806e-d2af2b010b15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f59fa887-54a5-4bd9-9442-22c3fe7fb7d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 37,
    "yorig": 50
}