{
    "id": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_axeATUP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 20,
    "bbox_right": 91,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdf2712b-7175-4488-9749-6d202116623c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
            "compositeImage": {
                "id": "e0c87fcf-246a-475f-bc5d-4ee9ce017be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf2712b-7175-4488-9749-6d202116623c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e2d2d1-5df0-4071-b3c6-c1b3bb23c334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf2712b-7175-4488-9749-6d202116623c",
                    "LayerId": "d7f1669e-9eb6-4611-ab35-97ce8ad43c14"
                },
                {
                    "id": "c629e758-f282-4aa3-a1f1-e41e1d3b8136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf2712b-7175-4488-9749-6d202116623c",
                    "LayerId": "3b167b48-87a4-44a6-9b3f-527dc2789964"
                }
            ]
        },
        {
            "id": "fcb11bd8-1b3e-467f-ad16-7d0c67dab6c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
            "compositeImage": {
                "id": "be6be942-7b3e-4210-b7be-a328edde805b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb11bd8-1b3e-467f-ad16-7d0c67dab6c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c321fe6-88e8-450f-9619-a7969b3b37e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb11bd8-1b3e-467f-ad16-7d0c67dab6c1",
                    "LayerId": "d7f1669e-9eb6-4611-ab35-97ce8ad43c14"
                },
                {
                    "id": "e4e39864-0633-4fca-8848-beba76d3255f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb11bd8-1b3e-467f-ad16-7d0c67dab6c1",
                    "LayerId": "3b167b48-87a4-44a6-9b3f-527dc2789964"
                }
            ]
        },
        {
            "id": "0a58da24-1ae2-422d-ada1-1a6a254f7f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
            "compositeImage": {
                "id": "e83ece2b-3b60-461f-9c5d-b3ee1b255a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a58da24-1ae2-422d-ada1-1a6a254f7f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4d8562-c269-4c97-afcd-f4fb9d7dfe04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a58da24-1ae2-422d-ada1-1a6a254f7f86",
                    "LayerId": "d7f1669e-9eb6-4611-ab35-97ce8ad43c14"
                },
                {
                    "id": "e080abdd-f599-46e5-a876-31dfc6622900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a58da24-1ae2-422d-ada1-1a6a254f7f86",
                    "LayerId": "3b167b48-87a4-44a6-9b3f-527dc2789964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "d7f1669e-9eb6-4611-ab35-97ce8ad43c14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3b167b48-87a4-44a6-9b3f-527dc2789964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3ca81f7-5c5d-473c-b466-345cfea23f48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}