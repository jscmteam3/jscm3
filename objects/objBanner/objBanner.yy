{
    "id": "1d13035f-a832-4c1a-b456-01b13bab430c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBanner",
    "eventList": [
        {
            "id": "d6feb0c8-8571-4809-99cf-61126c64d3ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "1d13035f-a832-4c1a-b456-01b13bab430c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b16759c5-d1e9-4ec0-a4e0-08c03f2914f7",
    "visible": true
}