{
    "id": "89d3f7c2-9808-4dc3-bb7c-cdc95f427d66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_twinbed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 16,
    "bbox_right": 111,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0451462-9b7b-45e9-a4df-bfc9ea124f2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d3f7c2-9808-4dc3-bb7c-cdc95f427d66",
            "compositeImage": {
                "id": "162b3dfc-8dbf-4541-b353-35d2f3d1a866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0451462-9b7b-45e9-a4df-bfc9ea124f2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ca6408-944d-4dea-baef-a23304df5b4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0451462-9b7b-45e9-a4df-bfc9ea124f2a",
                    "LayerId": "8bffc3f2-7267-49ef-8666-ef78a0515862"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "8bffc3f2-7267-49ef-8666-ef78a0515862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d3f7c2-9808-4dc3-bb7c-cdc95f427d66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}