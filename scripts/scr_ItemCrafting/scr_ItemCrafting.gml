///@description crafting

///@arg x
///@arg y
var xmouse = device_mouse_x_to_gui(0); 
var ymouse = device_mouse_y_to_gui(0);

var xx = argument0;
var yy = argument1;
var ingredient_1 = ds_grid_get(playerInventory, 3, list_ingre[| 0]);
var amount_1     = ds_grid_get(playerInventory, 1, list_ingre[| 0]);

//var ingredient_2 = playerInventory[# 3, list_ingre[| 1] ];
//var amount_2     = playerInventory[# 1, list_ingre[| 1] ];


//-------------CoalStove
with(obj_craftTable){
if (ds_list_size(list_ingre) == 1)	{
		if (ingredient_1 == itemVienDa && amount_1 >= 8){
			draw_sprite_ext(itemStone, 0, xx + 41*scaleSprite, yy + 193*scaleSprite, scale + 0.5, scale + 0.5, 0, c_white, 1);
				return itemStone	;	
		}
		if (ingredient_1 == itemStone && amount_1 >= 2){
			draw_sprite_ext(itemCoalStove, 0, xx + 41*scaleSprite, yy + 193*scaleSprite, scale + 0.5, scale + 0.5, 0, c_white, 1);
				return itemCoalStove;			
		}
	}

	else if (ds_list_size(list_ingre) == 2){
		var ingredient_2 = playerInventory[# 3, list_ingre[| 1] ];
		var amount_2     = playerInventory[# 1, list_ingre[| 1] ];
			if ((ingredient_1 == itemAxe && ingredient_2 == itemWood && amount_2 >=1 )||( ingredient_2 == itemAxe && ingredient_1 == itemWood && amount_1 >=1) ){
				draw_sprite_ext(itemWoodPlat, 0, xx + 41*scaleSprite, yy + 193*scaleSprite, scale + 0.5, scale + 0.5, 0, c_white, 1);
					return itemWoodPlat;	
			}
			else if ((ingredient_1 == itemWood && ingredient_2 == itemVienDa && amount_2 >=1 )||( ingredient_2 == itemWood && ingredient_1 == itemVienDa && amount_1 >=1) ){
				draw_sprite_ext(itemAxe, 0, xx + 41*scaleSprite, yy + 193*scaleSprite, scale + 0.5, scale + 0.5, 0, c_white, 1);
					return itemAxe;	
			}
	}
	
}






