//this script create a data structure storing value Y of the object using it.	
if (ds_exists(ds_depthgrid, ds_type_grid)){		 	//resize current grid (realloc in dynamic array)
		var depthgrid = ds_depthgrid;
		var disapearID = persistentData.ds_disapearID;
		var instNum = instance_number(objParents);	
		ds_grid_resize(depthgrid, 2, instNum);		
		var yy = 0;	
		with (objParents){
			depthgrid[# 0,yy] = id;					
			//add value Y
			depthgrid[# 1,yy] = y;	
			yy+=1;
		}
	ds_grid_sort(ds_depthgrid, 1, true);	
	var ii = 0; repeat(instNum){	//draw the instances
			
			var instanceID =  ds_depthgrid[# 0,ii];
			
			with(instanceID) event_perform(ev_draw, 0);	
			
			ii+=1;
	}
	ds_grid_clear(ds_depthgrid, 0);
}
