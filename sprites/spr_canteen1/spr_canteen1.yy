{
    "id": "32b1851b-badf-4567-ab61-fb8a8622ff0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_canteen1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3390c1d0-a51b-4e48-a73b-88d08334b401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32b1851b-badf-4567-ab61-fb8a8622ff0e",
            "compositeImage": {
                "id": "f78afc26-3a1a-496e-874c-0a50e30e8868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3390c1d0-a51b-4e48-a73b-88d08334b401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c34ef1-7c3e-4ae4-a2a1-41833b56539d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3390c1d0-a51b-4e48-a73b-88d08334b401",
                    "LayerId": "85afc050-5f3d-4884-bad6-61fdea91092e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "85afc050-5f3d-4884-bad6-61fdea91092e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32b1851b-badf-4567-ab61-fb8a8622ff0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}