{
    "id": "ff7bc3ed-a408-4954-89ff-2bc516efdedb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "SlashAxe",
    "eventList": [
        {
            "id": "3461c538-f292-4c9e-9040-9c0619a9f6df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ff7bc3ed-a408-4954-89ff-2bc516efdedb"
        },
        {
            "id": "b3a5e90e-88d7-40bf-bd3a-e5d871f4af4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff7bc3ed-a408-4954-89ff-2bc516efdedb"
        },
        {
            "id": "2f8483d9-229f-4316-ad74-efbaaacea91c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ff7bc3ed-a408-4954-89ff-2bc516efdedb"
        }
    ],
    "maskSpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
    "visible": false
}