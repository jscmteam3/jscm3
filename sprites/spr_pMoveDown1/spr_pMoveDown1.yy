{
    "id": "a960b9ce-ee52-45fc-94c6-072421cf2024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveDown1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "94df149d-2ca9-4983-9d43-007f2b6936db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9642177d-0552-43be-9855-3ba7e2444a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee024d70-1c8b-474f-9e38-f5998ae8e5eb",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                }
            ]
        },
        {
            "id": "1102eaef-53eb-4862-94a9-105c3ad768d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "9f0f4f80-a4c9-4a01-be58-cd76c969f1b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9c112f-1275-4416-9c12-6f85dc75d52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102eaef-53eb-4862-94a9-105c3ad768d4",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                }
            ]
        },
        {
            "id": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "compositeImage": {
                "id": "c4d47631-7be6-4283-818e-77962447daa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e212fa59-78b8-4be9-a451-3a6b2fad697d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c4f731-fe80-4e0c-9106-ad9ba9da090f",
                    "LayerId": "512a0610-8a68-4da7-afb8-7e6654a384e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "512a0610-8a68-4da7-afb8-7e6654a384e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a960b9ce-ee52-45fc-94c6-072421cf2024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}