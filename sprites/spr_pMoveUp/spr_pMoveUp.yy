{
    "id": "0a04bd3e-6ca4-4589-b3c9-320c497fd04b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMoveUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10d9f2a7-f481-4851-b62c-d6358ce08300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a04bd3e-6ca4-4589-b3c9-320c497fd04b",
            "compositeImage": {
                "id": "585e14ca-ee33-4b56-a04e-2d404660e038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d9f2a7-f481-4851-b62c-d6358ce08300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d18716-9864-45c2-ac32-1ff2d6bc2fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d9f2a7-f481-4851-b62c-d6358ce08300",
                    "LayerId": "ad3494e0-3c7f-47ae-9f75-e4e1503d6bd3"
                }
            ]
        },
        {
            "id": "a5c0b5a6-4d51-43d5-895b-77ccbb5dd351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a04bd3e-6ca4-4589-b3c9-320c497fd04b",
            "compositeImage": {
                "id": "b827c885-8dc4-4d86-8e74-908af641071e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c0b5a6-4d51-43d5-895b-77ccbb5dd351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19081ba4-bf27-4bd6-96c2-fdd9e39459ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c0b5a6-4d51-43d5-895b-77ccbb5dd351",
                    "LayerId": "ad3494e0-3c7f-47ae-9f75-e4e1503d6bd3"
                }
            ]
        },
        {
            "id": "aba6050f-a723-477e-9e47-a5ad117c8bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a04bd3e-6ca4-4589-b3c9-320c497fd04b",
            "compositeImage": {
                "id": "a553962d-96b7-423c-8dde-82644aa6e425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba6050f-a723-477e-9e47-a5ad117c8bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28186cce-5762-4cba-bef0-1823113f4df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba6050f-a723-477e-9e47-a5ad117c8bc9",
                    "LayerId": "ad3494e0-3c7f-47ae-9f75-e4e1503d6bd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad3494e0-3c7f-47ae-9f75-e4e1503d6bd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a04bd3e-6ca4-4589-b3c9-320c497fd04b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}