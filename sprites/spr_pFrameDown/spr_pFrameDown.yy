{
    "id": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 43,
    "bbox_right": 86,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8a9c8c7-7be8-41f8-b74b-db23cdf5b84d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
            "compositeImage": {
                "id": "bd61019f-a055-44a5-a31e-be4aca3dda24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a9c8c7-7be8-41f8-b74b-db23cdf5b84d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c4d975-2394-4f53-966e-42654aabbaa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a9c8c7-7be8-41f8-b74b-db23cdf5b84d",
                    "LayerId": "87bdf6b7-f7a5-4486-808c-784756511a5e"
                },
                {
                    "id": "bfb979a1-ad54-480b-bff6-d724135c2398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a9c8c7-7be8-41f8-b74b-db23cdf5b84d",
                    "LayerId": "6f1fad1f-737a-4559-9c3c-3399fab5b9fb"
                }
            ]
        },
        {
            "id": "47470014-aa59-4227-9a7e-b46d4811e93a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
            "compositeImage": {
                "id": "592d4132-70eb-444d-b45a-a58296a538b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47470014-aa59-4227-9a7e-b46d4811e93a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d447ed-fbe6-4400-8416-963ef57178b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47470014-aa59-4227-9a7e-b46d4811e93a",
                    "LayerId": "87bdf6b7-f7a5-4486-808c-784756511a5e"
                },
                {
                    "id": "49e7c659-4438-473b-980a-5206bc829685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47470014-aa59-4227-9a7e-b46d4811e93a",
                    "LayerId": "6f1fad1f-737a-4559-9c3c-3399fab5b9fb"
                }
            ]
        },
        {
            "id": "5e1f9ed3-8712-4980-9f0a-1c43847b0cd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
            "compositeImage": {
                "id": "d5ade813-a08e-4811-bb02-f626a19f1802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1f9ed3-8712-4980-9f0a-1c43847b0cd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa7b7c9-cb72-4a75-83f1-88afa6e20e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1f9ed3-8712-4980-9f0a-1c43847b0cd5",
                    "LayerId": "87bdf6b7-f7a5-4486-808c-784756511a5e"
                },
                {
                    "id": "b3ce4c88-a701-4527-942a-019a6e70e0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1f9ed3-8712-4980-9f0a-1c43847b0cd5",
                    "LayerId": "6f1fad1f-737a-4559-9c3c-3399fab5b9fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "87bdf6b7-f7a5-4486-808c-784756511a5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6f1fad1f-737a-4559-9c3c-3399fab5b9fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1bcfe6c-7a48-45f8-8408-c92b8d5baf6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}