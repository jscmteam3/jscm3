{
    "id": "53be11b6-3406-4997-8d85-d6d90b18242e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game",
    "eventList": [
        {
            "id": "148566ea-2ebe-4923-8bb6-4255b602571c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        },
        {
            "id": "a9f49271-187b-4420-a7d0-a2223963d754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        },
        {
            "id": "bb8c3c4a-7c62-4a5f-8655-456d3d899dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        },
        {
            "id": "cf7cdeec-2cc6-46c1-af60-c7d42190ed98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        },
        {
            "id": "8119d01e-9f40-4012-9f38-b2823e68c1c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        },
        {
            "id": "89f1d919-12f6-4915-93ee-489e2cb47856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "53be11b6-3406-4997-8d85-d6d90b18242e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}