{
    "id": "e3cca61d-48c6-452f-9b4f-df2e2f07fc1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_carpet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76f6eb30-ee57-4ec6-91d5-63171f580989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3cca61d-48c6-452f-9b4f-df2e2f07fc1d",
            "compositeImage": {
                "id": "73794078-16af-42c4-b486-16e48f652fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f6eb30-ee57-4ec6-91d5-63171f580989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2e2dc7-deb2-403c-a6db-9dad61c0fd7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f6eb30-ee57-4ec6-91d5-63171f580989",
                    "LayerId": "829dd776-0fd2-4bc0-b2d3-894fad509a0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "829dd776-0fd2-4bc0-b2d3-894fad509a0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3cca61d-48c6-452f-9b4f-df2e2f07fc1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": -38,
    "yorig": 21
}