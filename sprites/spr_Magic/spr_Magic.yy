{
    "id": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Magic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 3,
    "bbox_right": 62,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c38a058-8653-4206-97c5-7fcd7bbb9e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "967e7f9a-ddc0-4b08-b65b-937599aaa7d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c38a058-8653-4206-97c5-7fcd7bbb9e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b45f94d0-2f8e-48a5-9c44-cbe2308370cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c38a058-8653-4206-97c5-7fcd7bbb9e0b",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "dbc80ca8-9750-40f5-bae3-34b42064757d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c38a058-8653-4206-97c5-7fcd7bbb9e0b",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "64863e01-8ad2-4bbc-8838-771612e8a931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "6c925d7b-10d1-455b-8cef-09145f10a533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64863e01-8ad2-4bbc-8838-771612e8a931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25f3c85-9b4d-4dfd-a1c2-af8a72d2ee5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64863e01-8ad2-4bbc-8838-771612e8a931",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "b44dfe9a-89af-4bbe-a3a5-76c5e2359a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64863e01-8ad2-4bbc-8838-771612e8a931",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "f7b97232-87f0-426c-b0a6-d324c1bac041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "1c533ef9-f7a0-41c7-b349-eb589865ec3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b97232-87f0-426c-b0a6-d324c1bac041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8053e70b-d3ca-4375-b9da-bb128a8b5cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b97232-87f0-426c-b0a6-d324c1bac041",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "1d3385e0-f984-4fa4-aeae-c225dc346e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b97232-87f0-426c-b0a6-d324c1bac041",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "7ef7ecc8-3dae-4389-bdae-00eaef00cb52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "66af9fb0-18d1-4e07-8a09-b8f96136e981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef7ecc8-3dae-4389-bdae-00eaef00cb52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac112f9-c57a-4fdd-adcc-f09cfc28120d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef7ecc8-3dae-4389-bdae-00eaef00cb52",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "63d7aa19-904f-4c2a-8519-01833d9ce2ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef7ecc8-3dae-4389-bdae-00eaef00cb52",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "da4f40a0-63a1-4981-b627-0347ffbb77db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "bb628f8e-8245-451b-91be-0bce77501704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4f40a0-63a1-4981-b627-0347ffbb77db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87c39a2f-82c2-470d-9ccb-abb91ada34d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4f40a0-63a1-4981-b627-0347ffbb77db",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "8f8910eb-5d5d-4541-88a5-a057c2fee02d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4f40a0-63a1-4981-b627-0347ffbb77db",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "91183b1c-0226-40a7-806c-36fb0a46e5f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "f94af852-bb33-4dad-9471-23f8452d55b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91183b1c-0226-40a7-806c-36fb0a46e5f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a9f60d-08d9-4205-871c-2055640efade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91183b1c-0226-40a7-806c-36fb0a46e5f8",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "c34bc206-e620-46b0-9d15-df13880b99bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91183b1c-0226-40a7-806c-36fb0a46e5f8",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "c366205d-4c47-44a9-b230-091b288c5c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "682ea307-756a-4f14-bf01-3309eafa2cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c366205d-4c47-44a9-b230-091b288c5c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ecac5e1-0a0c-4c40-87ac-b32b013baf45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c366205d-4c47-44a9-b230-091b288c5c51",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "af826725-ebdc-475b-953b-015696cce4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c366205d-4c47-44a9-b230-091b288c5c51",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "0d197bf3-573d-4f4a-9829-fae4bea1acf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "4da741d2-8577-4de6-ab74-515aa8f16f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d197bf3-573d-4f4a-9829-fae4bea1acf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a51321-1db2-410b-afb2-7c88c1247cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d197bf3-573d-4f4a-9829-fae4bea1acf2",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "e1ea8d40-5360-4e39-b8a9-2fe89daad09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d197bf3-573d-4f4a-9829-fae4bea1acf2",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "6b1473fa-7331-4b54-9056-a7dc16b92c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "42f6ccb9-2b65-4b82-85bf-eded8f2cdb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1473fa-7331-4b54-9056-a7dc16b92c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612df745-e85e-40aa-bca7-457780364090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1473fa-7331-4b54-9056-a7dc16b92c54",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "c00fcd8a-68ab-4e0e-a5f0-0d1643cb266c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1473fa-7331-4b54-9056-a7dc16b92c54",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "bc9081df-004e-44f7-b5a7-1e670f6c2f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "4664499c-a9df-45c8-920f-58c07fd059c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc9081df-004e-44f7-b5a7-1e670f6c2f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8faafbfb-8114-432d-abe1-cc5bee131afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9081df-004e-44f7-b5a7-1e670f6c2f14",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "0af4d727-bb9a-430b-a12a-8b7d9061ab05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9081df-004e-44f7-b5a7-1e670f6c2f14",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "265d89e8-2c1f-45bc-894c-424c9a18dfc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "982ef06a-4dfa-48ad-87fa-4a482b7bbda9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265d89e8-2c1f-45bc-894c-424c9a18dfc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aabda6f-c106-4cc3-a944-78e626af640e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265d89e8-2c1f-45bc-894c-424c9a18dfc9",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "7f1d500c-b069-48a6-af87-661b0ec3b6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265d89e8-2c1f-45bc-894c-424c9a18dfc9",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "be4332b4-669c-47e8-acf6-ed957276bd2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "0f4200d4-889a-4524-868f-92bdcfccdce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4332b4-669c-47e8-acf6-ed957276bd2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e890c942-d3e0-4181-8628-347c9de6e90d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4332b4-669c-47e8-acf6-ed957276bd2f",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "aa4ddd81-1dc3-4bb0-b211-0e635b3ab0fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4332b4-669c-47e8-acf6-ed957276bd2f",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "99bf6d4d-a90d-4e87-ae27-f6071f0d0e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "4f809268-5a17-4d03-a12a-ad108553f437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99bf6d4d-a90d-4e87-ae27-f6071f0d0e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d0b391-68c8-429e-8041-62cac9f911df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99bf6d4d-a90d-4e87-ae27-f6071f0d0e5c",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "d1382a18-7e48-42bc-ae80-a5b349baf815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99bf6d4d-a90d-4e87-ae27-f6071f0d0e5c",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "e7e45e31-17b4-48b5-8aa8-9ec6a1ffce72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "b8df161c-6d2c-4cc5-9103-1f9166fd2cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e45e31-17b4-48b5-8aa8-9ec6a1ffce72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39338cd7-6333-442e-b780-9a81476cc392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e45e31-17b4-48b5-8aa8-9ec6a1ffce72",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "8de59ee9-17a1-4eb6-91a0-70dd17f023a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e45e31-17b4-48b5-8aa8-9ec6a1ffce72",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "bc775646-8af7-4bcf-a4b3-5e51f0d07e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "ecaeb400-ceea-41b3-b908-b468779405b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc775646-8af7-4bcf-a4b3-5e51f0d07e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73fe93c1-911b-49f3-abcd-3f622acdb0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc775646-8af7-4bcf-a4b3-5e51f0d07e85",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "4a9262c2-5f46-4d33-a04a-ceec9707e150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc775646-8af7-4bcf-a4b3-5e51f0d07e85",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        },
        {
            "id": "0bb6e77b-d02e-487c-96a1-cf6a865a63b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "compositeImage": {
                "id": "6a65f8d7-aaec-4318-ad65-afc5e00394f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb6e77b-d02e-487c-96a1-cf6a865a63b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2baf03-8566-45ec-a34e-dae0df978a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb6e77b-d02e-487c-96a1-cf6a865a63b4",
                    "LayerId": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc"
                },
                {
                    "id": "9ad2725a-201b-456a-9e99-e5d7d4989602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb6e77b-d02e-487c-96a1-cf6a865a63b4",
                    "LayerId": "53cddedb-0002-4abd-a564-d1d95ec8f574"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 64,
    "layers": [
        {
            "id": "53cddedb-0002-4abd-a564-d1d95ec8f574",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "53dd5165-0fc3-499d-b9da-24b4ae6bb6bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f68a55-c741-4a0b-8cbd-d045921cc8e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}