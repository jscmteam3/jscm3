{
    "id": "39532db4-d285-4d39-a356-c5cf79cf87b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "961c3cdd-5248-4bf5-9d82-5d128790c363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "compositeImage": {
                "id": "6163ab46-7e95-46e3-8866-e649a86ed1a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961c3cdd-5248-4bf5-9d82-5d128790c363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8810831-64fd-49ab-8aab-062d132e9537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961c3cdd-5248-4bf5-9d82-5d128790c363",
                    "LayerId": "201f4f5f-09e7-41cc-ab1b-a820260b9041"
                }
            ]
        },
        {
            "id": "e8676154-36fb-4dbf-ab1c-ce73e07394ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "compositeImage": {
                "id": "356f31c3-bd16-4619-80e3-b245359399b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8676154-36fb-4dbf-ab1c-ce73e07394ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d3ffc33-cd1a-485c-b778-94f161605eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8676154-36fb-4dbf-ab1c-ce73e07394ed",
                    "LayerId": "201f4f5f-09e7-41cc-ab1b-a820260b9041"
                }
            ]
        },
        {
            "id": "de36d348-4737-4c97-a7df-02bf39de047f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "compositeImage": {
                "id": "5eee3fa6-9466-4543-81e3-a28e1d834ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de36d348-4737-4c97-a7df-02bf39de047f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf63aa1f-df13-40c4-ae59-ebc289d02047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de36d348-4737-4c97-a7df-02bf39de047f",
                    "LayerId": "201f4f5f-09e7-41cc-ab1b-a820260b9041"
                }
            ]
        },
        {
            "id": "9d2a5e5a-a90e-4fd3-a27f-b943b8d5b4fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "compositeImage": {
                "id": "bc5105de-1dbe-4388-9374-75da91938cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d2a5e5a-a90e-4fd3-a27f-b943b8d5b4fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef44502-f4b3-4ebf-8748-d8216049ef1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d2a5e5a-a90e-4fd3-a27f-b943b8d5b4fc",
                    "LayerId": "201f4f5f-09e7-41cc-ab1b-a820260b9041"
                }
            ]
        },
        {
            "id": "49530388-2052-4e69-89ec-3917ce741612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "compositeImage": {
                "id": "b07c1a1f-b8ce-488e-9c53-ac459a778686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49530388-2052-4e69-89ec-3917ce741612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c572ff-ff7c-43c4-aa5a-9ab5a9d59612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49530388-2052-4e69-89ec-3917ce741612",
                    "LayerId": "201f4f5f-09e7-41cc-ab1b-a820260b9041"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "201f4f5f-09e7-41cc-ab1b-a820260b9041",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39532db4-d285-4d39-a356-c5cf79cf87b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}