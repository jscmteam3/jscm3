{
    "id": "2829979b-6323-4d7f-a0f0-731d416c23a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 96,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc6df9f-3796-4d6e-ad04-1676a3180f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2829979b-6323-4d7f-a0f0-731d416c23a1",
            "compositeImage": {
                "id": "17d45cc1-0279-4257-9986-f34e6e8b5647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc6df9f-3796-4d6e-ad04-1676a3180f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2c9364-768b-41c2-987f-121a4404b6eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc6df9f-3796-4d6e-ad04-1676a3180f04",
                    "LayerId": "36bcb5ea-027c-4e29-9e18-f0d608c2303b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "36bcb5ea-027c-4e29-9e18-f0d608c2303b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2829979b-6323-4d7f-a0f0-731d416c23a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}