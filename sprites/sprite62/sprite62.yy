{
    "id": "32ecf83c-7914-4693-b583-881ec3e34f4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite62",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d642aa35-14b8-48e4-8494-b9bcce6c693a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecf83c-7914-4693-b583-881ec3e34f4e",
            "compositeImage": {
                "id": "5c1077bf-8863-4093-ac68-66abfd54e1fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d642aa35-14b8-48e4-8494-b9bcce6c693a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5af5a8-7ae3-4918-ad24-02814484ffa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d642aa35-14b8-48e4-8494-b9bcce6c693a",
                    "LayerId": "d26ae33e-97ee-4c53-8138-92a24888a1cf"
                }
            ]
        }
    ],
    "gridX": 25,
    "gridY": 14,
    "height": 14,
    "layers": [
        {
            "id": "d26ae33e-97ee-4c53-8138-92a24888a1cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32ecf83c-7914-4693-b583-881ec3e34f4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}