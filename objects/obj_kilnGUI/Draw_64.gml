/// @description Insert description here
// You can write your code in this editor
var xmouse = device_mouse_x_to_gui(0);
var ymouse = device_mouse_y_to_gui(0);


if (show_stove){
		with(inventoryGUI){
			other.x_top_stove = x - x_offset*3;
			other.y_top_stove = y - y_offset;
		}
	draw_sprite_ext(spr_kilnGUI, isBurning, x_top_stove, y_top_stove, 1, 1, 0, c_white, 1);	
	var i = 0; repeat(2){
		if (gridCooking[# 1,i] > 0){
				draw_sprite(gridCooking[# 3,i], 0, x_top_stove + 12, y_top_stove + 85 - 71*i);	
				if (i==1) numberSlots = 2;
		}	
		i++;
	}
	
	if (xmouse >= x_top_stove + 125 && xmouse <= x_top_stove + 175){
		var ii = 0; repeat(2){							
			if (ymouse  >= y_top_stove + 41 && ymouse<= y_top_stove + 90){
				draw_sprite_ext(spr_slotBG, 0, x_top_stove + 125, y_top_stove + 41, 1.75, 1.75, 0 ,c_white,1);
				if (mouse_check_button_pressed(mb_right)){
					if (fish_cooking_time == fish_cooking_time_max){
						if (gridCooking[# 3, 2] == itemVitNuong){
							scr_addItems(playerInventory, "Vit nuong", 1, "consume", itemVitNuong, "Energy + 10 - HP + 10");				
						}
						if (gridCooking[# 3, 2] == itemBeefNuong){
							scr_addItems(playerInventory, "Thit bo nuong", 1, "consume", itemBeefNuong, "Energy + 20 - HP + 15");				
						}
						ds_grid_destroy(gridCooking);
						gridCooking = ds_grid_create(5, 3);
						foodReturn = noone;
						numberSlots = 0;
						isBurning = 0;
						indexOfStoveItem = 0;
						fish_cooking_time = 0;
					}
				}							
			}
			ii++;
		}		
	}	
if (numberSlots == 2)	
		scr_food_cooking();			
if(foodReturn != noone){
	if (isBurning)
		draw_rectangle_color(
			x_top_stove + 125,y_top_stove +  92,x_top_stove + 125 + 47*(fish_cooking_time/fish_cooking_time_max),
			y_top_stove+ 100, c_red, c_red, c_red, c_red, false
			);
		if (fish_cooking_time < fish_cooking_time_max) fish_cooking_time ++;
		if (fish_cooking_time == fish_cooking_time_max)
		{
			draw_sprite_ext(foodReturn, 0, x_top_stove + 125, y_top_stove + 42, 2, 2, 0,c_white, 1);
		}						
	}	
}