{
    "id": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fullMoving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bd17172-cf7e-49de-bcdf-e7e356c15666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "7e85cec6-04fd-420f-af3a-88e763056af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd17172-cf7e-49de-bcdf-e7e356c15666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074fcf5d-3909-4071-888d-e8c823ad87d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd17172-cf7e-49de-bcdf-e7e356c15666",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "55a5ddba-3fed-40a8-a6e8-0e0968efecd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "d8953e09-23cc-40fb-a49e-2de157f2d760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a5ddba-3fed-40a8-a6e8-0e0968efecd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf87359-08a0-47fd-9296-e965b2839cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a5ddba-3fed-40a8-a6e8-0e0968efecd2",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "7f733909-e8e2-411f-bf70-37193ab1cb73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "9dde28ce-46e0-4779-94cf-7ed50902cd1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f733909-e8e2-411f-bf70-37193ab1cb73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f16a3d-cb9f-4e0f-b710-34e9a957bf37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f733909-e8e2-411f-bf70-37193ab1cb73",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "472aaba3-7832-4c20-9316-561e6584f4ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "fd8f30ce-0ce9-4a3e-848a-7b02e5c79a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472aaba3-7832-4c20-9316-561e6584f4ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ccc2bf5-b623-4ef4-8b7b-a17b7471b502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472aaba3-7832-4c20-9316-561e6584f4ec",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "24ae941c-c96c-4222-a762-408ebb24a53c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "90dfe3f9-c0a5-420e-baf0-22dd7adda034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ae941c-c96c-4222-a762-408ebb24a53c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa3d0d7-5cba-4e96-b28e-a40eb39ad2d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ae941c-c96c-4222-a762-408ebb24a53c",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "5eeffd88-d768-427b-8b75-66470147a467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "f1e95880-46d0-44f1-8731-c59bfb883136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eeffd88-d768-427b-8b75-66470147a467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d10a61a2-e114-4b4d-90d9-c01b9c789b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eeffd88-d768-427b-8b75-66470147a467",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "9d81a2c6-901e-4c2e-a1b5-7fdad96fa9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "9728ead5-d8f1-4c17-b3d6-f94ef3a6f0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d81a2c6-901e-4c2e-a1b5-7fdad96fa9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035202c4-af8d-4a5f-a04c-074fb1447c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d81a2c6-901e-4c2e-a1b5-7fdad96fa9eb",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "f7324cd0-8b9c-44dc-8796-c674baf4a48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "cb7b1a90-7d9b-44c5-81ae-c18cd2df26e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7324cd0-8b9c-44dc-8796-c674baf4a48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd3d9a8-3278-4c2d-8321-caf8acedbe4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7324cd0-8b9c-44dc-8796-c674baf4a48d",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "c8cc00ec-fa23-450f-a076-169cb724a584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "abc3abbe-e40d-45a1-9595-9c4e5f792180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8cc00ec-fa23-450f-a076-169cb724a584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f35d1fe-5ce0-406d-be69-00c867291c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8cc00ec-fa23-450f-a076-169cb724a584",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "5051af7e-343c-4ca3-b3de-f01a0ea32ece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "32a9477c-9947-4ace-83f7-f32eccf168f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5051af7e-343c-4ca3-b3de-f01a0ea32ece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79511913-ca56-43b7-b442-75b3596626b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5051af7e-343c-4ca3-b3de-f01a0ea32ece",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "777ffcee-ce9a-49ae-bea8-e77284c48b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "1d2cd896-a35d-462d-a8d9-f22ecaa42062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777ffcee-ce9a-49ae-bea8-e77284c48b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b2952c3-4ce6-439b-9ae6-311ceb64c01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777ffcee-ce9a-49ae-bea8-e77284c48b21",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        },
        {
            "id": "6e513814-de8a-46d9-96be-fcd8d706ae4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "compositeImage": {
                "id": "fdf490f4-5968-43ef-b0d2-cc65a1691030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e513814-de8a-46d9-96be-fcd8d706ae4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdb3490-b1b0-4c79-847e-ad44015e7fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e513814-de8a-46d9-96be-fcd8d706ae4b",
                    "LayerId": "ff40fd0e-9623-410d-b9e8-8df575aeb723"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff40fd0e-9623-410d-b9e8-8df575aeb723",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c22dead-5d63-4b64-9e76-c53ed43f518b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}