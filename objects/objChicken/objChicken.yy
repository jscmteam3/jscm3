{
    "id": "5cf47a62-eea9-4989-a7ab-5bff042a3718",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objChicken",
    "eventList": [
        {
            "id": "83a6de55-e4ba-4354-ba8c-fd08dd5cc61f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        },
        {
            "id": "b55a165a-4ae0-4510-b6d8-4ef05a498d58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        },
        {
            "id": "da25fee8-138f-4770-8b6e-3a0f5271c094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        },
        {
            "id": "a1e2fada-38a6-4468-98f4-2cabd58a9245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        },
        {
            "id": "7e12717b-c3fc-447e-a92d-93e3eff5a8e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        },
        {
            "id": "a042203f-f687-4b8c-b5da-932ec609e3bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5cf47a62-eea9-4989-a7ab-5bff042a3718"
        }
    ],
    "maskSpriteId": "b9ba2edd-0c9d-4d77-97e8-cef6d0d73c48",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b9ba2edd-0c9d-4d77-97e8-cef6d0d73c48",
    "visible": false
}