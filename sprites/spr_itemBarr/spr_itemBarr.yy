{
    "id": "2f21f123-8d27-4046-8f73-72a6c8ddd7df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_itemBarr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 243,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50680016-306f-40ca-b974-eb11f98efc94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f21f123-8d27-4046-8f73-72a6c8ddd7df",
            "compositeImage": {
                "id": "3ac5e489-8367-40d6-8364-62cf93b23a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50680016-306f-40ca-b974-eb11f98efc94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1902f12e-8509-49f1-90cf-59dd7b4bd0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50680016-306f-40ca-b974-eb11f98efc94",
                    "LayerId": "37f46a30-f2fc-4e96-8d32-fb1e1b71741f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "37f46a30-f2fc-4e96-8d32-fb1e1b71741f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f21f123-8d27-4046-8f73-72a6c8ddd7df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 0,
    "yorig": 0
}