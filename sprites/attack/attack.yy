{
    "id": "59130049-037f-44dd-b164-7ab8f6aaf967",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 24,
    "bbox_right": 46,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b23010c-7d21-46a9-9935-f4cae4955d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "39ef29cb-bdbf-4d0d-856c-c16fe56cac8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b23010c-7d21-46a9-9935-f4cae4955d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d55d28-68ca-4b10-8fcc-fe20d094d968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b23010c-7d21-46a9-9935-f4cae4955d78",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "ce333119-b28a-4b05-ad16-9bf3cb9675f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "7f7cacbe-f589-43f5-9422-babc8b4ff0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce333119-b28a-4b05-ad16-9bf3cb9675f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681e8277-6230-4a69-bbd1-0c56236cce26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce333119-b28a-4b05-ad16-9bf3cb9675f5",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "5dd1377e-ae3b-41a4-93c5-f8e8e523e60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "61345381-f108-435e-9e2c-c39c6664d46f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd1377e-ae3b-41a4-93c5-f8e8e523e60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454f2dcd-10f0-41f6-bec6-0a1ee07905ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd1377e-ae3b-41a4-93c5-f8e8e523e60a",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "d6ce481f-f71b-4330-a097-8516a6a5f216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "ac56da8e-fb00-4896-a05e-f0bca5dc5b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ce481f-f71b-4330-a097-8516a6a5f216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72acd394-8fa6-440f-9d61-90cb5801cbba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ce481f-f71b-4330-a097-8516a6a5f216",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "bd45d6f1-8d39-4586-ba21-c3d4c72f744b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "ff54aed7-f75d-49ba-ad11-c27ba854b3ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd45d6f1-8d39-4586-ba21-c3d4c72f744b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c76d198-485c-48de-b323-29399cd3fefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd45d6f1-8d39-4586-ba21-c3d4c72f744b",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "f3efe4e5-8ca7-432e-95d8-58a6a25aac03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "5334ffc6-1cea-4ef1-a9a8-ac9e0f2add50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3efe4e5-8ca7-432e-95d8-58a6a25aac03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74f29b5a-0b09-41db-ab06-ddec5c2110de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3efe4e5-8ca7-432e-95d8-58a6a25aac03",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "64cdf17e-ba76-4348-bea4-17bf1dcb3075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "2feae20d-0393-4b5c-8506-fa5ed48ee126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64cdf17e-ba76-4348-bea4-17bf1dcb3075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e637346-edfe-48ce-bfa4-cb561853bba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64cdf17e-ba76-4348-bea4-17bf1dcb3075",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "277d6186-bd8c-4471-9bfa-c4b1d2e18417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "13271011-ba5f-4c46-856c-32bf017f4fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "277d6186-bd8c-4471-9bfa-c4b1d2e18417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19dcae8c-5e02-46a9-83ea-debc838928bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "277d6186-bd8c-4471-9bfa-c4b1d2e18417",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "75a6db71-a246-462b-b286-fcbc85cffc86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "59ef8a53-fabb-4b66-b992-00d5c3a13ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a6db71-a246-462b-b286-fcbc85cffc86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5912fe20-99c0-4bed-9773-bf0eef0a22e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a6db71-a246-462b-b286-fcbc85cffc86",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "9eddb86a-c449-499c-8d20-c621a1f09ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "9b4219ad-255a-43b7-9200-cc117cef7696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eddb86a-c449-499c-8d20-c621a1f09ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3914838d-a0a5-4638-8665-22b0639f9eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eddb86a-c449-499c-8d20-c621a1f09ac3",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "37ba8b3d-a44a-485e-8669-f189c7a35200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "4e4fe857-a62e-44cc-9999-35f48493972d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ba8b3d-a44a-485e-8669-f189c7a35200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e69ba6b-d946-485d-9fe7-d138db92a26a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ba8b3d-a44a-485e-8669-f189c7a35200",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "0a670932-6007-4d0c-808b-31de06736b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "10d03706-be01-422d-8b6d-6b4fa22e9347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a670932-6007-4d0c-808b-31de06736b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3688e96d-b78c-4a2c-8a2a-c6f33a9f7f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a670932-6007-4d0c-808b-31de06736b31",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "eb1ad425-cf53-473c-a336-066e376ce140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "3edd0d61-9cde-4c66-8a4f-5ac92ee237e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1ad425-cf53-473c-a336-066e376ce140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c932a06d-72a9-4d95-b6f5-2166feee58d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1ad425-cf53-473c-a336-066e376ce140",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "643bedc4-a8e9-4f88-90c9-16c8cd732502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "de4eaeac-d0f3-4cd2-a8cc-3b66c469387d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643bedc4-a8e9-4f88-90c9-16c8cd732502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bb4103-e14d-45ee-9369-b2b230ce3fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643bedc4-a8e9-4f88-90c9-16c8cd732502",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        },
        {
            "id": "585a5031-93b2-4367-a8c1-bf8e12a21088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "compositeImage": {
                "id": "f4e5235f-4cb3-4a8b-a047-47085e2c986b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585a5031-93b2-4367-a8c1-bf8e12a21088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040b6577-a383-49ef-848a-0fc224655c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585a5031-93b2-4367-a8c1-bf8e12a21088",
                    "LayerId": "c859df3d-b757-4d69-9f2a-6edbf242344d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c859df3d-b757-4d69-9f2a-6edbf242344d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59130049-037f-44dd-b164-7ab8f6aaf967",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 36,
    "yorig": 49
}