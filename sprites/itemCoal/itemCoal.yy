{
    "id": "61af75c0-7fba-41aa-9b26-7fbc1fd2fdf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemCoal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e912391e-2343-489d-8299-db1e842e9675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61af75c0-7fba-41aa-9b26-7fbc1fd2fdf5",
            "compositeImage": {
                "id": "d428d00c-87bb-4c24-89c1-d92e8d3a12d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e912391e-2343-489d-8299-db1e842e9675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb86264-5c66-44a5-aad6-b07a931d2c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e912391e-2343-489d-8299-db1e842e9675",
                    "LayerId": "9e2e1733-eb21-4368-bd13-18cf5104b6de"
                },
                {
                    "id": "7034ff86-bb36-45a0-8e02-74379cbb58e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e912391e-2343-489d-8299-db1e842e9675",
                    "LayerId": "813bd37b-ebff-4103-8365-e9f88ed30a26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e2e1733-eb21-4368-bd13-18cf5104b6de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61af75c0-7fba-41aa-9b26-7fbc1fd2fdf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "813bd37b-ebff-4103-8365-e9f88ed30a26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61af75c0-7fba-41aa-9b26-7fbc1fd2fdf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}