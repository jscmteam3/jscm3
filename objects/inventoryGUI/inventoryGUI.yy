{
    "id": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "inventoryGUI",
    "eventList": [
        {
            "id": "1d121e37-4afa-4093-8783-55a4e62e14c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24"
        },
        {
            "id": "b9fbb71e-9ba2-4041-b88a-efc33a9751d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24"
        },
        {
            "id": "f0544ee7-6614-4587-b379-eba2472996b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24"
        },
        {
            "id": "d5ec1689-42a4-4f72-ac2f-7a8a3100cbc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24"
        },
        {
            "id": "80efac4b-919f-4009-9051-7c9b957f1fd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dfe2bb08-24ac-4f31-a225-2e0cb0334f24"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d33d381-3b4b-4db2-80b7-c017e581a50f",
    "visible": true
}