{
    "id": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reptileIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 30,
    "bbox_right": 91,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06ee8a20-e1de-4f43-8fca-e0ea0fca8cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "compositeImage": {
                "id": "d760ffb1-28a1-4601-bc73-26f254819734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ee8a20-e1de-4f43-8fca-e0ea0fca8cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15931bd1-b40e-4f8c-8e0f-15ff3007a208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ee8a20-e1de-4f43-8fca-e0ea0fca8cdd",
                    "LayerId": "fd733ce0-4e73-4bbe-8591-f0e0160aa346"
                }
            ]
        },
        {
            "id": "360cb67e-fcfa-4a4e-81f0-730cc629a21b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "compositeImage": {
                "id": "56acde51-70c4-485c-9e75-a36324ea8cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360cb67e-fcfa-4a4e-81f0-730cc629a21b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364c6fd4-6163-4c93-bfed-7481f5ea93ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360cb67e-fcfa-4a4e-81f0-730cc629a21b",
                    "LayerId": "fd733ce0-4e73-4bbe-8591-f0e0160aa346"
                }
            ]
        },
        {
            "id": "2145f67a-b2d4-4a32-998f-b9561cc89b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "compositeImage": {
                "id": "4339220c-3e4b-448e-bb66-4b0eb35970f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2145f67a-b2d4-4a32-998f-b9561cc89b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bee15d1f-5db9-47ac-89e8-dbf47eeb1d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2145f67a-b2d4-4a32-998f-b9561cc89b3b",
                    "LayerId": "fd733ce0-4e73-4bbe-8591-f0e0160aa346"
                }
            ]
        },
        {
            "id": "2ae1565d-9004-4ef0-b2f3-74b737df4607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "compositeImage": {
                "id": "91d56d2b-fc78-4605-9cdf-b037b409969a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae1565d-9004-4ef0-b2f3-74b737df4607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b10423-891f-4ffc-a541-d821ad14b2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae1565d-9004-4ef0-b2f3-74b737df4607",
                    "LayerId": "fd733ce0-4e73-4bbe-8591-f0e0160aa346"
                }
            ]
        },
        {
            "id": "6ea25d11-b21c-4afb-8e4c-5236cdcd16e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "compositeImage": {
                "id": "8c3cc280-8629-440a-abef-86d53a542308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea25d11-b21c-4afb-8e4c-5236cdcd16e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316819e7-d90b-45e6-babf-fad8cd07b397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea25d11-b21c-4afb-8e4c-5236cdcd16e0",
                    "LayerId": "fd733ce0-4e73-4bbe-8591-f0e0160aa346"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "fd733ce0-4e73-4bbe-8591-f0e0160aa346",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 57
}