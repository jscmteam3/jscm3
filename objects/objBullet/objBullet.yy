{
    "id": "f3711ef1-a14b-4e1f-bb62-00cace65c067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBullet",
    "eventList": [
        {
            "id": "e2d7acd2-3b18-475e-8e28-a0248713ef8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3711ef1-a14b-4e1f-bb62-00cace65c067"
        },
        {
            "id": "d78feadf-5226-4e9f-9541-14eb1a7ff959",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3711ef1-a14b-4e1f-bb62-00cace65c067"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30fdbb45-f4eb-45dd-b681-5995342cef34",
    "visible": true
}