{
    "id": "09021f4c-48e7-423d-a4b3-17120fa87afc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_kilnGUI",
    "eventList": [
        {
            "id": "e3c44a3b-3674-4b38-b04c-68649a78684a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        },
        {
            "id": "61dd4568-f906-4159-bf55-aaa2fd6153cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        },
        {
            "id": "8dbfe5b0-e2b4-49a1-9bdf-00e731fc4fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        },
        {
            "id": "c115bfb4-d8e3-4e8b-b8fe-2fc2dfe7397a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        },
        {
            "id": "8b4ca1d3-ee60-40e3-b609-84ef6e8763f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        },
        {
            "id": "134b70aa-62f7-4311-b611-c3fa605dbd7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "09021f4c-48e7-423d-a4b3-17120fa87afc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc821566-55ee-47f3-8132-ffab96adc558",
    "visible": true
}