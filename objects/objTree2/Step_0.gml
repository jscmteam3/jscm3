/// @description Insert description here
// You can write your code in this editor
with(objPlayer){
if(place_meeting(x + _lengthX, y, object_index))
{
	while(!place_meeting(x + sign(_lengthX), y, object_index))
	x += sign(_lengthX);
	_lengthX = 0;
}

if(place_meeting(x , y + _lengthY, object_index))
{
	while(!place_meeting(x , y + sign(_lengthY), object_index))
		y += sign(_lengthY);
	_lengthY = 0;
}
}
if (tree_hp<=0){
	audio_play_sound(snd_woodimpact, 10, false);
	var instID = instance_create_layer(x, y, "Inventory",objPickup);
	with(instID){
		instID.myItemName = "Wood";
		instID.myItemAmount = 4;
		instID.myItemType = "ingredient";
		instID.myItemScript = "Cui nau bep";
		instID.myItemSprite = itemWood;
	}
	ds_list_add(persistentData.ds_disapearID, id);
	instance_destroy();
}