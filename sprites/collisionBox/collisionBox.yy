{
    "id": "381f921b-173c-4685-982a-10124d1bb0da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "collisionBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 91,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5bed3a3-f043-4ffe-95a4-842c9936144b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
            "compositeImage": {
                "id": "7a932b9d-102a-4db4-a068-a1a46b6c5bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5bed3a3-f043-4ffe-95a4-842c9936144b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11db9889-01ec-4201-a783-010de2e8504c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5bed3a3-f043-4ffe-95a4-842c9936144b",
                    "LayerId": "95aad37f-068e-4847-b9b3-51a5a8e58051"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "95aad37f-068e-4847-b9b3-51a5a8e58051",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}