{
    "id": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FrameSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4efa257c-d67b-4773-a9d5-f926ad66be99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "a7f1251c-e458-46ee-b3c9-aa95996dcd98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4efa257c-d67b-4773-a9d5-f926ad66be99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de52c3eb-0e52-428f-87e6-479407ee1261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4efa257c-d67b-4773-a9d5-f926ad66be99",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "35a8dd31-13c7-40df-9905-7ca032c4844a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "b0038584-5f92-4005-9903-0052cbedc3cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a8dd31-13c7-40df-9905-7ca032c4844a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c242d75-65d1-4835-a181-28b32f26dabc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a8dd31-13c7-40df-9905-7ca032c4844a",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "76fb5dbf-29a1-4a8a-892d-0263b773f5dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "7d511d09-3dd9-4fd9-9ed6-b175b850f3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fb5dbf-29a1-4a8a-892d-0263b773f5dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293955fc-3503-4629-85f2-2ce37dbd68e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fb5dbf-29a1-4a8a-892d-0263b773f5dc",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "d5900b7b-ee69-4946-be35-eeab468dc2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "6d3fb0e1-dbc0-4fca-9852-3a47f0aa4911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5900b7b-ee69-4946-be35-eeab468dc2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c130e70-c32f-4fba-bb89-bb3bb8e957ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5900b7b-ee69-4946-be35-eeab468dc2d3",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "ca324173-e2e0-4587-93b1-8b06a9caf3ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "987b1324-ac86-4c49-aac6-09e4dab307be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca324173-e2e0-4587-93b1-8b06a9caf3ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68eeac43-6d93-438f-9141-6347381b3bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca324173-e2e0-4587-93b1-8b06a9caf3ea",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "ef14beec-4258-44e1-b3b9-e7253fe10c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "08f474cb-bf39-4820-b211-0d8497a89406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef14beec-4258-44e1-b3b9-e7253fe10c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e0dae5-b76e-4914-bfeb-60742f8daff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef14beec-4258-44e1-b3b9-e7253fe10c81",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "25a4c087-6599-4675-8ea0-b17830e68463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "1531ab22-e60c-433c-96eb-0beb7c5f2897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a4c087-6599-4675-8ea0-b17830e68463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862704f7-add0-4313-9abd-c4bf34abb5b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a4c087-6599-4675-8ea0-b17830e68463",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "6901ca05-f6ab-4832-b892-ddc88f242eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "2816b46b-dd98-4017-baca-04a2413bb602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6901ca05-f6ab-4832-b892-ddc88f242eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d029d0c8-2f46-4a95-ac70-043b35124704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6901ca05-f6ab-4832-b892-ddc88f242eba",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "f3722808-4e0d-48a9-ba43-420f2d8ee399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "56c7da87-ae45-4e76-8a8d-c797355c3b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3722808-4e0d-48a9-ba43-420f2d8ee399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41096e9a-0a61-47a0-a4f7-2079fe59ba12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3722808-4e0d-48a9-ba43-420f2d8ee399",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "f15e5fb5-cebd-455e-b6f3-8d7d4f2ddcd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "d4990f26-270e-4cbf-9527-254973c4340d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15e5fb5-cebd-455e-b6f3-8d7d4f2ddcd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a3276e-ab08-4ab0-8719-31cdf5e4ff6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15e5fb5-cebd-455e-b6f3-8d7d4f2ddcd8",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        },
        {
            "id": "49c66778-c8df-4c40-979e-e0ebd8dd736e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "compositeImage": {
                "id": "682a7153-1d75-467c-a509-4041796beda7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c66778-c8df-4c40-979e-e0ebd8dd736e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825fcdbd-2781-4f4b-9adc-dbf51e1a283d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c66778-c8df-4c40-979e-e0ebd8dd736e",
                    "LayerId": "9ad51252-9da2-40dc-8f73-4e949433f3bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ad51252-9da2-40dc-8f73-4e949433f3bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "797ee2ba-ae96-43e8-b2db-6973c14467b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}