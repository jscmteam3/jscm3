{
    "id": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenWalkLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abd82f7e-2b76-46a1-8908-6b243302d14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
            "compositeImage": {
                "id": "f12280c4-e661-45a7-a679-f7eb7478861c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd82f7e-2b76-46a1-8908-6b243302d14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4d60c9-f5c7-4434-9d3a-84c8f4ecd5a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd82f7e-2b76-46a1-8908-6b243302d14e",
                    "LayerId": "1e3b0177-8b52-456d-9390-953f4209a019"
                }
            ]
        },
        {
            "id": "b01d151a-33fa-4f36-897a-2fff6a208dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
            "compositeImage": {
                "id": "1ced2979-9b62-447f-b6cc-c113b02cd1c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01d151a-33fa-4f36-897a-2fff6a208dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ce5a68-8ecf-4f8d-bae1-8ecf11bce565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01d151a-33fa-4f36-897a-2fff6a208dfa",
                    "LayerId": "1e3b0177-8b52-456d-9390-953f4209a019"
                }
            ]
        },
        {
            "id": "fdf0371e-6543-4d0a-872d-cce86f120a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
            "compositeImage": {
                "id": "5a5cb305-fb0e-4b1b-9d9b-869f4d1a6429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf0371e-6543-4d0a-872d-cce86f120a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a322803f-2551-42ce-8465-7db9f87e478b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf0371e-6543-4d0a-872d-cce86f120a4c",
                    "LayerId": "1e3b0177-8b52-456d-9390-953f4209a019"
                }
            ]
        },
        {
            "id": "866c9947-4322-44da-821e-9ea421bf2b11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
            "compositeImage": {
                "id": "7e7670b5-617e-4dd0-bb7c-49a74ab7e30f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866c9947-4322-44da-821e-9ea421bf2b11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ca7246-3d8b-4c8b-bc32-ce067972f8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866c9947-4322-44da-821e-9ea421bf2b11",
                    "LayerId": "1e3b0177-8b52-456d-9390-953f4209a019"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1e3b0177-8b52-456d-9390-953f4209a019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cd01e0b-ff89-41b3-979d-a6ed70a116bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}