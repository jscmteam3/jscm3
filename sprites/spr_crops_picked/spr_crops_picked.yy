{
    "id": "0dae0f0f-988d-45e2-b673-580339be4a56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crops_picked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15ea2601-74b7-4208-acae-7d80ab322cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "279e0dfa-9ce8-4c1b-8b94-3c83c6fd53ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ea2601-74b7-4208-acae-7d80ab322cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb38e53-8c05-4272-b437-2b57940460a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ea2601-74b7-4208-acae-7d80ab322cd3",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "e62e8786-cf3b-4488-ba69-9f37aba674c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "c68da616-fdb1-417b-aa84-0162df476011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62e8786-cf3b-4488-ba69-9f37aba674c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "917863aa-4c39-4522-9ba0-d7d4d2dc4d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62e8786-cf3b-4488-ba69-9f37aba674c2",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "bb83db19-60d1-4e4c-b6a5-5e6e9a815e3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "dc8c52c3-fe53-495a-8819-a6e82fd6cffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb83db19-60d1-4e4c-b6a5-5e6e9a815e3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "386d9d29-5b54-493e-83ff-69d160040714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb83db19-60d1-4e4c-b6a5-5e6e9a815e3a",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "b1b73fb5-b10a-4037-ba25-52b7a7c468a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "1ee7f2bf-e018-4dff-8b7c-46ef335ecc71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b73fb5-b10a-4037-ba25-52b7a7c468a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ae0479-23fe-4a46-9ef3-241e689c5777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b73fb5-b10a-4037-ba25-52b7a7c468a0",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "66d77775-1385-4fe2-9972-37019696cfb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "702a862c-75ab-4414-a799-90d519fb6b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d77775-1385-4fe2-9972-37019696cfb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657144cd-5045-4fae-878a-9bb11215640f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d77775-1385-4fe2-9972-37019696cfb9",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "b0af66ec-a802-4178-87bb-ef8ac4758eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "f284185f-95a3-4383-9603-4a1c0bc9d59a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0af66ec-a802-4178-87bb-ef8ac4758eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0727bb4a-4e97-4e24-8adf-8174e5c61bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0af66ec-a802-4178-87bb-ef8ac4758eb3",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        },
        {
            "id": "0fdf6372-a3f4-4208-b669-e10e41d49a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "compositeImage": {
                "id": "a2cc637d-edd1-4ca7-b59c-d37a1af8b8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fdf6372-a3f4-4208-b669-e10e41d49a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a54232-21a4-4968-a6c3-f52f5583dff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fdf6372-a3f4-4208-b669-e10e41d49a39",
                    "LayerId": "caea5f32-be41-4137-a843-03aac7ccaf52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "caea5f32-be41-4137-a843-03aac7ccaf52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}