{
    "id": "a06d602e-e4f0-42df-a29b-98cd8432a39c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FUgate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 191,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "955f0218-bf86-44d4-b4e7-b42471d2278e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a06d602e-e4f0-42df-a29b-98cd8432a39c",
            "compositeImage": {
                "id": "0a9ed513-834c-4307-9384-30bb60f39e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955f0218-bf86-44d4-b4e7-b42471d2278e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07cf651-118e-460d-821b-442006384958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955f0218-bf86-44d4-b4e7-b42471d2278e",
                    "LayerId": "fa1f9659-8e31-4e67-acca-ec3f876a32a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "fa1f9659-8e31-4e67-acca-ec3f876a32a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a06d602e-e4f0-42df-a29b-98cd8432a39c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 195,
    "xorig": 97,
    "yorig": 20
}