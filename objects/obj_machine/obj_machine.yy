{
    "id": "eb5a1016-c3ab-431d-9148-4ba1b32ca9c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_machine",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "752409de-0d59-4711-97ba-11b777800763",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2631ce6-54df-4785-850e-0e14b0dd83f4",
    "visible": true
}