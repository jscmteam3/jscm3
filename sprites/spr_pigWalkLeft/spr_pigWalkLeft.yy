{
    "id": "b323d1b6-1527-455f-857b-13995130339b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigWalkLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 2,
    "bbox_right": 56,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2442398-b7e9-4b28-98d1-4d28ff832776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b323d1b6-1527-455f-857b-13995130339b",
            "compositeImage": {
                "id": "f1084c94-5c1d-4f96-b118-fece7edb4f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2442398-b7e9-4b28-98d1-4d28ff832776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b5986c-802b-41d4-b8a3-245fc62254ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2442398-b7e9-4b28-98d1-4d28ff832776",
                    "LayerId": "6f6e0266-ae2e-4170-af4b-9cb09d11f525"
                }
            ]
        },
        {
            "id": "4b0c7343-6963-4087-9128-64363693f932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b323d1b6-1527-455f-857b-13995130339b",
            "compositeImage": {
                "id": "84e8574d-dbf4-40c5-ae0f-dda11e00603d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b0c7343-6963-4087-9128-64363693f932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45454d5-65f0-4439-bd58-60a52b20eee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b0c7343-6963-4087-9128-64363693f932",
                    "LayerId": "6f6e0266-ae2e-4170-af4b-9cb09d11f525"
                }
            ]
        },
        {
            "id": "a2aa6a2b-98f1-4a1f-a737-daff1cbfa215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b323d1b6-1527-455f-857b-13995130339b",
            "compositeImage": {
                "id": "c78d5a27-80bc-439e-b8d4-7f7bb4acb45f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2aa6a2b-98f1-4a1f-a737-daff1cbfa215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6128cca6-bdc6-4055-bd67-736605dd2580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2aa6a2b-98f1-4a1f-a737-daff1cbfa215",
                    "LayerId": "6f6e0266-ae2e-4170-af4b-9cb09d11f525"
                }
            ]
        },
        {
            "id": "1a8fb032-d597-491a-b1eb-8a164a725d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b323d1b6-1527-455f-857b-13995130339b",
            "compositeImage": {
                "id": "2ab7d2ef-751f-47b8-b79e-7ca4812345e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8fb032-d597-491a-b1eb-8a164a725d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ca641f-e930-4462-b960-99e701b3c67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8fb032-d597-491a-b1eb-8a164a725d85",
                    "LayerId": "6f6e0266-ae2e-4170-af4b-9cb09d11f525"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f6e0266-ae2e-4170-af4b-9cb09d11f525",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b323d1b6-1527-455f-857b-13995130339b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}