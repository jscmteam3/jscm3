{
    "id": "569653d4-867e-4bef-ad4d-b567be013361",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemVitSong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2329a8ab-4fdc-42ce-8322-2bd0376c4598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "569653d4-867e-4bef-ad4d-b567be013361",
            "compositeImage": {
                "id": "b8c3130b-fc0d-4ee8-b19f-bf71155f83fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2329a8ab-4fdc-42ce-8322-2bd0376c4598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "446721cc-151b-400f-951c-07099e74d699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2329a8ab-4fdc-42ce-8322-2bd0376c4598",
                    "LayerId": "15a3d3e3-4ea3-4b45-8a19-3308ec1d54ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15a3d3e3-4ea3-4b45-8a19-3308ec1d54ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "569653d4-867e-4bef-ad4d-b567be013361",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}