{
    "id": "a279a088-055a-420c-b4c8-71d6556ad66e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite155",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 51,
    "bbox_right": 158,
    "bbox_top": 62,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99b347ab-2a49-4256-b299-406fe89fb924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "9b10d9dc-fb40-4e21-b520-19da5309e187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b347ab-2a49-4256-b299-406fe89fb924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9165c853-bd1f-4e96-a4d9-ec909f558892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b347ab-2a49-4256-b299-406fe89fb924",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "15b6e30d-2079-4294-a3ee-aa41c8bb5d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "72a7dd1e-cbdd-4df1-aa7a-2b3696de5805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b6e30d-2079-4294-a3ee-aa41c8bb5d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "762ef3e9-82fa-4266-9478-29199ac554c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b6e30d-2079-4294-a3ee-aa41c8bb5d65",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "ba41c4b8-3955-4d1f-8629-df36b0952bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "aeeca0f0-3216-45cc-95ca-724b9ae8c7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba41c4b8-3955-4d1f-8629-df36b0952bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b074b0f5-78a0-4b51-accb-3367c172e62c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba41c4b8-3955-4d1f-8629-df36b0952bfd",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "dd88ec4c-e6d4-4451-98ae-343086c322ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "d8ad048c-2aca-4d3a-8950-8752ee25d3cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd88ec4c-e6d4-4451-98ae-343086c322ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28289b48-495a-4625-8803-88dbbabba69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd88ec4c-e6d4-4451-98ae-343086c322ba",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "782fb870-d138-4c79-9d1b-43b842044851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "2c0cc837-4c07-4e48-98c7-508f2e2e4c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782fb870-d138-4c79-9d1b-43b842044851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8bdea49-a197-46a8-9043-4646771b13fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782fb870-d138-4c79-9d1b-43b842044851",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "9c71f3e9-b0de-43f4-af3f-566ab864355d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "f9dca5d3-2795-41b3-baff-d10f3a4f6fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c71f3e9-b0de-43f4-af3f-566ab864355d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4ecb18-9b62-4d2b-a7f3-d5c420123ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c71f3e9-b0de-43f4-af3f-566ab864355d",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "8bd55af8-df02-4e8f-bc59-78576e378709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "9510f1e1-8f03-4790-ab75-339cf2fef9c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd55af8-df02-4e8f-bc59-78576e378709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d335ba5e-f9da-4f45-af0b-83d9f3d823c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd55af8-df02-4e8f-bc59-78576e378709",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "0e24306b-1452-4276-80bc-5ea716928018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "038892ad-d550-47e1-8428-a061abfdcad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e24306b-1452-4276-80bc-5ea716928018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54f2ae9-67a8-46ac-bd63-e4d1b5ab3081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e24306b-1452-4276-80bc-5ea716928018",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "a89e93f1-11ea-4b10-b840-d6306fe29b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "fe636465-af96-4be5-a8f1-a2c005ae5665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89e93f1-11ea-4b10-b840-d6306fe29b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9010968-923d-4d88-be3a-563f4e595cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89e93f1-11ea-4b10-b840-d6306fe29b8b",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "16f26dc4-3318-4ba9-af8e-b6cf032fef6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "b79e3d28-0410-482c-a28e-2af2a8170ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f26dc4-3318-4ba9-af8e-b6cf032fef6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c566b93f-6c8a-4e85-9df5-6b63d165cd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f26dc4-3318-4ba9-af8e-b6cf032fef6f",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "718dc195-34ee-4175-8b33-6d4f19ff89ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "313e3dae-948d-42f0-8891-acc22a637327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "718dc195-34ee-4175-8b33-6d4f19ff89ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663f2aae-38a7-4831-b7e8-3ca5c6251da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "718dc195-34ee-4175-8b33-6d4f19ff89ac",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "bb1f70aa-d497-4e5d-ad34-85a42e684f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "4527c735-4dfc-4305-850a-7626b447ce2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1f70aa-d497-4e5d-ad34-85a42e684f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b836869-438c-4a8c-91cb-95604425c8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1f70aa-d497-4e5d-ad34-85a42e684f42",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "aeb99cc2-d54e-433f-a070-eed36c094ec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "35817f29-c578-4d5b-bba8-2701fba7e613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeb99cc2-d54e-433f-a070-eed36c094ec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd235ccb-4819-4331-85b0-35448f27e665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeb99cc2-d54e-433f-a070-eed36c094ec8",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        },
        {
            "id": "dd89bad8-0264-4a19-9020-265a7e93b7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "compositeImage": {
                "id": "111a48c1-ca9e-4468-9a94-a294422b4bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd89bad8-0264-4a19-9020-265a7e93b7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8356f8-35cd-4d40-9843-28f4eb1e0056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd89bad8-0264-4a19-9020-265a7e93b7a8",
                    "LayerId": "43c3050f-64f3-4d49-9600-2c0da6bec351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "43c3050f-64f3-4d49-9600-2c0da6bec351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a279a088-055a-420c-b4c8-71d6556ad66e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}