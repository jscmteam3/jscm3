{
    "id": "357b7f7b-20f4-4330-9df7-0e552a31c1c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_terain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b63fac8-4767-46c7-8b40-737b07d196a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "357b7f7b-20f4-4330-9df7-0e552a31c1c3",
            "compositeImage": {
                "id": "84788d76-1fa5-4292-b4cf-a7f2b065b6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b63fac8-4767-46c7-8b40-737b07d196a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb14ceb-7cf5-4305-8eca-ea04f4f7e03d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b63fac8-4767-46c7-8b40-737b07d196a5",
                    "LayerId": "a3e81090-8f6d-475e-adc5-323e29b224bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "a3e81090-8f6d-475e-adc5-323e29b224bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "357b7f7b-20f4-4330-9df7-0e552a31c1c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}