{
    "id": "fe39cf8b-06b3-49cb-bb9c-57e0e5f75442",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SnakeRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "130837a2-5e96-4f68-b10c-967359f1b550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe39cf8b-06b3-49cb-bb9c-57e0e5f75442",
            "compositeImage": {
                "id": "cdee8ee4-60c9-43bb-b24a-995bc42fa9da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130837a2-5e96-4f68-b10c-967359f1b550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9fdcde4-3992-4445-b81f-ca5ec560e869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130837a2-5e96-4f68-b10c-967359f1b550",
                    "LayerId": "a47d5eb1-c440-489a-9f5e-de34700c29bf"
                }
            ]
        },
        {
            "id": "4a6cfc8a-99be-420c-9c57-1daa44f1bb8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe39cf8b-06b3-49cb-bb9c-57e0e5f75442",
            "compositeImage": {
                "id": "f2869bd3-8ec3-4689-bf09-cd374a696675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6cfc8a-99be-420c-9c57-1daa44f1bb8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a79ef9a-6305-40fd-adce-64b0aaa9b0af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6cfc8a-99be-420c-9c57-1daa44f1bb8f",
                    "LayerId": "a47d5eb1-c440-489a-9f5e-de34700c29bf"
                }
            ]
        },
        {
            "id": "61e88145-8909-40bb-aab4-be0476b40633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe39cf8b-06b3-49cb-bb9c-57e0e5f75442",
            "compositeImage": {
                "id": "1e6180e9-c6c6-486e-87cc-ebed48295a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e88145-8909-40bb-aab4-be0476b40633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b7a4ae-a923-4571-a288-30c0efa052be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e88145-8909-40bb-aab4-be0476b40633",
                    "LayerId": "a47d5eb1-c440-489a-9f5e-de34700c29bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a47d5eb1-c440-489a-9f5e-de34700c29bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe39cf8b-06b3-49cb-bb9c-57e0e5f75442",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}