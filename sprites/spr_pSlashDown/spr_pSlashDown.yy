{
    "id": "b32481b4-8530-434d-a644-e166fa6af75e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pSlashDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 0,
    "bbox_right": 75,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f655122d-ac7c-4218-8eb5-804be7e485c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "compositeImage": {
                "id": "e97cd3ac-bdd7-4c7e-adcd-2e29ee3a3b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f655122d-ac7c-4218-8eb5-804be7e485c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e07f8c-2ff0-4a78-ac3c-acd9de687a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f655122d-ac7c-4218-8eb5-804be7e485c6",
                    "LayerId": "4b450776-7154-431e-9695-d08d9b83ce98"
                },
                {
                    "id": "a026bd87-12fe-48eb-8199-83bd153cae95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f655122d-ac7c-4218-8eb5-804be7e485c6",
                    "LayerId": "126c964d-1452-498e-8f42-5f21e033a4f1"
                },
                {
                    "id": "76c102c2-272b-44a1-bb99-67a2e411693d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f655122d-ac7c-4218-8eb5-804be7e485c6",
                    "LayerId": "a92a121f-099c-4ca5-8aa6-ec119955e242"
                }
            ]
        },
        {
            "id": "77466b6b-1f5d-426d-a11e-8c5775b3e3a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "compositeImage": {
                "id": "113cc983-219e-4bc3-975c-72b38643ecb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77466b6b-1f5d-426d-a11e-8c5775b3e3a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c257b8-6b78-41af-a8c0-79f96a349087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77466b6b-1f5d-426d-a11e-8c5775b3e3a7",
                    "LayerId": "126c964d-1452-498e-8f42-5f21e033a4f1"
                },
                {
                    "id": "425dc19e-f7f0-4106-bc30-dcbc1f5a7041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77466b6b-1f5d-426d-a11e-8c5775b3e3a7",
                    "LayerId": "4b450776-7154-431e-9695-d08d9b83ce98"
                },
                {
                    "id": "0290a4df-8ef3-4845-a469-9e7718a7bf8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77466b6b-1f5d-426d-a11e-8c5775b3e3a7",
                    "LayerId": "a92a121f-099c-4ca5-8aa6-ec119955e242"
                }
            ]
        },
        {
            "id": "b88d4afd-1efa-4ea0-804a-751b2664e8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "compositeImage": {
                "id": "c047eb7f-6dd8-47e9-bbff-7885a736087c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88d4afd-1efa-4ea0-804a-751b2664e8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34bba869-3e11-49c1-b989-a7084b5204cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88d4afd-1efa-4ea0-804a-751b2664e8a5",
                    "LayerId": "126c964d-1452-498e-8f42-5f21e033a4f1"
                },
                {
                    "id": "5795d89b-5309-4a1b-920b-5a6981f564ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88d4afd-1efa-4ea0-804a-751b2664e8a5",
                    "LayerId": "4b450776-7154-431e-9695-d08d9b83ce98"
                },
                {
                    "id": "d5d98b21-2601-4d15-b36b-e5c3dd2f4439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88d4afd-1efa-4ea0-804a-751b2664e8a5",
                    "LayerId": "a92a121f-099c-4ca5-8aa6-ec119955e242"
                }
            ]
        },
        {
            "id": "edbd3082-f2ae-4914-9fee-4e06089826da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "compositeImage": {
                "id": "2682b689-759e-4184-a974-4b0f58fcf52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbd3082-f2ae-4914-9fee-4e06089826da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5127a107-f51c-4a3f-bb4c-71d762b6aa63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbd3082-f2ae-4914-9fee-4e06089826da",
                    "LayerId": "4b450776-7154-431e-9695-d08d9b83ce98"
                },
                {
                    "id": "1915dc82-a6f5-4ff1-a4a4-7cd9fb224ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbd3082-f2ae-4914-9fee-4e06089826da",
                    "LayerId": "126c964d-1452-498e-8f42-5f21e033a4f1"
                },
                {
                    "id": "da0ed79c-3344-4b3c-8a09-612e616d2a6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbd3082-f2ae-4914-9fee-4e06089826da",
                    "LayerId": "a92a121f-099c-4ca5-8aa6-ec119955e242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "4b450776-7154-431e-9695-d08d9b83ce98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "126c964d-1452-498e-8f42-5f21e033a4f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a92a121f-099c-4ca5-8aa6-ec119955e242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b32481b4-8530-434d-a644-e166fa6af75e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}