{
    "id": "4834c98e-6248-4d45-9d46-7e06a3dab745",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 41,
    "bbox_right": 84,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "178574e9-2573-4482-9647-8598576691e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4834c98e-6248-4d45-9d46-7e06a3dab745",
            "compositeImage": {
                "id": "01e88c7d-1ee6-4517-a20a-33356a24a2fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178574e9-2573-4482-9647-8598576691e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6db4bd-83f0-42e3-9ba6-38bf7bee54f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178574e9-2573-4482-9647-8598576691e0",
                    "LayerId": "bf1df559-de57-494a-9967-09b9bac6335e"
                },
                {
                    "id": "a4dce3e5-2032-4915-9100-6b100caba4b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178574e9-2573-4482-9647-8598576691e0",
                    "LayerId": "ba8ade6a-ae66-4607-8a69-21e7e90398c5"
                }
            ]
        },
        {
            "id": "f6786ff3-ef49-41b6-a2cb-b0eff119b635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4834c98e-6248-4d45-9d46-7e06a3dab745",
            "compositeImage": {
                "id": "17c89412-00df-44ac-a85c-50694642f95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6786ff3-ef49-41b6-a2cb-b0eff119b635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e0ca18-77dc-4a0c-9a0e-ccc8ab13da2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6786ff3-ef49-41b6-a2cb-b0eff119b635",
                    "LayerId": "bf1df559-de57-494a-9967-09b9bac6335e"
                },
                {
                    "id": "26f44c38-dfc9-4a4f-a595-d5d58ee80c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6786ff3-ef49-41b6-a2cb-b0eff119b635",
                    "LayerId": "ba8ade6a-ae66-4607-8a69-21e7e90398c5"
                }
            ]
        },
        {
            "id": "0b09ff18-3c22-4d13-a41b-4e00d10f062c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4834c98e-6248-4d45-9d46-7e06a3dab745",
            "compositeImage": {
                "id": "004cfef6-16ab-41de-a102-d53128c4ffe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b09ff18-3c22-4d13-a41b-4e00d10f062c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "748fb982-4572-453b-b3cc-bbf91f5401b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b09ff18-3c22-4d13-a41b-4e00d10f062c",
                    "LayerId": "bf1df559-de57-494a-9967-09b9bac6335e"
                },
                {
                    "id": "40111c47-af18-444c-addb-cd4c329df173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b09ff18-3c22-4d13-a41b-4e00d10f062c",
                    "LayerId": "ba8ade6a-ae66-4607-8a69-21e7e90398c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bf1df559-de57-494a-9967-09b9bac6335e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4834c98e-6248-4d45-9d46-7e06a3dab745",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ba8ade6a-ae66-4607-8a69-21e7e90398c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4834c98e-6248-4d45-9d46-7e06a3dab745",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}