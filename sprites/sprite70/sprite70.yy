{
    "id": "6597654f-0a28-46c4-ad87-769237737866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite70",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "944dc2ee-c0cd-4900-897d-f53bf3985b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "81feeaaa-4dfa-4a0c-a7ef-94ddbc6e222a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944dc2ee-c0cd-4900-897d-f53bf3985b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "868b0373-3d29-43cd-97c9-0fabf63bcf43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944dc2ee-c0cd-4900-897d-f53bf3985b6d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ec2ed541-d8c8-411a-8088-09df745468ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ed1ccb38-06e7-4f06-ab3a-7139eace96fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2ed541-d8c8-411a-8088-09df745468ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f0d2b7-aa32-453f-8b76-c64d5d1ed2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2ed541-d8c8-411a-8088-09df745468ea",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4c4b741d-e3d4-43e9-b3ab-1fbbb759a726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f3bad2f4-9f14-4da6-8c53-60bfacfb0d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4b741d-e3d4-43e9-b3ab-1fbbb759a726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc2f5a65-9b97-4aa6-ad26-3af0e0073424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4b741d-e3d4-43e9-b3ab-1fbbb759a726",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "c256daa9-736e-42c4-8e86-d2e8e87ff16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ab2333c0-2a9d-4d2a-ad2f-dd75197a66e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c256daa9-736e-42c4-8e86-d2e8e87ff16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1563f85-5c85-46dc-a73b-1391a188f5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c256daa9-736e-42c4-8e86-d2e8e87ff16f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ac307217-3582-4004-838f-fbd71aa2ec97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fc00a4ae-a502-45af-b629-25ffe872309c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac307217-3582-4004-838f-fbd71aa2ec97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a1bbe84-12be-43df-aadb-54799c56a97c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac307217-3582-4004-838f-fbd71aa2ec97",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fb80f11d-e16a-4991-b5be-f0b286c603d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f3386c42-cbbb-46a9-a4f1-79f9f2c02997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb80f11d-e16a-4991-b5be-f0b286c603d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a62a257-f422-4059-98d2-52feff2677e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb80f11d-e16a-4991-b5be-f0b286c603d5",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "58d4af83-430e-4830-ba2e-15cdf9e209e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e10ed28b-0f56-41df-8ac7-e66a6798cc3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d4af83-430e-4830-ba2e-15cdf9e209e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab1a949-1fc2-480e-a3f2-fd65ceabcead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d4af83-430e-4830-ba2e-15cdf9e209e9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e7bfbde7-58e7-4052-9615-c24a5e679a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1177154a-db0b-4a65-8a57-28e510ce5225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bfbde7-58e7-4052-9615-c24a5e679a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c0e7988-387e-4020-af5e-b15e07e08a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bfbde7-58e7-4052-9615-c24a5e679a2f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "43a0a6cd-6b3a-4fbb-8ed0-698996d38bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "11bf5d2d-2b85-400d-a776-9c3df554badb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a0a6cd-6b3a-4fbb-8ed0-698996d38bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c20208c-6046-4b8d-a0ad-36a378d2163b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a0a6cd-6b3a-4fbb-8ed0-698996d38bff",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5f70f33b-777f-4756-bd66-58d0ff0d6f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "24a84215-7b21-44a1-80e3-0153b407497a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f70f33b-777f-4756-bd66-58d0ff0d6f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb2e3ee-f5a4-441e-9e62-477ae18ea405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f70f33b-777f-4756-bd66-58d0ff0d6f23",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2988763b-d647-4bdd-88f9-990a6d79af9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9b020455-d5dd-42e0-a1c9-9f9f3232046b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2988763b-d647-4bdd-88f9-990a6d79af9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135a6cf8-ed16-4017-a65d-3a92dfa19f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2988763b-d647-4bdd-88f9-990a6d79af9a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "41a81872-c224-4f8e-8d08-b534a473a5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "65c757cb-f76b-4d7e-8e56-4891747ca9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a81872-c224-4f8e-8d08-b534a473a5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb14575-70b2-492e-9270-c1a4532899bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a81872-c224-4f8e-8d08-b534a473a5a4",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2c2e7535-1413-4414-95b0-98de81b20a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9c78a5ce-c8c7-47c1-a2af-d68c0a8931e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2e7535-1413-4414-95b0-98de81b20a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0b2f9f-696f-42ef-b8a1-8d97f20396bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2e7535-1413-4414-95b0-98de81b20a8e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "919fd9f2-f083-4fec-bfe4-11a5ce79f87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b695d2f5-1b56-48bd-affb-f5d54418c823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "919fd9f2-f083-4fec-bfe4-11a5ce79f87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a3227d7-bbb9-4f53-9b52-5fab6fae2934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "919fd9f2-f083-4fec-bfe4-11a5ce79f87e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "864beb74-0e75-40ae-a672-7aaeb0b459bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8ed0e066-e8d1-441f-9975-39343c5181cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864beb74-0e75-40ae-a672-7aaeb0b459bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e6c9b8-7214-4ff2-ae0b-085f853eef4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864beb74-0e75-40ae-a672-7aaeb0b459bc",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fa445c21-6c91-46d8-855e-f678bebb5512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "45f9eb8a-dbb8-46fc-bf65-c27ff6469811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa445c21-6c91-46d8-855e-f678bebb5512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d706f4-57d4-46ee-ae07-16ee66adac82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa445c21-6c91-46d8-855e-f678bebb5512",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "53448c17-b2f4-4f88-84fe-3206f7c37c41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f23638f6-7118-4141-a9b9-5d7971cc6c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53448c17-b2f4-4f88-84fe-3206f7c37c41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f127c0ca-9b16-4ba3-b5bb-487d2e483751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53448c17-b2f4-4f88-84fe-3206f7c37c41",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "93d34e72-dc46-4aae-b0db-118a58f3625a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "0fb0cf3a-b1ca-4110-9f04-dc400543ece7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d34e72-dc46-4aae-b0db-118a58f3625a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a880cf-bf15-4918-8a57-29fb4be47c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d34e72-dc46-4aae-b0db-118a58f3625a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6714035d-5d17-4a84-966e-2c2c92b3b993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "74e0d708-0c8d-46d3-a12a-74ef4c3a9f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6714035d-5d17-4a84-966e-2c2c92b3b993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b5bc4d-03c8-443f-ac63-479b57baf9e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6714035d-5d17-4a84-966e-2c2c92b3b993",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a7794ac5-5b59-45da-87f0-c21890106826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "59fd745e-03f3-4b8e-a07b-d9b38ef3a7cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7794ac5-5b59-45da-87f0-c21890106826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34b2074-5bc2-478b-bb36-5bbd0890d126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7794ac5-5b59-45da-87f0-c21890106826",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e616e100-36d6-4ee3-8a39-aadd2b459f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2405a9ba-ec00-4f99-9717-bea9f60f8eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e616e100-36d6-4ee3-8a39-aadd2b459f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca41e90-d2c5-4edb-90df-1f7b6f611a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e616e100-36d6-4ee3-8a39-aadd2b459f4c",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f440ab30-7cc2-4e4b-96f4-e20bda439b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f9a24ecd-87c0-4c91-aee4-23fa0ad48550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f440ab30-7cc2-4e4b-96f4-e20bda439b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d80e37f8-3d87-40bc-8d00-11481ea58485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f440ab30-7cc2-4e4b-96f4-e20bda439b3b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "aab8a234-ad51-4291-a830-d42fc4888459",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "685c831f-799b-4ad9-91e0-b0cbde558a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab8a234-ad51-4291-a830-d42fc4888459",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1ac337-6a1f-4e25-ad30-798d58bb2b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab8a234-ad51-4291-a830-d42fc4888459",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "52e5c5b2-d0ca-464e-bc54-85b02763c052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a8dbf00f-249c-4eec-abae-1ad97e40c7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e5c5b2-d0ca-464e-bc54-85b02763c052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "953d4328-bc0d-4e55-b5b4-4501bb1ede28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e5c5b2-d0ca-464e-bc54-85b02763c052",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "3c6d5422-2b17-475d-8053-c7c9c6149f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "83a859f5-746a-41a6-85a0-af62fece3757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c6d5422-2b17-475d-8053-c7c9c6149f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51b75f9-8412-4641-b500-d43dcf1b5410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c6d5422-2b17-475d-8053-c7c9c6149f50",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e9334bce-d9e1-4c18-b975-9ca78bdb7395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5299e1b3-ed7e-4f8b-9322-04a3dc76f212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9334bce-d9e1-4c18-b975-9ca78bdb7395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47661547-5d8c-4b2e-b191-5490c70ea8c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9334bce-d9e1-4c18-b975-9ca78bdb7395",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "27670e7f-9be6-4208-95d7-957d38474995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a71c8685-36d0-4013-a577-8a52199e66f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27670e7f-9be6-4208-95d7-957d38474995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f141ccc2-0468-4f86-980e-2b2e2dd6c246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27670e7f-9be6-4208-95d7-957d38474995",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "475d69bf-235b-4b21-87c3-035dd324560f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "47dfd691-7cec-4623-acaa-a454e73ebd97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475d69bf-235b-4b21-87c3-035dd324560f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebe92e6-72b9-4a70-91ba-c65b5798a6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475d69bf-235b-4b21-87c3-035dd324560f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5e163590-ec08-40dc-b037-6f937c3530c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ae55cf5b-5e42-4afd-a63a-b74b95751611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e163590-ec08-40dc-b037-6f937c3530c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95888a8f-aca8-4116-ad59-918b98a2398f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e163590-ec08-40dc-b037-6f937c3530c1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "623857be-96ae-402d-8f5a-b30df8215e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3a703b58-fbf5-4431-b5fd-0ada8ebec8ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623857be-96ae-402d-8f5a-b30df8215e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeb728c0-7117-4009-941d-cfb2b5033824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623857be-96ae-402d-8f5a-b30df8215e82",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "7d6ee50d-4066-4c06-9798-272c8546664d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "238eb455-0ca9-46da-a7d1-0595871207dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6ee50d-4066-4c06-9798-272c8546664d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd7a2d6-e523-49b1-b902-f29eaac838b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6ee50d-4066-4c06-9798-272c8546664d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "05810bea-f235-4e51-a48e-0de856577df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "57ec2dfe-d7d4-473b-9ef4-9c932c6ad142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05810bea-f235-4e51-a48e-0de856577df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15734c37-9c8a-4158-b9dd-942bebfc9328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05810bea-f235-4e51-a48e-0de856577df4",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ed223a30-b569-4de8-b2ac-1c41eea076aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a2282570-78be-408e-a002-da364e31a916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed223a30-b569-4de8-b2ac-1c41eea076aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eee17f9-1658-4693-9001-cbe6f5a41196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed223a30-b569-4de8-b2ac-1c41eea076aa",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4d9364fb-6937-416f-908e-cedca65bf239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "746aeac1-28a7-417a-88c4-a869538b603f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9364fb-6937-416f-908e-cedca65bf239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4d4d47-b78c-4db2-85e1-5d39ce71ec9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9364fb-6937-416f-908e-cedca65bf239",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ad0f572c-625c-4a5f-bd1d-ff24ca7a2397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1605f2d7-04ea-4e2d-b244-f1752527a1c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0f572c-625c-4a5f-bd1d-ff24ca7a2397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196958ef-ebce-4e59-baeb-3003e5efc264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0f572c-625c-4a5f-bd1d-ff24ca7a2397",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ee924772-a0ee-425a-ae34-fd4476b39acc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "977127d8-64cd-4bee-b8f0-b0eb253ce40a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee924772-a0ee-425a-ae34-fd4476b39acc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3199e49c-2907-4f31-bd04-a68b3ffe61d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee924772-a0ee-425a-ae34-fd4476b39acc",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "34502f7a-18c5-4599-a0ca-a94a3680a11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "150a67f8-334e-4fe5-acbf-217e306b1748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34502f7a-18c5-4599-a0ca-a94a3680a11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc2d1781-d567-4344-aefe-cf7e70ff5461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34502f7a-18c5-4599-a0ca-a94a3680a11d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "11c3d33b-7bb4-4acd-ad38-3a2b29f6c7ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "34360588-5f87-4dda-8d18-850cca2e03f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c3d33b-7bb4-4acd-ad38-3a2b29f6c7ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e38bee-3f5e-4a4c-8d67-cd3e001232ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c3d33b-7bb4-4acd-ad38-3a2b29f6c7ef",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fae46a2a-3ef7-4dfc-a445-510525f44c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7c6310f7-9c9c-4eeb-a082-3fa588f475dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae46a2a-3ef7-4dfc-a445-510525f44c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893f1304-f09b-401e-9def-b94fceaea94b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae46a2a-3ef7-4dfc-a445-510525f44c3d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6ac6571a-9c2e-489c-b82e-26ead97b88d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3e174726-dd9e-4428-9c4e-00517f4dd440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac6571a-9c2e-489c-b82e-26ead97b88d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30effa3-02c6-4a60-bb1f-18523396bb52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac6571a-9c2e-489c-b82e-26ead97b88d2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "aa1adbcf-ee35-4f39-983a-0f6fa69b7db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d61ca538-0595-4547-a613-4bc84d11471c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1adbcf-ee35-4f39-983a-0f6fa69b7db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9941ae6-4b51-4f51-b3b5-48622c4153cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1adbcf-ee35-4f39-983a-0f6fa69b7db9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6a9aaef7-daba-4413-b66c-6650c547c67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7c17be0d-c403-4adc-83a1-04ac27e5d549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9aaef7-daba-4413-b66c-6650c547c67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b605031-eed5-40c2-953a-59f387ff68d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9aaef7-daba-4413-b66c-6650c547c67d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f25d9b85-c81e-468a-b277-77143dd2edb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9538fd23-c774-420b-85fc-f02133dc2f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25d9b85-c81e-468a-b277-77143dd2edb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a462200d-0995-4b0e-9123-a9fc7d2d139f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25d9b85-c81e-468a-b277-77143dd2edb5",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9721011b-1563-49b5-9742-31a35bca5288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fc512bf8-7fed-4f9e-a9a8-b0d3f4148bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9721011b-1563-49b5-9742-31a35bca5288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7faef893-afec-4611-9a2c-53cef14c70f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9721011b-1563-49b5-9742-31a35bca5288",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "776465f0-ab94-4bd4-b514-efb802004a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "34fa3609-8943-4721-b1b3-42649a48f69b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776465f0-ab94-4bd4-b514-efb802004a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a7064f-089d-4c10-acbf-0f905b1c8ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776465f0-ab94-4bd4-b514-efb802004a21",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "88142387-ff8d-45a3-a2e0-d71fb4fc9f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d18af6eb-b1da-483a-8b92-8e06d4446d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88142387-ff8d-45a3-a2e0-d71fb4fc9f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f8f638a-163f-4a2b-b3cb-7298a1e3a1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88142387-ff8d-45a3-a2e0-d71fb4fc9f3f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b0b136ec-98ce-454f-8920-7947322d88b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "bc27856d-8ed2-49f3-b4d7-8ef651d71cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b136ec-98ce-454f-8920-7947322d88b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4718eb-ec92-4cd8-b667-0dc190963ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b136ec-98ce-454f-8920-7947322d88b1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4ff8835a-521d-47cc-adf8-f46d0e0aeba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "127774aa-db7e-4aa0-a82f-f9d8be279df0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff8835a-521d-47cc-adf8-f46d0e0aeba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a9afe72-9633-4dc9-ba53-808ddaffb3f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff8835a-521d-47cc-adf8-f46d0e0aeba5",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "406c566d-4516-47fb-b2fc-8d39bb20d5aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e00f477e-0d71-4298-9e2c-677dd1b3e97d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "406c566d-4516-47fb-b2fc-8d39bb20d5aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad93431b-9513-4662-8914-68b16f5d5cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "406c566d-4516-47fb-b2fc-8d39bb20d5aa",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "078a3866-0d3d-4323-abf4-7a4df8b9b7e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "da4aa410-12f3-4020-bb9a-204d429dc18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "078a3866-0d3d-4323-abf4-7a4df8b9b7e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edced0fc-e291-435a-a7d9-af2a2ffd3286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "078a3866-0d3d-4323-abf4-7a4df8b9b7e1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "749498ef-b2f2-48ed-aff7-335c22ba26a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "edf7a7f3-9a17-4ead-9c4a-29ab9f5ff6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749498ef-b2f2-48ed-aff7-335c22ba26a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3bbf6a-a657-448c-8aa7-db0eb18febf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749498ef-b2f2-48ed-aff7-335c22ba26a5",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9e574f74-f281-4426-9ef3-65d8f50d4f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "793eb09e-f8d1-408c-97c8-b6ac32a6e95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e574f74-f281-4426-9ef3-65d8f50d4f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "284a9469-0407-44b5-8e01-9b23b7e48ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e574f74-f281-4426-9ef3-65d8f50d4f05",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5f03c6d3-cd26-493d-a538-22056ac908bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "54bd0389-98a2-4769-8167-1ee7a8896718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f03c6d3-cd26-493d-a538-22056ac908bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6131552-4f5a-4d7f-b72b-38faeb4f8d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f03c6d3-cd26-493d-a538-22056ac908bf",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0ddc6a68-a2ae-4ddb-999a-b7066d1de500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5bdb4742-b77e-4940-9541-0e273c2b3738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ddc6a68-a2ae-4ddb-999a-b7066d1de500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d787c66e-e7dd-44db-9d4b-1208a8e40377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ddc6a68-a2ae-4ddb-999a-b7066d1de500",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9c372612-58c6-452b-ae23-82b282427504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "73e130f5-c003-4853-8c9f-8b84138f2ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c372612-58c6-452b-ae23-82b282427504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42aab634-bf43-450c-90bf-16ebb39a7bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c372612-58c6-452b-ae23-82b282427504",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "472377ed-2a11-4c29-bcef-09a1971bf40d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b3616d53-d707-4e94-a6a8-223c0bfdb494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472377ed-2a11-4c29-bcef-09a1971bf40d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395dadfe-3c9b-4c07-884f-ea73af97e579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472377ed-2a11-4c29-bcef-09a1971bf40d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "00b28a30-8a9c-466f-b03d-27f1d2c3eeb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8d425d8a-969d-4927-a039-84364d5d1e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b28a30-8a9c-466f-b03d-27f1d2c3eeb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b4c052-88c0-4027-8a8c-505dcd32ee6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b28a30-8a9c-466f-b03d-27f1d2c3eeb4",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "cd0e4be0-90fd-4093-9041-ee49bffcc802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "edba3406-71a1-449f-b11a-83ac8cb8af6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0e4be0-90fd-4093-9041-ee49bffcc802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "262a69af-e461-4b33-9499-480f813fb636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0e4be0-90fd-4093-9041-ee49bffcc802",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "28f053b9-f5b7-4e44-aee2-f0ff05153f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "267ac384-d374-4de3-a773-25f4616cef3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f053b9-f5b7-4e44-aee2-f0ff05153f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138ed8d3-c7cd-4dc8-bbfa-03b38881716d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f053b9-f5b7-4e44-aee2-f0ff05153f85",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f4e7f2a4-ed01-49eb-a39c-b15e527db5c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d8b8c4e8-035e-4269-86f6-baf345a8d83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e7f2a4-ed01-49eb-a39c-b15e527db5c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53dd8dc9-75da-4391-8f46-907eb508ca4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e7f2a4-ed01-49eb-a39c-b15e527db5c8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "515a4231-3790-48d2-b714-70a281960ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "557420c9-b54e-4513-9de3-2d0ba1bb528e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515a4231-3790-48d2-b714-70a281960ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf0f901-03f1-4ee9-9e89-bda7df50af54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515a4231-3790-48d2-b714-70a281960ef0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "81fddf64-4a4d-4485-88f6-734603332d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "43fca34e-adfc-42ac-ae5a-006c42959c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81fddf64-4a4d-4485-88f6-734603332d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec068c5-81e8-4509-8c21-c683f41b719e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81fddf64-4a4d-4485-88f6-734603332d3a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "78cb46e1-38a4-48c6-9e04-9a257c58ec5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6c4766fa-b78c-4104-8ed7-aaa2ffef176f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78cb46e1-38a4-48c6-9e04-9a257c58ec5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ba8274-0f10-4748-af09-caba7ed3e83c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78cb46e1-38a4-48c6-9e04-9a257c58ec5d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "25fd6a68-3bf1-4183-9a96-fb33a9a0ac42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "665962f4-641c-45e2-9005-dfb7d3eb687a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25fd6a68-3bf1-4183-9a96-fb33a9a0ac42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7748693-23b8-46b5-a2ce-553309ed07d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25fd6a68-3bf1-4183-9a96-fb33a9a0ac42",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "58477019-776a-4e70-bc25-f47cb6d6df76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3af30352-6d6f-439c-a086-032b9ee9fe46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58477019-776a-4e70-bc25-f47cb6d6df76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cacfb80-7a4c-4043-bd83-01c32f1b32f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58477019-776a-4e70-bc25-f47cb6d6df76",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "066441be-7512-46d3-8430-2c3fbd3fe92b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "33b3951a-9b6b-415e-9229-029e8a53a81e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "066441be-7512-46d3-8430-2c3fbd3fe92b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95ecac3-e3ea-4645-9119-1f3017a9cd92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "066441be-7512-46d3-8430-2c3fbd3fe92b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f37db733-50d7-4b01-9211-df047075f3f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "57974de0-2b08-45f0-a938-2ef178afa15c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37db733-50d7-4b01-9211-df047075f3f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb333e9-a274-45ff-bc29-595b84e9bfcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37db733-50d7-4b01-9211-df047075f3f8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b75cf10b-07c7-4b06-9d9a-bd7578b5525c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fb52a45a-ffd4-4e30-a9b9-eb130baa4c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75cf10b-07c7-4b06-9d9a-bd7578b5525c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e0f8243-28ce-49fd-844f-1b5e6574c3f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75cf10b-07c7-4b06-9d9a-bd7578b5525c",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "355d6d02-91a0-448d-8a8d-75c132fc6bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "dfa60125-7faa-4cfe-bdca-542fa8fb4e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "355d6d02-91a0-448d-8a8d-75c132fc6bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d022eff-54f8-4dde-a2e6-d088dbf59c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "355d6d02-91a0-448d-8a8d-75c132fc6bc6",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "510fcace-c29d-4a1b-9dbe-f037482bc36e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6f777159-fc1a-4e7a-912d-7b4bdf62876b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510fcace-c29d-4a1b-9dbe-f037482bc36e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94eab6c4-196d-4efc-9a53-f1beabb4d9cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510fcace-c29d-4a1b-9dbe-f037482bc36e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "33cc1d21-875a-481c-bd6f-c95219b1672e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "44a35ea7-2ac8-4f3e-b477-7af9c8b42b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33cc1d21-875a-481c-bd6f-c95219b1672e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a608fa2-f269-4083-bd4a-867f80599ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33cc1d21-875a-481c-bd6f-c95219b1672e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2e1f98d5-dcb6-4aa6-b8aa-3e3c6782cf5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d558dc12-6317-4270-ada6-7fe1603c1114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1f98d5-dcb6-4aa6-b8aa-3e3c6782cf5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca32944-6960-48e1-abed-02a32edadaf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1f98d5-dcb6-4aa6-b8aa-3e3c6782cf5a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4ce21952-e417-4f06-b9e3-d32836be1e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "316782f8-d836-49d0-8bcf-c9ab62b13bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ce21952-e417-4f06-b9e3-d32836be1e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e41533-24a8-49aa-bd98-ea36ac85c7cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ce21952-e417-4f06-b9e3-d32836be1e63",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "679085ea-00d8-4016-8aa8-9103c2eb9e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ea6809e3-1024-4adb-8df3-cd73b8da125c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679085ea-00d8-4016-8aa8-9103c2eb9e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d477ae-92dd-4c11-aafd-1e74bd0d4a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679085ea-00d8-4016-8aa8-9103c2eb9e55",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0729c147-39fc-4758-9acf-c8a568df0956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d220d857-70e4-4bbd-9a0b-0ecb752dd3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0729c147-39fc-4758-9acf-c8a568df0956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c18dbf-70a5-4308-ba2a-582f7ecab2c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0729c147-39fc-4758-9acf-c8a568df0956",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fa35b8a5-8d18-4577-83cb-6da9b614082c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d98ca172-50f8-4aa0-b475-f17d80817e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa35b8a5-8d18-4577-83cb-6da9b614082c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dffc5c19-c3ff-44ba-ad9e-774b2ba99bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa35b8a5-8d18-4577-83cb-6da9b614082c",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1d6c431b-80c9-4161-a1c8-06d03ead0faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "173c9c79-624f-4895-8881-66d4578e4d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6c431b-80c9-4161-a1c8-06d03ead0faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e52755-9535-48de-b863-1b56b7727661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6c431b-80c9-4161-a1c8-06d03ead0faa",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fc5a6528-4f32-4160-8317-6995141e2e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "23e1156f-1c5f-4095-828b-92b4a520fe0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5a6528-4f32-4160-8317-6995141e2e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d0f5a4-7ea6-4cc2-adac-cbcccf0268f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5a6528-4f32-4160-8317-6995141e2e59",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e284b782-89f7-413a-8e9a-44a688c43c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a824705d-dcd7-4512-b155-46a67127311e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e284b782-89f7-413a-8e9a-44a688c43c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0308684d-28d4-4eeb-b590-eee6c63294f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e284b782-89f7-413a-8e9a-44a688c43c6e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "488c36e2-69c3-4b8d-854f-6d69394a57c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2b9a8639-51a0-4c76-b803-70e33a356426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488c36e2-69c3-4b8d-854f-6d69394a57c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f63e26e-8864-4ec7-a005-0004f85c5a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488c36e2-69c3-4b8d-854f-6d69394a57c0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "46c3a96f-731c-4763-b0a2-94556dbd4992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "799729bd-69a3-4105-86dd-0f5e27e78925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c3a96f-731c-4763-b0a2-94556dbd4992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa19a0e-be5f-40f4-ac5d-6e890e34f620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c3a96f-731c-4763-b0a2-94556dbd4992",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2e718c04-2e81-460f-bc7e-edf63bbd0214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7a88a012-edde-4653-b6e3-500452c75a0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e718c04-2e81-460f-bc7e-edf63bbd0214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5964b08c-7660-40ca-adf5-4f292fd161c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e718c04-2e81-460f-bc7e-edf63bbd0214",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a134da48-aec9-4b69-9d60-62ffbb3ed866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6ec20593-92ba-4da8-96c4-cc07654768b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a134da48-aec9-4b69-9d60-62ffbb3ed866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b86e0fa-0eee-420f-be9d-569395c864cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a134da48-aec9-4b69-9d60-62ffbb3ed866",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "958c4fd4-bcf1-43d2-aac5-64b079f25bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a6193ad6-52e4-4128-99d5-dbef8ffca129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "958c4fd4-bcf1-43d2-aac5-64b079f25bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ebe875-6eda-493d-a677-110f1f4ad1a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "958c4fd4-bcf1-43d2-aac5-64b079f25bbb",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "3e388cde-7bf7-4705-a037-ccc3b09fc5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "848ab93b-5e8b-476e-b135-5c7306216297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e388cde-7bf7-4705-a037-ccc3b09fc5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fd49e9-d672-492f-b4c7-68995333b172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e388cde-7bf7-4705-a037-ccc3b09fc5b0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1bb68407-7ede-4f8e-a878-55fc10679942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "4b931e20-1a7d-4ba4-910c-42d50a7acdd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb68407-7ede-4f8e-a878-55fc10679942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4597dca7-5741-4320-a557-ad60965c5455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb68407-7ede-4f8e-a878-55fc10679942",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2fb42f6f-ae32-4b9d-b15b-ba217c2a25f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6a35820e-d3ad-4fc2-8097-d66a68bd765e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb42f6f-ae32-4b9d-b15b-ba217c2a25f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1566f5bb-e946-4b10-a12c-2aee25baa400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb42f6f-ae32-4b9d-b15b-ba217c2a25f8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "62d340e9-bd43-4a37-9528-72c27337bddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "19fa7a35-93dc-469e-8fe1-5c1834d6aef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d340e9-bd43-4a37-9528-72c27337bddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce4c7c7-5c26-4ba0-ab81-0679875b734f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d340e9-bd43-4a37-9528-72c27337bddf",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "26e14927-8459-4541-9329-1866da21c7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f79cc1e7-d1fb-4205-ab0a-6934ab8b6fe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e14927-8459-4541-9329-1866da21c7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f674927f-544f-445a-8211-653042d674de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e14927-8459-4541-9329-1866da21c7b2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "98d09ef9-8170-49d5-b1e8-b6451a98d658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "93c6b5d7-b57a-4e2e-a94b-152a4d3bfe3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d09ef9-8170-49d5-b1e8-b6451a98d658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff5cc56-f918-4b80-ad4e-2a2392a6a0eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d09ef9-8170-49d5-b1e8-b6451a98d658",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "8872286c-672d-47ba-a8d7-355302483b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1e9cc824-680e-499b-8613-596589ccfbd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8872286c-672d-47ba-a8d7-355302483b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badc257d-ece6-4497-9a2e-0aba56e93c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8872286c-672d-47ba-a8d7-355302483b96",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9ec376e3-bd21-4f46-b999-9344b00164d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "83a51c16-50ab-4938-b48e-d880754f291a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec376e3-bd21-4f46-b999-9344b00164d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e92ea1c6-b7dc-451e-8ab8-b95925983c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec376e3-bd21-4f46-b999-9344b00164d8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b19e211b-d7ae-4a70-9a19-91088869e08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8cde093a-6ce8-4620-9f7c-f5ba7efa4180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b19e211b-d7ae-4a70-9a19-91088869e08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7145b5-1a67-401e-b19a-173c834215ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b19e211b-d7ae-4a70-9a19-91088869e08f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "81e72f5b-f4df-4d2d-b1af-c1cb93bab402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e45ac022-8f19-4135-a092-1f9b5af7fd84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e72f5b-f4df-4d2d-b1af-c1cb93bab402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efff1bec-2b62-4d23-9e64-ec0c3f3549f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e72f5b-f4df-4d2d-b1af-c1cb93bab402",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "eff402cd-e914-444e-aab4-18f57f1e9a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "793d40b3-77bc-40f7-88b7-c60ec92314d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff402cd-e914-444e-aab4-18f57f1e9a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c1bc214-d7e5-4ac0-9a45-1e021a7741ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff402cd-e914-444e-aab4-18f57f1e9a36",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "af807dd7-66ed-4308-9e55-dc34aa8b2681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f8f99c88-1cde-4ea8-b5a5-5fa9155769b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af807dd7-66ed-4308-9e55-dc34aa8b2681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5692041d-4772-487d-8399-2555d36f4394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af807dd7-66ed-4308-9e55-dc34aa8b2681",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "8742e429-734e-4a83-b2f8-8892546f5d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "395572b2-c92d-41a0-bcd2-75289e93881b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8742e429-734e-4a83-b2f8-8892546f5d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ecd6fc-e566-4990-9be6-d0a8c331e02f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8742e429-734e-4a83-b2f8-8892546f5d2e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0411fb26-0913-4323-ac68-b9077ba3fd0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e175d5c9-bd81-4940-acc4-78777224ac0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0411fb26-0913-4323-ac68-b9077ba3fd0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c108373f-bbf5-41c6-a4c2-2e41198e100a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0411fb26-0913-4323-ac68-b9077ba3fd0c",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "679dae5d-08cc-47ec-b46a-a4a8dbd26f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "60e064c6-1e78-4d8b-a75c-50e73a8f1fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679dae5d-08cc-47ec-b46a-a4a8dbd26f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac80ee8-63e7-4c63-a7e7-3a50a701bd00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679dae5d-08cc-47ec-b46a-a4a8dbd26f28",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ec30798c-5d23-4c70-b6a0-f035102004a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2a37da5b-51aa-4678-893b-1a0fc7dd1333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec30798c-5d23-4c70-b6a0-f035102004a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18c2181-e39e-4409-9032-15bfb0332a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec30798c-5d23-4c70-b6a0-f035102004a0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "50ddf968-9717-4176-b1fa-3ce7af2158ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fdd005ca-a0d6-45e8-be7c-e679d859ce65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ddf968-9717-4176-b1fa-3ce7af2158ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6de2b9-52aa-4aec-b2b1-532de38a5790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ddf968-9717-4176-b1fa-3ce7af2158ce",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0b14d1ed-9407-4a53-9fae-f620597a7ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a5145bb9-b516-49b7-b7d8-8d267bbc88e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b14d1ed-9407-4a53-9fae-f620597a7ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64cb9fcc-e53b-4077-a089-e41d602a7500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b14d1ed-9407-4a53-9fae-f620597a7ab8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "742106db-68f5-4a78-8c0d-642b8663bc67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2733f537-19aa-4633-9e46-ce47ce5db663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742106db-68f5-4a78-8c0d-642b8663bc67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c82319-c7e0-4f50-a152-67e7a7ad4a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742106db-68f5-4a78-8c0d-642b8663bc67",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e037ab23-3561-4909-93e4-f6f604982420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "582db62d-1dc1-4a37-b8f8-29d3f34feba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e037ab23-3561-4909-93e4-f6f604982420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f94a80-dad8-4c86-9de4-1c6dfcd3bf92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e037ab23-3561-4909-93e4-f6f604982420",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "04d8b0b0-50e2-415f-824c-20c76d66b1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "c9eafaa4-f82c-4c80-b616-31f59e0d835a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04d8b0b0-50e2-415f-824c-20c76d66b1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d611624-6d8a-4e8b-b348-b13a65023ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04d8b0b0-50e2-415f-824c-20c76d66b1e9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e62a71a4-04c7-48de-b44f-67a95a82d852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "409efd68-d5da-460c-9ce0-52ba40a8f947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62a71a4-04c7-48de-b44f-67a95a82d852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca16a0a2-f258-4b98-9c8c-324785d40837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62a71a4-04c7-48de-b44f-67a95a82d852",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "c1963a72-6381-43be-899f-32cbdf4143c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1a238cae-dac4-4d31-9d4f-4bbc772ca549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1963a72-6381-43be-899f-32cbdf4143c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131be76f-452b-442f-bc62-d08748cc9ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1963a72-6381-43be-899f-32cbdf4143c2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9c9e12f1-8d26-4e8f-89f9-fcd344e8f636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "397e1138-aee5-4fba-b072-f4e36eb76d8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c9e12f1-8d26-4e8f-89f9-fcd344e8f636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beff3d67-765b-4940-a7d8-2eb35c4f2cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9e12f1-8d26-4e8f-89f9-fcd344e8f636",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "e1b8d7ab-a2bd-4515-95cc-c916d79c9b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "979f9f56-ef82-4e6a-ae19-3c2091be7f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b8d7ab-a2bd-4515-95cc-c916d79c9b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330631cf-bcb9-4c5e-b052-dbb6743fb2f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b8d7ab-a2bd-4515-95cc-c916d79c9b42",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "10b2ded4-faa9-4dba-abc8-e16b549a2823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5092c58e-93e7-4415-83a8-9886505d2eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b2ded4-faa9-4dba-abc8-e16b549a2823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658a04c8-034e-4eb4-947e-a17fc01a37c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b2ded4-faa9-4dba-abc8-e16b549a2823",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "d7d5606e-6898-4f90-9c22-075961d4cc2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "63006735-86cb-49cc-a060-600a1ec50455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7d5606e-6898-4f90-9c22-075961d4cc2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d090fa-0efa-4856-942e-c5a679867a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7d5606e-6898-4f90-9c22-075961d4cc2b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "7531da9a-d1d4-47b6-9041-9493ab9aa3fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a361f4a8-3a59-47fd-a4d4-cfa01a94b37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7531da9a-d1d4-47b6-9041-9493ab9aa3fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e25c92-e94f-4879-8b9c-adde3a36fb97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7531da9a-d1d4-47b6-9041-9493ab9aa3fb",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5d8d985d-b399-447b-ae70-676bb98613be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "50d5ee39-41f1-4d29-a4fa-fda3e8453e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8d985d-b399-447b-ae70-676bb98613be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402c334d-58a2-436e-b6c6-85816eeed213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8d985d-b399-447b-ae70-676bb98613be",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2dafb811-fffc-49b8-b271-3958c8471ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3ebc2781-488c-4375-86d1-842342fb19bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dafb811-fffc-49b8-b271-3958c8471ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38dc64b-3e2d-4cac-81e3-661941fb42e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dafb811-fffc-49b8-b271-3958c8471ae4",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "372af3f8-4c08-4ac0-8b4a-853c4d785c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ef21afb1-05b7-4137-a521-412fc1d51f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372af3f8-4c08-4ac0-8b4a-853c4d785c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f22edc-a1fb-4c3d-9ba9-49baaa13aae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372af3f8-4c08-4ac0-8b4a-853c4d785c2f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0ecc77e4-e9a3-4b3c-af8e-7c828f4417db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9ce92ee6-aa7f-48b4-a326-9c0320a93188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ecc77e4-e9a3-4b3c-af8e-7c828f4417db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fddc311-6c18-4256-a1a9-ad68eb928942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ecc77e4-e9a3-4b3c-af8e-7c828f4417db",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "d5f73ac2-5729-4160-9a8a-98dbb0e1c2a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e4f414e5-c230-4670-b8ad-99b79a4380d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f73ac2-5729-4160-9a8a-98dbb0e1c2a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "271295ce-1796-4286-a402-914396c74a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f73ac2-5729-4160-9a8a-98dbb0e1c2a8",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ba6a6ad8-6e7f-4ec9-a9f9-b0b9497d80fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f965da5f-bb03-4c65-8d2a-e557cc73781e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6a6ad8-6e7f-4ec9-a9f9-b0b9497d80fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3d9bae-380f-4ce1-9630-4e155a54208b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6a6ad8-6e7f-4ec9-a9f9-b0b9497d80fd",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "18973c69-77d1-4f6d-95cb-390499686dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "c6dc419c-ba1d-43e1-bee4-99ebdefc7f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18973c69-77d1-4f6d-95cb-390499686dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d617e6ca-7f33-4aa4-807a-9a64024d1527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18973c69-77d1-4f6d-95cb-390499686dbe",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a12d3040-852d-4373-a490-33cf28634854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "860ed0a0-40f7-47e1-8221-e6195bebfb2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a12d3040-852d-4373-a490-33cf28634854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40c343d-00d5-4635-8a13-032ae21bcc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a12d3040-852d-4373-a490-33cf28634854",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fc3dd8d0-138f-4889-91dd-1789095d9ab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d0c9ab7f-b484-4c5b-a040-dbf465058a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3dd8d0-138f-4889-91dd-1789095d9ab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a5bc508-8fe1-4e3d-a2af-fa596835292b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3dd8d0-138f-4889-91dd-1789095d9ab6",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "808c9da3-9d16-4a7f-9e49-49ea338cc7c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6746dfb9-7893-4824-b072-a5820e6158bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "808c9da3-9d16-4a7f-9e49-49ea338cc7c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96c6b7a0-c5de-4402-baf5-5aef5df258de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "808c9da3-9d16-4a7f-9e49-49ea338cc7c3",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6cc1cfab-bb0d-410b-bdb8-713e5cce3d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a92311db-87d3-47bc-997a-2ff8ed12b8eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc1cfab-bb0d-410b-bdb8-713e5cce3d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7a3465-8da4-4c3a-a7d8-76e9eba701b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc1cfab-bb0d-410b-bdb8-713e5cce3d2e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4c4ee394-7e42-4fd4-8d02-a66490d3c8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "50ab878e-713d-4bcd-91c9-61a06893eb71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4ee394-7e42-4fd4-8d02-a66490d3c8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97363bc0-ee76-4a79-a16f-56e7eeba959c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4ee394-7e42-4fd4-8d02-a66490d3c8ec",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "78a6342b-6b9f-40f3-b3a9-87c2fad4708e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1118b873-2c39-4f74-a346-6eb1f3e84d12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78a6342b-6b9f-40f3-b3a9-87c2fad4708e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a55c2c-c489-4d7b-a6f5-8d2345a49f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78a6342b-6b9f-40f3-b3a9-87c2fad4708e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "3a4bbb0e-b464-4f97-af56-b0485b02c82a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "000addc8-7b13-4596-9cc7-f57e095dccb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4bbb0e-b464-4f97-af56-b0485b02c82a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b829c4cc-d0d2-4d1c-a4cf-05985c0bb23f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4bbb0e-b464-4f97-af56-b0485b02c82a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b358c7c0-0c54-4b3a-836c-b41654713c93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d7471f99-dd54-4dc7-bfd5-d0166ab5fd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b358c7c0-0c54-4b3a-836c-b41654713c93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4f10e5-0a82-412a-bcef-1be32fcc1dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b358c7c0-0c54-4b3a-836c-b41654713c93",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "77e30b17-c4db-4c11-b8a6-67526f457a03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f03c0f09-4a25-4755-959a-af31502e42f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e30b17-c4db-4c11-b8a6-67526f457a03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6983c7fe-3d79-43d9-8a5d-f94e65b14013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e30b17-c4db-4c11-b8a6-67526f457a03",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a279e581-9966-4a96-8942-938ed93818a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "c52dcf32-5125-4ce6-b702-3d6ec7b13740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a279e581-9966-4a96-8942-938ed93818a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5a7c3b-4e77-4ab3-bdeb-2560d54377f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a279e581-9966-4a96-8942-938ed93818a5",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "427c2f52-2df7-481a-be9b-5a2bd943befc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "051fd043-fa2e-448b-8651-55bb2ef1a045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427c2f52-2df7-481a-be9b-5a2bd943befc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41d27ab4-7d92-4fed-ae17-15bf34530a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427c2f52-2df7-481a-be9b-5a2bd943befc",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a0ea2036-d0cc-4157-8827-02218a1d2aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "685e1311-c26f-43fb-a245-584e00513980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ea2036-d0cc-4157-8827-02218a1d2aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5090df-5b4c-42d4-a0bc-7804f338c9fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ea2036-d0cc-4157-8827-02218a1d2aa9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1fb059ab-9d6e-4f0e-8499-d446f491ae69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5cadc5a9-b927-4945-bd02-1ff77b2feb26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb059ab-9d6e-4f0e-8499-d446f491ae69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f92c264-d0b3-4995-ab3c-e659393d690c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb059ab-9d6e-4f0e-8499-d446f491ae69",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "20919e06-b9c2-4e8a-8041-bf27f985d900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "c1bd6cd7-65eb-405d-aa34-e00d54a99651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20919e06-b9c2-4e8a-8041-bf27f985d900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de028935-ccde-4794-baef-d8d6b58cecab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20919e06-b9c2-4e8a-8041-bf27f985d900",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f7ae2d73-6d51-4d22-8851-36e113e34858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "edf33f80-1dbc-4f55-b324-7bae2ef3ce22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ae2d73-6d51-4d22-8851-36e113e34858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c24704-5c12-4ae1-87f8-10cac04041f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ae2d73-6d51-4d22-8851-36e113e34858",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fd7372a7-0b4f-448d-a816-5bc5f8dfac4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d52e254a-8800-4372-ab13-219555ff2a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7372a7-0b4f-448d-a816-5bc5f8dfac4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ddcdc07-038d-43d9-b0bb-d9bbcf544d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7372a7-0b4f-448d-a816-5bc5f8dfac4b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "43311a1b-381b-4cb9-84bd-49e0deafe976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3e2597f5-8cee-473f-b13a-2609c23b0439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43311a1b-381b-4cb9-84bd-49e0deafe976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e48ca61e-68d7-4669-9da4-5f7c273179f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43311a1b-381b-4cb9-84bd-49e0deafe976",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "dbf566dc-f2fb-4250-ba4d-3281baea3b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b96e245a-f1aa-4ec9-aba0-c929aa3c5446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf566dc-f2fb-4250-ba4d-3281baea3b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22647aa8-d02d-4f01-95cb-c14c677c6574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf566dc-f2fb-4250-ba4d-3281baea3b7d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "c6aa3c27-ae0b-4672-9c89-87b1d0641152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2d382594-d676-4945-a2e6-6608ffe7de0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aa3c27-ae0b-4672-9c89-87b1d0641152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023f524a-9c42-45e7-a14d-886cd7c187c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aa3c27-ae0b-4672-9c89-87b1d0641152",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9ae843c5-ec06-416c-a9bb-035876f840b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7294411e-ed5c-4760-883c-8eeae5ea8e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ae843c5-ec06-416c-a9bb-035876f840b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f953d726-3747-46d6-a024-03d3081eea0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ae843c5-ec06-416c-a9bb-035876f840b0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "7c8bfd37-9d8d-49a7-97d4-dfa78f3af00f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "22dd51fc-a4fe-4b1a-b695-2f7994e6f3fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8bfd37-9d8d-49a7-97d4-dfa78f3af00f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87438a0-56b9-42db-94b8-5438326066fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8bfd37-9d8d-49a7-97d4-dfa78f3af00f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "43cf7847-6055-4dd8-92c2-4518c4499483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "05cb440a-6061-4807-8857-455a4a686910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43cf7847-6055-4dd8-92c2-4518c4499483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d83c64b-6518-46ef-9fa7-69d968332da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43cf7847-6055-4dd8-92c2-4518c4499483",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b8969c2e-cd21-4a92-83d6-396078a85861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "af08207f-4c8f-4e05-a51c-fc3627259520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8969c2e-cd21-4a92-83d6-396078a85861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a92aea3-854f-4148-b642-6d0ed29d19ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8969c2e-cd21-4a92-83d6-396078a85861",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ea14d77a-8b62-4633-b918-1b5822755dd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7546fb93-4188-4102-86fa-089c829e2b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea14d77a-8b62-4633-b918-1b5822755dd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06ef4f16-d8e6-472e-a805-46698c56e001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea14d77a-8b62-4633-b918-1b5822755dd2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "79d72178-d7ee-4ecd-8144-1468034ab405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "41196ffd-6451-4eca-8029-bc15c85d7e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d72178-d7ee-4ecd-8144-1468034ab405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59e76a3-6fb7-459e-a026-ca2707a79eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d72178-d7ee-4ecd-8144-1468034ab405",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f38b4559-97fe-4cf3-ba22-a90251fed72d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1b64b2b1-1c90-4b7a-bfe3-4139652dc615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38b4559-97fe-4cf3-ba22-a90251fed72d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eba58e9-4d41-4412-9459-685d00d1530a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38b4559-97fe-4cf3-ba22-a90251fed72d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "12570fd7-a0b6-4834-b145-5f601503478d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "637ea9d0-6ba9-4aae-9f94-1c40ac3f794b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12570fd7-a0b6-4834-b145-5f601503478d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfe8994-be9e-494b-9b2a-e620306554cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12570fd7-a0b6-4834-b145-5f601503478d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "9cb078b0-18ed-45f7-b838-35472d2275e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "15810509-88ca-4c05-ab59-520546ba8eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb078b0-18ed-45f7-b838-35472d2275e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aee70d89-8e8e-43d6-8262-b07fedeabc4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb078b0-18ed-45f7-b838-35472d2275e1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "3ba6010a-60ce-4a56-890a-bb1abfebd5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9271cae5-aeb1-412c-85b0-e601f1a7f4d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ba6010a-60ce-4a56-890a-bb1abfebd5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62fb0c71-94fe-4298-96a7-78e7ffa17085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ba6010a-60ce-4a56-890a-bb1abfebd5a2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "29cb5da1-d34b-41b9-b85d-1ab7877c235a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fae5ca1b-2962-4f16-ada8-58557e0a87b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29cb5da1-d34b-41b9-b85d-1ab7877c235a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920b32d3-833b-4a4f-bd83-000a109b58a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29cb5da1-d34b-41b9-b85d-1ab7877c235a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1d03f74c-3a5e-49df-a48b-efdf69bde9da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9d1f2d12-d5f4-4c19-834b-2789c639d324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d03f74c-3a5e-49df-a48b-efdf69bde9da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77412257-b56c-4002-a0e5-d4df138ded8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d03f74c-3a5e-49df-a48b-efdf69bde9da",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4a9228c5-387c-4eaa-81bb-72312795ec35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e821eafc-a7d3-4e48-ad1a-0197235099da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a9228c5-387c-4eaa-81bb-72312795ec35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1a4d4a-0fb7-48ff-a66f-a7cc6633bce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a9228c5-387c-4eaa-81bb-72312795ec35",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a80696ec-e179-4228-ae7a-9e35db32727e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e8ed86db-8dc4-40e6-bd00-8b083c926483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80696ec-e179-4228-ae7a-9e35db32727e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4106e92a-554d-4abe-a318-357f33aada4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80696ec-e179-4228-ae7a-9e35db32727e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5ab1c0d7-c153-4f93-87c6-49b8580de738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3bc97b19-c83c-4bf2-8583-33cb2b417806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab1c0d7-c153-4f93-87c6-49b8580de738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302c0605-e2c8-47d8-b26f-fbaabe9eda69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab1c0d7-c153-4f93-87c6-49b8580de738",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ef46d910-70d4-4ffa-89b8-ca2673e1313f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7b46b97a-03e7-43e3-a420-3e553c1a8bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef46d910-70d4-4ffa-89b8-ca2673e1313f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e41c651-3b55-4643-b9da-18e9f0dd7d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef46d910-70d4-4ffa-89b8-ca2673e1313f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ee703927-34b8-401f-a883-567e5737f891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b01f8370-1b65-44fd-bf0a-fb99a25053f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee703927-34b8-401f-a883-567e5737f891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78ccc74-a285-4bf2-a1a4-6499b6a13c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee703927-34b8-401f-a883-567e5737f891",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "7366a3d1-9aa5-41a0-b8dc-47e14617ae06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a56e5a19-7739-419e-bd99-4f1643d78295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7366a3d1-9aa5-41a0-b8dc-47e14617ae06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23726e5e-1637-421a-a9fb-4296a440321d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7366a3d1-9aa5-41a0-b8dc-47e14617ae06",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "8998e420-9843-485d-9756-97da2bd050d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "94a69f9b-bf1e-485d-9f02-e0c384af4c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8998e420-9843-485d-9756-97da2bd050d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d457d2-bb5b-4f6a-b4a0-33430b64583a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8998e420-9843-485d-9756-97da2bd050d1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a6f00045-01ed-4ecd-a279-d5395cf4faca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a233479c-bb6c-45d4-8d77-6e8c84de9a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f00045-01ed-4ecd-a279-d5395cf4faca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d5b095-4dd3-4d39-9415-9cb0de02a6ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f00045-01ed-4ecd-a279-d5395cf4faca",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ba449727-ba74-4b07-925c-9bb21fb2c6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2a9c0ad2-08f8-4f62-a6ad-b10ac995d6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba449727-ba74-4b07-925c-9bb21fb2c6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2671e901-9d76-4b68-ab9a-db9bf4221620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba449727-ba74-4b07-925c-9bb21fb2c6fc",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "96966e50-3a27-465c-96b2-730153040ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "fe7bbc20-2463-4326-8276-1a94d652c931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96966e50-3a27-465c-96b2-730153040ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c65bdf-8213-4a75-b2e8-8c079ac8c30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96966e50-3a27-465c-96b2-730153040ed2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5941bc51-513f-4c64-b998-2a5063648773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e50c0905-73b6-4496-92b5-101f1f91a78d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5941bc51-513f-4c64-b998-2a5063648773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45df0451-16e8-4579-825f-8efe3558ff9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5941bc51-513f-4c64-b998-2a5063648773",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fb6090db-eef9-4cd0-b581-26cf10c7d42d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "631f9fc7-0317-48c9-87dd-a707b75a652c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6090db-eef9-4cd0-b581-26cf10c7d42d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab082d8-20a1-437d-a712-2631dada47e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6090db-eef9-4cd0-b581-26cf10c7d42d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a26379af-d10d-45ac-8870-b8989589de4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "914b0641-2ec1-4991-ad93-0bdee69812e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26379af-d10d-45ac-8870-b8989589de4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44085fbe-8081-451e-ba98-5a807b7bea05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26379af-d10d-45ac-8870-b8989589de4a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "73c490d6-0c06-4d8d-bb30-5fa48c39b9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5dc74346-66a8-4ad4-b50d-a4ef5e6cc548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c490d6-0c06-4d8d-bb30-5fa48c39b9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "012a6817-384a-49ca-b49b-96ffb24d0d31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c490d6-0c06-4d8d-bb30-5fa48c39b9c6",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "304a40c1-c748-4b56-afe1-6a9520b97812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8185d978-a507-4119-b927-eb2d349a8688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "304a40c1-c748-4b56-afe1-6a9520b97812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8823241-acaf-4ca6-8c6d-de44d786946b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "304a40c1-c748-4b56-afe1-6a9520b97812",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ae19c17b-8585-469a-b3d0-c13cc66697f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f60af6cf-6aaf-43c4-ae7a-ecb655a640f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae19c17b-8585-469a-b3d0-c13cc66697f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aa5a4f4-07d0-4756-9519-8e137598a189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae19c17b-8585-469a-b3d0-c13cc66697f7",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4332653b-c934-47dd-8f6c-40da94674872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e75ca806-44bb-466c-b2e3-aa0e5f5c75c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4332653b-c934-47dd-8f6c-40da94674872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6f1157-e428-495e-9d26-e4635726ed9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4332653b-c934-47dd-8f6c-40da94674872",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1d139f0d-a72a-4723-97e1-122ff5278342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "75ee0fba-5ed2-45df-af3d-781d72ca0150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d139f0d-a72a-4723-97e1-122ff5278342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd96e58f-d98e-4b79-b88a-4d3c4e894d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d139f0d-a72a-4723-97e1-122ff5278342",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "cb58f307-874e-4dc5-aaa3-9c45420abe82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "50b824fa-9b7e-4ca8-903f-0b1bc755466f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb58f307-874e-4dc5-aaa3-9c45420abe82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a45d36-bec9-4bea-b398-fe31d772c415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb58f307-874e-4dc5-aaa3-9c45420abe82",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "37dc04d0-1e61-4278-8789-4fad3fd800db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "17d2d9b8-f009-43af-9546-eddef32e59b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37dc04d0-1e61-4278-8789-4fad3fd800db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52ab696-de84-4fa4-9542-cfc31caf34f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37dc04d0-1e61-4278-8789-4fad3fd800db",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5a97a574-90f8-4f4b-bd1e-1d060cf9e2a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8b9c5eb4-788f-4327-aa01-78a857a4aa1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a97a574-90f8-4f4b-bd1e-1d060cf9e2a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8e30f5-632c-4aeb-a4ac-880a1baccaca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a97a574-90f8-4f4b-bd1e-1d060cf9e2a0",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1673452b-0938-4168-837c-41ed72b2bcdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "10510320-b12f-4192-86d2-5d20fb35348e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1673452b-0938-4168-837c-41ed72b2bcdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca663108-661c-435d-9eaa-25007eaddfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1673452b-0938-4168-837c-41ed72b2bcdd",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "8c239bdc-0564-41cb-860b-2f2e70c06df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "14809ee0-adc0-41e4-89f7-6888508ca03e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c239bdc-0564-41cb-860b-2f2e70c06df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18bf492-75c6-4a07-9544-72f15889cb57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c239bdc-0564-41cb-860b-2f2e70c06df4",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "05280cf3-6bf0-4bca-addf-4b2532a65cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "23ce5d83-e9d2-4a7c-83ba-c1760fbdfc24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05280cf3-6bf0-4bca-addf-4b2532a65cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e39bcf-3835-4ccc-87fa-e56675f71227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05280cf3-6bf0-4bca-addf-4b2532a65cd3",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "d6135989-3b8a-41f1-a4c2-0e70cb043edc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "1853bf7d-9b51-45ff-a1f0-67c43862e632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6135989-3b8a-41f1-a4c2-0e70cb043edc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39289fd-5622-48c3-bcc9-9daf5532badb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6135989-3b8a-41f1-a4c2-0e70cb043edc",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f02d8459-257c-47d4-a3ae-ddb69a77c87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9426e022-67ac-4092-a6f0-152596e71dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02d8459-257c-47d4-a3ae-ddb69a77c87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152e0e0d-b171-4df8-a751-6dcce121ce50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02d8459-257c-47d4-a3ae-ddb69a77c87e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ec9ff87c-5a31-4490-9e85-1de45090c30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "d30b0ca5-bb0e-4ae6-abba-a13ed2adee59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9ff87c-5a31-4490-9e85-1de45090c30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53ffd66-f0ef-46b2-8238-cab6779c0fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9ff87c-5a31-4490-9e85-1de45090c30e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ef468d5b-2531-4b49-b165-6a116ec2c8e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "f2d9266c-5b72-42e7-bc66-00e529001e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef468d5b-2531-4b49-b165-6a116ec2c8e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097dbdb4-0315-466c-8cf6-70c60bd933a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef468d5b-2531-4b49-b165-6a116ec2c8e1",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "f283b63d-3698-4623-9ee5-4884151e14e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "955b493c-9c4d-449b-9b8d-d3b56d73caa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f283b63d-3698-4623-9ee5-4884151e14e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a885a4a8-7a18-4897-85f0-783d83188121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f283b63d-3698-4623-9ee5-4884151e14e9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "be423088-a435-4ad7-92eb-4bfe667fb1a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ee20da44-4096-4b33-bf55-ac28fdf6f70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be423088-a435-4ad7-92eb-4bfe667fb1a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9cae821-1982-4d6b-bc7c-e3e7bceb4866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be423088-a435-4ad7-92eb-4bfe667fb1a7",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "7c314750-c1f2-480f-a727-5a66f9f85410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "c5f4ff72-b164-41f2-9ec2-ed1d9f0c0c44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c314750-c1f2-480f-a727-5a66f9f85410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db683633-7d30-4881-97f5-3ca065526cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c314750-c1f2-480f-a727-5a66f9f85410",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "22796e7d-8a04-419e-b0a0-1e6b1de8114e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ff6941c8-2cfb-4df5-b257-7a81edd3b249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22796e7d-8a04-419e-b0a0-1e6b1de8114e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5590010-5b4f-4562-a4e2-8d99cbe0cef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22796e7d-8a04-419e-b0a0-1e6b1de8114e",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "3fdd3bb0-df94-4ad0-b112-547a4116dd4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "cbed23d3-505c-4664-8bdd-9b1dfc271f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fdd3bb0-df94-4ad0-b112-547a4116dd4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2912092a-95df-480f-8d6b-2177dfe7cb0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fdd3bb0-df94-4ad0-b112-547a4116dd4f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "59a556fd-5053-461b-87b8-c27a0493c6ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "7f9a0472-0520-4036-9255-2cf3ca54ff6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a556fd-5053-461b-87b8-c27a0493c6ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57835f77-ac99-44b5-a8fd-2192003600bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a556fd-5053-461b-87b8-c27a0493c6ad",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "00c31c68-05b0-4cae-9d72-5c4aff7c1a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "8e044e74-dcdc-4dc2-b9c3-0e81aa942a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c31c68-05b0-4cae-9d72-5c4aff7c1a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3739aff8-20f4-49e5-91b8-d6d11ccb6a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c31c68-05b0-4cae-9d72-5c4aff7c1a9b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4986b701-a03f-4dbc-aa68-d8efcf5764e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a2f9aac7-07cc-4696-801f-51cf366fa31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4986b701-a03f-4dbc-aa68-d8efcf5764e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3594e5-4cc2-4914-9ad3-bc90b04a843b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4986b701-a03f-4dbc-aa68-d8efcf5764e9",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "d1016f76-9808-4a30-bbdc-9a4836573d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "e96b99cf-ac9b-47e8-9cd9-b4d5377a03cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1016f76-9808-4a30-bbdc-9a4836573d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc4cc6d-cc24-4a54-b755-1f5b936bbee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1016f76-9808-4a30-bbdc-9a4836573d8a",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "42835c03-cb7c-4872-9fb8-fd48bcaba44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "5057bdd7-fc7e-4d23-8c9e-6585ea70f8dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42835c03-cb7c-4872-9fb8-fd48bcaba44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b73433-0041-4534-b482-d945988a07fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42835c03-cb7c-4872-9fb8-fd48bcaba44f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "de4c6107-f067-4868-903b-597da6e4f146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "6d342fac-5046-49cb-8a45-33a9fc9dcad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4c6107-f067-4868-903b-597da6e4f146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf416fe4-5f3d-4413-b988-1d32d0127e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4c6107-f067-4868-903b-597da6e4f146",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "ce399914-cd6b-4e73-b2ef-2ac0b7dabfe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "12989377-fa49-433f-8bad-415533b63254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce399914-cd6b-4e73-b2ef-2ac0b7dabfe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f34a71b-46d4-49b9-a077-71e1658c675a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce399914-cd6b-4e73-b2ef-2ac0b7dabfe2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "0b247e64-82bf-44fc-afd5-54dd0ca67fe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "a4f1d357-5277-4264-8ac8-009708c9f996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b247e64-82bf-44fc-afd5-54dd0ca67fe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77eb23e-7218-4c9c-ad84-4dfcd4798c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b247e64-82bf-44fc-afd5-54dd0ca67fe2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "674cd17e-d6fa-41c0-8e9d-1c5e95ed98db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ff9bb2dd-2d9a-45d9-b8a3-7df1eea2621f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "674cd17e-d6fa-41c0-8e9d-1c5e95ed98db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef500fa3-c2ec-49c9-9ad7-3bdd6270ab35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "674cd17e-d6fa-41c0-8e9d-1c5e95ed98db",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6caa9659-c23b-4f22-8277-c473c428f84f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ef779f49-d79a-45df-a3cc-90a9f70604f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6caa9659-c23b-4f22-8277-c473c428f84f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59bfe4e3-b85a-4259-81ff-99405f952cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6caa9659-c23b-4f22-8277-c473c428f84f",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "a996d663-4b7b-4143-adfa-f83d82863a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "2ec5315f-fbb0-436f-985e-f7832480b6a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a996d663-4b7b-4143-adfa-f83d82863a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e45839a-b96c-4876-b28b-2fa7ef9bb0e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a996d663-4b7b-4143-adfa-f83d82863a53",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "b4830088-fc38-4c3f-b275-ba0736088c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9a5ea319-50ec-4948-88e3-c5d700e5e7e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4830088-fc38-4c3f-b275-ba0736088c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd2afe4c-08f6-417b-b7cb-52e5a681349a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4830088-fc38-4c3f-b275-ba0736088c4d",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "750c1634-fc1a-4a3a-a4d7-ef822fe41b85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "493ae010-37c4-428c-9ce2-50e3daa7c523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750c1634-fc1a-4a3a-a4d7-ef822fe41b85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e0d8c4b-08b1-426c-9635-81cd2ece2fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750c1634-fc1a-4a3a-a4d7-ef822fe41b85",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "4611ec7b-b643-4c10-ab83-b56ca12bbbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "28989b1f-a39c-443d-9dd0-5ae01af469a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4611ec7b-b643-4c10-ab83-b56ca12bbbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdadd86-779f-442c-b498-963fba7c15ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4611ec7b-b643-4c10-ab83-b56ca12bbbdd",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "2aa44dbc-27ac-4f35-932e-237c58c895ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "03178d80-887f-4a75-92fe-5e2dd44e3aff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa44dbc-27ac-4f35-932e-237c58c895ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "358ef074-9399-4363-9dc6-f5e23e0eba04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa44dbc-27ac-4f35-932e-237c58c895ba",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "fe93ce3a-62c9-4d55-98a2-e8ea42f5f065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "43431228-c37e-481a-99de-7969a4f5af2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe93ce3a-62c9-4d55-98a2-e8ea42f5f065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074bf5b4-397e-4131-83db-d1b80b343662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe93ce3a-62c9-4d55-98a2-e8ea42f5f065",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "72de0cc4-fecf-481e-b593-673edf341397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "67edd26e-ace3-40e9-a34b-fbcdabc8132d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72de0cc4-fecf-481e-b593-673edf341397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425d1997-e003-4888-89b2-8ad1f3605f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72de0cc4-fecf-481e-b593-673edf341397",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "37946fab-b1cd-4ab7-9a54-951008e49331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "3703c909-68c4-4d2b-8620-e0f318320caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37946fab-b1cd-4ab7-9a54-951008e49331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50f6c57-44d1-4e9a-8733-1603806af33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37946fab-b1cd-4ab7-9a54-951008e49331",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "82b2694c-6bc2-4b72-b7d8-b14565a8eb20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b74e95a0-c3b0-4b45-99b7-68ef451fea71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b2694c-6bc2-4b72-b7d8-b14565a8eb20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92edfb14-4061-4520-8e20-b821ad077961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b2694c-6bc2-4b72-b7d8-b14565a8eb20",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "eaee7743-17f3-4d0e-ada4-06a14214dc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "48d5e44c-7b0b-4eab-9e70-81a0ef0b393e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaee7743-17f3-4d0e-ada4-06a14214dc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a0124c-8293-4a6a-b50a-6f8ff19127ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaee7743-17f3-4d0e-ada4-06a14214dc22",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "6ea04d8c-37bd-46bd-b9b7-cb034026db0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "492333c2-c6bb-41d6-a1fd-9c3b9610006d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea04d8c-37bd-46bd-b9b7-cb034026db0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66a87e5-a697-46d5-a43e-90108c7ddfd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea04d8c-37bd-46bd-b9b7-cb034026db0b",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "208c70bc-8df7-48de-aa69-dd37513b2836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "287c5f24-d336-4704-9734-45d5d8103767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208c70bc-8df7-48de-aa69-dd37513b2836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ad0f3c-3acf-4a2f-ad42-43f61840f2be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208c70bc-8df7-48de-aa69-dd37513b2836",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "14d6c437-460e-4e1f-9af6-25418b1651e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "18f318c7-a185-470a-a594-e4b8679e335e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d6c437-460e-4e1f-9af6-25418b1651e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59327ce-37da-4e32-839f-74737395d4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d6c437-460e-4e1f-9af6-25418b1651e6",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "c4cf0e11-51f6-4f73-9e9f-49e2f3239d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "b97fcca4-b9da-4b37-9917-32ffbcddfaec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4cf0e11-51f6-4f73-9e9f-49e2f3239d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8fbcab1-a3e1-430d-8663-2c3b39b90cdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4cf0e11-51f6-4f73-9e9f-49e2f3239d62",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "5e6812eb-0eb0-47a0-8166-c24850207ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "ab17f36c-e9cd-4a43-95a6-229a355a441a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e6812eb-0eb0-47a0-8166-c24850207ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8810e457-6e7e-48d8-b6c2-018e1bae2802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e6812eb-0eb0-47a0-8166-c24850207ea2",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "1cac975a-82d1-45ca-ace5-2548224a3cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "9d1c36cd-a459-4e27-8c8c-7f8a5736da8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cac975a-82d1-45ca-ace5-2548224a3cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc51645-24c7-4058-9954-cb7c99fd14e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cac975a-82d1-45ca-ace5-2548224a3cc3",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        },
        {
            "id": "55e46cc0-0d30-4473-9a44-ba68955c8ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "compositeImage": {
                "id": "14732430-ed30-4857-9621-9cfe54515218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e46cc0-0d30-4473-9a44-ba68955c8ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dd825df-f75a-4779-8447-98cad7bf5fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e46cc0-0d30-4473-9a44-ba68955c8ea3",
                    "LayerId": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "0fb2deee-5921-4f78-8f3a-1816ce9b82f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6597654f-0a28-46c4-ad87-769237737866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}