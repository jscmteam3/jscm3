{
    "id": "704589ef-88a2-452c-860f-c312bd41bc92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FrameBullets",
    "eventList": [
        {
            "id": "14cf880e-f207-42e6-aacc-90368f538609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "704589ef-88a2-452c-860f-c312bd41bc92"
        },
        {
            "id": "6cec9103-c86c-401e-a10a-44d12fd442ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "704589ef-88a2-452c-860f-c312bd41bc92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "707e2bf9-8bac-47c0-8f2f-ecce21c483af",
    "visible": true
}