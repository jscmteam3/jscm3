{
    "id": "171e51ba-bb6e-4984-8670-619476052b60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemHintPaper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69747498-1ace-42cf-ae6f-bae1de65782e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171e51ba-bb6e-4984-8670-619476052b60",
            "compositeImage": {
                "id": "e755926d-6bb7-4452-8a4e-4ea7ffa1e66d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69747498-1ace-42cf-ae6f-bae1de65782e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a54eb53-b239-4826-9dd1-63f47b7e17d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69747498-1ace-42cf-ae6f-bae1de65782e",
                    "LayerId": "e1d4690a-8b5c-452f-ae40-00385ed8412e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1d4690a-8b5c-452f-ae40-00385ed8412e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "171e51ba-bb6e-4984-8670-619476052b60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}