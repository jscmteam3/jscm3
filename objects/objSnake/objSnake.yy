{
    "id": "eda6048b-f7eb-4163-9e06-e86c94204160",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSnake",
    "eventList": [
        {
            "id": "b5e97e5a-2d08-4afc-ab0b-16671bd6798c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        },
        {
            "id": "8baee61b-90a5-424b-9fc4-8e8f1ba55c74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        },
        {
            "id": "f5d8294a-6be8-4f6b-8cb1-73dcb9789863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        },
        {
            "id": "3f0b95f8-7546-431c-8e75-acc38900e3c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        },
        {
            "id": "6bc56a75-8748-4e41-ab68-a835f849461e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        },
        {
            "id": "c9e5d39b-13e5-420d-ab2e-4ecfbe21d0a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "eda6048b-f7eb-4163-9e06-e86c94204160"
        }
    ],
    "maskSpriteId": "402ebeb1-9b70-4ffc-9a4a-cf9b50671f48",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
    "visible": false
}