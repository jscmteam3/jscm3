{
    "id": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_axeATDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 21,
    "bbox_right": 83,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "302264d4-4b6e-4e5b-ba71-1211723b65e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
            "compositeImage": {
                "id": "d8bc0a75-cac3-49cd-8800-7145c5957e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302264d4-4b6e-4e5b-ba71-1211723b65e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8dc306b-ded3-4f5c-ace2-a775a8ed2a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302264d4-4b6e-4e5b-ba71-1211723b65e9",
                    "LayerId": "ebde762a-2a37-47cc-ac5a-0b5073291121"
                },
                {
                    "id": "ad7872af-16b6-429f-b5d9-3dd902e67c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302264d4-4b6e-4e5b-ba71-1211723b65e9",
                    "LayerId": "922ab51c-6336-40d5-9947-03e7f51dd25a"
                }
            ]
        },
        {
            "id": "98e33882-bf1c-4021-af97-64d2422ce699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
            "compositeImage": {
                "id": "854bd705-3443-43bf-87f0-6414b7f70ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98e33882-bf1c-4021-af97-64d2422ce699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a81ea0f-f4b7-4ea9-a620-4cfe839ad21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e33882-bf1c-4021-af97-64d2422ce699",
                    "LayerId": "ebde762a-2a37-47cc-ac5a-0b5073291121"
                },
                {
                    "id": "4ce584ab-3075-4042-8f36-b7ad43ee26a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e33882-bf1c-4021-af97-64d2422ce699",
                    "LayerId": "922ab51c-6336-40d5-9947-03e7f51dd25a"
                }
            ]
        },
        {
            "id": "99cc4c06-bf8a-4442-95d1-5d95e5a9044e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
            "compositeImage": {
                "id": "c92142c8-7daa-4f52-9500-23cb9fc7cec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cc4c06-bf8a-4442-95d1-5d95e5a9044e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00570547-af65-480e-a862-58f9bbaad363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cc4c06-bf8a-4442-95d1-5d95e5a9044e",
                    "LayerId": "ebde762a-2a37-47cc-ac5a-0b5073291121"
                },
                {
                    "id": "dd0c1fae-4bfd-46e6-afc8-0f0aea3576f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cc4c06-bf8a-4442-95d1-5d95e5a9044e",
                    "LayerId": "922ab51c-6336-40d5-9947-03e7f51dd25a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "ebde762a-2a37-47cc-ac5a-0b5073291121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "922ab51c-6336-40d5-9947-03e7f51dd25a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52fe5d81-f783-4338-b9d8-d861a60d03a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}