{
    "id": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashAXEright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 17,
    "bbox_right": 55,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa0689e7-e6a2-464c-94ed-6f23bef7f801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
            "compositeImage": {
                "id": "615f0a52-d280-4a46-ae32-b6ffce59cd78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0689e7-e6a2-464c-94ed-6f23bef7f801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175bd1aa-e2a8-4049-9dcc-0ce1a69dd7f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0689e7-e6a2-464c-94ed-6f23bef7f801",
                    "LayerId": "45be8033-cb2a-44fe-acd1-66e65a194a2c"
                }
            ]
        },
        {
            "id": "3dc1b7ca-a338-46e4-8bc5-ee5de6738e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
            "compositeImage": {
                "id": "78cbf280-2b3d-49e8-9f76-88b940326523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc1b7ca-a338-46e4-8bc5-ee5de6738e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af99a3e8-1df7-4882-b21c-a3c9094f186c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc1b7ca-a338-46e4-8bc5-ee5de6738e48",
                    "LayerId": "45be8033-cb2a-44fe-acd1-66e65a194a2c"
                }
            ]
        },
        {
            "id": "76d56125-20e3-4421-b687-7c198c1c3e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
            "compositeImage": {
                "id": "6c093419-895c-4f50-ac13-5c5ed734d8eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d56125-20e3-4421-b687-7c198c1c3e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae840fb-74b4-4ed5-b3ef-23e74f056b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d56125-20e3-4421-b687-7c198c1c3e68",
                    "LayerId": "45be8033-cb2a-44fe-acd1-66e65a194a2c"
                }
            ]
        },
        {
            "id": "793c65be-6acf-478e-939f-b611bca3bc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
            "compositeImage": {
                "id": "abf12dde-4d94-41e1-b22e-db22e4efb73d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "793c65be-6acf-478e-939f-b611bca3bc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c905dc7a-6e66-4b70-bec2-9984b608ba88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "793c65be-6acf-478e-939f-b611bca3bc10",
                    "LayerId": "45be8033-cb2a-44fe-acd1-66e65a194a2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "45be8033-cb2a-44fe-acd1-66e65a194a2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d46f1d6-0748-4fa6-9dde-b7ca9b7c14d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}