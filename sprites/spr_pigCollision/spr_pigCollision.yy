{
    "id": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 12,
    "bbox_right": 50,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "compositeImage": {
                "id": "f1b646f1-9400-4688-accd-9b4c7de92d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc55a78-0e30-4bff-b7db-26d5dae033e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
                    "LayerId": "c4bdcece-7f3a-4188-b398-dc9898d40b23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c4bdcece-7f3a-4188-b398-dc9898d40b23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}