{
    "id": "e469dd9c-75e6-4a72-a9bd-10ae4cf72e9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite71",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 15,
    "bbox_right": 44,
    "bbox_top": 79,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ced71b91-ee8b-47f4-91f7-76747fb69733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e469dd9c-75e6-4a72-a9bd-10ae4cf72e9b",
            "compositeImage": {
                "id": "57f291be-802e-457c-84c0-ab5526f5a0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced71b91-ee8b-47f4-91f7-76747fb69733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e6afcb-8f00-4dbc-a6c0-ed34af26dce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced71b91-ee8b-47f4-91f7-76747fb69733",
                    "LayerId": "80f6af91-f2bd-4d7c-bff9-939d93bdaeab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "80f6af91-f2bd-4d7c-bff9-939d93bdaeab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e469dd9c-75e6-4a72-a9bd-10ae4cf72e9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 80
}