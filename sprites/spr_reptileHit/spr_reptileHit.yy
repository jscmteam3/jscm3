{
    "id": "6a468732-8ff5-4894-b35e-964531b4a22f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reptileHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 23,
    "bbox_right": 91,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "313d67c6-48d8-4ebd-a719-91cea7af62c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "e18ca3c9-f774-4dc9-8a15-4550262319b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313d67c6-48d8-4ebd-a719-91cea7af62c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d156ca-f435-41b0-a871-53db9bf01820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313d67c6-48d8-4ebd-a719-91cea7af62c0",
                    "LayerId": "2494eda2-b3af-43b0-a0d6-f42a20b7ea13"
                }
            ]
        },
        {
            "id": "eb74a8ff-cfd2-4e49-82af-5aa389d2f11f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "c52ec2f3-00b0-4d96-a9a0-b4f058c447d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb74a8ff-cfd2-4e49-82af-5aa389d2f11f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f32b41-1e96-4305-8ec4-e3ee1af8fec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb74a8ff-cfd2-4e49-82af-5aa389d2f11f",
                    "LayerId": "2494eda2-b3af-43b0-a0d6-f42a20b7ea13"
                }
            ]
        },
        {
            "id": "4a672ce4-d073-463c-b607-80f9da05abc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "5f49bcbe-2678-4171-9844-1c7ab369a0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a672ce4-d073-463c-b607-80f9da05abc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a92b2f-1b09-42ed-aa9e-711e38cf1650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a672ce4-d073-463c-b607-80f9da05abc8",
                    "LayerId": "2494eda2-b3af-43b0-a0d6-f42a20b7ea13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "2494eda2-b3af-43b0-a0d6-f42a20b7ea13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 57
}