{
    "id": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigWalkLeft1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 2,
    "bbox_right": 56,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fc44127-9c34-4a90-8e33-003b1644d4a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "compositeImage": {
                "id": "7ffcab45-09b9-4b69-8168-2211ef1004a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc44127-9c34-4a90-8e33-003b1644d4a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2a1c5e-bbff-4936-abea-539062f2164a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc44127-9c34-4a90-8e33-003b1644d4a4",
                    "LayerId": "c4bdcece-7f3a-4188-b398-dc9898d40b23"
                }
            ]
        },
        {
            "id": "9e24834c-61bd-416c-9d3b-ea5c346fc73d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "compositeImage": {
                "id": "2fa4d6be-1ffa-49f5-b1dc-3825d1ff2c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e24834c-61bd-416c-9d3b-ea5c346fc73d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db50fe0-c6d5-4e9d-afb1-3dcfc1aaafcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e24834c-61bd-416c-9d3b-ea5c346fc73d",
                    "LayerId": "c4bdcece-7f3a-4188-b398-dc9898d40b23"
                }
            ]
        },
        {
            "id": "76a00464-66ae-46b6-8537-a03fa4383b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "compositeImage": {
                "id": "c4db379c-dc9a-45c5-9044-452d8f7e051a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a00464-66ae-46b6-8537-a03fa4383b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29337776-2eca-4991-aea9-d09b34e144f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a00464-66ae-46b6-8537-a03fa4383b96",
                    "LayerId": "c4bdcece-7f3a-4188-b398-dc9898d40b23"
                }
            ]
        },
        {
            "id": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "compositeImage": {
                "id": "f1b646f1-9400-4688-accd-9b4c7de92d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc55a78-0e30-4bff-b7db-26d5dae033e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d587a3-c3ab-45ac-8fb4-6eddd4ab999f",
                    "LayerId": "c4bdcece-7f3a-4188-b398-dc9898d40b23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c4bdcece-7f3a-4188-b398-dc9898d40b23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d27c8a1a-f7e6-41be-8ec6-d72d88cd92e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}