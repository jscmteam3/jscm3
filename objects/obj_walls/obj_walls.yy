{
    "id": "c890540e-3000-4b1f-9957-130fe873ff55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_walls",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "752409de-0d59-4711-97ba-11b777800763",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a63ddc90-6f0b-4196-80bf-0e4fca175e07",
    "visible": true
}