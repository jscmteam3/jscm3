{
    "id": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigWalkUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1464c1e-e4bd-4f9d-b4d3-62eb4720945c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
            "compositeImage": {
                "id": "0755b868-9a69-40e2-9751-17a3178599dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1464c1e-e4bd-4f9d-b4d3-62eb4720945c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a367a9f0-377b-4292-9e4c-76da4ea41a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1464c1e-e4bd-4f9d-b4d3-62eb4720945c",
                    "LayerId": "67f32d44-27d0-4241-b853-941abb42d3ce"
                }
            ]
        },
        {
            "id": "29b52525-5e5b-451f-8a09-da86a0b04b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
            "compositeImage": {
                "id": "b6b6e787-bbf1-4393-a675-f644e7e50abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b52525-5e5b-451f-8a09-da86a0b04b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d7191d-2f36-4d12-9803-c8d9dffb8c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b52525-5e5b-451f-8a09-da86a0b04b1f",
                    "LayerId": "67f32d44-27d0-4241-b853-941abb42d3ce"
                }
            ]
        },
        {
            "id": "0505ef83-d1ab-4df8-b9d4-d32cf69586ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
            "compositeImage": {
                "id": "2ee5bb6b-f188-483f-8e7b-c809e8b2897f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0505ef83-d1ab-4df8-b9d4-d32cf69586ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fd9075d-f791-498f-8675-ae5362b170c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0505ef83-d1ab-4df8-b9d4-d32cf69586ba",
                    "LayerId": "67f32d44-27d0-4241-b853-941abb42d3ce"
                }
            ]
        },
        {
            "id": "31647547-1973-4a71-8724-d3b582b640cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
            "compositeImage": {
                "id": "8a19a77d-5fcb-4e2e-b946-eab5b0581674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31647547-1973-4a71-8724-d3b582b640cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f3334b-e2d8-42bd-ae0a-349b997e1d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31647547-1973-4a71-8724-d3b582b640cc",
                    "LayerId": "67f32d44-27d0-4241-b853-941abb42d3ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "67f32d44-27d0-4241-b853-941abb42d3ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "541473ef-f376-4fa9-8667-d29bb6f6ca38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}