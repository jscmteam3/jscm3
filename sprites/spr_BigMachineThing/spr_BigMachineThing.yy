{
    "id": "b2631ce6-54df-4785-850e-0e14b0dd83f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BigMachineThing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd56c9d6-4527-47a7-91bc-a428e3032ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2631ce6-54df-4785-850e-0e14b0dd83f4",
            "compositeImage": {
                "id": "68bd2d73-c3ce-4fb7-b4e6-e33b315d8c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd56c9d6-4527-47a7-91bc-a428e3032ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049c57d3-55f7-4a1a-803b-38c8e92f35ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd56c9d6-4527-47a7-91bc-a428e3032ed3",
                    "LayerId": "c0f084a7-c82c-4f0c-aeba-4ab3dda76cd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c0f084a7-c82c-4f0c-aeba-4ab3dda76cd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2631ce6-54df-4785-850e-0e14b0dd83f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}