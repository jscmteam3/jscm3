{
    "id": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_itembar",
    "eventList": [
        {
            "id": "de629a32-631f-486d-92a7-5064b3fedbfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b"
        },
        {
            "id": "1dd2cdde-cda6-4886-a59d-c6eb3f7919cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b"
        },
        {
            "id": "bb4c0bf3-3de1-4b58-8a19-2bb7c9ba75c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b"
        },
        {
            "id": "e7199249-288a-4b7e-953e-32896825d8a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b"
        },
        {
            "id": "3c5ea686-9090-4804-981d-448d02ae7c84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "29506d9f-8ccf-4ec3-8048-bcb9ada3102b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}