/// @description Insert description here
 var anim_length = 9 
 var anim_speed = 15
 var xx = x - x_offset
 var yy = y - y_offset
 
 if (moveX < 0) y_frame = 9;
else if ( moveX > 0 ) y_frame = 11;
else if ( moveY > 0 ) y_frame = 10;
else if ( moveY < 0 ) y_frame = 8;
else x_frame = 0
draw_sprite_part(spr_base, 0, floor(x_frame)*64, y_frame*64, 64, 64, xx, yy)
draw_sprite_part(spr_hair, 0, floor(x_frame)*64, y_frame*64, 64, 64, xx, yy)
draw_sprite_part(spr_torso, 0, floor(x_frame)*64, y_frame*64, 64, 64, xx, yy)
draw_sprite_part(spr_legs, 0, floor(x_frame)*64, y_frame*64, 64, 64, xx, yy)
draw_sprite_part(spr_feet, 0, floor(x_frame)*64, y_frame*64, 64, 64, xx, yy)
if (x_frame < anim_length - 1) { x_frame += anim_speed/60}
     else                      { x_frame = 0 }
	 
draw_rectangle_color(bbox_left, bbox_right,bbox_top,bbox_bottom,c_yellow,c_yellow,c_yellow,c_yellow,true)