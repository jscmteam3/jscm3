{
    "id": "7a5ce17d-7040-4101-aeea-2ad9a62131b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite44",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1681f33-98b6-441c-ae7d-1d5a3d4bf04f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a5ce17d-7040-4101-aeea-2ad9a62131b3",
            "compositeImage": {
                "id": "29aab82d-4427-44c3-8c03-29624cf970a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1681f33-98b6-441c-ae7d-1d5a3d4bf04f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68093766-84da-4573-8e87-ee2bdadeca50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1681f33-98b6-441c-ae7d-1d5a3d4bf04f",
                    "LayerId": "fad14627-856a-4987-a6ec-794b60a44293"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fad14627-856a-4987-a6ec-794b60a44293",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a5ce17d-7040-4101-aeea-2ad9a62131b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 10,
    "yorig": 5
}