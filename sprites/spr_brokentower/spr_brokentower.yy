{
    "id": "50c672e8-b7c2-4daf-81c7-ff52d916e59c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brokentower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "191ced5d-ff6c-4269-baa7-0498d5b8059e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c672e8-b7c2-4daf-81c7-ff52d916e59c",
            "compositeImage": {
                "id": "32e7a5d0-98ba-43d3-82f5-daaeeac97fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "191ced5d-ff6c-4269-baa7-0498d5b8059e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe9ed85-48c0-4407-aa39-2924fe566ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "191ced5d-ff6c-4269-baa7-0498d5b8059e",
                    "LayerId": "31b6e56b-001d-4f5d-93e9-9e43c33c6b31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "31b6e56b-001d-4f5d-93e9-9e43c33c6b31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c672e8-b7c2-4daf-81c7-ff52d916e59c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}