{
    "id": "87f69349-4245-4549-98cc-7b8078931e4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile_build_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 663,
    "bbox_left": 4,
    "bbox_right": 495,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "894e9080-072c-4ecd-ae29-391a5065139b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87f69349-4245-4549-98cc-7b8078931e4f",
            "compositeImage": {
                "id": "ae0f1675-7e7d-4e0e-837e-353de10003e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "894e9080-072c-4ecd-ae29-391a5065139b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5fc690-8d5d-48fa-bb66-508e5b023490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "894e9080-072c-4ecd-ae29-391a5065139b",
                    "LayerId": "7009bfc3-12fc-4c91-9f9c-96abca12b440"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 672,
    "layers": [
        {
            "id": "7009bfc3-12fc-4c91-9f9c-96abca12b440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87f69349-4245-4549-98cc-7b8078931e4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}