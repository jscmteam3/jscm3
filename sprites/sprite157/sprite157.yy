{
    "id": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite157",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 60,
    "bbox_right": 137,
    "bbox_top": 61,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a7d992c-bd14-4c1d-9c09-65f59635f204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "2610c835-edf5-415e-a64f-20e1875004f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7d992c-bd14-4c1d-9c09-65f59635f204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d71ea6ee-a542-422a-b3eb-d72b1c19cfab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7d992c-bd14-4c1d-9c09-65f59635f204",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        },
        {
            "id": "b64633d0-16fa-48ef-b512-694d7e1e9010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "d46245f1-0fd1-47c2-909c-71d32be56699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b64633d0-16fa-48ef-b512-694d7e1e9010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebe52866-5b7b-40e3-aedb-fe5852fbfdbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b64633d0-16fa-48ef-b512-694d7e1e9010",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        },
        {
            "id": "66036081-e0f6-4742-8f16-1dbffe493e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "aede2042-f6e0-49eb-896a-bfaffa888c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66036081-e0f6-4742-8f16-1dbffe493e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b59834-f83b-4b2c-a5b7-b8e7ccae2e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66036081-e0f6-4742-8f16-1dbffe493e3e",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        },
        {
            "id": "eb262989-74c5-48a9-b111-a17c8b1d1621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "25ff366e-d6d3-40bd-b1d2-dd96cb1b6439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb262989-74c5-48a9-b111-a17c8b1d1621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac0090d-da9d-404a-b4fc-6edbc79a4115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb262989-74c5-48a9-b111-a17c8b1d1621",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        },
        {
            "id": "c7474c12-8dad-439c-a88a-30f95aea61bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "c4953106-9770-47ea-b439-acfd2fb3d8e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7474c12-8dad-439c-a88a-30f95aea61bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e01e3dd-1761-4cee-b051-b3914378c70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7474c12-8dad-439c-a88a-30f95aea61bd",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        },
        {
            "id": "0e35c996-068c-4b45-8b33-c395b4adc712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "compositeImage": {
                "id": "80b71e2c-9af3-4b2b-a853-1946c9926469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e35c996-068c-4b45-8b33-c395b4adc712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79fe7896-3925-4faa-b9b4-8c49853a6bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e35c996-068c-4b45-8b33-c395b4adc712",
                    "LayerId": "3ff418d5-9702-4c50-9a25-192cd1323125"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "3ff418d5-9702-4c50-9a25-192cd1323125",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe5a58a-3ae9-4579-8811-1038d73aead6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}