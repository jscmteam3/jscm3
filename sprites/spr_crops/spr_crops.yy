{
    "id": "c397656f-9b03-43d6-8cc5-848895011211",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crops",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 446,
    "bbox_left": 3,
    "bbox_right": 159,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a62f955-0709-4b9c-bc33-9f99768c8c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c397656f-9b03-43d6-8cc5-848895011211",
            "compositeImage": {
                "id": "c7d32bbf-c22d-4b7c-8f32-9c89dec5baf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a62f955-0709-4b9c-bc33-9f99768c8c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2650c790-79c6-4c97-b7d6-ea307ef27f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a62f955-0709-4b9c-bc33-9f99768c8c03",
                    "LayerId": "3d10dc28-024f-410a-878e-d2839008c3b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 448,
    "layers": [
        {
            "id": "3d10dc28-024f-410a-878e-d2839008c3b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c397656f-9b03-43d6-8cc5-848895011211",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}