{
    "id": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashAXEup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51f9d12f-c394-4d1b-b89c-c7cce23f5f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
            "compositeImage": {
                "id": "fc9bc22e-feb0-41bd-8d9a-cb1f6fe0667b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f9d12f-c394-4d1b-b89c-c7cce23f5f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ec8953-86e4-47f6-bf95-c9bbd8d4d085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f9d12f-c394-4d1b-b89c-c7cce23f5f54",
                    "LayerId": "a9d1bf4d-7ea3-4271-ba5f-8bc2691ae31d"
                }
            ]
        },
        {
            "id": "6c53f1e7-1c79-47f0-9990-2e05f21780d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
            "compositeImage": {
                "id": "7aa7a62f-8323-4fd2-bc96-436d4f35678e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c53f1e7-1c79-47f0-9990-2e05f21780d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4049462d-40bc-4212-8dfa-0c862555eb81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c53f1e7-1c79-47f0-9990-2e05f21780d0",
                    "LayerId": "a9d1bf4d-7ea3-4271-ba5f-8bc2691ae31d"
                }
            ]
        },
        {
            "id": "1d212492-ba44-4f17-9519-ede69d525f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
            "compositeImage": {
                "id": "3c220a98-5e3c-47c3-aafa-a80f9adc1c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d212492-ba44-4f17-9519-ede69d525f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812c24ad-47b3-49a7-bc73-beaa1fb39923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d212492-ba44-4f17-9519-ede69d525f45",
                    "LayerId": "a9d1bf4d-7ea3-4271-ba5f-8bc2691ae31d"
                }
            ]
        },
        {
            "id": "e0aeb958-42d9-4af6-a954-d55cd48a01cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
            "compositeImage": {
                "id": "d269d664-3dd4-49ab-b7d0-b3da14958155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0aeb958-42d9-4af6-a954-d55cd48a01cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "784ba84e-598e-4ea4-aecc-d83973beb3e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0aeb958-42d9-4af6-a954-d55cd48a01cd",
                    "LayerId": "a9d1bf4d-7ea3-4271-ba5f-8bc2691ae31d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "a9d1bf4d-7ea3-4271-ba5f-8bc2691ae31d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75ebc4c5-81dd-4b77-8c39-5e3f07628f75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}