{
    "id": "3875eacf-16d2-464c-bdf1-c3ba7fbbacf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stuff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 659,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85d39ab7-d125-467c-a444-df5d813502c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3875eacf-16d2-464c-bdf1-c3ba7fbbacf1",
            "compositeImage": {
                "id": "f766476f-0c18-4977-95cf-82b07f5dc116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d39ab7-d125-467c-a444-df5d813502c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a5186e-de99-43df-9f04-5d01a6c54b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d39ab7-d125-467c-a444-df5d813502c3",
                    "LayerId": "4288778e-f350-49c3-8c47-9708bfd0aa1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "4288778e-f350-49c3-8c47-9708bfd0aa1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3875eacf-16d2-464c-bdf1-c3ba7fbbacf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 672,
    "xorig": 0,
    "yorig": 0
}