/// @description
var faceX,faceY;
with (objPlayer){
	if (Direction == RIGHT){ faceX = 1; faceY = 0;}
	else if(Direction == LEFT){ faceX = -1; faceY = 0;}
	else if(Direction == UP) { faceX = 0; faceY = -1;}
	else { faceX = 0; faceY = 1; }
}
if(place_meeting(x + Speed, y, objEnermy))
{
	while(!place_meeting(x + sign(Speed), y, objEnermy))
	x += sign(Speed);
	Speed = 0;
}

if(place_meeting(x , y + Speed, objEnermy))
{
	while(!place_meeting(x , y + sign(Speed), objEnermy))
		y += sign(Speed);
	Speed = 0;
}
x +=  Speed*faceX;
y += Speed*faceY;
alarm[0] =10
