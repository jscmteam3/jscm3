{
    "id": "88bf7875-a390-4991-93c5-531cf206dfb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite147",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 8,
    "bbox_right": 96,
    "bbox_top": 53,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f4ca7f3-aab5-461b-b766-0a657214cc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "edfa8e6d-62cd-47e5-91ef-31a62c80ee6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4ca7f3-aab5-461b-b766-0a657214cc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b57118-6774-4c98-983c-700523018b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4ca7f3-aab5-461b-b766-0a657214cc7a",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "b39abcf0-e422-4e9a-a31c-e8ec913507da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "4e2be35a-c739-41f1-a4e6-65d7597ecfa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b39abcf0-e422-4e9a-a31c-e8ec913507da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6160dbd-7536-4092-936f-28cd4defc079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b39abcf0-e422-4e9a-a31c-e8ec913507da",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "c00a6648-0be7-439e-b7bf-3a4ed72e0f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "e8d72237-42f2-451e-9c43-b9247d029579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c00a6648-0be7-439e-b7bf-3a4ed72e0f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e714f6-653f-4075-8351-c9643402228d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c00a6648-0be7-439e-b7bf-3a4ed72e0f76",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "f9c22d73-6818-417e-9fa5-bc5f28608bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "eeded8cb-2106-495b-9a81-e3335d6ab338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c22d73-6818-417e-9fa5-bc5f28608bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d96811-54ab-4527-b394-39c702a7e7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c22d73-6818-417e-9fa5-bc5f28608bbb",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "03713c7e-91a4-4195-8ec6-90eebf7cba3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "14091b78-a251-4fac-9ae3-5c7c9a0f8382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03713c7e-91a4-4195-8ec6-90eebf7cba3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c148de-5b5b-44ce-9483-b8612110ae32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03713c7e-91a4-4195-8ec6-90eebf7cba3f",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "1318e6c6-0ae5-4359-88ba-8cd5b6cf63a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "3942dc58-6c11-4fbc-b6f2-c0fb5db20442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1318e6c6-0ae5-4359-88ba-8cd5b6cf63a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4084d857-153c-498b-99fc-e86fc55be0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1318e6c6-0ae5-4359-88ba-8cd5b6cf63a3",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        },
        {
            "id": "77524778-6907-4799-89f5-6805498d31dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "compositeImage": {
                "id": "fbb25bd1-3a00-4e81-a864-b2c0004a1a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77524778-6907-4799-89f5-6805498d31dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8696af-73b5-4336-91b0-9f24d6e3bf35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77524778-6907-4799-89f5-6805498d31dd",
                    "LayerId": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "61972b1d-a0fd-4e2a-8ef1-c7980aadba0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88bf7875-a390-4991-93c5-531cf206dfb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}