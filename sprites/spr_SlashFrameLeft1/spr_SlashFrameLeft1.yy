{
    "id": "41d8d67e-7d73-451c-a394-f74891d3be86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashFrameLeft1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d28cfdf0-a967-4aa3-8f16-68c9f2d791c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d8d67e-7d73-451c-a394-f74891d3be86",
            "compositeImage": {
                "id": "35ef4ef9-6965-4326-b145-703470a8ec84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d28cfdf0-a967-4aa3-8f16-68c9f2d791c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afef589f-03e5-4360-8ec7-8f186c4ec48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d28cfdf0-a967-4aa3-8f16-68c9f2d791c5",
                    "LayerId": "3ea04809-c97b-49f8-a821-14ce891e12e1"
                }
            ]
        },
        {
            "id": "0f3e72f4-6aff-412a-88f0-8ce7a1ef7080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d8d67e-7d73-451c-a394-f74891d3be86",
            "compositeImage": {
                "id": "c1ad3863-71a8-4ec5-a01d-7084f98b4fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3e72f4-6aff-412a-88f0-8ce7a1ef7080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d20eff7-f123-4bda-a011-5102e1b756c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3e72f4-6aff-412a-88f0-8ce7a1ef7080",
                    "LayerId": "3ea04809-c97b-49f8-a821-14ce891e12e1"
                }
            ]
        },
        {
            "id": "1b89dc0d-4056-4cd9-aa03-19b4ef33d354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d8d67e-7d73-451c-a394-f74891d3be86",
            "compositeImage": {
                "id": "bff3952c-3838-4d4e-9209-ee44aed1a27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b89dc0d-4056-4cd9-aa03-19b4ef33d354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c765fa-2fae-4d83-9136-e614a83cae14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b89dc0d-4056-4cd9-aa03-19b4ef33d354",
                    "LayerId": "3ea04809-c97b-49f8-a821-14ce891e12e1"
                }
            ]
        },
        {
            "id": "9e93af9e-59c7-43dd-b566-6ce36a6afc0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41d8d67e-7d73-451c-a394-f74891d3be86",
            "compositeImage": {
                "id": "6356ab87-e89a-4e16-85ca-870639319820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e93af9e-59c7-43dd-b566-6ce36a6afc0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcef97d1-0733-411a-8654-986a300bd85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e93af9e-59c7-43dd-b566-6ce36a6afc0f",
                    "LayerId": "3ea04809-c97b-49f8-a821-14ce891e12e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "3ea04809-c97b-49f8-a821-14ce891e12e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41d8d67e-7d73-451c-a394-f74891d3be86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}