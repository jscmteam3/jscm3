{
    "id": "ddc08890-496d-4f0b-a5ab-cb9739535b47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cottage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28d2e5af-e8b0-4e19-b43c-bae8eed5427f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddc08890-496d-4f0b-a5ab-cb9739535b47",
            "compositeImage": {
                "id": "12d41c77-addb-45c7-a9dc-d91665607a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d2e5af-e8b0-4e19-b43c-bae8eed5427f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d972a2a1-43fc-4524-a3fe-0b1c883abc9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d2e5af-e8b0-4e19-b43c-bae8eed5427f",
                    "LayerId": "976a4f80-cbe5-4a5b-98a3-5498ef1c9459"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "976a4f80-cbe5-4a5b-98a3-5498ef1c9459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddc08890-496d-4f0b-a5ab-cb9739535b47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}