{
    "id": "f683bed5-aea9-4d51-83a3-840335fbdadb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite49",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a0e419d-54ca-4b75-a928-5f9be0708ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f683bed5-aea9-4d51-83a3-840335fbdadb",
            "compositeImage": {
                "id": "a423bb39-1ab6-4cf2-8b26-5c39f41e4c8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0e419d-54ca-4b75-a928-5f9be0708ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0cf4d5-1eed-415e-b8b6-51916e349d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0e419d-54ca-4b75-a928-5f9be0708ab0",
                    "LayerId": "373ddcf8-20ca-4502-aff0-68f1db657202"
                }
            ]
        },
        {
            "id": "a016ebf5-b62d-4553-b5b6-88caa1a3589f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f683bed5-aea9-4d51-83a3-840335fbdadb",
            "compositeImage": {
                "id": "9ebd60a4-3962-43fa-a271-581a3da70451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a016ebf5-b62d-4553-b5b6-88caa1a3589f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea1f950-e9be-46fb-b7fd-181748eefc57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a016ebf5-b62d-4553-b5b6-88caa1a3589f",
                    "LayerId": "373ddcf8-20ca-4502-aff0-68f1db657202"
                }
            ]
        },
        {
            "id": "dc3ace6b-371a-4971-9b5d-a63075b505be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f683bed5-aea9-4d51-83a3-840335fbdadb",
            "compositeImage": {
                "id": "75bfde31-4ab9-4029-b50d-67e3ba69f42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc3ace6b-371a-4971-9b5d-a63075b505be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80749110-e95b-4049-b02a-0a3792161d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc3ace6b-371a-4971-9b5d-a63075b505be",
                    "LayerId": "373ddcf8-20ca-4502-aff0-68f1db657202"
                }
            ]
        }
    ],
    "gridX": 48,
    "gridY": 48,
    "height": 64,
    "layers": [
        {
            "id": "373ddcf8-20ca-4502-aff0-68f1db657202",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f683bed5-aea9-4d51-83a3-840335fbdadb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}