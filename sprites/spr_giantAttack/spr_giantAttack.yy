{
    "id": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_giantAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 1,
    "bbox_right": 116,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f839ffc8-51f3-458e-808d-b0727cec2c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "84e17b28-e8b2-4ac8-995c-ffbdb10fd474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f839ffc8-51f3-458e-808d-b0727cec2c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e486dab7-a560-49d1-a873-006f8d275c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f839ffc8-51f3-458e-808d-b0727cec2c76",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "72b4e452-1e7f-4210-8044-bc1c3925fbbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "3e07019b-6f17-40d2-b1a3-035a5815f58f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b4e452-1e7f-4210-8044-bc1c3925fbbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9964dd-779d-4194-b689-c87b428fedca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b4e452-1e7f-4210-8044-bc1c3925fbbe",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "a9328739-6bff-4f53-85b5-b9561b10fdc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "7d04bede-20f3-45db-ae91-08714a87d251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9328739-6bff-4f53-85b5-b9561b10fdc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3301b4ae-be69-44c5-8076-223ea3da027f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9328739-6bff-4f53-85b5-b9561b10fdc7",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "6e706856-adb5-47f9-80e1-cf0545231eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "343a46c3-ef05-4639-a4fc-b88ced39330a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e706856-adb5-47f9-80e1-cf0545231eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f603fe5f-05f6-45fe-9032-b75b388b87ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e706856-adb5-47f9-80e1-cf0545231eb6",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "190e5e31-ad51-40c7-b855-77e4bb710023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "00729568-1b3a-4ad9-b1b8-ae6272a9a9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190e5e31-ad51-40c7-b855-77e4bb710023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f6ed43-3298-48a8-b994-bbd384879c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190e5e31-ad51-40c7-b855-77e4bb710023",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "4f14dd87-8eb1-43e0-becd-8ebe71c2d96f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "4d5b0d8e-b8d2-45d9-bfec-c15c09937b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f14dd87-8eb1-43e0-becd-8ebe71c2d96f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f77a1dd5-a9da-41e5-9ce7-a0eb46f96f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f14dd87-8eb1-43e0-becd-8ebe71c2d96f",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "6b31df5b-f85b-4919-902c-a363b11dc594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "76cca419-0f12-4079-8431-c219ecdd6f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b31df5b-f85b-4919-902c-a363b11dc594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac1f2839-4237-4f98-ad3d-ad76d9eb2fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b31df5b-f85b-4919-902c-a363b11dc594",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "50fc7339-6fd9-4f97-b747-840148caa079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "f53b96d3-d7bc-4648-9602-a84a09e8f5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50fc7339-6fd9-4f97-b747-840148caa079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea087712-828a-47d1-9367-8342d1126836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50fc7339-6fd9-4f97-b747-840148caa079",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "4f6d8570-887f-4c6a-9aad-a3bde6854c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "f5d302e4-8ac4-45e0-b3ac-d70a6ddd89f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f6d8570-887f-4c6a-9aad-a3bde6854c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0242187c-8366-4a31-afe1-70706f05d59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f6d8570-887f-4c6a-9aad-a3bde6854c22",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "32a9456d-d739-49fe-8311-9d3cd9fee649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "7e35a5d5-53be-42e4-a14b-2e7769beee14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a9456d-d739-49fe-8311-9d3cd9fee649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38db831-5e30-49f5-b932-6b1ab2306530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a9456d-d739-49fe-8311-9d3cd9fee649",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "4cbcc6fc-21a7-4c68-bc9a-15930268f930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "e012e441-b9a1-4090-9b0c-cb644b79fd3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cbcc6fc-21a7-4c68-bc9a-15930268f930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d456c6-3945-45bf-88cb-3a8d86503777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cbcc6fc-21a7-4c68-bc9a-15930268f930",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "71fd9093-05ba-4fbb-8e0a-4f9a47425158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "29f450ba-b22f-4d5d-aab6-a8a989ad3980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71fd9093-05ba-4fbb-8e0a-4f9a47425158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6072d8ff-2726-476a-9c1d-5f733ffb96a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71fd9093-05ba-4fbb-8e0a-4f9a47425158",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "438acecd-3cad-492f-9679-a59d3d5e2dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "3179d698-735f-430b-92ce-ba96e9ec07fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "438acecd-3cad-492f-9679-a59d3d5e2dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afdf159e-0fff-448c-b9b6-7b74c81c7be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "438acecd-3cad-492f-9679-a59d3d5e2dc8",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "9e9b980e-75a4-47cc-bfe1-81cc0bc2e3ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "355df9de-47b7-43bf-92fa-f5ec541b968c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e9b980e-75a4-47cc-bfe1-81cc0bc2e3ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527bae55-883d-4170-afca-68e8d6c96b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9b980e-75a4-47cc-bfe1-81cc0bc2e3ee",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        },
        {
            "id": "8051e06d-c050-4041-980f-106be9c0e769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "compositeImage": {
                "id": "0b676029-f9ee-46fb-b575-51c30e312a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8051e06d-c050-4041-980f-106be9c0e769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17b362d-295b-4466-bfc8-01e9abd426d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8051e06d-c050-4041-980f-106be9c0e769",
                    "LayerId": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "53255a3f-c0fc-4d13-8c72-40ad4909dcfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef5e5e68-9673-4fa7-bf42-6fc42874a109",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 53
}