{
    "id": "d06913e4-d25c-4f41-9e66-c6cae6d6b0ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "HealthBar",
    "eventList": [
        {
            "id": "b980134c-9080-4a33-a136-64747ff213d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d06913e4-d25c-4f41-9e66-c6cae6d6b0ec"
        },
        {
            "id": "59929afc-4b8c-485d-96f0-f2bd2ecca543",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d06913e4-d25c-4f41-9e66-c6cae6d6b0ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4423e897-291f-4cde-9daa-d4ca23e781f3",
    "visible": true
}