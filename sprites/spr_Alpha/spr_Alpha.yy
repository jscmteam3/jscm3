{
    "id": "a9c859e9-91bc-4003-91b5-07eafcfe35af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Alpha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 296,
    "bbox_left": -1,
    "bbox_right": 1130,
    "bbox_top": 182,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "301958f1-0235-46ac-a1e8-92ab46cac2b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9c859e9-91bc-4003-91b5-07eafcfe35af",
            "compositeImage": {
                "id": "319679d9-9395-4e32-9310-305b1a9a7a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "301958f1-0235-46ac-a1e8-92ab46cac2b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb548ded-d42c-4bba-b9ef-07ed16768ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "301958f1-0235-46ac-a1e8-92ab46cac2b0",
                    "LayerId": "da99e298-da63-49ba-8444-d82f50f67aa0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 405,
    "layers": [
        {
            "id": "da99e298-da63-49ba-8444-d82f50f67aa0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9c859e9-91bc-4003-91b5-07eafcfe35af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1188,
    "xorig": 594,
    "yorig": 202
}