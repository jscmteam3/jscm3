{
    "id": "89f3eb85-4654-4535-af16-39ef47340055",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite72",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 13,
    "bbox_right": 279,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea8c418f-779d-46d7-b0a5-cb3ab2cda3c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89f3eb85-4654-4535-af16-39ef47340055",
            "compositeImage": {
                "id": "a187f35e-1786-4464-8036-64ba5e8fbef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea8c418f-779d-46d7-b0a5-cb3ab2cda3c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d994e0ea-981e-4cc1-a649-8e782211d267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea8c418f-779d-46d7-b0a5-cb3ab2cda3c9",
                    "LayerId": "daa98dc3-3b57-47c9-97fa-9a2195c41f2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "daa98dc3-3b57-47c9-97fa-9a2195c41f2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89f3eb85-4654-4535-af16-39ef47340055",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 280,
    "xorig": 0,
    "yorig": 0
}