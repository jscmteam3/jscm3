{
    "id": "b731a45f-9313-441e-994f-23d4c0f77499",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 43,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a35a6a00-0146-4f84-9b2f-1a9368c041b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b731a45f-9313-441e-994f-23d4c0f77499",
            "compositeImage": {
                "id": "8168fec1-14ff-4fa1-a638-e1972bcf3097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35a6a00-0146-4f84-9b2f-1a9368c041b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c610c1d-4859-4ff4-8aa3-3708547e4b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35a6a00-0146-4f84-9b2f-1a9368c041b5",
                    "LayerId": "babb6da2-6933-410d-a283-bfc90b9bedf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "babb6da2-6933-410d-a283-bfc90b9bedf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b731a45f-9313-441e-994f-23d4c0f77499",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 56
}