{
    "id": "70cd0dbc-6db5-48ee-b572-96756ee6db72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interior2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a088d5a-55ba-4e76-bea7-f13d8bc2a036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70cd0dbc-6db5-48ee-b572-96756ee6db72",
            "compositeImage": {
                "id": "7e5cced4-6b17-46a2-979b-80199d32ffeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a088d5a-55ba-4e76-bea7-f13d8bc2a036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19de5419-09b7-4863-936b-80065b4f6199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a088d5a-55ba-4e76-bea7-f13d8bc2a036",
                    "LayerId": "3cb94eb4-d42d-4f9c-a826-9758b4c03d93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "3cb94eb4-d42d-4f9c-a826-9758b4c03d93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70cd0dbc-6db5-48ee-b572-96756ee6db72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}