{
    "id": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenWalkUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cdc90ee-ca97-425f-aeb4-ac99adb0bd75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
            "compositeImage": {
                "id": "9b5e9522-d181-4cfe-aa51-9f4138d3b049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cdc90ee-ca97-425f-aeb4-ac99adb0bd75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323a0f47-1e27-481e-9fd5-4ef850d9db83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cdc90ee-ca97-425f-aeb4-ac99adb0bd75",
                    "LayerId": "6c444d36-0a6a-413c-9b9f-cf43f98ded47"
                }
            ]
        },
        {
            "id": "8cfed4f8-76c6-4cf6-8a64-23e8fdf77503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
            "compositeImage": {
                "id": "01f11b43-5e1b-4367-9b0f-57085c77af01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cfed4f8-76c6-4cf6-8a64-23e8fdf77503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0fcdd80-c503-400a-8602-32af3bb4d298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfed4f8-76c6-4cf6-8a64-23e8fdf77503",
                    "LayerId": "6c444d36-0a6a-413c-9b9f-cf43f98ded47"
                }
            ]
        },
        {
            "id": "aad2e8d8-b2a9-4061-a1bc-77dce0184522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
            "compositeImage": {
                "id": "32fdd16f-86b4-4b6c-b9fb-d0002bf1648d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad2e8d8-b2a9-4061-a1bc-77dce0184522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033cd2a0-f6af-4052-80bc-c8632051338d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad2e8d8-b2a9-4061-a1bc-77dce0184522",
                    "LayerId": "6c444d36-0a6a-413c-9b9f-cf43f98ded47"
                }
            ]
        },
        {
            "id": "4aecb1b7-46f5-4a12-b109-91a168920cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
            "compositeImage": {
                "id": "30800758-c46a-44a7-a1c0-f77bb505fca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aecb1b7-46f5-4a12-b109-91a168920cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a0ca70-a718-4f87-a1fb-1c01121678bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aecb1b7-46f5-4a12-b109-91a168920cb7",
                    "LayerId": "6c444d36-0a6a-413c-9b9f-cf43f98ded47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6c444d36-0a6a-413c-9b9f-cf43f98ded47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b90cdb05-f5df-4850-8181-1688a48c9cf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}