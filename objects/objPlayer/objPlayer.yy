{
    "id": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "7623397b-880d-42bd-af26-6e5f190994af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0"
        },
        {
            "id": "47312a65-707e-4dff-9752-542047a38be2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0"
        },
        {
            "id": "fb9f7354-9fc9-4ac0-a605-c41fca2a9b40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0"
        }
    ],
    "maskSpriteId": "4ea2b0af-327a-4914-bfb8-1adcdd1e49fd",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6994db48-0dba-443b-b4b3-22d6e03817f7",
    "visible": false
}