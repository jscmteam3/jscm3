{
    "id": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashFrameDown1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11b77aa6-8cec-4d9d-8c1c-e36b071cb31b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
            "compositeImage": {
                "id": "7241eec4-3346-40ac-87fc-4cb8a9831e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11b77aa6-8cec-4d9d-8c1c-e36b071cb31b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b507070f-7d40-4b47-b112-15c8bac439f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11b77aa6-8cec-4d9d-8c1c-e36b071cb31b",
                    "LayerId": "b04e802f-f9cc-496d-a49f-8f06c831b292"
                }
            ]
        },
        {
            "id": "e7372c7c-bd78-4da8-abca-a6c43db86af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
            "compositeImage": {
                "id": "f027be58-91ea-4046-8799-f140579b33f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7372c7c-bd78-4da8-abca-a6c43db86af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab44bec-0223-4c3e-8f47-dc18555453a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7372c7c-bd78-4da8-abca-a6c43db86af4",
                    "LayerId": "b04e802f-f9cc-496d-a49f-8f06c831b292"
                }
            ]
        },
        {
            "id": "f54c028f-0b83-4dfa-9562-03f4d8fb4184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
            "compositeImage": {
                "id": "bf36da7a-cd2c-4542-aa55-8ccf45e862a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54c028f-0b83-4dfa-9562-03f4d8fb4184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36fe446e-8568-4a40-ba02-efa62d97b767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54c028f-0b83-4dfa-9562-03f4d8fb4184",
                    "LayerId": "b04e802f-f9cc-496d-a49f-8f06c831b292"
                }
            ]
        },
        {
            "id": "cdb8a2f0-8cb3-49b9-aad2-1fcd2d546c5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
            "compositeImage": {
                "id": "ea1b3986-8c99-40e3-b4d1-912a9ba15305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdb8a2f0-8cb3-49b9-aad2-1fcd2d546c5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d907224-577c-4857-a249-52c0b8e95224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdb8a2f0-8cb3-49b9-aad2-1fcd2d546c5d",
                    "LayerId": "b04e802f-f9cc-496d-a49f-8f06c831b292"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "b04e802f-f9cc-496d-a49f-8f06c831b292",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9de5018-842c-4eb7-8717-124d0f2b98f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}