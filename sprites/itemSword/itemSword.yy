{
    "id": "956fd3f1-f789-472b-afc4-d63f05cdb34f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78f89beb-4b18-4e8c-9ce4-e203a0eeefe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "956fd3f1-f789-472b-afc4-d63f05cdb34f",
            "compositeImage": {
                "id": "569d71a8-fe40-4a25-8a81-e306f9755ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f89beb-4b18-4e8c-9ce4-e203a0eeefe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d7be39-5055-4bc5-b21f-c46d75e8bbf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f89beb-4b18-4e8c-9ce4-e203a0eeefe4",
                    "LayerId": "0694d89c-6d62-4de1-b072-2a776853189b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0694d89c-6d62-4de1-b072-2a776853189b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "956fd3f1-f789-472b-afc4-d63f05cdb34f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}