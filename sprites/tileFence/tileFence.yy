{
    "id": "38a4c041-579d-4b86-9eff-0587d1f061f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileFence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 4,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d12a975-cbf6-4d2b-aa3c-99e65174f3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38a4c041-579d-4b86-9eff-0587d1f061f6",
            "compositeImage": {
                "id": "825a8d42-f71b-4809-b2d5-2a53f63458d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d12a975-cbf6-4d2b-aa3c-99e65174f3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33445377-3306-4cf2-871e-5179004e2e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d12a975-cbf6-4d2b-aa3c-99e65174f3e3",
                    "LayerId": "3f273de5-c0f2-4d37-86cd-0b07466dd6d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "3f273de5-c0f2-4d37-86cd-0b07466dd6d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38a4c041-579d-4b86-9eff-0587d1f061f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}