{
    "id": "e56b304a-ed70-473b-9122-f51ab294a349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ChickenEatLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75939d14-e7dd-4893-88c9-4e1a8d8d6871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56b304a-ed70-473b-9122-f51ab294a349",
            "compositeImage": {
                "id": "c28e0433-8499-4288-a4d2-5ecc39e007bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75939d14-e7dd-4893-88c9-4e1a8d8d6871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fdb156a-6021-4eea-9c08-faee756a4487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75939d14-e7dd-4893-88c9-4e1a8d8d6871",
                    "LayerId": "5a4065c5-cecf-436f-b7ed-ad5ea87da14f"
                }
            ]
        },
        {
            "id": "a0be2d11-297e-4c9b-9386-3c0933aa14ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56b304a-ed70-473b-9122-f51ab294a349",
            "compositeImage": {
                "id": "d6bdae4c-e091-45a7-b6d1-3b6672617669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0be2d11-297e-4c9b-9386-3c0933aa14ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910e2eb4-cd1b-4074-bab2-0a1e3b2cdda4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0be2d11-297e-4c9b-9386-3c0933aa14ca",
                    "LayerId": "5a4065c5-cecf-436f-b7ed-ad5ea87da14f"
                }
            ]
        },
        {
            "id": "a628b439-da10-4500-8171-3f64b00ffbdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56b304a-ed70-473b-9122-f51ab294a349",
            "compositeImage": {
                "id": "f84eb0cc-f518-463e-bf1b-d2e38a40e254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a628b439-da10-4500-8171-3f64b00ffbdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a474284-f9bb-4a9d-ac41-6be92e4a3e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a628b439-da10-4500-8171-3f64b00ffbdb",
                    "LayerId": "5a4065c5-cecf-436f-b7ed-ad5ea87da14f"
                }
            ]
        },
        {
            "id": "8decfed0-7052-4256-adcb-7f515335d519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e56b304a-ed70-473b-9122-f51ab294a349",
            "compositeImage": {
                "id": "173a4d66-6b07-4a7e-b7fc-5483af0e7153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8decfed0-7052-4256-adcb-7f515335d519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7128892d-edc1-467d-bec4-4d922d0da749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8decfed0-7052-4256-adcb-7f515335d519",
                    "LayerId": "5a4065c5-cecf-436f-b7ed-ad5ea87da14f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5a4065c5-cecf-436f-b7ed-ad5ea87da14f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e56b304a-ed70-473b-9122-f51ab294a349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}