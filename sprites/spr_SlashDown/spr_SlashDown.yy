{
    "id": "8c64d971-1084-4944-8096-fc41a3acf656",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "228f5968-d676-4d5a-924e-10729ca49b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c64d971-1084-4944-8096-fc41a3acf656",
            "compositeImage": {
                "id": "355a0977-f2f8-4e20-920f-d8db883fbb3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228f5968-d676-4d5a-924e-10729ca49b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67598bb1-451f-41da-9b4e-026232924610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228f5968-d676-4d5a-924e-10729ca49b41",
                    "LayerId": "b7fa749c-f9a7-4a45-9ea8-a0c0442669be"
                }
            ]
        },
        {
            "id": "13d21ebb-e277-4aaf-b3f0-39fe7279aeb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c64d971-1084-4944-8096-fc41a3acf656",
            "compositeImage": {
                "id": "987ebd5c-4607-4a6a-99c4-2136541c93bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d21ebb-e277-4aaf-b3f0-39fe7279aeb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3575ac8e-2af7-4779-861b-3b284941ae59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d21ebb-e277-4aaf-b3f0-39fe7279aeb0",
                    "LayerId": "b7fa749c-f9a7-4a45-9ea8-a0c0442669be"
                }
            ]
        },
        {
            "id": "e1d28911-9564-4525-94c7-94774bbd5db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c64d971-1084-4944-8096-fc41a3acf656",
            "compositeImage": {
                "id": "ae67bc61-91af-4485-80a4-963547157226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d28911-9564-4525-94c7-94774bbd5db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3b4ec6-9951-41a5-81dd-b15fae09ab4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d28911-9564-4525-94c7-94774bbd5db3",
                    "LayerId": "b7fa749c-f9a7-4a45-9ea8-a0c0442669be"
                }
            ]
        },
        {
            "id": "2d2cd45c-6f27-430a-b91c-e2f3bc10c226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c64d971-1084-4944-8096-fc41a3acf656",
            "compositeImage": {
                "id": "ed0d6119-ecd3-4f90-a5c1-a8f4804983ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d2cd45c-6f27-430a-b91c-e2f3bc10c226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f0d692-1cb4-4ce5-a6c1-f233b1a771b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d2cd45c-6f27-430a-b91c-e2f3bc10c226",
                    "LayerId": "b7fa749c-f9a7-4a45-9ea8-a0c0442669be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "b7fa749c-f9a7-4a45-9ea8-a0c0442669be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c64d971-1084-4944-8096-fc41a3acf656",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}