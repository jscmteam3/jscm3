playerInventory = ds_grid_create(5,7);

ds_grid_set(playerInventory, 0,0 ,"HP");
ds_grid_set(playerInventory, 1,0 ,15);
ds_grid_set(playerInventory, 2,0 ,"consume");
ds_grid_set(playerInventory, 3,0 ,spr_smallHP);
ds_grid_set(playerInventory, 4, 0 ,"Heal 20 HP");


playerInventory[# 0,1] = "Vien Da";
playerInventory[# 1,1] = 16;
playerInventory[# 2,1] = "ingredient";
playerInventory[# 3,1] = itemVienDa;
playerInventory[# 4,1] = "Che tao";

playerInventory[# 0,2] = "Thuy Kiem";
playerInventory[# 1,2] = 1;
playerInventory[# 2,2] = "weapon";
playerInventory[# 3,2] = itemBlueSword;
playerInventory[# 4,2] = "Tan cong + 500";


playerInventory[# 0,3] = "Wood";
playerInventory[# 1,3] = 6;
playerInventory[# 2,3] = "ingredient";
playerInventory[# 3,3] = itemWood;
playerInventory[# 4,3] = "khoi go";


playerInventory[# 0,4] = "Than";
playerInventory[# 1,4] = 6;
playerInventory[# 2,4] = "ingredient";
playerInventory[# 3,4] = itemCoal;
playerInventory[# 4,4] = "Dung trong lo nuong";

playerInventory[# 0,5] = "Ca Song";
playerInventory[# 1,5] = 1;
playerInventory[# 2,5] = "ingredient";
playerInventory[# 3,5] = itemRawFish;
playerInventory[# 4,5] = "Khoi phuc the luc";

playerInventory[# 0,6] = "Vit Song";
playerInventory[# 1,6] = 1;
playerInventory[# 2,6] = "ingredient";
playerInventory[# 3,6] = itemVitSong;
playerInventory[# 4,6] = "Khoi phuc the luc";
