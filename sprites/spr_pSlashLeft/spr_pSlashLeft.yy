{
    "id": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pSlashLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00a21601-42c2-4fe9-a6e9-6a00b4d85c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "compositeImage": {
                "id": "9011b74e-55c5-4c28-a432-f9ca02670450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a21601-42c2-4fe9-a6e9-6a00b4d85c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb1c7d0-060c-43aa-92e0-adcb3e41a0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a21601-42c2-4fe9-a6e9-6a00b4d85c53",
                    "LayerId": "326e7e33-953d-44a6-883a-a744f08e2083"
                },
                {
                    "id": "cdba90fd-a16b-4d63-bf24-36d07667af0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a21601-42c2-4fe9-a6e9-6a00b4d85c53",
                    "LayerId": "8f31103b-0a99-4899-9edc-2cbeb4c4094d"
                }
            ]
        },
        {
            "id": "34234202-3590-4c21-bf6f-2dad00d071ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "compositeImage": {
                "id": "0da06a54-a372-4ac9-a612-8bad674dee54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34234202-3590-4c21-bf6f-2dad00d071ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1a8941-bc7b-4ca9-b158-cb905755484e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34234202-3590-4c21-bf6f-2dad00d071ea",
                    "LayerId": "326e7e33-953d-44a6-883a-a744f08e2083"
                },
                {
                    "id": "b2fce6f9-e128-45c1-81aa-46b7f6ed1bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34234202-3590-4c21-bf6f-2dad00d071ea",
                    "LayerId": "8f31103b-0a99-4899-9edc-2cbeb4c4094d"
                }
            ]
        },
        {
            "id": "5eca80ae-fb84-4624-8a3c-0768f2706cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "compositeImage": {
                "id": "478e71a3-e6e3-445f-8a05-ddd80af80d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eca80ae-fb84-4624-8a3c-0768f2706cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcefd26c-0572-453b-ac39-6206b07f7094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eca80ae-fb84-4624-8a3c-0768f2706cb3",
                    "LayerId": "326e7e33-953d-44a6-883a-a744f08e2083"
                },
                {
                    "id": "8fd440a7-e2ac-4382-8d6f-a8bdac35127b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eca80ae-fb84-4624-8a3c-0768f2706cb3",
                    "LayerId": "8f31103b-0a99-4899-9edc-2cbeb4c4094d"
                }
            ]
        },
        {
            "id": "43dafb0d-d74b-4215-b2ac-24b1949e9f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "compositeImage": {
                "id": "363e814c-1b69-4716-8f1a-7a260a02e66b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43dafb0d-d74b-4215-b2ac-24b1949e9f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "879813b6-83d0-49ff-b626-000ab0e70863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43dafb0d-d74b-4215-b2ac-24b1949e9f31",
                    "LayerId": "326e7e33-953d-44a6-883a-a744f08e2083"
                },
                {
                    "id": "ce6fc541-df73-4762-8fda-a983de5c09f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43dafb0d-d74b-4215-b2ac-24b1949e9f31",
                    "LayerId": "8f31103b-0a99-4899-9edc-2cbeb4c4094d"
                }
            ]
        }
    ],
    "gridX": 48,
    "gridY": 48,
    "height": 92,
    "layers": [
        {
            "id": "326e7e33-953d-44a6-883a-a744f08e2083",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8f31103b-0a99-4899-9edc-2cbeb4c4094d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4cd1eab-8e28-48ca-9cda-f599a1d7e922",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578432,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}