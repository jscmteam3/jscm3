{
    "id": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Daycycle",
    "eventList": [
        {
            "id": "e157ffcc-7c24-466b-ba75-a6a182da64af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        },
        {
            "id": "1215cf22-8987-472d-b6d3-56867458ba81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        },
        {
            "id": "42a40eec-7683-46da-90ea-53ec1f652276",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        },
        {
            "id": "ab7cbb1c-9d7f-4539-8992-a94fa4cd9e7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        },
        {
            "id": "9170fe0d-1cf6-4f87-8bae-6b1370cd5451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        },
        {
            "id": "3033f25c-952f-43c1-90f4-93ec44ef2d03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "5e9ce9bd-fd14-485a-bee8-626bf6194a2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}