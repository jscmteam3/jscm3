{
    "id": "56359dbc-54aa-4e91-9fd0-f136e76422c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemWoodPlat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cfdf578-4b27-449b-8f80-c3d94b12f895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56359dbc-54aa-4e91-9fd0-f136e76422c2",
            "compositeImage": {
                "id": "ebb5570b-4226-4229-9d96-aac2cf6d9111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cfdf578-4b27-449b-8f80-c3d94b12f895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6c86bc-61ba-4419-a47e-44e6e0ab31f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cfdf578-4b27-449b-8f80-c3d94b12f895",
                    "LayerId": "629c6979-ee05-4904-8a38-a1612a79a8cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "629c6979-ee05-4904-8a38-a1612a79a8cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56359dbc-54aa-4e91-9fd0-f136e76422c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}