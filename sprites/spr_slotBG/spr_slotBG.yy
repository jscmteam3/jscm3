{
    "id": "4c1d1b5e-464f-41e6-905f-60d5b177701c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slotBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc762a5c-26b4-4ad3-8208-8497d9be2c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c1d1b5e-464f-41e6-905f-60d5b177701c",
            "compositeImage": {
                "id": "3abdafc3-0d28-4eb0-8436-1fb1ee80af69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc762a5c-26b4-4ad3-8208-8497d9be2c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db038b3d-6fd2-4329-9b41-3752ca779749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc762a5c-26b4-4ad3-8208-8497d9be2c8a",
                    "LayerId": "14e6bbd5-b8fe-4036-a125-45fa5bd8ac1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "14e6bbd5-b8fe-4036-a125-45fa5bd8ac1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c1d1b5e-464f-41e6-905f-60d5b177701c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}