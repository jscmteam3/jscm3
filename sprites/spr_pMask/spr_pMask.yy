{
    "id": "4ea2b0af-327a-4914-bfb8-1adcdd1e49fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 15,
    "bbox_right": 47,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3682786b-54e7-40b0-b75a-b1688f995535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ea2b0af-327a-4914-bfb8-1adcdd1e49fd",
            "compositeImage": {
                "id": "3f8dfa1c-cae4-43a8-8d07-63e0f434867f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3682786b-54e7-40b0-b75a-b1688f995535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482b2856-a1e5-4d0c-8707-b04a5c75b6ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3682786b-54e7-40b0-b75a-b1688f995535",
                    "LayerId": "96d9421b-d044-417d-af01-f57dd098727f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "96d9421b-d044-417d-af01-f57dd098727f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ea2b0af-327a-4914-bfb8-1adcdd1e49fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}