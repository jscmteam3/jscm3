{
    "id": "a3490b95-6582-436a-9a25-fb5ff61b340a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Crops",
    "eventList": [
        {
            "id": "eb2fe8f3-4763-4051-9d19-2d3fdb68d637",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3490b95-6582-436a-9a25-fb5ff61b340a"
        },
        {
            "id": "19776679-76c3-4d19-9f6d-3011793492b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a3490b95-6582-436a-9a25-fb5ff61b340a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0dae0f0f-988d-45e2-b673-580339be4a56",
    "visible": true
}