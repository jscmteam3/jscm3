{
    "id": "402ebeb1-9b70-4ffc-9a4a-cf9b50671f48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SnakeHurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 8,
    "bbox_right": 30,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16b017ba-51fc-4ecb-aa74-e80e1b546dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402ebeb1-9b70-4ffc-9a4a-cf9b50671f48",
            "compositeImage": {
                "id": "2087b50b-c128-4924-9870-c5133f934fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b017ba-51fc-4ecb-aa74-e80e1b546dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f36761-0070-4776-b261-3257f60f0cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b017ba-51fc-4ecb-aa74-e80e1b546dc5",
                    "LayerId": "427ba120-4119-4254-bcc6-75f6d04d6516"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "427ba120-4119-4254-bcc6-75f6d04d6516",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "402ebeb1-9b70-4ffc-9a4a-cf9b50671f48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}