{
    "id": "25b8b9c5-c453-4b6d-995e-25b3c6d52f90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "persistentData",
    "eventList": [
        {
            "id": "04bfa279-5551-41f2-bf05-329d6e19eddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25b8b9c5-c453-4b6d-995e-25b3c6d52f90"
        },
        {
            "id": "e9219c75-a58e-4cc6-be20-2a6cb4e23e32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25b8b9c5-c453-4b6d-995e-25b3c6d52f90"
        },
        {
            "id": "f1c417bd-9ded-4588-981d-5dc9d6bb52f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "25b8b9c5-c453-4b6d-995e-25b3c6d52f90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}