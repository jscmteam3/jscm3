{
    "id": "8262f32c-b446-4142-833e-a04ee83564a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 43,
    "bbox_right": 86,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19a2daec-f290-4385-8db5-443b3b28e801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8262f32c-b446-4142-833e-a04ee83564a1",
            "compositeImage": {
                "id": "e0da97ac-837f-48f3-b611-06725d2f52b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a2daec-f290-4385-8db5-443b3b28e801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c6a9aa1-2f2e-43df-8d23-36508fa7d481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a2daec-f290-4385-8db5-443b3b28e801",
                    "LayerId": "dce7c508-fc54-4d2d-a910-fcfb96a1703b"
                },
                {
                    "id": "143afd1d-70fd-488c-ae4c-cb75945307c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a2daec-f290-4385-8db5-443b3b28e801",
                    "LayerId": "ad9b55b2-5b3d-492f-afbe-2d8bcf41733a"
                }
            ]
        },
        {
            "id": "07a4abfc-6619-41e9-8907-b93b0cb5b5c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8262f32c-b446-4142-833e-a04ee83564a1",
            "compositeImage": {
                "id": "6afccb68-82e2-4fdd-a3fc-d2cd98524587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a4abfc-6619-41e9-8907-b93b0cb5b5c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "321a8d6e-7702-4443-9fdb-ccc863660ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a4abfc-6619-41e9-8907-b93b0cb5b5c3",
                    "LayerId": "dce7c508-fc54-4d2d-a910-fcfb96a1703b"
                },
                {
                    "id": "6d842742-32d2-4f1e-b066-4e274bfab29d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a4abfc-6619-41e9-8907-b93b0cb5b5c3",
                    "LayerId": "ad9b55b2-5b3d-492f-afbe-2d8bcf41733a"
                }
            ]
        },
        {
            "id": "6b4ebb8b-aa50-4fa3-a257-8d94055b0aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8262f32c-b446-4142-833e-a04ee83564a1",
            "compositeImage": {
                "id": "56e01b34-94e4-496e-a389-1e7d1365585d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b4ebb8b-aa50-4fa3-a257-8d94055b0aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb279d9b-c15e-433d-9cbd-7233b3efcf31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4ebb8b-aa50-4fa3-a257-8d94055b0aba",
                    "LayerId": "dce7c508-fc54-4d2d-a910-fcfb96a1703b"
                },
                {
                    "id": "298b3e0f-844d-4101-935e-8bcaeef3976d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4ebb8b-aa50-4fa3-a257-8d94055b0aba",
                    "LayerId": "ad9b55b2-5b3d-492f-afbe-2d8bcf41733a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ad9b55b2-5b3d-492f-afbe-2d8bcf41733a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8262f32c-b446-4142-833e-a04ee83564a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "dce7c508-fc54-4d2d-a910-fcfb96a1703b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8262f32c-b446-4142-833e-a04ee83564a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}