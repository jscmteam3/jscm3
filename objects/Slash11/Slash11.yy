{
    "id": "702397cf-5a53-4c96-9b49-1bf4f813f68a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Slash11",
    "eventList": [
        {
            "id": "caf885c9-5495-4b33-ab11-7524a21be043",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "702397cf-5a53-4c96-9b49-1bf4f813f68a"
        },
        {
            "id": "26d5cd6d-81d0-487b-a501-3ed52bdbb0d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "702397cf-5a53-4c96-9b49-1bf4f813f68a"
        },
        {
            "id": "3725b18b-8d55-4169-81a0-7965b87c94a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "eda6048b-f7eb-4163-9e06-e86c94204160",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "702397cf-5a53-4c96-9b49-1bf4f813f68a"
        },
        {
            "id": "5db89b99-0531-4f86-892d-2833ea4e22f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "702397cf-5a53-4c96-9b49-1bf4f813f68a"
        }
    ],
    "maskSpriteId": "381f921b-173c-4685-982a-10124d1bb0da",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de664d52-0ecb-4a3d-9745-48ed25c61973",
    "visible": false
}