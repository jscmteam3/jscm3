{
    "id": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pigWalkDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06908f91-f6f3-4000-9cb4-580ad50fdd41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
            "compositeImage": {
                "id": "7ba8bf92-e477-44a3-b788-92e1248e6ab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06908f91-f6f3-4000-9cb4-580ad50fdd41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df16753e-5417-4d0a-b80f-fec36f4fac89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06908f91-f6f3-4000-9cb4-580ad50fdd41",
                    "LayerId": "8c79a071-9d0a-4dc9-9871-f0fc5d9e5a07"
                }
            ]
        },
        {
            "id": "6501fa3e-dff2-49b8-a721-32884f59df86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
            "compositeImage": {
                "id": "f5b9db69-a797-4e9d-a701-e958c9213e23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6501fa3e-dff2-49b8-a721-32884f59df86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "757063f6-93e7-41e3-938e-e8522a9ed70a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6501fa3e-dff2-49b8-a721-32884f59df86",
                    "LayerId": "8c79a071-9d0a-4dc9-9871-f0fc5d9e5a07"
                }
            ]
        },
        {
            "id": "974986ba-01cb-432f-b5af-2a7a1669b1bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
            "compositeImage": {
                "id": "f00ae9c5-6a4c-4ff8-9235-85cfefd9ac6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974986ba-01cb-432f-b5af-2a7a1669b1bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9694493-9a99-44e6-bff8-5cec1a6abd30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974986ba-01cb-432f-b5af-2a7a1669b1bd",
                    "LayerId": "8c79a071-9d0a-4dc9-9871-f0fc5d9e5a07"
                }
            ]
        },
        {
            "id": "8c02c11f-b632-484c-951f-bda9f54b6cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
            "compositeImage": {
                "id": "86c62bfa-9828-4617-b625-9f4ae4f96c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c02c11f-b632-484c-951f-bda9f54b6cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c684f52-051d-46f8-98df-6c9d8b6d76b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c02c11f-b632-484c-951f-bda9f54b6cd1",
                    "LayerId": "8c79a071-9d0a-4dc9-9871-f0fc5d9e5a07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8c79a071-9d0a-4dc9-9871-f0fc5d9e5a07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc64ee4-b021-4f71-984f-7e9ff3b20a68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}