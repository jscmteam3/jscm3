{
    "id": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FrameSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "312c9a80-6b73-4e92-a190-08b34ed98708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "2a782ba9-4b97-46a9-ab0a-d54a93cdcc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "312c9a80-6b73-4e92-a190-08b34ed98708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74a5448-c97b-4550-949d-9d9f1789d5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "312c9a80-6b73-4e92-a190-08b34ed98708",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "9e75d58a-cb3f-4d8e-ba1c-96efd1a45338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "61ba2b03-d634-4296-ba70-b401c57731a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e75d58a-cb3f-4d8e-ba1c-96efd1a45338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bebc2ef-5a48-4c7f-b34a-0e4318698ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e75d58a-cb3f-4d8e-ba1c-96efd1a45338",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "2df98a8e-a55e-463b-9ef2-4929f9e83e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "fb4a2484-054c-49d9-8d81-38281ff1cc47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2df98a8e-a55e-463b-9ef2-4929f9e83e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e562f09-831a-4a94-977f-b7b6a3ceba56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2df98a8e-a55e-463b-9ef2-4929f9e83e52",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "150149e5-c91b-44ab-bebb-230316c658d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "22bfbff6-c6a2-4bc2-bf09-b9452de708f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150149e5-c91b-44ab-bebb-230316c658d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427ea71f-b8ff-45f8-baf0-754e3449d8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150149e5-c91b-44ab-bebb-230316c658d1",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "3838421b-65e4-4b75-b4b9-17c81f6e5ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "64473fe9-110b-44d1-a97e-cfc31b44dd52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3838421b-65e4-4b75-b4b9-17c81f6e5ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265a353b-d5b0-45b7-a058-389b34501cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3838421b-65e4-4b75-b4b9-17c81f6e5ccb",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "d4b680c2-3830-4bc0-b6c2-82a00d9f37dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "1628c1bb-c790-4e0a-92f2-68263a4dac2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b680c2-3830-4bc0-b6c2-82a00d9f37dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6072ca00-2896-4283-9dea-06dd6325bba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b680c2-3830-4bc0-b6c2-82a00d9f37dd",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        },
        {
            "id": "0ea86651-1c1b-437d-8cbf-94039d3eff9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "compositeImage": {
                "id": "91132c07-6384-4d1a-9d48-84d4126beeb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea86651-1c1b-437d-8cbf-94039d3eff9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224d0b01-a5af-4b69-8d13-c10c8b1685e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea86651-1c1b-437d-8cbf-94039d3eff9d",
                    "LayerId": "d8d75a78-82a8-4426-85ab-40df1ff886f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d8d75a78-82a8-4426-85ab-40df1ff886f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "779aa6fe-8e8f-46e9-bff1-04019c829faf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        2583691263,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}