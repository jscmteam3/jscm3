{
    "id": "d7ef65b3-a63a-4215-b77d-ae2bf08b9330",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 24,
    "bbox_right": 41,
    "bbox_top": 57,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f791539-351e-4b60-9a96-636f29dd00b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7ef65b3-a63a-4215-b77d-ae2bf08b9330",
            "compositeImage": {
                "id": "06b4cf08-65c2-48ef-a8b5-d58a61371771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f791539-351e-4b60-9a96-636f29dd00b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc3dcbe-e152-4c8b-97cc-13e5ef89725e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f791539-351e-4b60-9a96-636f29dd00b0",
                    "LayerId": "24aa39c3-a78b-47e8-aca8-93e7fab4f466"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 64,
    "layers": [
        {
            "id": "24aa39c3-a78b-47e8-aca8-93e7fab4f466",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7ef65b3-a63a-4215-b77d-ae2bf08b9330",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 62
}