{
    "id": "aabea7d3-dc5f-4cf7-b668-8fdec27e07b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dormitory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 0,
    "bbox_right": 279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fab8c688-0f23-43bd-bfde-77a67572faf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aabea7d3-dc5f-4cf7-b668-8fdec27e07b4",
            "compositeImage": {
                "id": "cecc9eb3-d3b5-47d7-8fdf-e600eec39079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab8c688-0f23-43bd-bfde-77a67572faf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e51555-baa2-496b-8dc7-80726e896835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab8c688-0f23-43bd-bfde-77a67572faf1",
                    "LayerId": "bcf84a30-ef4b-41f7-8ae7-da512e8b00bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 228,
    "layers": [
        {
            "id": "bcf84a30-ef4b-41f7-8ae7-da512e8b00bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aabea7d3-dc5f-4cf7-b668-8fdec27e07b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 280,
    "xorig": 0,
    "yorig": 0
}