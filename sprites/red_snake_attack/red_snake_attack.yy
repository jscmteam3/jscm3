{
    "id": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "red_snake_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 166,
    "bbox_left": 96,
    "bbox_right": 197,
    "bbox_top": 99,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d66ad5d-789d-445c-8477-b59d6cd31d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "compositeImage": {
                "id": "22c501df-7273-454f-8950-a51ff9198b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d66ad5d-789d-445c-8477-b59d6cd31d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8d8871a-06a5-4eff-8ba4-e6b640def26c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d66ad5d-789d-445c-8477-b59d6cd31d2e",
                    "LayerId": "a53d3f59-4719-459b-a737-c98cbc973803"
                }
            ]
        },
        {
            "id": "ef3becfc-0a26-43ea-b366-5368c696bb61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "compositeImage": {
                "id": "d3daf15c-f3d3-4171-ab03-ccdd4373023c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3becfc-0a26-43ea-b366-5368c696bb61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc453e0-d109-40bf-bf32-91d9cdccb0fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3becfc-0a26-43ea-b366-5368c696bb61",
                    "LayerId": "a53d3f59-4719-459b-a737-c98cbc973803"
                }
            ]
        },
        {
            "id": "dab02f53-3ad9-4d18-9aaa-d7d4b28001a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "compositeImage": {
                "id": "f7a0ecf0-b949-4403-92ce-344ae5a659cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dab02f53-3ad9-4d18-9aaa-d7d4b28001a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1da6b99e-a278-4fe4-8820-3c45af7d69bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dab02f53-3ad9-4d18-9aaa-d7d4b28001a0",
                    "LayerId": "a53d3f59-4719-459b-a737-c98cbc973803"
                }
            ]
        },
        {
            "id": "f9de1983-05c3-47da-b1b7-0f554725cbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "compositeImage": {
                "id": "0f78d228-c0e5-45cf-bf28-d84d1ac66754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9de1983-05c3-47da-b1b7-0f554725cbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e7f9f2-dbe5-4f53-a32b-addfae6d3939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9de1983-05c3-47da-b1b7-0f554725cbdd",
                    "LayerId": "a53d3f59-4719-459b-a737-c98cbc973803"
                }
            ]
        },
        {
            "id": "4c992ee8-f6ba-4580-8a7f-8fdd167055ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "compositeImage": {
                "id": "88b91a75-0c69-46c0-9c21-06e869f74af7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c992ee8-f6ba-4580-8a7f-8fdd167055ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41746db2-3423-4ddd-95c3-b240d8ac5bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c992ee8-f6ba-4580-8a7f-8fdd167055ac",
                    "LayerId": "a53d3f59-4719-459b-a737-c98cbc973803"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 266,
    "layers": [
        {
            "id": "a53d3f59-4719-459b-a737-c98cbc973803",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0714746-2125-4e6e-8be8-1cb9eaf326d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 0,
    "yorig": 0
}