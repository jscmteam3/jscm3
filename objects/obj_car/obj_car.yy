{
    "id": "7ab4e5b3-6679-4326-844b-03fd627965d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_car",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "752409de-0d59-4711-97ba-11b777800763",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e38b9e1-bbaf-4ee6-ab01-0f37d9581c74",
    "visible": false
}