{
    "id": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DeadScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed5fbad6-b01d-487b-8a60-db10c0622034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "5cde8791-81dd-4735-a39f-3323900e297f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5fbad6-b01d-487b-8a60-db10c0622034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4a2675-aa2e-457d-85dc-d4b75e971072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5fbad6-b01d-487b-8a60-db10c0622034",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "67f8e417-3c9e-42fc-bdeb-6a7b3737e9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "8b9337a7-08fb-43fc-b193-411049abc436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f8e417-3c9e-42fc-bdeb-6a7b3737e9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50142c05-0c4d-423d-94ac-189f902f5e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f8e417-3c9e-42fc-bdeb-6a7b3737e9f8",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "4d2a14a2-e0fb-4773-aac1-6dedc5014be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "b29a4f92-2b23-4909-8002-06fd3c2a9672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2a14a2-e0fb-4773-aac1-6dedc5014be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4fb329-f815-4f80-bfc3-b45bd36e8ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2a14a2-e0fb-4773-aac1-6dedc5014be2",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "35b17a87-50bd-46d7-b092-192b484da94c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "5817627f-233e-4f8d-92cf-3be7e12d7821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b17a87-50bd-46d7-b092-192b484da94c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dfb84b4-7bbe-4f4f-a181-3c354e501100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b17a87-50bd-46d7-b092-192b484da94c",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "c78c3a69-f80e-4667-adf5-281230ea7bb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "555bb409-6bf3-4ee1-bef4-3adcf56ca73e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c78c3a69-f80e-4667-adf5-281230ea7bb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89915709-1a0c-4a7c-97af-4e6d3b22e3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c78c3a69-f80e-4667-adf5-281230ea7bb6",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "d5c68484-b56e-4e35-b556-f3759ea1d7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "b7d8cb23-bbd9-44ff-a44c-591d6d86e82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c68484-b56e-4e35-b556-f3759ea1d7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88aa2fbf-e6b8-4757-80ad-f8c552a227ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c68484-b56e-4e35-b556-f3759ea1d7cc",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        },
        {
            "id": "a48e574c-f15f-42c4-82c1-7b55e4d03856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "compositeImage": {
                "id": "ff6997ee-7133-4571-95d1-76b0895bfc9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48e574c-f15f-42c4-82c1-7b55e4d03856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "628b96a3-b7cd-42ac-b640-25474e358b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48e574c-f15f-42c4-82c1-7b55e4d03856",
                    "LayerId": "79cc4093-ae02-4c64-b467-241b10a694a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "79cc4093-ae02-4c64-b467-241b10a694a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75e6db26-186d-477f-8abd-085ce6f0dfa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}