/// @description Insert description here
// You can write your code in this editor
XAxis = _lengthX;
YAxis = _lengthY;
getDirection();

if (state == states.idle)
{
	counter +=1;
	//transition triggers
	if (counter >= room_speed * 3){ // make sure condition is true every 3 seconds
		var change = choose(0,1);	// choosing argument randomly
		switch(change){
			case 0: state = states.wander;
			case 1: counter = 0;
					break;// reset 
		}
	}
	if(collision_circle(x, y, 96, objPlayer, false, false))
			state = states.alert;
	sprite_index = ViewPig[Direction,Condition];
}
else if (state == states.wander)
{
	counter +=1;
	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	if (counter >= room_speed * 3){
		var change = choose(0,1);
		switch(change){
			case 0: state = states.idle;
			case 1:
				snake_dir = irandom_range(0,359);
				_lengthX = lengthdir_x(Speed,snake_dir);
				_lengthY = lengthdir_y(Speed,snake_dir);
				counter = 0;
				break;	
		}	
	}
	if (collision_circle(x, y, 96, objPlayer, false, false))
		state = states.alert;	
		sprite_index = ViewPig[Direction,Condition];
	
}
else if (state == states.alert)
{
	counter += 1;
	snake_dir = point_direction(x,y, objPlayer.x, objPlayer.y);
	
	_lengthX = -lengthdir_x(Speed,snake_dir);	
	_lengthY = -lengthdir_y(Speed,snake_dir);	
	checkCollision();
	x += _lengthX;
	y += _lengthY;
	//out of chasing
	
	if (!collision_circle(x, y, 96, objPlayer, false, false))
		state = states.idle;
	sprite_index = ViewPig[Direction,Condition];
}
if (monster_hp <= 0){	
	var instID = instance_create_layer(x, y, "Inventory",objPickup);
	with(instID){
		instID.myItemName = "Thit Lon Chet";
		instID.myItemAmount = 3
		instID.myItemType = "ingredient";
		instID.myItemScript = "Sau khi nau co the dung de hoi phuc nang luong";
		instID.myItemSprite = itemRawBeef;
	}
	ds_list_add(persistentData.ds_disapearID, id);
	instance_destroy();
}


