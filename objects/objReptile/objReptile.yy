{
    "id": "5a26105e-a51a-4e4e-a678-9ebc376e8673",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objReptile",
    "eventList": [
        {
            "id": "0ce2878e-74f0-460a-8e68-0a36c93c566d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        },
        {
            "id": "995db665-226a-4016-bded-3a33b4008a85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df8ef4cf-18e6-4b37-bd07-b117003369c3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        },
        {
            "id": "cd530ff7-3bfb-40b5-acaf-935a8a589aa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        },
        {
            "id": "0e1936c3-82f7-4507-8263-08f41fd18363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        },
        {
            "id": "c684a287-9a16-4154-98d2-e6b4d16cc08b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        },
        {
            "id": "78089d87-2d71-497f-8644-69452a2bf99b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a26105e-a51a-4e4e-a678-9ebc376e8673"
        }
    ],
    "maskSpriteId": "5a78e610-938a-4be5-b2c9-3e5a481a3c0e",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08af01f9-8f94-47b5-ba99-2e23d7e3e48a",
    "visible": false
}