/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_sprite_ext(spr_HealthBar, 0, x, y+64,0.75, 0.75, 0, c_white, 1);
draw_set_alpha(0.75);
draw_rectangle_color(
	x + 24, y + 8, x + 24 + 142* (persistentData.playerhp/persistentData.maxPlayerHp),
	y + 42, c_red,c_red,c_red,c_red, false
	);
draw_rectangle_color(
	x + 18, y + 75, x + 18 + 106.5* (persistentData.playerShield/persistentData.maxPlayerShield),
	y + 92,c_yellow,c_yellow,c_yellow,c_yellow, false
	);