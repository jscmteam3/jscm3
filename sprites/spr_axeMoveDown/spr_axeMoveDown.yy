{
    "id": "647be9c0-cdc8-426d-b01e-802260605899",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_axeMoveDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 16,
    "bbox_right": 54,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c2fc5d1-2900-4f6a-b179-4c16a150503d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "647be9c0-cdc8-426d-b01e-802260605899",
            "compositeImage": {
                "id": "c202bf5e-cbf9-4a9d-a4c2-dbca60fd8f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2fc5d1-2900-4f6a-b179-4c16a150503d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc133f8-f5de-4a9d-9393-b813e2d8331b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2fc5d1-2900-4f6a-b179-4c16a150503d",
                    "LayerId": "9ffae277-f288-4896-bb99-24a9032c93c0"
                },
                {
                    "id": "ae5523e9-0e5d-4695-952f-c4ee8462f0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2fc5d1-2900-4f6a-b179-4c16a150503d",
                    "LayerId": "919458cb-4704-4054-825d-5fe8680ec08e"
                }
            ]
        },
        {
            "id": "99dc5b45-a1bf-490e-a8dc-a030edef6762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "647be9c0-cdc8-426d-b01e-802260605899",
            "compositeImage": {
                "id": "bc8b8a86-b146-4531-b736-db6132d8790f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99dc5b45-a1bf-490e-a8dc-a030edef6762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed996ce-e53c-4fc1-834e-1daf9b5986ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99dc5b45-a1bf-490e-a8dc-a030edef6762",
                    "LayerId": "9ffae277-f288-4896-bb99-24a9032c93c0"
                },
                {
                    "id": "cf1c73b5-48f5-4810-958a-007104ad6e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99dc5b45-a1bf-490e-a8dc-a030edef6762",
                    "LayerId": "919458cb-4704-4054-825d-5fe8680ec08e"
                }
            ]
        },
        {
            "id": "3471b943-8689-48a7-87e1-2a98e9706320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "647be9c0-cdc8-426d-b01e-802260605899",
            "compositeImage": {
                "id": "60e30889-4708-49ab-bfde-e52c4aa526f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3471b943-8689-48a7-87e1-2a98e9706320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2927588d-07d6-44a6-bee4-32828d31466a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3471b943-8689-48a7-87e1-2a98e9706320",
                    "LayerId": "9ffae277-f288-4896-bb99-24a9032c93c0"
                },
                {
                    "id": "f1960aff-c656-459d-8fa8-d5fb9aaa67ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3471b943-8689-48a7-87e1-2a98e9706320",
                    "LayerId": "919458cb-4704-4054-825d-5fe8680ec08e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9ffae277-f288-4896-bb99-24a9032c93c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "647be9c0-cdc8-426d-b01e-802260605899",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "919458cb-4704-4054-825d-5fe8680ec08e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "647be9c0-cdc8-426d-b01e-802260605899",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}