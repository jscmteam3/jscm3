getInput();
getDirection();

//if (game.Condition == AMODE) itemUsing = itemSword;
//if (game.Condition == AXEMODE) itemUsing = itemAxe;
//if (game.Condition == FRAMEMODE) itemUsing = FrameSword;



if(game.Condition != ATTACK && game.Condition != AXEATTACK && game.Condition != FRAMEATTACK){
	if(XAxis == 0 && YAxis == 0){
		image_speed = 0;
		image_index = 0;
	}
	_lengthX = sign(XAxis)*Speed;
	_lengthY = sign(YAxis)*Speed;
	checkCollision();

	if(XAxis!=0 || YAxis !=0){
		image_speed = 1;
		sprite_index = View2[Direction,game.Condition];
		}
}
delayAttack+=1;
if (keyboard_check_pressed(vk_space) && delayAttack > room_speed/2){
	if (itemUsing = itemBlueSword){
		game.Condition = ATTACK;	
	}
	else if (itemUsing = itemAxe){
		game.Condition = AXEATTACK;	
	}	
	else if (itemUsing = FrameSword){
		game.Condition = FRAMEATTACK;	
	}
	delayAttack = 0;
}
var inst = instance_place(x, y, objTransition);
if (inst != noone){
	with(game){
		if(!doTransition){
	spawnRoom = inst.targetRoom;
	spawnX = inst.targetX;
	spawnY = inst.targetY;
	doTransition = true;
		}
	}
}
x += _lengthX;
y += _lengthY;
