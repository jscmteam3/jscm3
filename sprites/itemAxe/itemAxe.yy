{
    "id": "67187bd8-d51d-4a77-b540-f087ec51872b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemAxe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 9,
    "bbox_right": 45,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc7990fd-3975-4250-912e-1f4680501d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67187bd8-d51d-4a77-b540-f087ec51872b",
            "compositeImage": {
                "id": "13b22f70-d498-438b-b3f6-a6b2b609b82f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc7990fd-3975-4250-912e-1f4680501d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610e9c87-3c25-4a53-aa2c-e4ac81654506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc7990fd-3975-4250-912e-1f4680501d64",
                    "LayerId": "3d767b37-743a-420e-9107-1dcba8b6463e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3d767b37-743a-420e-9107-1dcba8b6463e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67187bd8-d51d-4a77-b540-f087ec51872b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 8,
    "yorig": 12
}