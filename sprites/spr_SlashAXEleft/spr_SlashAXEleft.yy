{
    "id": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SlashAXEleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ec1702b-4fd3-4ac5-9ce7-bd7bc85b075e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
            "compositeImage": {
                "id": "28417e75-982a-45c5-aecd-46cc230560c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec1702b-4fd3-4ac5-9ce7-bd7bc85b075e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9500feb9-90f3-4940-87b7-a9e477e4ac82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec1702b-4fd3-4ac5-9ce7-bd7bc85b075e",
                    "LayerId": "49112694-c23c-42db-8f2d-e077edfcdba0"
                }
            ]
        },
        {
            "id": "397314b0-6d90-43e2-a75e-4cc376b9ac1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
            "compositeImage": {
                "id": "5fef335e-ed2d-403e-a4b8-aef117b607f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397314b0-6d90-43e2-a75e-4cc376b9ac1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0c77cf-5f47-4df8-a7ca-5d7e28388a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397314b0-6d90-43e2-a75e-4cc376b9ac1f",
                    "LayerId": "49112694-c23c-42db-8f2d-e077edfcdba0"
                }
            ]
        },
        {
            "id": "b4de4c8a-f5dc-4387-9d41-adc2d089b64c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
            "compositeImage": {
                "id": "8c6f54ee-b83b-4e68-b5d8-31dd52951544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4de4c8a-f5dc-4387-9d41-adc2d089b64c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663aea34-a6e9-418e-80a7-93cbe3cf3bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4de4c8a-f5dc-4387-9d41-adc2d089b64c",
                    "LayerId": "49112694-c23c-42db-8f2d-e077edfcdba0"
                }
            ]
        },
        {
            "id": "cc0fbc60-f930-498b-9cae-a150f3aa7f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
            "compositeImage": {
                "id": "99212390-745a-416e-b62a-d4c7e60c187d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0fbc60-f930-498b-9cae-a150f3aa7f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f1dd77-bf2e-437f-8f9e-1d1db30846a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0fbc60-f930-498b-9cae-a150f3aa7f51",
                    "LayerId": "49112694-c23c-42db-8f2d-e077edfcdba0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "49112694-c23c-42db-8f2d-e077edfcdba0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11e1ea72-51dd-4b73-8233-63d38fed0f72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4286578533,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}