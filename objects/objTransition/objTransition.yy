{
    "id": "82ef3ffd-62f6-40ca-b2f2-8caa91b1112b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTransition",
    "eventList": [
        {
            "id": "9a91afce-58e4-4d50-8d67-ca23790f8f6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82ef3ffd-62f6-40ca-b2f2-8caa91b1112b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
    "visible": true
}