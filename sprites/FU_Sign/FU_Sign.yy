{
    "id": "ecf7a95e-241d-481e-a5c6-4c183fcef64b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FU_Sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39eb06b1-12b6-4435-b476-aeadaeee1037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf7a95e-241d-481e-a5c6-4c183fcef64b",
            "compositeImage": {
                "id": "1915435c-3ad4-412f-9d74-7cf679810157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39eb06b1-12b6-4435-b476-aeadaeee1037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869887d6-3294-417b-bf70-1b79930ccba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39eb06b1-12b6-4435-b476-aeadaeee1037",
                    "LayerId": "154edcff-c877-457b-b789-c4f15c6f477e"
                },
                {
                    "id": "ac51283f-1d29-4107-85fa-4b6a442562df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39eb06b1-12b6-4435-b476-aeadaeee1037",
                    "LayerId": "bba3cc4a-dfc3-4b89-a263-7174af9a2ff3"
                },
                {
                    "id": "0b80f4c1-90a4-4190-b963-524bcd394a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39eb06b1-12b6-4435-b476-aeadaeee1037",
                    "LayerId": "5b568667-d825-4abe-96d5-356e6ade3dcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b568667-d825-4abe-96d5-356e6ade3dcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf7a95e-241d-481e-a5c6-4c183fcef64b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bba3cc4a-dfc3-4b89-a263-7174af9a2ff3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf7a95e-241d-481e-a5c6-4c183fcef64b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "154edcff-c877-457b-b789-c4f15c6f477e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf7a95e-241d-481e-a5c6-4c183fcef64b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}