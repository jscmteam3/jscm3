{
    "id": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Transition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08f88ec2-fd5c-433a-9f2b-d4884f037b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "142c7437-5e0a-4d79-85b7-11e3e92d615c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f88ec2-fd5c-433a-9f2b-d4884f037b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8d65d81-467d-420a-8f5d-1468d4ab6f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f88ec2-fd5c-433a-9f2b-d4884f037b0d",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        },
        {
            "id": "c70a0db4-b0bc-4b8d-8397-67637b60b27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "3796b020-9260-46de-9567-e80a7f0eabcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70a0db4-b0bc-4b8d-8397-67637b60b27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdcceee5-a314-4553-8029-edf09cd53455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70a0db4-b0bc-4b8d-8397-67637b60b27d",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        },
        {
            "id": "02bf57ab-1a2b-463f-9518-19785009c390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "2111797a-4e24-419f-a2a4-6a0c86a5502a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02bf57ab-1a2b-463f-9518-19785009c390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d97b8be-45ac-448b-8101-12caf97d42ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02bf57ab-1a2b-463f-9518-19785009c390",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        },
        {
            "id": "2c8c043a-0c2c-4728-b680-a63313c30016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "b3aeabbc-fc66-49cc-b3b4-6afdd69e6bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c8c043a-0c2c-4728-b680-a63313c30016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f227281d-b29e-4e33-8e0b-f1c52d07f8b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c8c043a-0c2c-4728-b680-a63313c30016",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        },
        {
            "id": "4f7f9e59-9a8f-4169-b919-e2c70e26085c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "610958a0-3082-4594-bb00-df45a6b0960e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7f9e59-9a8f-4169-b919-e2c70e26085c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b576f36f-cd4e-40bb-992c-c54d702a0a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7f9e59-9a8f-4169-b919-e2c70e26085c",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        },
        {
            "id": "a6cf980c-ff35-41c1-82da-06d9961b9786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "compositeImage": {
                "id": "be1f8d9c-66f9-4dd2-8d4b-8fffa67ed2ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6cf980c-ff35-41c1-82da-06d9961b9786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2a5eff-dd24-43ab-8257-1c243d715f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6cf980c-ff35-41c1-82da-06d9961b9786",
                    "LayerId": "dfe64933-c44c-4f73-8927-3fa367e9905f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dfe64933-c44c-4f73-8927-3fa367e9905f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4ea7de9-955c-4a6a-9af0-38983d7a219b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}