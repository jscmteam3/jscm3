{
    "id": "d4836981-e91f-4758-bd16-1ada05a423ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 25,
    "bbox_right": 47,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65fa2ecd-c4fb-4606-b542-2864019f354c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "a03ba73f-ca38-4554-a409-2d08d9b7dac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65fa2ecd-c4fb-4606-b542-2864019f354c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c81f07a-4606-4d6a-aab3-847ca2aaf6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65fa2ecd-c4fb-4606-b542-2864019f354c",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "33166b39-5e83-4f1f-b15e-9537f005a43f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "c648f6b9-dd3a-43f5-9268-29c287381226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33166b39-5e83-4f1f-b15e-9537f005a43f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d7bc17-d53b-475b-a8cf-f8bae215b697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33166b39-5e83-4f1f-b15e-9537f005a43f",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "1de230ea-d0f3-4b6e-9429-95af4011f8ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "56850d78-7129-49aa-b348-c396f9aaf323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de230ea-d0f3-4b6e-9429-95af4011f8ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37137bb2-34ba-48bd-b698-bb7c2582724b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de230ea-d0f3-4b6e-9429-95af4011f8ab",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "86062b5b-346b-4ee8-93d5-6eb50e7aa7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "7ea5c43e-5824-4aca-8dde-633b49bc00a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86062b5b-346b-4ee8-93d5-6eb50e7aa7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3012113-4548-42c2-9553-5c09e1fa68b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86062b5b-346b-4ee8-93d5-6eb50e7aa7cc",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "16e89585-420d-4d02-8efe-4c427976c095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "d70a582f-2ec8-4d7d-a91d-b7163ff8c0c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e89585-420d-4d02-8efe-4c427976c095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24736c80-c08b-4290-b7a6-7476dadf672e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e89585-420d-4d02-8efe-4c427976c095",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "740a649f-19dd-4ad6-96fe-247881121b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "5261b185-9523-4905-b992-e06f40dcb07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740a649f-19dd-4ad6-96fe-247881121b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9392af17-a51c-4ced-b1b6-b022b14dce2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740a649f-19dd-4ad6-96fe-247881121b40",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        },
        {
            "id": "53d561e2-5b64-46de-bb17-b567b2c2257a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "compositeImage": {
                "id": "374951fa-aad0-46fa-84ee-c0d6d5a02d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d561e2-5b64-46de-bb17-b567b2c2257a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ac5fe9f-b296-414a-b81d-97a65ab21188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d561e2-5b64-46de-bb17-b567b2c2257a",
                    "LayerId": "ef9410ad-5815-43d9-a18c-5eec801f4a45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ef9410ad-5815-43d9-a18c-5eec801f4a45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4836981-e91f-4758-bd16-1ada05a423ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 36,
    "yorig": 49
}