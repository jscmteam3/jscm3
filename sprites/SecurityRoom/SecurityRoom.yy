{
    "id": "70b4d560-b0bf-4e61-a6d1-c3b441dc7fc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SecurityRoom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "674e6cb8-f947-4f93-b1d1-32d75acc2a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b4d560-b0bf-4e61-a6d1-c3b441dc7fc9",
            "compositeImage": {
                "id": "aa84b474-3fa9-4004-b323-0af983a47cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "674e6cb8-f947-4f93-b1d1-32d75acc2a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e22dce-f389-414d-9734-acfc6fdd49dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "674e6cb8-f947-4f93-b1d1-32d75acc2a3f",
                    "LayerId": "73529900-a121-45e7-829d-48bd58481941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "73529900-a121-45e7-829d-48bd58481941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70b4d560-b0bf-4e61-a6d1-c3b441dc7fc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 47,
    "yorig": 40
}