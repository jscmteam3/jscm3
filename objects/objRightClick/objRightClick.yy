{
    "id": "c9977309-08d4-4919-a16b-fd2f8f0b127e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRightClick",
    "eventList": [
        {
            "id": "7f5f5b53-800c-4de9-91b9-566436aca27b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c9977309-08d4-4919-a16b-fd2f8f0b127e"
        },
        {
            "id": "5c22d8f1-c7d7-413d-946c-6901d62eaa2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9977309-08d4-4919-a16b-fd2f8f0b127e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d85c459-e017-43c1-9d26-0ff8752e130a",
    "visible": true
}