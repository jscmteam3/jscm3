{
    "id": "73e321d3-8b00-4d0d-b3f4-1dd31622057a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 43,
    "bbox_right": 81,
    "bbox_top": 127,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75a01eb7-dfb3-46c3-bd21-2e0cecd525f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e321d3-8b00-4d0d-b3f4-1dd31622057a",
            "compositeImage": {
                "id": "eae176d0-07ee-425a-8c08-2759a0487d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a01eb7-dfb3-46c3-bd21-2e0cecd525f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0449e1fd-2c68-4c77-aae7-b1301526b7df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a01eb7-dfb3-46c3-bd21-2e0cecd525f5",
                    "LayerId": "6d6c7888-eb44-4a9e-8b0c-89301c7ea40f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 162,
    "layers": [
        {
            "id": "6d6c7888-eb44-4a9e-8b0c-89301c7ea40f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73e321d3-8b00-4d0d-b3f4-1dd31622057a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 61,
    "yorig": 143
}