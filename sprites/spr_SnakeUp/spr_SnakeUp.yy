{
    "id": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SnakeUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed2e7b62-924c-4d3c-984a-4e7bf0ba9b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
            "compositeImage": {
                "id": "1597bc79-76e4-4e81-8e30-a84809f7d23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2e7b62-924c-4d3c-984a-4e7bf0ba9b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc44b2ac-45c2-4179-aff8-6dfa9a80767a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2e7b62-924c-4d3c-984a-4e7bf0ba9b83",
                    "LayerId": "ac9e484d-1322-4603-8bfb-ec87272d1b01"
                }
            ]
        },
        {
            "id": "c9b0436d-2b46-4abd-84a4-55bbc516f600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
            "compositeImage": {
                "id": "0b102270-65b5-4376-8f4d-759445b8aa80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b0436d-2b46-4abd-84a4-55bbc516f600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fac8cb7-e565-47a1-a993-5c3e241320f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b0436d-2b46-4abd-84a4-55bbc516f600",
                    "LayerId": "ac9e484d-1322-4603-8bfb-ec87272d1b01"
                }
            ]
        },
        {
            "id": "19b48265-52ed-4c7e-ac1c-a2a2750ba8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
            "compositeImage": {
                "id": "53033379-2d5d-47a7-8daa-6f8975187952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b48265-52ed-4c7e-ac1c-a2a2750ba8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50e1e44d-064a-434c-9692-257a80b34c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b48265-52ed-4c7e-ac1c-a2a2750ba8a0",
                    "LayerId": "ac9e484d-1322-4603-8bfb-ec87272d1b01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac9e484d-1322-4603-8bfb-ec87272d1b01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6dd1ef2-8d0f-4440-a593-c776a7579b3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}