{
    "id": "852cf1c1-a96f-4be6-b074-8709801d0141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "red_snake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 166,
    "bbox_left": 98,
    "bbox_right": 173,
    "bbox_top": 108,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf3e9da2-0c41-4c9b-b591-0726d573be83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "compositeImage": {
                "id": "5497ce83-7f98-442b-a1a5-cecb142b755c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3e9da2-0c41-4c9b-b591-0726d573be83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b813fe0e-b38d-4f8f-9439-3f9d88b851c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3e9da2-0c41-4c9b-b591-0726d573be83",
                    "LayerId": "1032a25f-e5cf-4829-baa7-c38f3ad0d219"
                }
            ]
        },
        {
            "id": "523e8c58-e8de-43b0-be12-7be5a41cf831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "compositeImage": {
                "id": "9a780e5a-7578-4a78-9fa5-104eacabfbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523e8c58-e8de-43b0-be12-7be5a41cf831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269f7b07-a39a-4505-b6fb-4f8823f29820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523e8c58-e8de-43b0-be12-7be5a41cf831",
                    "LayerId": "1032a25f-e5cf-4829-baa7-c38f3ad0d219"
                }
            ]
        },
        {
            "id": "ab8eae5c-5f91-44c9-be50-1d3c69ba26bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "compositeImage": {
                "id": "1e212428-94fa-4507-b2c4-793a1b4bf2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab8eae5c-5f91-44c9-be50-1d3c69ba26bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68b217a-130d-410e-b134-1cb213f27c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab8eae5c-5f91-44c9-be50-1d3c69ba26bf",
                    "LayerId": "1032a25f-e5cf-4829-baa7-c38f3ad0d219"
                }
            ]
        },
        {
            "id": "86f294f1-88d3-4601-90fd-1c22818d6dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "compositeImage": {
                "id": "405344c1-20d6-49ec-89dc-7e3ac55aa156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f294f1-88d3-4601-90fd-1c22818d6dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9cce88c-5e0f-4328-872d-82c059f3440a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f294f1-88d3-4601-90fd-1c22818d6dd4",
                    "LayerId": "1032a25f-e5cf-4829-baa7-c38f3ad0d219"
                }
            ]
        },
        {
            "id": "39596352-f65e-43a9-bc35-38378b1d0209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "compositeImage": {
                "id": "21fd2601-78d8-477e-945e-ff1ad55f0853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39596352-f65e-43a9-bc35-38378b1d0209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb29c67-093b-4f47-89cb-1f24d773b5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39596352-f65e-43a9-bc35-38378b1d0209",
                    "LayerId": "1032a25f-e5cf-4829-baa7-c38f3ad0d219"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 266,
    "layers": [
        {
            "id": "1032a25f-e5cf-4829-baa7-c38f3ad0d219",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "852cf1c1-a96f-4be6-b074-8709801d0141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 127,
    "yorig": 133
}