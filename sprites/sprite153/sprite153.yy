{
    "id": "1617018c-3ac2-41f9-913e-f3d6dc930599",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite153",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d70d2493-6d20-410a-9986-248ba1e85074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1617018c-3ac2-41f9-913e-f3d6dc930599",
            "compositeImage": {
                "id": "2f6f9701-1445-4a2b-a832-53e7d4a494d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d70d2493-6d20-410a-9986-248ba1e85074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2307191-2f7d-4e91-9396-b5e951b2b7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d70d2493-6d20-410a-9986-248ba1e85074",
                    "LayerId": "9ca3c536-8adc-412d-9ba5-b550332778a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9ca3c536-8adc-412d-9ba5-b550332778a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1617018c-3ac2-41f9-913e-f3d6dc930599",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}