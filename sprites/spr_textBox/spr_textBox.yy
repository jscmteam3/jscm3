{
    "id": "ea1ef1ec-9770-46d3-98f7-fb23a547970a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d7729bc-c4e9-4033-9eac-7d0f74194c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1ef1ec-9770-46d3-98f7-fb23a547970a",
            "compositeImage": {
                "id": "4797fd3f-b14f-40ee-8e3e-a266c1cbbdc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7729bc-c4e9-4033-9eac-7d0f74194c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f7e36f-381a-4c03-8b8f-929a894af8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7729bc-c4e9-4033-9eac-7d0f74194c5a",
                    "LayerId": "bac5438e-2d92-496e-882a-8123833c86c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bac5438e-2d92-496e-882a-8123833c86c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1ef1ec-9770-46d3-98f7-fb23a547970a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 0
}