{
    "id": "06d56b18-3aa1-45f2-90bf-3858b3a7e11d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tree3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 63,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d7f27bb-96a7-4029-b582-633ff28eced2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06d56b18-3aa1-45f2-90bf-3858b3a7e11d",
            "compositeImage": {
                "id": "c2fd2a05-31eb-4d81-af2a-a9730981a326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d7f27bb-96a7-4029-b582-633ff28eced2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024f6c97-3f28-4d90-963e-6a30b5e4f0c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d7f27bb-96a7-4029-b582-633ff28eced2",
                    "LayerId": "50f9bf85-bb32-4b2f-a855-f32ae119269a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "50f9bf85-bb32-4b2f-a855-f32ae119269a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06d56b18-3aa1-45f2-90bf-3858b3a7e11d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 76
}