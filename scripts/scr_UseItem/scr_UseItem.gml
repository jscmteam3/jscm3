/// @description Using item
/// @arg itemIndex

if (playerInventory[# 2, argument0] == "consume"){
	if (playerInventory[# 0, argument0] == "HP"){
		playerInventory[# 1, argument0]--;	
		persistentData.playerhp += 20;
	}
	else if (playerInventory[# 3, argument0] == itemBeefNuong){
		playerInventory[# 1, argument0]--;	
		persistentData.playerhp += 15;
		persistentData.playerShield += 20;
	}
	else if (playerInventory[# 3, argument0] == itemVitNuong){
		playerInventory[# 1, argument0]--;	
		persistentData.playerhp += 10;
		persistentData.playerShield += 10;
	}
	
}
if (playerInventory[# 3, argument0] == itemBlueSword){
	game.Condition = AMODE;
	objPlayer.currentCondition = game.Condition;
	objPlayer.itemUsing = itemBlueSword;
}
if (playerInventory[# 3, argument0] == FrameSword){
	game.Condition = FRAMEMODE;
	objPlayer.currentCondition = game.Condition;
	objPlayer.itemUsing = FrameSword;
}
if (playerInventory[# 3, argument0] == itemAxe){
	game.Condition = AXEMODE;
	objPlayer.currentCondition = game.Condition;
	objPlayer.itemUsing = itemAxe;
}
if (playerInventory[# 3, argument0] == itemWood){
	var flag = 0;
	var h = ds_grid_height(playerInventory);
	for (var i = 0; i< h; i++)
		if (ds_grid_get(playerInventory, 3,i) == itemCraftingTable) 
			flag = 1;
	if (!flag){
		scr_addItems(playerInventory,"Crafting Table", 1, "tool", itemCraftingTable, "Crafting");
		playerInventory[# 1, argument0]--;	
	}
}

if (playerInventory[# 2, argument0] == "tool"){
	objInventory.show_inventory = false;
	game.debug = true;
	crops.isUsingCrafting = true;
	crops.item_hb_used = argument0;
	playerInventory[# 1, argument0] --;	
	if (playerInventory[# 3, argument0] == itemCraftingTable)
		crops.stuffID = obj_craftTable;
	else if (playerInventory[# 3, argument0] == itemCoalStove){
		crops.stuffID = obj_kilnGUI;
	}
}
if (playerInventory[# 3, argument0] == itemHintPaper){
	if(!instance_exists(obj_HintPaper)){
		instance_create_depth(objInventory.width_gui/2,objInventory.height_gui/2, -2, obj_HintPaper);
	}	
}