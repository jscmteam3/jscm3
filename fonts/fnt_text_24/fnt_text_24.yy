{
    "id": "0b40b18e-f1f8-470d-90fd-bd2aeefee7e6",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text_24",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "938bb681-c130-4363-91bf-0b58486b4f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 143
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "948d977d-b6ed-49c7-99a3-3d4c7bbb8e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 169,
                "y": 143
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "df3c7b28-8679-46e9-9da5-bc237f346fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 41,
                "y": 143
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "471cc920-ba56-45da-9718-7ccf68534afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "596d647c-4827-4bef-80e1-e0961a334afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 24,
                "y": 49
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ccb9d412-fb73-46a8-81f5-ff4064079027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e4a5b042-e46e-427e-b747-de8fefc2b631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 45,
                "y": 49
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "93fb2eda-8562-43c4-9ddc-92e18b55271e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 183,
                "y": 143
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ed7df05b-aca4-4da4-a10f-014e17bb4045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 65,
                "y": 143
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a2969b37-af0b-4ee1-bd33-d97edfba0dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 77,
                "y": 143
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b1642d9e-0515-4d9b-882b-9eb0f19fa392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 56,
                "y": 96
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a1d6d35f-d9ff-4477-a282-7a181c7b21ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 333,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f2d28ce4-117e-4dc6-9c0f-ec59bc44577a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 141,
                "y": 143
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ccac6cca-c474-4704-8a86-740cc41c0e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 143
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1b1863f5-ff17-4677-9e6b-ddf36e5cc0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 148,
                "y": 143
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "67f62a12-6031-4b0a-85d6-d852b95a9071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 350,
                "y": 96
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "59daceac-9080-4234-ab69-a5a1f4f25677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 108,
                "y": 49
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "acf3bac5-f88a-49df-b8dd-0162b863a7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 15,
                "y": 143
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "56b9a335-463e-4050-8445-bcb541e57118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 74,
                "y": 96
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9c884542-a708-43b6-bfbf-ed5a5ee36e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 146,
                "y": 96
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9f034011-a065-4875-afbc-88a9c8a97673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 66,
                "y": 49
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1a7a5ec0-5ce2-486c-a2e3-e0f467b71947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 211,
                "y": 49
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "733ac824-e864-403d-aec8-0346c042f12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 423,
                "y": 49
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dd76b6f9-29e5-4dda-b9c8-a25870eaebdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 171,
                "y": 49
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6a42b804-b629-4e46-a8d1-66a500c50223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 442,
                "y": 49
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c11b4438-a1d8-4642-a9a7-a50fde6239d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 191,
                "y": 49
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b997754e-0875-48cd-8f49-d114d1371905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 176,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9a123b96-4269-4bb7-92f8-c89b43da923f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 133,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "58cb7585-0095-45a1-9a79-89de9f4f6c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 89,
                "y": 143
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6f3de3e4-93e7-44d1-abb0-1956b0904f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 430,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7fc54acb-af49-4fd5-a9e7-4e8b297a5754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 460,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7fcf148a-c2c1-4bc0-b174-a3c9d822f592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 110,
                "y": 96
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f49e6053-8c0d-435a-9623-627dab127894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2567e994-99e9-4b1e-ba3c-8c7ad603f327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9121b8b2-ebc7-4a47-9ad9-08b218164d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 385,
                "y": 49
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b7b4e4d1-a511-4540-a274-bf338610d802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 251,
                "y": 49
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d97da8e6-37dc-4294-a4e3-4dae2d1e557e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a8923827-616f-47f5-a0ac-3cb4fdcff526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 404,
                "y": 49
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "99b88982-c045-4640-a4ef-75a792cebc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 347,
                "y": 49
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5246c8ea-3009-4352-a059-0e6df4363971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0560f1ef-8ce2-4e20-98cd-103ef0740bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2010bd27-4d9c-45f6-ad0c-8daecf80af5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 128,
                "y": 96
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c52d2153-8de1-4794-aeb0-fdb9181eaaaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "be21545d-abda-4394-80da-59750b4e968c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 309,
                "y": 49
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "de79211d-1075-45fb-be13-ebde5fc43fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 290,
                "y": 49
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f5e3fcb7-1af3-437b-b4bd-938f3b45d6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ab3dffbe-4775-4a11-a351-1d4bf838fa98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9b2c5241-c0f8-4c14-8615-4d4781e85cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3e670db2-864a-4d84-aed4-d202b4067f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 282,
                "y": 96
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8ad5930b-437c-4d73-869a-d1a100545a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "604279cc-2083-449b-b988-4bcf2a8e6284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 150,
                "y": 49
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3338caf9-843d-430e-8b9e-feb3b1440359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 87,
                "y": 49
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5a1d2100-930e-424e-b5af-cd3b4d079b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d5dd15e0-3aab-46ee-80cc-57cd385b3436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d7adf3b4-aabf-4d78-bce7-74b82ee5d888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 129,
                "y": 49
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1cc040ea-da2c-4d91-a6ec-dd3eceabd0f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2f5458e2-a661-4f58-8fd4-8132a98add80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c16c6e13-f6ed-4278-98a2-5b1e3215283a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "63bee7bb-eb95-47ab-b5b8-96a73e44e01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c5eef239-3936-4ead-9584-be008844a9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 101,
                "y": 143
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d8fa5caa-f7cd-4755-a2a7-5990a3b0e768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 383,
                "y": 96
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "18a4da21-2957-40b6-8142-74d66e0e630e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 112,
                "y": 143
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "dabde73d-62aa-45fe-978b-6d97a2a2bdb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 415,
                "y": 96
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3dcd0930-2006-4fe3-9f83-733d84d90775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2c85154c-64d1-4179-a2a6-9179a52e3645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 8,
                "x": 123,
                "y": 143
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "88b4a131-142d-471e-9ded-11b248635fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 96
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b421a043-6dbf-40d4-b980-f795f6ec6a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 96
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bd6757d0-89d4-4497-846a-9b3cb08abe3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 299,
                "y": 96
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e57c8d78-a309-499f-adc5-44b359a2c76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 271,
                "y": 49
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a5090cc7-a98d-4978-839e-53419d495c3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 96
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "33796172-396a-4e6f-8356-d63fb0c61556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 367,
                "y": 96
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "274e54da-3f69-4b59-9c67-84a6b04e7803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "21831eff-77de-4396-976b-b64997d121e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 316,
                "y": 96
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d7c51579-7a84-45a9-a9a8-ca5bb864a5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 162,
                "y": 143
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1cf79106-ee9a-45f0-b0aa-08ba0ffa0b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -1,
                "shift": 13,
                "w": 12,
                "x": 474,
                "y": 96
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4d697fa3-41be-4d5b-aa29-936ca2496df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 265,
                "y": 96
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a35b7d09-c005-4f30-8a03-7412e7b9b10c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 155,
                "y": 143
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7defdc8c-8bdb-4205-b6ac-cc3ce702e9e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "def9cdfa-f9e0-49b0-898c-b231f4be1072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 248,
                "y": 96
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "75d37b5e-bfc7-4461-b9ee-eec93981982e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 231,
                "y": 96
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "70e2575f-beec-43c4-9962-caa108e9cb21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 214,
                "y": 96
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f54bb633-79b2-4e21-a295-20d24db26f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 197,
                "y": 96
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "262c829b-9bf8-49f2-bd21-30ddd6c3f028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 96
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8da5763e-d947-482d-829d-a93115b9058d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 180,
                "y": 96
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "82e6d5dc-5804-43f6-b5ec-d6fe26254b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 399,
                "y": 96
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f9e8cfa1-08fd-48db-b2f8-ac5d2aae5e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 163,
                "y": 96
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae682078-943c-414b-a98b-724dc73f9205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 479,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4356f052-b327-4e54-ab61-33f6ed4a66e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "81f94aa8-5867-4629-b190-8d0fd0daae3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 231,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b62902cf-a3a2-479b-94af-47d6d883253c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 328,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "47e97541-5e3d-4e22-a154-2f3f97f1a869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 461,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "725f915b-3a7d-482d-96e4-e0d1bf8abd9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 488,
                "y": 96
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a72d8965-7d00-4ff1-8c75-7f1d6b93bc1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 189,
                "y": 143
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "732a67e0-2bdb-4237-b8b0-6be2b46cd02a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7406fbec-05dc-4eaa-8283-922bd4497d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 366,
                "y": 49
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}