{
    "id": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pSlashUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 13,
    "bbox_right": 86,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6527d5a-7e07-4f7a-b1d3-794bd22168ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "compositeImage": {
                "id": "1195be00-3f13-48da-901c-c9a77b2cbd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6527d5a-7e07-4f7a-b1d3-794bd22168ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35bb5127-562a-4653-a316-e0037a02a1f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6527d5a-7e07-4f7a-b1d3-794bd22168ad",
                    "LayerId": "f837d102-eddb-4ded-8317-8f4955b7821c"
                },
                {
                    "id": "00ca9959-703d-4545-8ae5-705a3c16b89c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6527d5a-7e07-4f7a-b1d3-794bd22168ad",
                    "LayerId": "adc9fa1a-0630-4d47-b253-7ecca592d05a"
                }
            ]
        },
        {
            "id": "e744a288-d4fc-4ff7-b4c4-e1ac3ce60209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "compositeImage": {
                "id": "bca2bb3b-2ab0-4d18-92c9-1fd6bdeddd79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e744a288-d4fc-4ff7-b4c4-e1ac3ce60209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb2aa1a-aa94-4c66-aba1-31a4234dd891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e744a288-d4fc-4ff7-b4c4-e1ac3ce60209",
                    "LayerId": "f837d102-eddb-4ded-8317-8f4955b7821c"
                },
                {
                    "id": "9fa14b20-8ba5-4a04-b931-6e7060077356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e744a288-d4fc-4ff7-b4c4-e1ac3ce60209",
                    "LayerId": "adc9fa1a-0630-4d47-b253-7ecca592d05a"
                }
            ]
        },
        {
            "id": "80cf2ee6-75f5-4aeb-b7a2-6c595d929501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "compositeImage": {
                "id": "5aca963b-7315-494d-be19-159d6f1d1d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80cf2ee6-75f5-4aeb-b7a2-6c595d929501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3da3020-33a8-4c8d-a61d-bb7d8ed6eeea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80cf2ee6-75f5-4aeb-b7a2-6c595d929501",
                    "LayerId": "f837d102-eddb-4ded-8317-8f4955b7821c"
                },
                {
                    "id": "67f955ce-9cb0-4cdf-aaa1-9a6162ebb4bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80cf2ee6-75f5-4aeb-b7a2-6c595d929501",
                    "LayerId": "adc9fa1a-0630-4d47-b253-7ecca592d05a"
                }
            ]
        },
        {
            "id": "9021436b-7cf6-444c-b3a0-9d124f474d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "compositeImage": {
                "id": "99023c35-d869-4a14-936f-4b5f753f5eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9021436b-7cf6-444c-b3a0-9d124f474d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95189f3e-7c75-4bd2-aca4-1707edb82acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9021436b-7cf6-444c-b3a0-9d124f474d9e",
                    "LayerId": "f837d102-eddb-4ded-8317-8f4955b7821c"
                },
                {
                    "id": "1fa40885-efe9-4a54-b35f-8e2d96a69293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9021436b-7cf6-444c-b3a0-9d124f474d9e",
                    "LayerId": "adc9fa1a-0630-4d47-b253-7ecca592d05a"
                }
            ]
        },
        {
            "id": "c7f7e938-fd63-4fe9-819a-6d15b94de396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "compositeImage": {
                "id": "3e353c28-458a-40ea-89ae-d30759344dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f7e938-fd63-4fe9-819a-6d15b94de396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88263446-4189-45a0-8693-4364433125c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f7e938-fd63-4fe9-819a-6d15b94de396",
                    "LayerId": "f837d102-eddb-4ded-8317-8f4955b7821c"
                },
                {
                    "id": "01c83ba6-bb78-46db-8652-3fd2d50dac71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f7e938-fd63-4fe9-819a-6d15b94de396",
                    "LayerId": "adc9fa1a-0630-4d47-b253-7ecca592d05a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "f837d102-eddb-4ded-8317-8f4955b7821c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "adc9fa1a-0630-4d47-b253-7ecca592d05a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3017fb8-2721-4e8a-a4bc-300ed042cca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 46
}