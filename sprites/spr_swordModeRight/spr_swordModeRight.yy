{
    "id": "04d39c00-01b0-40c1-ae46-c9c600908712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_swordModeRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 13,
    "bbox_right": 51,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e0a82bc-21b9-4301-89d3-9793f29206dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d39c00-01b0-40c1-ae46-c9c600908712",
            "compositeImage": {
                "id": "aa78085e-941e-4ba3-b486-f5c1e8f6645b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e0a82bc-21b9-4301-89d3-9793f29206dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1bb3115-1bfa-4b3b-8e6e-9e0e3a8cd8bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e0a82bc-21b9-4301-89d3-9793f29206dc",
                    "LayerId": "42ea28f5-1c00-4ea1-ba38-041945caadc8"
                },
                {
                    "id": "6637a23e-cd58-48fb-a32f-a959aa2c752f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e0a82bc-21b9-4301-89d3-9793f29206dc",
                    "LayerId": "df6728ed-2427-4a30-9537-1acabf4ac850"
                }
            ]
        },
        {
            "id": "ce728308-ec41-46da-854f-596ecdeb7355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d39c00-01b0-40c1-ae46-c9c600908712",
            "compositeImage": {
                "id": "ef06f021-8529-4d80-9944-5e3697f77df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce728308-ec41-46da-854f-596ecdeb7355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bcb432-9be3-4fd5-b0e7-83d26dbdaa2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce728308-ec41-46da-854f-596ecdeb7355",
                    "LayerId": "42ea28f5-1c00-4ea1-ba38-041945caadc8"
                },
                {
                    "id": "70602c6d-d598-431f-ac20-9b70f0490046",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce728308-ec41-46da-854f-596ecdeb7355",
                    "LayerId": "df6728ed-2427-4a30-9537-1acabf4ac850"
                }
            ]
        },
        {
            "id": "7d599b3e-fa88-4781-8259-809f4bf75ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04d39c00-01b0-40c1-ae46-c9c600908712",
            "compositeImage": {
                "id": "7b30da19-a67f-43c1-b333-fa4aa03acc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d599b3e-fa88-4781-8259-809f4bf75ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de9c9dc-3006-4ddf-b99f-d4fffc1ff234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d599b3e-fa88-4781-8259-809f4bf75ca0",
                    "LayerId": "42ea28f5-1c00-4ea1-ba38-041945caadc8"
                },
                {
                    "id": "b1ed1ced-66e4-482e-acd9-cfa57de83ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d599b3e-fa88-4781-8259-809f4bf75ca0",
                    "LayerId": "df6728ed-2427-4a30-9537-1acabf4ac850"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "42ea28f5-1c00-4ea1-ba38-041945caadc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04d39c00-01b0-40c1-ae46-c9c600908712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "df6728ed-2427-4a30-9537-1acabf4ac850",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04d39c00-01b0-40c1-ae46-c9c600908712",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}