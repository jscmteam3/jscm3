{
    "id": "a2354cd0-92ae-4ef8-a556-24aecc138c45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTree4",
    "eventList": [
        {
            "id": "e160c802-e932-4bf2-a78c-8e7935cd7122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2354cd0-92ae-4ef8-a556-24aecc138c45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9c0fced9-e7d3-4443-b9a8-cac67e7abd05",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e469dd9c-75e6-4a72-a9bd-10ae4cf72e9b",
    "visible": false
}