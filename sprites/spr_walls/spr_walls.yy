{
    "id": "a63ddc90-6f0b-4196-80bf-0e4fca175e07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1068bf5d-825a-4f3a-a911-bdb79a930f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63ddc90-6f0b-4196-80bf-0e4fca175e07",
            "compositeImage": {
                "id": "7644302b-e98c-45ae-9411-2074b2afb767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1068bf5d-825a-4f3a-a911-bdb79a930f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a889c7e-d5e7-4927-8e0e-596a94bce021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1068bf5d-825a-4f3a-a911-bdb79a930f10",
                    "LayerId": "5c821e3c-8774-43ae-a75c-0dc81472e5b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5c821e3c-8774-43ae-a75c-0dc81472e5b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a63ddc90-6f0b-4196-80bf-0e4fca175e07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}