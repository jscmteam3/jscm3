{
    "id": "c4994989-8c31-4ced-aa65-1171af473243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ItemInventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "850a28c7-54fc-4467-bb0a-50920f80d42e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4994989-8c31-4ced-aa65-1171af473243",
            "compositeImage": {
                "id": "ac160b18-40d8-40af-8737-8c263cf059a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850a28c7-54fc-4467-bb0a-50920f80d42e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18059c1a-6d7f-4b34-b429-1ad2fe833acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850a28c7-54fc-4467-bb0a-50920f80d42e",
                    "LayerId": "25db0f5f-0e6e-479e-95a0-1c951d8b218a"
                }
            ]
        },
        {
            "id": "5237abc9-4fa0-4533-8036-f2bb0c3eb085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4994989-8c31-4ced-aa65-1171af473243",
            "compositeImage": {
                "id": "257eec6c-0be3-40dd-966b-39117f78ceac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5237abc9-4fa0-4533-8036-f2bb0c3eb085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02106ac1-b138-4272-a388-5f0f7bd59823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5237abc9-4fa0-4533-8036-f2bb0c3eb085",
                    "LayerId": "25db0f5f-0e6e-479e-95a0-1c951d8b218a"
                }
            ]
        },
        {
            "id": "fd4afa32-cc5b-4ea8-aa82-ce12557fe649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4994989-8c31-4ced-aa65-1171af473243",
            "compositeImage": {
                "id": "860c60f4-834d-4ae8-a010-6e90ee975ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd4afa32-cc5b-4ea8-aa82-ce12557fe649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9debb501-26e7-48cb-9d31-16b00944dd43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd4afa32-cc5b-4ea8-aa82-ce12557fe649",
                    "LayerId": "25db0f5f-0e6e-479e-95a0-1c951d8b218a"
                }
            ]
        },
        {
            "id": "01b07965-9273-4cf3-aac9-c45f7b31d445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4994989-8c31-4ced-aa65-1171af473243",
            "compositeImage": {
                "id": "20876efb-c513-4647-820c-3ac4b193192c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01b07965-9273-4cf3-aac9-c45f7b31d445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0933ba43-7e46-4914-8059-b66b1cd7d929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01b07965-9273-4cf3-aac9-c45f7b31d445",
                    "LayerId": "25db0f5f-0e6e-479e-95a0-1c951d8b218a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "25db0f5f-0e6e-479e-95a0-1c951d8b218a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4994989-8c31-4ced-aa65-1171af473243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}