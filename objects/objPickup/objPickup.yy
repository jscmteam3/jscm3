{
    "id": "c3570f55-d16e-4db3-831f-9c0614666b11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPickup",
    "eventList": [
        {
            "id": "a1de83ef-ab5a-4a36-a4f9-9bc3600d2624",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3570f55-d16e-4db3-831f-9c0614666b11"
        },
        {
            "id": "1ded9bfd-540e-4a3e-8743-b40afd06e90f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5bea6ee8-10f5-4b2d-8c85-10ed8f4257d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3570f55-d16e-4db3-831f-9c0614666b11"
        },
        {
            "id": "f6588d36-fe96-4e59-bcac-d723fdd0b7ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c3570f55-d16e-4db3-831f-9c0614666b11"
        },
        {
            "id": "ce897a1f-d904-4395-b2c7-42b5661241fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c3570f55-d16e-4db3-831f-9c0614666b11"
        },
        {
            "id": "4f9533f0-c7a3-4eb4-a6e7-8bf2a4703727",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3570f55-d16e-4db3-831f-9c0614666b11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}