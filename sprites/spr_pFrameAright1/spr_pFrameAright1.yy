{
    "id": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pFrameAright1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 32,
    "bbox_right": 121,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ce8e057-0afb-4e9c-ad39-7d2bd95a18a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "4cafac42-f37d-4671-bf04-71105eb1975c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce8e057-0afb-4e9c-ad39-7d2bd95a18a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0621c36c-ddc2-46a1-a76b-ce8c8fdc5fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce8e057-0afb-4e9c-ad39-7d2bd95a18a4",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "da8e8870-fb87-4939-a640-a5a0c76dbe32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce8e057-0afb-4e9c-ad39-7d2bd95a18a4",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        },
        {
            "id": "358a898b-4052-4cf7-904b-f599209eb739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "16862c82-85f8-4115-a3e6-5f96aeb05081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "358a898b-4052-4cf7-904b-f599209eb739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5384cb0-57f5-4e25-93ff-d32990e28408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "358a898b-4052-4cf7-904b-f599209eb739",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "2501e204-f379-4cb0-ba86-cfef42d3827d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "358a898b-4052-4cf7-904b-f599209eb739",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        },
        {
            "id": "e2724d92-52a2-4144-a117-7ca7361fb206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "acae58de-1b1b-456e-be06-147861880683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2724d92-52a2-4144-a117-7ca7361fb206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f4374bb-4a93-4eb6-9178-86377165c513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2724d92-52a2-4144-a117-7ca7361fb206",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "93d79026-5269-44c5-b506-5fe691477c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2724d92-52a2-4144-a117-7ca7361fb206",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        },
        {
            "id": "ba4969af-4efd-4301-a59b-2fc1767e8761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "fc9ad36e-b301-49d3-9099-e35a87d4e5a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4969af-4efd-4301-a59b-2fc1767e8761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da29ada-6adb-4ce8-b272-e0df8b0b2d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4969af-4efd-4301-a59b-2fc1767e8761",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "9a9dca8f-2b5c-401e-8f7e-ec2b38b05ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4969af-4efd-4301-a59b-2fc1767e8761",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        },
        {
            "id": "9d690567-a6e8-46bd-9062-644844f414df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "906229b1-d89e-4821-b0f9-d42d10618f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d690567-a6e8-46bd-9062-644844f414df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2aa021a-64fc-4c84-8303-cd397a82942d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d690567-a6e8-46bd-9062-644844f414df",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "f75943df-2b2b-4312-b882-e1074e06e4a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d690567-a6e8-46bd-9062-644844f414df",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        },
        {
            "id": "3e2baf7d-7504-4092-9b84-a57c0ee6d0f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "compositeImage": {
                "id": "fa4cf600-2db9-41eb-bac0-492582a7b45c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2baf7d-7504-4092-9b84-a57c0ee6d0f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1660e922-f25d-48cc-920e-c12943431dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2baf7d-7504-4092-9b84-a57c0ee6d0f4",
                    "LayerId": "91b40ae0-d22f-4404-a24b-26b05b379d96"
                },
                {
                    "id": "edd9017a-cf4a-4926-a84a-cecb83413d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2baf7d-7504-4092-9b84-a57c0ee6d0f4",
                    "LayerId": "bbf0f56f-2848-4254-891b-a66a7f130b82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "91b40ae0-d22f-4404-a24b-26b05b379d96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bbf0f56f-2848-4254-891b-a66a7f130b82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f5c3f38-d199-4eb8-8f6a-bb6113e618a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}