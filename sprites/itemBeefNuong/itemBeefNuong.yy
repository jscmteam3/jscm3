{
    "id": "435810bf-c325-44c2-87f1-9e1111c1549e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemBeefNuong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0108934-2480-4662-8eae-a2e6649cff48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "435810bf-c325-44c2-87f1-9e1111c1549e",
            "compositeImage": {
                "id": "1f910a4b-ea1e-4759-ae65-3e1df1f4e914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0108934-2480-4662-8eae-a2e6649cff48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b760a34-125d-45b4-869f-589537402f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0108934-2480-4662-8eae-a2e6649cff48",
                    "LayerId": "abe34c2f-7297-4d8b-a592-74a510520b67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "abe34c2f-7297-4d8b-a592-74a510520b67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "435810bf-c325-44c2-87f1-9e1111c1549e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}