{
    "id": "cf695127-dbba-4e8d-8602-a8f37362dff3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_giantCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 27,
    "bbox_right": 90,
    "bbox_top": 59,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c18292b7-3d1b-4067-a45d-7805adac5548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf695127-dbba-4e8d-8602-a8f37362dff3",
            "compositeImage": {
                "id": "89f985b6-da1d-4274-892c-6e49747d722e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c18292b7-3d1b-4067-a45d-7805adac5548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c59f15-21e8-4bb4-ad6b-8ffdbadc928b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c18292b7-3d1b-4067-a45d-7805adac5548",
                    "LayerId": "091d1496-da5b-4f69-9163-ca094c194fcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "091d1496-da5b-4f69-9163-ca094c194fcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf695127-dbba-4e8d-8602-a8f37362dff3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 53
}