{
    "id": "30dd4f14-6477-4229-8e51-697ec1e86bdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite108",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5311,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dbed006-b42d-4b37-9bfe-530337c1dae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30dd4f14-6477-4229-8e51-697ec1e86bdc",
            "compositeImage": {
                "id": "5712dbfc-eeb5-4b59-a306-98b55d4d94d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbed006-b42d-4b37-9bfe-530337c1dae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e24bd72-aad8-4142-b643-e65a91b57f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbed006-b42d-4b37-9bfe-530337c1dae6",
                    "LayerId": "dc63875a-e7d3-455d-b8e5-c71ffb65b4cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6000,
    "layers": [
        {
            "id": "dc63875a-e7d3-455d-b8e5-c71ffb65b4cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30dd4f14-6477-4229-8e51-697ec1e86bdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}