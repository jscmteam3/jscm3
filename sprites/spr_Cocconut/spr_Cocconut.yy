{
    "id": "33f59d20-b948-433d-86b3-4a09e615db23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Cocconut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 63,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "202fc75a-3d37-43b9-b59f-dc4bd35fd366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f59d20-b948-433d-86b3-4a09e615db23",
            "compositeImage": {
                "id": "da286eb3-b894-4c31-a311-da33c3e28a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "202fc75a-3d37-43b9-b59f-dc4bd35fd366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662eb67d-3afd-4c0c-a358-6ad6eca52e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "202fc75a-3d37-43b9-b59f-dc4bd35fd366",
                    "LayerId": "0c3d9c4f-2f75-4e06-9d7b-5f8c4defde7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "0c3d9c4f-2f75-4e06-9d7b-5f8c4defde7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33f59d20-b948-433d-86b3-4a09e615db23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 75
}