{
    "id": "6a468732-8ff5-4894-b35e-964531b4a22f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite126",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 17,
    "bbox_right": 65,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a2bb75a-a24c-4bc9-a60a-e3246fb55549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "c2ed3628-3aec-4399-b8ca-f0134a13212b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2bb75a-a24c-4bc9-a60a-e3246fb55549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e15b6a-54f6-4460-bd21-cf40f3cec51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2bb75a-a24c-4bc9-a60a-e3246fb55549",
                    "LayerId": "a3bc2a9b-3c79-4205-83c4-bd8d5f35d95b"
                }
            ]
        },
        {
            "id": "49eac652-6b02-4261-bdcb-dfc44e958452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "1a7cfa87-9d52-4ac6-b3fe-631dd3093072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49eac652-6b02-4261-bdcb-dfc44e958452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2663189f-c9be-47d2-b546-e2043d166084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49eac652-6b02-4261-bdcb-dfc44e958452",
                    "LayerId": "a3bc2a9b-3c79-4205-83c4-bd8d5f35d95b"
                }
            ]
        },
        {
            "id": "172400dd-707c-4656-9dea-a1674bfaa77e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "compositeImage": {
                "id": "9f8d9783-bd1f-4039-a148-323fd8727f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "172400dd-707c-4656-9dea-a1674bfaa77e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b99bcd-5922-42a1-98a3-224a63149f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "172400dd-707c-4656-9dea-a1674bfaa77e",
                    "LayerId": "a3bc2a9b-3c79-4205-83c4-bd8d5f35d95b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "a3bc2a9b-3c79-4205-83c4-bd8d5f35d95b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a468732-8ff5-4894-b35e-964531b4a22f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 41
}