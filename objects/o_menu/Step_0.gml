menu_back = keyboard_check_pressed(vk_escape);

menu_move1 = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
menu_move2 = keyboard_check(vk_left) - keyboard_check(vk_right);

menu_index += menu_move1;
if(menulevel = 0)
{
	if (menu_index < 0) menu_index = buttons - 1;
	if (menu_index > 3) menu_index = 0;
}
if(menulevel = 1)
{
	if (menu_index < 4) menu_index = buttons - 1;
	if (menu_index > 6) menu_index = 0;
}
if(menulevel = 2)
{
	if(menu_move2 < 0) 
	{
		if(soundlevel<100) {delay1 = delay1 + 0.2; if(delay1=1) {soundlevel = soundlevel + 1; delay1 = 0;}}
	}
	if(menu_move2 > 0) 
		if(soundlevel>0)   {delay2 = delay2 + 0.2; if(delay2=1) {soundlevel = soundlevel - 1; delay2 = 0;}}
}

if(menulevel>0){ if(menu_back = 1) menulevel = menulevel - 1;}

if (menu_index != last_selected) audio_play_sound(chose, 1, false);

last_selected = menu_index;

