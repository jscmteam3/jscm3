{
    "id": "b75da7ba-f5d4-4448-84c6-64fde0c3fd07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SecurityGate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5999b387-2f30-486c-963a-e90dfbc266c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b75da7ba-f5d4-4448-84c6-64fde0c3fd07",
            "compositeImage": {
                "id": "789ee532-4bca-4dfe-bc81-b8d1e6219e32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5999b387-2f30-486c-963a-e90dfbc266c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b3f48e8-f307-460b-a77b-948d08535add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5999b387-2f30-486c-963a-e90dfbc266c6",
                    "LayerId": "d0c642fd-dff1-472d-a677-c466024489e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "d0c642fd-dff1-472d-a677-c466024489e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b75da7ba-f5d4-4448-84c6-64fde0c3fd07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}